# KnitDown
KnitDown is a framework for constructing flat sheets of knitted textures sourced from KnitSpeak Files. 
##Set Up
###Requirements
Get the latest version of Intellij Community Edition (not ultimate!): https://www.jetbrains.com/idea/download/#section=windows

Have the community edition source files on your computer. You will need these to see detailed plugin related errors: 
```
git clone git://git.jetbrains.org/idea/community.git idea 
```

Install the required plugins:
https://plugins.jetbrains.com/plugin/6606-grammar-kit
https://plugins.jetbrains.com/plugin/227-psiviewer

###Setting Up Intellij
Follow the instructions for setting up intellij here: https://www.jetbrains.com/help/idea/install-and-set-up-product.html

Look at the following sections
+ Installation requirements
+ Download and install Intellij IDEA
+ Run Intellij IDEA for the first time
    + select the user interface theme
    + Disable the unnecessary plugins
        + You must keep the Build Tools (to access Gradle) and I reccomend keeping your favorite version control plug in enabled (Git will help with this project)
        + Everything else you will not need
    + Download and install additional plugins
        + you do not need these plugins. You will install the needed plugins later
        
Stop when the instructions ask you to create a new project and move onto the next section of this ReadMe
###Starting a Plugin Project with Gradle
You will now be following the instructions for creating a new Gradle Intellij plugin: https://www.jetbrains.org/intellij/sdk/docs/tutorials/build_system/prerequisites.html 
1. You should have already installed Intellij but if not, do so now.
2. For step 1.1, while checking that the Gradle and DevKit plugins are installed you can take the time to install Grammar-Kit, PSIViewer, and JFlex
3. Follow step 1.2 exactly, create a new Gradle plugin, you will create it in the same directory as you have stored this repository. 
    1. When asked for the GroupID and ArtifactID:
        + leave GroupID: com.jetbrains
        + ArftifactID should be "KnitDown"
4. When asked to specify a JVM Gradle point to your java JDK 1.8. If you don't have a jdk set up, or you don't have JDK 1.8 or higher please follow the instructions here: https://www.jetbrains.com/help/idea/configuring-mobile-java-sdk.html 
5. After setting up the gradle settings the last system will ask for the location of the project. Select the folder where you are holding the repository. If any files (such as the gradle get overwritten) discard the changes using Git.
5. Ignore step 1.3 and beyond. You do not need to modify the Gradle build file that comes with the repository. You should now have a working version of plugin, but it will not compile.
6. Once the files open and everthing is the correct position go to view>toolwindows->gradle
    1. Your build file may have been modified by intellij. Discard those changes using git and it will revert to the correct build. 
    2. In the gradle project window hit the blue cycling refresh button in the top left corner.
    
###Generating the KnitSpeak Parser
For repo simplicity and to ensure that the parser code is regularly updated with intellij and Grammar-Kit you will need to generate the KnitSpeak parser. On a fresh install of the plugin you will see many errors in the KnitSpeakParser package, ignore them. 
1. Ensure that you have Grammar-Kit, Jflex, and PSIViewer installed
2. We will follow modified instructions from this tutorial: https://www.jetbrains.org/intellij/sdk/docs/tutorials/custom_language_support/language_and_filetype.html
3. First generate the Lexer.
    1. Find src/main/Java/KnitspeakParser/_KnitSPeakLexer.flex and right click on the file. 
    2. Navigate to "Run JFlex Generator". If no gen folder is in the src/main folder it should generate one with a new class "_KnitSpeakLexar".
4. Now generate the parser classes.
    1. Right click on src/main/Java/KnitspeakParser/KnitSpeak.bnf
    2. Navigate to "Generate Parser Code". Parsing code should be added to the gen folder. 
5. The project should compile now. If not, it worth checking that the gen folder is marked as a src directory (it will be blue in the explorer)

### Running the Plugin
Don't!
This project is a plugin to gain the benefits of the grammar kit framework but no work at this point has been done on integrating this plugin into the interface of Intellij (should be doable!). 
Instead everything is run through JUnit tests that create a lightweight version of the plugin so that everything can be managed through scripts. 
##Code Overview
Code in KnitDown can be broken into three key sections:
1. The Compiler: code that interacts with the Grammar-Kit Toolkit for language parsing
2. The Texture Graph: A representation of textured knitted sheets that is the primary data structure of this framework
3. Panels: The data structure that manipulate the relationships between different textures before constructing a TextureGraph. 

There are additional features that have not been thoroughly tested and are not publication ready or strong research contributions:
1. Texture Tracking: This collects tabular features about elements of the texture graph which can be useful for machine learning tasks
2.Customized Exceptions: This makes the error handling process in KnitDown simpler and spans all of the major components
###Package Overview
+ __CompilerExceptions__
  + All KnitDown specific Exceptions should extend the KnitDownException Class, this makes it easier to distinguish between errors thrown from outside code. 
  + KnitDown exceptions are broken into three subpackages:
    + Parsing Exceptions are exceptions caused by errors in KnitSpeak parsing. These are primarily caused by errors in KnitSpeak's logic, but with a valid grammar/syntax. These make up the majority of errors because they handle the most user input
    + Panel Exceptions handle errors in a user's specification of a panel. 
      + PreviouslyCoveredCellException is currently the only exception based on panel input
    + StitchManipulationExceptions are exceptions caused by invalid manipulation of a texture graph, such as removing more stitches then are available
      + NoTextureCarvesAvailableExceptions is currently the only exceptions based on Stitch Manipulation
+ __KnitSpeakParser:__ This package contains code that is primarily generated by Grammar-Kit. Details of which classes are implemented vs generated are in the next section. Aside from KnitSpeak.bnf and parser.KnitSpeakParser  little to know coding should be done here, isolating the grammar-kit plugin from the KnitDown framework.
+ __Panels:__ Panels handle combining textures over rectangular grids. 
  + The Patches Subpackage manages the majority of code which is done at the patch level, with Panels only handling global operations
+ __TestCases:__ This package holds the JUnit tests that are used to run specific cases of the framework.
+ __TextureDefinition:__ This is essentially the Abstract Syntax Tree structure that is constructed from the parsed knitspeak and outputs specific instances of a texture graph. This is the most stable package in the implementation and should only be edited sparingly or if changes to the grammar are introduced
+ __TextureGraph:__ This is the meat of KnitDown and describes the graph structure that specifies a sheet of knitted, textured fabric. 
+ __Tacking:__ Tracking is an extraneous package that breaks features of stitches up into tabular forms that are amenable to Weka machine learning. Much of thew work here has not been evaluated and could be redone or thrown away entirely
###Significant Classes
+ __Panel:__ The panel class implements a patch graph structure for rectangular sheets of fabric that use multiple textures.
+ __Patch:__ The patch class is the node in a panel that converts texture specifications into knitgraphs
+ __TextureDefinition:__ this holds the abstract syntax tree that is used to construct a knitgraph from knitspeak specification
+ __LineDefinition:__ this holds the sub syntax tree for individal courses of knitting instructions
+ __StitchDefinition:__ a stitch definition translates stitch tokens to loop to loop constructions in the graph
+ __LineDeclaration:__ this holds the sub syntax tree that dictates the construction order of courses
+ __TextureGraph:__ the texture graph class is the graph structure for describing knitted fabric
+ __Line:__ the course structure describes a course of knitted texture in a texture graph
+ __Stitch:__ the Stitch class manages many to one relationships of loops based on canonical constructions such as knit/purl, and decreases
+ __StitchSet:__ The stitch set strutcture maintains the relationship between multiple stitches in a many-many relationship. This includes cables and increases
+ __Loop:__ the loop class is the basic node in a knitgraph structure
+ __Yarn:__ yarn is the ordered set structure that maintains loops based on construction order
+ __YarnSection:__ yarn sections disambiguate the relationship between loops and turns in the courses of  a fabric. This is really only significant if short rows are implemented.
+ __TestSetUp:__ the test set up class handles the nuances of running code in the intellij plugin. Extending this class makes it easy to test functions without messing with Intellij's back end.  
###Significant Methods
+ __Panel.easePanel:__ Set the panels loop and course counts based on read in values from a optimized panel format
+ __Panel.adjustPanel:__ Heuristic based approach to seting the loop and course counts
+ __Panel.buildPanel:__ Converts the patch specifications to one large knitgraph
+ __Patch.adjustStitches:__ Determine the stitch/loop counts based on neighboring stitches
+ __Patch.adjustLines:__ Determine the course counts based on neighboring patches
+ __TextureGraph.narrowTextureGraph:__ The funciton call for texture carving
##Using KnitDown
###Knitspeak
Knitspeak is a language based on the texture patterns in "The Essential Stitch Collection" and on the StitchMaps Website. For a complete review of the grammer see KnitSpeakParser.KnitSpeak.bnf. 
###Making a Texture Graph From KnitSpeak
To make a texture graph from knitspeak the code follows the general format:
```java
public TextureBuiltInRow buildTexture(PsiFile psiFile, int w){
    KnitCompiler knitSpeakCompiler = KnitCompiler(psiFile);
    TextureBuiltInRow textureGraph = knitSpeakCompiler.buildGarmentByWidthReps(w);
}
```
The psiFile is the intellij file structure that holds the knitspeak parsed syntax tree. w is the number of tiles of the texture width-wise. 
###Making a Panel
Panels are constructed by laying out the texture and locations of patches. The example code below is taken from TestCases.Demos.StretchInDownTest:
```java
Texture a = new Texture(knitCompilers.get("006Seed.knitspeak").getTextureDefinition(),12.8, 17.9);
        Texture b = new Texture(knitCompilers.get("004Stockinette.knitspeak").getTextureDefinition(),15.4, 18.1);
        Texture c = new Texture(knitCompilers.get("037SubtleTwist.knitspeak").getTextureDefinition(),16.4, 19.0);
        Texture d = new Texture(knitCompilers.get("084NarrowRopeB.knitspeak").getTextureDefinition(),18.1, 21.2);
        Texture e = new Texture(knitCompilers.get("009k1p1rib.knitspeak").getTextureDefinition(),21.46, 14.8);
        Texture f = new Texture(knitCompilers.get("012Ribk2p2.knitspeak").getTextureDefinition(),24.98, 16.0);
        Panel panel = new Panel(2,6);
        panel.addPatch(f,0,0,1.99,0.99,1,false);
        panel.addPatch(e,0,1,1.99,0.99,1,false);
        panel.addPatch(d,0,2,1.99,0.99,1,false);
        panel.addPatch(c,0,3,1.99,0.99,1,false);
        panel.addPatch(b,0,4,1.99,0.99,1,false);
        panel.addPatch(a,0,5,1.99,0.99,1,false);
        panel.adjustPanel();
```
###Converting a Panel to a Texture Graph
Given the panel from the last section the code to create a texture graph of the whole panel is
```java
TextureBuiltInRow knitGraph = panel.buildPanel();
```
###Shaping a Texture Graph
Right now shaping is down by narrowing a whole texture graph:
```java
knitGraph.narrowTextureGraph(2, 0);
```
Or taping the fabric so that it narrows over time:
```java
knitGraph.taperNarrow(18,50,3,18);
```
### Making new tests
Building new panels and texture graphs requires access to PsiFiles in the intellij plugin structure. These are easiest to access from a test case that extends TestSetUp. Here is a sample:
```java
public class MyTest extends TestSetUp {
    @Before
    @Override
    /**
    * The setup method is needed to declare which directories
     * in the testData folder are being used to source files from
     * They are flagged in the following order:
     * boolean specialTests,
      * boolean allTest: the folder containing stitch essential knitspeak
      * boolean failedTest, the folder containing any files that have been failing. Useful place to store knitspeak that is causing errors
       * boolean stitchMapsTest, the location of stitch maps files
        * boolean testPanels:  location of panel structure, not knitspeak
    */
    public void setUp() throws Exception {
        super.setUp();
        this.setUpDirectories(true, false, false, false, true);
    }
    /**
    * Any functional that starts with the word "test" will be a junit test and can be run as the static function starting your code
    */
    public void testTriangle() throws Exception {
        //your code here:
    }
}
```

#FAQ
1. Its not working and I have no idea why? 
    + when in doubt, try file->invalidate and restart. Otherwise ¯\\_(ツ)_/¯
2. Jflex is asking me for a folder location.
    + select the top level folder of the repo. The jflex jars should be there. 
#Useful Resources
##Grammer kit 
Tutorial for creating a custom language. This is the basis for the generated code in the KnitSPeakParser Package: https://www.jetbrains.org/intellij/sdk/docs/tutorials/custom_language_support_tutorial.html

Source of Grammer Kit code base: https://github.com/JetBrains/Grammar-Kit

PsiViewer is a helpful debuging tool for writting knitspeak files: https://github.com/cmf/psiviewer 
##Junit
https://www.jetbrains.com/help/idea/create-tests.html

https://github.com/junit-team/junit4
##Stitch Maps
Guide to their knitspeak varient: https://stitch-maps.com/about/knitspeak/
