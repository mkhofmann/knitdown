package Tracking.RegionTracking;

import TextureGraph.Courses.StitchSets.Cables.Cable;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.OneToOneStitch;
import TextureGraph.Courses.Stitches.Stitch;
import Tracking.SymmetryTracking.SymmetryData;

import java.util.LinkedHashSet;
import java.util.LinkedList;

public class StitchAreaByNamesTracker {
    public static final String STITCH_AREA_NAME_HEADER = "onRS,posInSet,"+
            "priorStitchType,nextStitchType,"+
            "parentCount,"+
            "firstParentType,childType,"+
            SymmetryData.SYMMETRY_HEAD+",stitchType";
    private String stitchType;
    public boolean onRS;
    public SetPos posInSet;
    private LinkedHashSet<String> childTypes = new LinkedHashSet<>();
    private LinkedHashSet<String> parentTypes = new LinkedHashSet<>();
    private int parentCount=0;
    public String priorNeighborType=null;
    public String nextNeighborType=null;
    private String firstParentType = null;
    private String childType = null;
    private SymmetryData symmetryData;
    public StitchAreaByNamesTracker(Stitch stitch, SymmetryData symmetryData) {
        this.symmetryData = symmetryData;
        this.stitchType = stitch.getClass().getSimpleName();
        if(this.stitchType.contains("tog")){
            char count = String.valueOf(stitch.parentLoops.size()).charAt(0);
            if(this.stitchType.contains("x"))
                this.stitchType = this.stitchType.replace('x',count);
            else
                this.stitchType = this.stitchType.replace('X',count);
        }
        this.onRS = stitch.owningStitchSet.owningCourse.isStandardRightSide();
        this.posInSet = SetPos.flat;
        if(stitch.owningStitchSet instanceof Cable){
            Cable cableOwner = (Cable) stitch.owningStitchSet;
            if(cableOwner.isHeld((OneToOneStitch) stitch))
                this.posInSet= SetPos.heald;
            else
                this.posInSet = SetPos.unheald;
        }
        Stitch prior = stitch.priorNeighbor();
        if (prior != null) {
            this.priorNeighborType = prior.getClass().getSimpleName();
        }
        Stitch next = stitch.nextNeighbor();
        if(next != null) {
            this.nextNeighborType = next.getClass().getSimpleName();
        }
        for(Stitch child: stitch.childLoop.getParentOf()) {
            childTypes.add(child.getClass().getSimpleName());
        }
        if(this.childTypes.size()>0)
            this.childType = new LinkedList<>(this.childTypes).get(0);
        for(Loop parent: stitch.parentLoops)
            for( Stitch parentStitch: parent.getChildOf()) {
                parentTypes.add(parentStitch.getClass().getSimpleName());
                parentCount++;
            }
        if(this.parentTypes.size()>0)
            this.firstParentType=new LinkedList<>(this.parentTypes).get(0);
    }

    @Override
    public String toString(){
        return (this.onRS+","+this.posInSet+","+
                this.priorNeighborType+","+this.nextNeighborType+","+
                this.parentCount+","+
                this.firstParentType+","+this.childType+","+
                this.symmetryData.toString()+","+this.stitchType
                ).toUpperCase();
    }

    @Override
    public int hashCode() {
        String code = getCode();
        try {
            return Integer.parseInt(code);
        } catch (NumberFormatException e){
            return code.hashCode();
        }
    }

    private String getCode() {
        int typeBit = this.getBit(this.stitchType);
        int priorBit = this.getBit(this.priorNeighborType);
        int nextBit = this.getBit(this.nextNeighborType);
        String end = String.valueOf(priorBit)+String.valueOf(typeBit)+String.valueOf(nextBit);
        LinkedList<Integer> childBits = new LinkedList<>();
        for(String childType: this.childTypes)
            childBits.add(this.getBit(childType));
        StringBuilder childData = new StringBuilder();
        for(Integer bit: childBits)
            childData.append(bit);
        LinkedList<Integer> parentBits = new LinkedList<>();
        for(String parentType: this.parentTypes)
            parentBits.add(this.getBit(parentType));
        StringBuilder parentData = new StringBuilder();
        for(Integer bit: parentBits)
            parentData.append(bit);
        String heritage = parentData+"9"+childData;
        return heritage+end;
    }

    private int getBit(String stitchString) {
        assert stitchString != null;
        int typeBit =9;
        switch (stitchString){
            case "CastOn":
                typeBit=0;
                break;
            case "K":
                typeBit=1;
                break;
            case "P":
                typeBit =2;
                break;
            case "KXtog":
                typeBit=3;
                break;
            case "Pxtog":
                typeBit=4;
                break;
            case "S2KPO":
                typeBit=5;
                break;
            case "SK2PO":
                typeBit = 6;
                break;
            case "Skpo":
                typeBit = 7;
                break;
            case "Ssp":
                typeBit = 8;
                break;
        }
        return typeBit;
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object obj) {
        return obj.hashCode() == this.hashCode();
    }
}
