package Tracking.RegionTracking;

import TextureGraph.Courses.Lean;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Stitch;
import Tracking.TypeTracking.StitchSetTypeTracker;
import Tracking.TypeTracking.StitchTypeTracker;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.LinkedList;

public class StitchSetAreaTracker {
    public static final String SET_AREA_HEADER = "SetLean, isSingle, isCT, isIncrease," +
            " hasSinglePrior, hasCTPrior, hasIncreasePrior," +
            " hasSingleNext, hasCTNext, hasIncreaseNext," +
            " hasSingleParent, hasCTParent, hasIncreaseParent, " +
            " hasSingleChild, hasCTChild, hasIncreaseChild, " +
            " hasKp, hasXtog, hasSD, hasSlip, hasYo, " +
            " SetCode";
    private StitchSetTypeTracker type;
    Lean lean;
    private HashSet<StitchSetTypeTracker> childTypes = new HashSet<>();
    private HashSet<StitchSetTypeTracker> parentTypes = new HashSet<>();
    private HashSet<StitchTypeTracker> stitchTypes = new HashSet<>();
    private StitchSetTypeTracker priorType;
    private StitchSetTypeTracker nextType;

    public StitchSetAreaTracker(StitchSet set){
        this.type = StitchSetTypeTracker.getSetType(set);
        this.lean = set.lean;
        StitchSet priorSetInLine = set.getPriorSetInLine();
        if(priorSetInLine != null)
            this.priorType = StitchSetTypeTracker.getSetType(priorSetInLine);
        StitchSet nextSetInLine = set.getNextSetInLine();
        if(nextSetInLine != null)
            this.nextType = StitchSetTypeTracker.getSetType(nextSetInLine);
        for(Stitch stitch: set){
            stitchTypes.add(StitchTypeTracker.getStitchType(stitch));
            for(Stitch child: stitch.childLoop.getParentOf())
                childTypes.add(StitchSetTypeTracker.getSetType(child.owningStitchSet));
            for(Loop parent: stitch.parentLoops)
                for( Stitch parentStitch: parent.getChildOf())
                    parentTypes.add(StitchSetTypeTracker.getSetType(parentStitch.owningStitchSet));
        }
    }

    private boolean isSingle(){
        return this.type == StitchSetTypeTracker.Single;
    }
    private boolean isCableOrTwist(){
        return this.type == StitchSetTypeTracker.Cable|| this.type==StitchSetTypeTracker.Twist;
    }
    private boolean isIncrease(){
        return this.priorType == StitchSetTypeTracker.Increase;
    }
    private boolean hasSinglePrior(){
        return this.priorType == StitchSetTypeTracker.Single;
    }
    private boolean hasCableOrTwistPrior(){
        return this.priorType == StitchSetTypeTracker.Cable|| this.priorType==StitchSetTypeTracker.Twist;
    }
    private boolean hasIncreasePrior(){
        return this.priorType == StitchSetTypeTracker.Increase;
    }
    private boolean hasSingleNext(){
        return this.nextType == StitchSetTypeTracker.Single;
    }
    private boolean hasCableOrTwistNext(){
        return this.nextType == StitchSetTypeTracker.Cable|| this.nextType==StitchSetTypeTracker.Twist;
    }
    private boolean hasIncreaseNext(){
        return this.nextType == StitchSetTypeTracker.Increase;
    }
    private boolean hasSingleChild(){
        return this.childTypes.contains(StitchSetTypeTracker.Single);
    }
    private boolean hasCableOrTwistChild(){
        return this.childTypes.contains(StitchSetTypeTracker.Cable) || this.childTypes.contains(StitchSetTypeTracker.Twist);
    }
    private boolean hasIncreaseChild(){
        return this.childTypes.contains(StitchSetTypeTracker.Increase);
    }
    private boolean hasSingleParent(){
        return this.parentTypes.contains(StitchSetTypeTracker.Single);
    }
    private boolean hasCableOrTwistParent(){
        return this.parentTypes.contains(StitchSetTypeTracker.Cable) || this.parentTypes.contains(StitchSetTypeTracker.Twist);
    }
    private boolean hasIncreaseParent(){
        return this.parentTypes.contains(StitchSetTypeTracker.Increase);
    }
    private boolean hasKP(){
        return this.stitchTypes.contains(StitchTypeTracker.K) || this.stitchTypes.contains(StitchTypeTracker.P);
    }
    private boolean hasXtog(){
        return this.stitchTypes.contains(StitchTypeTracker.KXtog) || this.stitchTypes.contains(StitchTypeTracker.Pxtog);
    }
    private boolean hasSD(){
        return this.stitchTypes.contains(StitchTypeTracker.SlippingDecrease);
    }
    private boolean hasSlip(){
        return this.stitchTypes.contains(StitchTypeTracker.SlipStitch);
    }
    private boolean hasYo(){
        return this.stitchTypes.contains(StitchTypeTracker.YO);
    }

    @Override
    public String toString() {
        return (lean+","+
                isSingle()+","+isCableOrTwist()+","+isIncrease()+","+
                hasSinglePrior()+","+hasCableOrTwistPrior()+","+hasIncreasePrior()+","+
                hasSingleNext()+","+hasCableOrTwistNext()+","+hasIncreaseNext()+","+
                hasSingleParent()+","+hasCableOrTwistParent()+","+hasIncreaseParent()+","+
                hasSingleChild()+","+hasCableOrTwistChild()+","+hasIncreaseChild()+","+
                hasKP()+","+hasXtog()+","+hasSD()+","+hasSlip()+","+hasYo()+"," +
                this.getCode()).toUpperCase();
    }


    @Override
    public int hashCode() {
        String code = getCode();
        try {
            int i = Integer.parseInt(code);
            return i;
        } catch (NumberFormatException e){
            int i = code.hashCode();
            return i;
        }
    }

    @NotNull
    private String getCode() {
        int typeBit = 0;
        if(this.isCableOrTwist())
            typeBit = 1;
        if(this.isIncrease())
            typeBit = 2;
        int priorBit = 0;
        if(this.isCableOrTwist())
            priorBit = 1;
        if(this.isIncrease())
            priorBit = 2;
        int nextBit = 0;
        if(this.isCableOrTwist())
            nextBit = 1;
        if(this.isIncrease())
            nextBit = 2;
        String end = String.valueOf(priorBit)+String.valueOf(typeBit)+String.valueOf(nextBit);
        LinkedList<Integer> childBits = new LinkedList<>();
        for(StitchSetTypeTracker childType: this.childTypes){
            if(childType == StitchSetTypeTracker.Single)
                childBits.add(0);
            else if(childType == StitchSetTypeTracker.Cable || childType == StitchSetTypeTracker.Twist)
                childBits.add(1);
            else if(childType == StitchSetTypeTracker.Increase)
                childBits.add(2);
        }
        StringBuilder childData = new StringBuilder();
        for(Integer bit: childBits)
            childData.append(bit);
        LinkedList<Integer> parentBits = new LinkedList<>();
        for(StitchSetTypeTracker parentType: this.parentTypes){
            if(parentType == StitchSetTypeTracker.Single)
                parentBits.add(0);
            else if(parentType == StitchSetTypeTracker.Cable || parentType == StitchSetTypeTracker.Twist)
                parentBits.add(1);
            else if(parentType == StitchSetTypeTracker.Increase)
                parentBits.add(2);
        }
        StringBuilder parentData = new StringBuilder();
        for(Integer bit: parentBits)
            parentData.append(bit);
        LinkedList<Integer> stitchBits = new LinkedList<>();
        for(StitchTypeTracker stitchType: this.stitchTypes){
            if(stitchType == StitchTypeTracker.K || stitchType == StitchTypeTracker.P)
                stitchBits.add(3);
            if(stitchType == StitchTypeTracker.KXtog || stitchType == StitchTypeTracker.Pxtog)
                stitchBits.add(4);
            else if(stitchType == StitchTypeTracker.SlippingDecrease)
                stitchBits.add(5);
            else if(stitchType == StitchTypeTracker.SlipStitch)
                stitchBits.add(6);
            else if(stitchType == StitchTypeTracker.YO)
                stitchBits.add(7);
            else
                stitchBits.add(8);
        }
        StringBuilder stitchData = new StringBuilder();
        for(Integer bit: stitchBits)
            stitchData.append(bit);
        String heritage = parentData+"9"+stitchData+"9"+childData;
        return heritage+end;
    }

    @Override
    public boolean equals(Object obj) {
        return obj.hashCode() == this.hashCode();
    }
}
