package Tracking.SymmetryTracking;

import TextureGraph.Courses.Course;
import TextureGraph.Courses.Stitches.Stitch;
import TextureGraph.TextureGraph;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;

public class TextureSymmetryIndex {
    public int symmetryColumn = -1;
    public TextureSymmetryIndex(TextureGraph textureGraph) {
        LinkedHashMap<Integer, LinkedHashMap<Integer, Integer>> symmetryIndex = new LinkedHashMap<>();
        for(Course course : textureGraph){
            LinkedHashMap<Integer, Integer> lineIndex = new LinkedHashMap<>();
            LinkedList<Stitch> stitchesLinked = course.getStitches();
            ArrayList<Stitch> stitches = new ArrayList<>(stitchesLinked);
            for(int centerIndex= 1; centerIndex<stitches.size()-1; centerIndex++){
                int symmetryCount = 0;
                for(int stitchIndex = 0; stitchIndex<stitches.size(); stitchIndex++){
                    int diffToCenter = stitchIndex - centerIndex;
                    if(diffToCenter != 0) {
                        SymmetryData symmetryData = new SymmetryData(stitches.get(stitchIndex), diffToCenter, stitchesLinked);
                        if(symmetryData.isSymmetrical())
                            symmetryCount++;
                    }
                }
                lineIndex.put(centerIndex, symmetryCount);
            }
            symmetryIndex.put(course.getCourseID(), lineIndex);
        }
        LinkedHashMap<Integer, Integer> symmetryColumnIndex = new LinkedHashMap<>();
        for(LinkedHashMap<Integer, Integer> lineIndex: symmetryIndex.values())
            for (int centerIndex : lineIndex.keySet())
                if (symmetryColumnIndex.containsKey(centerIndex))
                    symmetryColumnIndex.put(centerIndex, symmetryColumnIndex.get(centerIndex) + lineIndex.get(centerIndex));
                else
                    symmetryColumnIndex.put(centerIndex, lineIndex.get(centerIndex));

        int bestSymmetry = 0;
        for(int centerIndex: symmetryColumnIndex.keySet())
            if (symmetryColumnIndex.get(centerIndex) > bestSymmetry) {
                this.symmetryColumn = centerIndex;
                bestSymmetry = symmetryColumnIndex.get(centerIndex);
            }
    }
}
