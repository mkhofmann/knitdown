package Tracking.SymmetryTracking;

import TextureGraph.Courses.Stitches.Stitch;

import java.util.LinkedList;

public class SymmetryData {
    public static final String SYMMETRY_HEAD = "oppositeStitchType,distanceFormSymmetryLine,isSymmetrical";
    private int distanceFromSymmetryLine;
    private Stitch oppositeStitch = null;
    private Stitch stitch;

    public SymmetryData(Stitch stitch, LinkedList<Stitch> stitches, int centerIndex){
        this(stitch, stitches.indexOf(stitch)-centerIndex, stitches);
    }

    SymmetryData(Stitch stitch, int distanceFromSymmetryLine, LinkedList<Stitch> stitches){
        this.stitch = stitch;
        this.distanceFromSymmetryLine = distanceFromSymmetryLine;
        int indexOfStitch = stitches.indexOf(this.stitch);
        int index = indexOfStitch - (distanceFromSymmetryLine * 2);
        if(index>0 && index<stitches.size())
            this.oppositeStitch = stitches.get(index);
    }

    boolean isSymmetrical(){
        return this.stitch.isSymmetrical(this.oppositeStitch);
    }

    @Override
    public String toString() {
        String oppositeName;
        if(this.oppositeStitch==null)
            oppositeName = "null";
        else
            oppositeName= this.oppositeStitch.getClass().getSimpleName();
        return oppositeName +","+distanceFromSymmetryLine+","+this.isSymmetrical();
    }
}
