package Tracking.TypeTracking;

import TextureGraph.Courses.StitchSets.StitchSet;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;

public enum StitchSetTypeTracker {
    Cable(TextureGraph.Courses.StitchSets.Cables.Cable.class),
    Twist(TextureGraph.Courses.StitchSets.Twists.Twist.class),
    Increase(TextureGraph.Courses.StitchSets.Increases.Increase.class),
    Single(TextureGraph.Courses.StitchSets.Single.class);
    private Class<? extends StitchSet> setType;
    public static int cableTwistCount = 0;
    public static int increaseCount = 0;
    public static int singleCount = 0;
    StitchSetTypeTracker(@Nonnull Class<? extends StitchSet> setType){
        this.setType = setType;
    }

    public static void countType(@Nonnull StitchSetTypeTracker tracker) {
        switch (tracker){
            case Cable:
            case Twist:
                cableTwistCount++;
                break;
            case Increase:
                increaseCount++;
                break;
            case Single:
                singleCount++;
                break;
        }
    }

    @NotNull
    public static StitchSetTypeTracker getSetType(@Nonnull StitchSet set){
        Class superType = set.getClass();
        StitchSetTypeTracker tracker = null;
        while(tracker == null && superType!=null) {
            try {
                tracker = StitchSetTypeTracker.valueOf(superType.getSimpleName());
            } catch (IllegalArgumentException e) {
                superType = superType.getSuperclass();
            }
        }
        assert tracker != null;
        countType(tracker);
        return tracker;
    }
}
