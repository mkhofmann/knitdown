package Tracking.TypeTracking;

import TextureGraph.Courses.Stitches.Knit.KXtog;
import TextureGraph.Courses.Stitches.Stitch;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;

public enum  StitchTypeTracker {
    K(TextureGraph.Courses.Stitches.Knit.K.class),
    KXtog(KXtog.class),
    P(TextureGraph.Courses.Stitches.Purl.P.class),
    Pxtog(TextureGraph.Courses.Stitches.Purl.Pxtog.class),
    SlippingDecrease(TextureGraph.Courses.Stitches.SlippingDecreases.SlippingDecrease.class),
    SlipStitch(TextureGraph.Courses.Stitches.StitchInRow.SlipStitch.class),
    CastOn(TextureGraph.Courses.Stitches.StitchInRow.CastOn.class),
    YO(TextureGraph.Courses.Stitches.YO.class);
    public static int kpCount = 0;
    public static int xTogCount = 0;
    public static int slipDecreaseCount = 0;
    public static int slipCount = 0;
    public static int yoCount=0;
    public static int castOnCount = 0;
    StitchTypeTracker(@Nonnull Class<? extends Stitch> stitchType){
    }

    public static void countType(@Nonnull StitchTypeTracker tracker) {
        switch (tracker){
            case K:
            case P:
                kpCount++;
                break;
            case KXtog:
            case Pxtog:
                xTogCount++;
                break;
            case SlippingDecrease:
                slipDecreaseCount++;
                break;
            case YO:
                yoCount++;
                break;
            case CastOn:
                castOnCount++;
                break;
            case SlipStitch:
                slipCount++;
                break;
        }
    }

    @NotNull
    public static StitchTypeTracker getStitchType(@Nonnull Stitch stitch){
        Class superType = stitch.getClass();
        StitchTypeTracker tracker = null;
        while(tracker == null && superType!=null) {
            try {
                tracker = StitchTypeTracker.valueOf(superType.getSimpleName());
            } catch (IllegalArgumentException e) {
                superType = superType.getSuperclass();
            }
        }
        assert tracker != null;
        countType(tracker);
        return tracker;
    }
}
