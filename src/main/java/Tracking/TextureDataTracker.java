package Tracking;

import KnitSpeakParser.PsiInterface.KnitCompiler;
import TextureGraph.Courses.Course;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Stitch;
import TextureGraph.Courses.Stitches.StitchInRow.CastOn;
import TextureGraph.TextureBuiltInRow;
import Tracking.RegionTracking.StitchAreaByNamesTracker;
import Tracking.RegionTracking.StitchSetAreaTracker;
import Tracking.SymmetryTracking.SymmetryData;
import Tracking.SymmetryTracking.TextureSymmetryIndex;
import Tracking.TextureTracking.DataSetType;
import Tracking.TextureTracking.TextureType;
import Tracking.TypeTracking.StitchSetTypeTracker;
import Tracking.TypeTracking.StitchTypeTracker;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

public class TextureDataTracker {
    private int kpCount;
    private int xTogCount;
    private int slipDecreaseCount;
    private int slipCount;
    private int yoCount;
    private int setTypeCount;
    private int stitchTypeCount;
    private int cableCount;
    private int increaseCount;
    private int loopCount=0;
    private int stitchCount=0;
    private int setCount=0;
    public String textureName;
    private DataSetType dataSetType;
    public TextureType textureType= TextureType.unknown;
    public LinkedList<StitchAreaByNamesTracker> allStitchAreaByNameTrackers = new LinkedList<>();

    public TextureDataTracker(@Nonnull KnitCompiler knitCompiler,DataSetType dataSetType) throws Exception {
        this.textureName = knitCompiler.psiName;
        this.textureName = this.textureName.replace('\'','_');
        this.textureName = this.textureName.replace('\"','_');
        this.dataSetType = dataSetType;
        if(this.dataSetType == DataSetType.StitchEssentials)
            this.textureType = textureTypeBySEName(this.textureName);
        TextureBuiltInRow texture = knitCompiler.buildTextureByWidthRepsAndHeightReps(1,1);
        TextureSymmetryIndex textureSymmetryIndex = new TextureSymmetryIndex(texture);
        this.cableCount= StitchSetTypeTracker.cableTwistCount;
        this.increaseCount= StitchSetTypeTracker.increaseCount;
        this.kpCount = StitchTypeTracker.kpCount;
        this.xTogCount = StitchTypeTracker.xTogCount;
        this.slipDecreaseCount = StitchTypeTracker.slipDecreaseCount;
        this.slipCount = StitchTypeTracker.slipCount;
        this.yoCount = StitchTypeTracker.yoCount;
        HashSet<StitchSetTypeTracker> setTypes = new HashSet<>();
        HashSet<StitchTypeTracker> stitchTypes = new HashSet<>();
        for(Course course : texture) {
            LinkedList<Stitch> stitches = course.getStitches();
            for (StitchSet set : course) {
                setTypes.add(StitchSetTypeTracker.getSetType(set));
                this.setCount++;
                StitchSetAreaTracker stitchSetAreaTracker = new StitchSetAreaTracker(set);
                HashMap<StitchSetAreaTracker, Integer> stitchSetAreasToCounts = new HashMap<>();
                if(stitchSetAreasToCounts.containsKey(stitchSetAreaTracker))
                    stitchSetAreasToCounts.put(stitchSetAreaTracker, stitchSetAreasToCounts.get(stitchSetAreaTracker)+1);
                else
                    stitchSetAreasToCounts.put(stitchSetAreaTracker,1);
                for(Stitch stitch: set) {
                    if(!(stitch instanceof CastOn)) {
                        stitchTypes.add(StitchTypeTracker.getStitchType(stitch));
                        this.stitchCount++;
                        this.loopCount++;
                        SymmetryData symmetryData = new SymmetryData(stitch,stitches, textureSymmetryIndex.symmetryColumn);
                        StitchAreaByNamesTracker byNamesTracker = new StitchAreaByNamesTracker(stitch, symmetryData);
                        this.allStitchAreaByNameTrackers.add(byNamesTracker);
                    }
                }
            }
        }
        this.cableCount=    StitchSetTypeTracker.cableTwistCount - this.cableCount;
        this.increaseCount= StitchSetTypeTracker.increaseCount   - this.increaseCount;
        this.kpCount = StitchTypeTracker.kpCount                     - this.kpCount;
        this.xTogCount = StitchTypeTracker.xTogCount                 - this.xTogCount;
        this.slipDecreaseCount = StitchTypeTracker.slipDecreaseCount - this.slipDecreaseCount;
        this.slipCount = StitchTypeTracker.slipCount                 - this.slipCount;
        this.yoCount = StitchTypeTracker.yoCount                     - this.yoCount;
        this.setTypeCount = setTypes.size();
        this.stitchTypeCount = stitchTypes.size();
    }

    public String toString(){
        return dataSetType+"," +
                textureName+","+
                setTypeCount+","+
                stitchTypeCount+","+
                loopCount+","+
                setCount+","+
                stitchCount+","+
                cableCount+","+
                increaseCount+","+
                kpCount+","+
                xTogCount+","+
                slipDecreaseCount+","+
                slipCount+","+
                yoCount+"\n";
    }

    public static TextureType textureTypeBySEName(String textureName){
        String numString = textureName.substring(0,3);
        int num = Integer.parseInt(numString);
        if(num<37)
            return TextureType.KP;
        else if(num<74)
            return TextureType.Twist;
        else if(num<122)
            return TextureType.Cable;
        else
            return TextureType.Lace;
    }

}
