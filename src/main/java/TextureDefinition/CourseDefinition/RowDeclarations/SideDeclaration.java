package TextureDefinition.CourseDefinition.RowDeclarations;

public enum SideDeclaration {
    RSOdd("rs", true),
    RSEven("rs", false),
    WSOdd("ws", true),
    WSEven("ws", false),
    Niether("null", false);

    String side;
    boolean odd;
    SideDeclaration(String side, boolean odd) {
        this.side = side;
        this.odd = odd;
    }

    public boolean isOdd() {
        return odd;
    }
    public boolean isRS(){
        return this.side.equals("rs");
    }

    public static SideDeclaration getSideDeclaration(String side, boolean odd){
        switch (side){
            case "rs":
                if(odd)
                    return RSOdd;
                else
                    return RSEven;
            case "ws":
                if(odd)
                    return WSOdd;
                else
                    return WSEven;
            default:
                return null;
        }
    }
}
