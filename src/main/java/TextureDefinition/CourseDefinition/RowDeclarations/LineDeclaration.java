package TextureDefinition.CourseDefinition.RowDeclarations;

import KnitSpeakParser.psi.*;
import TextureDefinition.NumConverter;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Megan on 12/1/2016.
 * a row decelartion to associate course operations with course numbers in the final textureGraph
 */
public class LineDeclaration {
    /**
     * the knitspeak parsed declaration
     */
    private KnitSpeakRowDeclaration rowDeclarationElement;

    /**
     * @return the index of the last row to be created
     */
    @Contract(pure = true)
    public int getLastRow() {return lastRow;}

    /**
     * @return the list of the row ids in their indexed order
     */
    @NotNull
    @Contract(pure = true)
    public ArrayList<Integer> getRow_ids() {return row_ids;}

    /**
     * declares the which sides were declared on odd or even units
     */
    public SideDeclaration sideDeclaration;
    /**
     * the index of the last row to be created
     */
    private int lastRow;
    /**
     * the list of the row ids in their indexed order
     */
    private ArrayList<Integer> row_ids = new ArrayList<>();

    /**
     * @param rowDeclarationElement the knitspeak parsing of the row declaration
     */
    public LineDeclaration(@NotNull KnitSpeakRowDeclaration rowDeclarationElement) {
        this.rowDeclarationElement = rowDeclarationElement;
        KnitSpeakRowId onlyRowID = this.rowDeclarationElement.getRowId();
        if(onlyRowID != null){
            this.parseRowId(onlyRowID);
        } else {
            assert this.rowDeclarationElement.getManyRowDeclaration() != null;
            List<KnitSpeakRowId> rowIdList = this.rowDeclarationElement.getManyRowDeclaration().getRowList().getRowIdList();
            for (KnitSpeakRowId rowId : rowIdList)
                this.parseRowId(rowId);
            Collections.sort(this.row_ids);
        }
        this.lastRow = this.row_ids.get(this.row_ids.size()-1);
    }

    /**
     * @param rowId the knitspeak definiton of the row identifier
     */
    private void parseRowId(@NotNull KnitSpeakRowId rowId) {
        KnitSpeakRowToRowId rowToRowId = rowId.getRowToRowId();
        if(rowToRowId != null){//x-x declaration
            List<KnitSpeakNumPossibleSuffixed> range =rowToRowId.getNumPossibleSuffixedList();
            int[] vals = new int[2];
            vals[0] = NumConverter.convertNum(range.get(0));
            vals[1] = NumConverter.convertNum(range.get(1));
            for(int i = vals[0]; i<=vals[1]; i++)
                this.row_ids.add(i);
            return;
        }
        KnitSpeakRowTypeId rowTypeId = rowId.getRowTypeId();
        if(rowTypeId != null){
            String side = rowTypeId.getSide().getText().toLowerCase();
            this.sideDeclaration = SideDeclaration.getSideDeclaration(side, this.row_ids.get(0)%2==1);
            return;
        }
        int id =  NumConverter.convertNum(rowId.getNumPossibleSuffixed());
        this.row_ids.add(id);
    }


    @Override
    public int hashCode(){return this.rowDeclarationElement.getText().hashCode();}
    @Override
    public String toString() {return this.rowDeclarationElement.getText();}
    @Override
    public boolean equals(Object o){
        if( o instanceof LineDeclaration){
            LineDeclaration rowDeclaration= (LineDeclaration) o;
            return rowDeclaration.rowDeclarationElement.getText().equals(this.rowDeclarationElement.getText());
        }
        return false;
    }


}
