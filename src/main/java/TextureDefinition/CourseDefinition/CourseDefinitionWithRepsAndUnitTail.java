package TextureDefinition.CourseDefinition;

import CompilerExceptions.ParsingExceptions.*;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.KnitSpeakLine;
import KnitSpeakParser.psi.KnitSpeakStitchStatementList;
import KnitSpeakParser.psi.KnitSpeakToLastST;
import TextureDefinition.CourseDefinition.RowDeclarations.LineDeclaration;
import TextureDefinition.StitchDefinition.StitchListDefinition;
import TextureDefinition.StitchDefinition.StitchStatementDefinition;
import TextureDefinition.TextureDefinition;
import TextureGraph.Courses.Course;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 12/16/2016.
 * abstraction for course with reps and a single stitch following them
 */
public class CourseDefinitionWithRepsAndUnitTail extends CourseDefinitionWithReps {
    /**
     * the number of stitches remaining when the last stitch is created
     */
    private int remainingStitches;
    /**
     * the last stitch(s) to be constructed on end border
     */
    private StitchListDefinition lastStitchList;
    /**
     * the definitoin of the last stitch
     */
    private StitchStatementDefinition lastStitchStatement;
    CourseDefinitionWithRepsAndUnitTail(@NotNull KnitSpeakLine course, @NotNull LineDeclaration rowDeclaration, TextureDefinition textureDefinition) throws LineTypePreviouslyDeclaredException, OnlyCommonStitchesInOneStitchException, TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        super(course, rowDeclaration, textureDefinition);
        KnitSpeakToLastST lastSTEnding = this.blockEnding.getToLastST();
        assert lastSTEnding != null;
        KnitSpeakStitchStatementList lastStitchStatmentListElement = lastSTEnding.getStitchStatementList();
        this.lastStitchList = new StitchListDefinition(lastStitchStatmentListElement);
        this.remainingStitches = this.lastStitchList.size();
        if(this.remainingStitches==1)
            this.lastStitchStatement = this.lastStitchList.get(0);
    }
    @Override
    public int getBorderLoopCount() {
        if(this.startingList == null)
            return this.lastStitchList.getRequiredLoops();
        assert this.lastStitchStatement!=null;
        return this.startingList.getRequiredLoops()+this.lastStitchStatement.getRequiredLoops();
    }
    @Override
    public int getRequiredLoops(){return super.getRequiredLoops() + this.lastStitchStatement.getRequiredLoops();}
    @Override
    public int getMultipleOfRequiredLoops(int multiplier){return super.getMultipleOfRequiredLoops(multiplier)+this.lastStitchList.getRequiredLoops();}
    @Override
    public int matchingWidth(int width, boolean pad){
        int startingReq =0;
        if (this.hasStartingList)
            startingReq = this.startingList.getRequiredLoops();
        int endingReq = this.lastStitchStatement.getRequiredLoops();
        int centerWidth = width-startingReq-endingReq;
        int reqLoops = this.repeatedList.getRequiredLoops();
        int leftover = centerWidth%reqLoops;
        if(leftover==0)
            return width;
        if(pad)
            return width - leftover;
        int sections = centerWidth/reqLoops;
        int min = sections*reqLoops;
        int max = sections*(reqLoops+1);
        int difMin = centerWidth-min;
        int difMax = max-centerWidth;
        if( difMin<= difMax)
            return min+startingReq+endingReq;
        return max+startingReq+endingReq;
    }
    @NotNull
    @Override
    public Course buildLine(@NotNull Course parentCourse, @NotNull Yarn yarn, int id, boolean allowLoopVariance) throws InsufficientLoopsException {
        boolean rightSide = this.idIsRS(id);
        LinkedList<Loop> availableLoops = CourseDefinition.availableParentLoops(parentCourse);
        LinkedList<StitchSet> stitchSets = new LinkedList<>();
        if(this.hasStartingList)
            stitchSets.addAll(this.startingList.buildStitchSetList(yarn, availableLoops,rightSide));
        //note: available loops should not have remaining loops to break up
        if( ( availableLoops.size() - this.remainingStitches )%this.repeatedList.getRequiredLoops() !=0)
            throw new InvalidRepetitionLoopsException(this.rowDeclaration.toString());
        int endRequieredLoops = this.lastStitchStatement.getRequiredLoops();
        while(availableLoops.size()>endRequieredLoops)
            stitchSets.addAll(this.repeatedList.buildStitchSetList(yarn, availableLoops, rightSide));
        stitchSets.addAll(this.lastStitchList.buildStitchSetList(yarn, availableLoops, rightSide));
        return new Course(stitchSets, id);
    }
}
