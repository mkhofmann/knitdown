package TextureDefinition.CourseDefinition;

import CompilerExceptions.ParsingExceptions.*;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.KnitSpeakLine;
import KnitSpeakParser.psi.KnitSpeakStitchStatementList;
import KnitSpeakParser.psi.KnitSpeakToLastSTS;
import TextureDefinition.CourseDefinition.RowDeclarations.LineDeclaration;
import TextureDefinition.NumConverter;
import TextureDefinition.StitchDefinition.StitchListDefinition;
import TextureDefinition.TextureDefinition;
import TextureGraph.Courses.Course;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 1/13/2017.
 * abstraction for course with a single rep and a tail border
 */
public class CourseDefinitionWithSingleRepAndTail extends CourseDefinitionWithSingleRep {
    /**
     * the number of stitches remaining when the border starts
     */
    private int remainingStitches;
    /**
     * the list of stitches that will be used in the final border
     */
    private StitchListDefinition finalList;
    CourseDefinitionWithSingleRepAndTail(@NotNull KnitSpeakLine course, @NotNull LineDeclaration rowDeclaration, TextureDefinition textureDefinition) throws LineTypePreviouslyDeclaredException, OnlyCommonStitchesInOneStitchException, TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        super(course, rowDeclaration, textureDefinition);
        KnitSpeakToLastSTS lastSTSEnding = this.blockEnding.getToLastSTS();
        assert lastSTSEnding != null;
        this.remainingStitches = NumConverter.convertNum(lastSTSEnding.getStitchesCount());
        KnitSpeakStitchStatementList finalListElement = lastSTSEnding.getStitchStatementList();
        this.finalList = new StitchListDefinition(finalListElement);
    }
    @Override
    public int getBorderLoopCount() {return this.startingList.getRequiredLoops()+this.finalList.getRequiredLoops();}

    @Override
    public int getRequiredLoops(){return super.getRequiredLoops() + this.finalList.getRequiredLoops();}

    @Override
    public int getMultipleOfRequiredLoops(int multiplier){return super.getMultipleOfRequiredLoops(multiplier)+this.finalList.getRequiredLoops();}

    @Override
    public int matchingWidth(int width, boolean pad){
        int startingReq = 0;
        if(this.hasStartingList)
            startingReq = this.startingList.getRequiredLoops();
        int endingReq = this.finalList.getRequiredLoops();
        int centerWidth = width-startingReq-endingReq;
        int reqLoops = this.repStatement.getRequiredLoops();
        int leftover = centerWidth%reqLoops;
        if(leftover==0)
            return width;
        if(pad)
            return width - leftover;
        int sections = centerWidth/reqLoops;
        int min = sections*reqLoops;
        int max = sections*(reqLoops+1);
        int difMin = centerWidth-min;
        int difMax = max-centerWidth;
        if( difMin<= difMax)
            return min+startingReq+endingReq;
        return max+startingReq+endingReq;
    }
    @NotNull
    @Override
    public Course buildLine(@NotNull Course parentCourse, @NotNull Yarn yarn, int id, boolean allowLoopVariance) throws InsufficientLoopsException {
        boolean rightSide = this.idIsRS(id);
        LinkedList<Loop> availableLoops = CourseDefinition.availableParentLoops(parentCourse);
        LinkedList<StitchSet> stitchSets = new LinkedList<>();
        if(this.hasStartingList)
            stitchSets.addAll(this.startingList.buildStitchSetList(yarn, availableLoops,rightSide));
        //note: available loops should not have remaining loops to break up
        int repList = this.repStatement.getRequiredLoops();
        if( ( availableLoops.size() - this.remainingStitches )%repList !=0)
            throw new InvalidRepetitionLoopsException("prior:"+this.toString()+id);
        int requiredFinalLoops = this.finalList.getRequiredLoops();
        while(availableLoops.size()>requiredFinalLoops)
            stitchSets.addAll(this.repStatement.buildStitches(availableLoops, yarn, rightSide, -1));
        stitchSets.addAll(this.finalList.buildStitchSetList(yarn, availableLoops,rightSide));
        return new Course(stitchSets, id);
    }
}
