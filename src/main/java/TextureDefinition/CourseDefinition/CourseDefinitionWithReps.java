package TextureDefinition.CourseDefinition;

import CompilerExceptions.ParsingExceptions.*;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.KnitSpeakBlockEnding;
import KnitSpeakParser.psi.KnitSpeakLastStitchBlock;
import KnitSpeakParser.psi.KnitSpeakLine;
import KnitSpeakParser.psi.KnitSpeakStitchStatementList;
import TextureDefinition.CourseDefinition.RowDeclarations.LineDeclaration;
import TextureDefinition.StitchDefinition.RepStitchListDefinition;
import TextureDefinition.StitchDefinition.StitchListDefinition;
import TextureDefinition.TextureDefinition;
import TextureGraph.Courses.Course;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 12/1/2016.
 * A course abstraction that has a repeating list
 */
public class CourseDefinitionWithReps extends CourseDefinition {
    /**
     * the list of stitch definitions that will be repeated over a tile
     */
    StitchListDefinition repeatedList;
    /**
     * the end definition in the knitspeak
     */
    KnitSpeakBlockEnding blockEnding;
    CourseDefinitionWithReps(@NotNull KnitSpeakLine course, @NotNull LineDeclaration rowDeclaration, TextureDefinition textureDefinition) throws LineTypePreviouslyDeclaredException, OnlyCommonStitchesInOneStitchException, TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        super(course,rowDeclaration, textureDefinition);
        KnitSpeakLastStitchBlock lastStitchBlock = this.stitchRowProcess.getLastStitchBlock();
        assert lastStitchBlock != null;
        this.blockEnding = lastStitchBlock.getBlockEnding();
        KnitSpeakStitchStatementList repeatedListElement = lastStitchBlock.getStitchStatementList();
        assert repeatedListElement != null;
        this.repeatedList = new RepStitchListDefinition(repeatedListElement);
    }
    @Override
    public int getRepLoopCount() {return this.repeatedList.getRequiredLoops();}
    @Override
    public int getBorderLoopCount() {
        if(this.startingList == null)
            return 0;
        return this.startingList.getRequiredLoops();
    }

    @Override
    public int getRequiredLoops(){return super.getRequiredLoops() + this.repeatedList.getRequiredLoops();}
    @Override
    public int getMultipleOfRequiredLoops(int multiplier) {
        int loops = 0;
        if (this.hasStartingList)
            loops += this.startingList.getRequiredLoops();
        loops += multiplier * this.repeatedList.getRequiredLoops();
        return loops;
    }
    @Override
    public int matchingWidth(int width, boolean pad){
        int startingReq = 0;
        if(this.hasStartingList)
            startingReq = this.startingList.getRequiredLoops();
        int widthAfterStart = width-startingReq;
        int reqLoops = this.repeatedList.getRequiredLoops();
        int leftover = widthAfterStart%reqLoops;
        if(leftover==0)
            return width;
        if(pad)
            return width - leftover;
        int sections = widthAfterStart/reqLoops;
        int min = sections*reqLoops;
        int max = sections*(reqLoops+1);
        int difMin = widthAfterStart-min;
        int difMax = max-widthAfterStart;
        if( difMin<= difMax)
            return min+startingReq;
        return max+startingReq;
    }
    @NotNull
    @Override
    public Course buildLine(@NotNull Course parentCourse, @NotNull Yarn yarn, int id, boolean allowLoopVariance) throws InsufficientLoopsException {
        boolean rightSide = this.idIsRS(id);
        LinkedList<Loop> availableLoops = CourseDefinition.availableParentLoops(parentCourse);
        LinkedList<StitchSet> stitchSets = new LinkedList<>();
        if( this.hasStartingList)
            stitchSets.addAll(this.startingList.buildStitchSetList(yarn, availableLoops, rightSide));
        //note: available loops should not have remaining loops to break up
        int requiredRep = this.repeatedList.getRequiredLoops();
        if(availableLoops.size()%requiredRep !=0)
            throw new InvalidRepetitionLoopsException(this.course.getText());
        int requiredRepList = this.repeatedList.getRequiredLoops();
        while(availableLoops.size()>=requiredRepList)
            stitchSets.addAll(this.repeatedList.buildStitchSetList(yarn, availableLoops,rightSide));
        return new Course(stitchSets, id);
    }


}
