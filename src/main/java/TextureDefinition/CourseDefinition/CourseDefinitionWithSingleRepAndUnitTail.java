package TextureDefinition.CourseDefinition;

import CompilerExceptions.ParsingExceptions.*;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.KnitSpeakLine;
import KnitSpeakParser.psi.KnitSpeakStitchStatementList;
import KnitSpeakParser.psi.KnitSpeakToLastST;
import TextureDefinition.CourseDefinition.RowDeclarations.LineDeclaration;
import TextureDefinition.StitchDefinition.StitchListDefinition;
import TextureDefinition.StitchDefinition.StitchStatementDefinition;
import TextureDefinition.TextureDefinition;
import TextureGraph.Courses.Course;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 1/13/2017.
 * abstraction for course with a single rep followed by a single stitch
 */
public class CourseDefinitionWithSingleRepAndUnitTail extends CourseDefinitionWithSingleRep {
    /**
     * the number of stitches remaining when the last stitch is created
     */
    private int remainingStitches;
    /**
     * the last stitch(s) to be constructed on end border
     */
    private StitchListDefinition lastStitchList;
    /**
     * the last stitch statement describing what is done with the last loop
     */
    private StitchStatementDefinition lastStitchStatement;
    CourseDefinitionWithSingleRepAndUnitTail(KnitSpeakLine course, LineDeclaration rowDeclaration, TextureDefinition textureDefinition) throws OnlyCommonStitchesInOneStitchException, LineTypePreviouslyDeclaredException, TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        super(course, rowDeclaration, textureDefinition);
        KnitSpeakToLastST lastSTEnding = this.blockEnding.getToLastST();
        assert lastSTEnding != null;
        KnitSpeakStitchStatementList lastStitchStatementListElement = lastSTEnding.getStitchStatementList();
        this.lastStitchList = new StitchListDefinition(lastStitchStatementListElement);
        this.remainingStitches = this.lastStitchList.size();
        if(this.remainingStitches == 1)
            this.lastStitchStatement = this.lastStitchList.get(0);
    }

    @Override
    public int getBorderLoopCount() {return this.startingList.getRequiredLoops()+this.lastStitchStatement.getRequiredLoops();}
    @Override
    public int getRequiredLoops(){return super.getRequiredLoops() + this.lastStitchStatement.getRequiredLoops();}
    @Override
    public int getMultipleOfRequiredLoops(int multiplier){return super.getMultipleOfRequiredLoops(multiplier)+this.lastStitchList.getRequiredLoops();}

    @Override
    public int matchingWidth(int width, boolean pad){
        int startingReq =0;
        if (this.hasStartingList)
            startingReq = this.startingList.getRequiredLoops();
        int endingReq = this.lastStitchStatement.getRequiredLoops();
        int centerWidth = width-startingReq-endingReq;
        int reqLoops = this.repStatement.getRequiredLoops();
        int leftover = centerWidth%reqLoops;
        if(leftover==0)
            return width;
        if(pad)
            return width - leftover;
        int sections = centerWidth/reqLoops;
        int min = sections*reqLoops;
        int max = sections*(reqLoops+1);
        int difMin = centerWidth-min;
        int difMax = max-centerWidth;
        if( difMin<= difMax)
            return min+startingReq+endingReq;
        return max+startingReq+endingReq;
    }
    @NotNull
    @Override
    public Course buildLine(@NotNull Course parentCourse, @NotNull Yarn yarn, int id, boolean allowLoopVariance) throws InsufficientLoopsException {
        boolean rightSide = this.idIsRS(id);
        LinkedList<Loop> availableLoops = CourseDefinition.availableParentLoops(parentCourse);
        LinkedList<StitchSet> stitchSets = new LinkedList<>();
        if(this.hasStartingList)
            stitchSets.addAll(this.startingList.buildStitchSetList(yarn, availableLoops,rightSide));
        //note: available loops should not have remaining loops to break up
        if( ( availableLoops.size() - this.remainingStitches )%this.repStatement.getRequiredLoops() !=0)
            throw new InvalidRepetitionLoopsException(this.rowDeclaration.toString());
        int endRequiredLoops = this.lastStitchStatement.getRequiredLoops();
        while(availableLoops.size()>endRequiredLoops)
            stitchSets.addAll(this.repStatement.buildStitches(availableLoops, yarn, rightSide, -1));
        stitchSets.addAll(this.lastStitchList.buildStitchSetList(yarn, availableLoops, rightSide));
        return new Course(stitchSets, id);
    }
}
