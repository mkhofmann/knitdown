package TextureDefinition.CourseDefinition;

import CompilerExceptions.ParsingExceptions.*;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.KnitSpeakBlockEnding;
import KnitSpeakParser.psi.KnitSpeakLastStitchBlock;
import KnitSpeakParser.psi.KnitSpeakLine;
import TextureDefinition.CourseDefinition.RowDeclarations.LineDeclaration;
import TextureDefinition.TextureDefinition;
import org.jetbrains.annotations.NotNull;

/**
 * Created by Megan on 12/1/2016.
 * creates the right course abstraction based on course structure
 */
public class CourseDefinitionFactory {
    /**
     * @param course the course's knitspeak syntax
     * @param rowDeclaration the declaration of the row location
     * @param textureDefinition the definition of the texture that this belongs too
     * @return the course definition of approriate type
     */
    @NotNull
    public static CourseDefinition buildCourseDefinition(@NotNull KnitSpeakLine course, @NotNull LineDeclaration rowDeclaration, @NotNull TextureDefinition textureDefinition) throws LineTypePreviouslyDeclaredException, OnlyCommonStitchesInOneStitchException, TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        assert course.getStitchRowProcess() != null;
        KnitSpeakLastStitchBlock lastStitchBlock = course.getStitchRowProcess().getLastStitchBlock();
        if(lastStitchBlock == null) {
            KnitSpeakBlockEnding blockEnding = course.getStitchRowProcess().getBlockEnding();
            if(blockEnding != null){
                if(blockEnding.getToEnd() != null)
                    return new CourseDefinitionWithSingleRep(course, rowDeclaration, textureDefinition);
                else if(blockEnding.getToLastSTS() != null)
                    return new CourseDefinitionWithSingleRepAndTail(course , rowDeclaration, textureDefinition);
                else if(blockEnding.getToLastST() != null)
                    return new CourseDefinitionWithSingleRepAndUnitTail(course, rowDeclaration, textureDefinition);
                assert false;
            }
            return new CourseDefinition(course, rowDeclaration, textureDefinition);
        }
        KnitSpeakBlockEnding blockEnding = lastStitchBlock.getBlockEnding();
        if(blockEnding == null || blockEnding.getToEnd() != null){
            return new CourseDefinitionWithReps(course, rowDeclaration, textureDefinition);
        } else if(blockEnding.getToLastSTS() != null ||
                (blockEnding.getToLastST() != null &&blockEnding.getToLastST().getStitchStatementList().getStitchStatementList().size()>1)){
            return new CourseDefinitionWithRepsAndTail(course, rowDeclaration, textureDefinition);
        }
        return new CourseDefinitionWithRepsAndUnitTail(course, rowDeclaration, textureDefinition);
    }
}
