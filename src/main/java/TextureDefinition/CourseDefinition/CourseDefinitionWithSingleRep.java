package TextureDefinition.CourseDefinition;

import CompilerExceptions.ParsingExceptions.*;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.KnitSpeakBlockEnding;
import KnitSpeakParser.psi.KnitSpeakLine;
import TextureDefinition.CourseDefinition.RowDeclarations.LineDeclaration;
import TextureDefinition.StitchDefinition.StitchStatementDefinition;
import TextureDefinition.TextureDefinition;
import TextureGraph.Courses.Course;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 1/13/2017.
 * abstraction for course with one stitch repeated
 */
public class CourseDefinitionWithSingleRep extends CourseDefinition {
    /**
     * the ending declaration knitspeak
     */
    KnitSpeakBlockEnding blockEnding;
    /**
     * the statement describing the repetiton stitch
     */
    StitchStatementDefinition repStatement;
    CourseDefinitionWithSingleRep(@NotNull KnitSpeakLine course, @NotNull LineDeclaration rowDeclaration, TextureDefinition textureDefinition) throws LineTypePreviouslyDeclaredException, OnlyCommonStitchesInOneStitchException, TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        super(course, rowDeclaration, textureDefinition);
        this.blockEnding = this.stitchRowProcess.getBlockEnding();
        assert blockEnding != null;
        this.repStatement = this.startingList.removeLast();//the last thing in this list is the stitch to be repeated

    }
    @Override
    public int getRepLoopCount() {return this.repStatement.getRequiredLoops();}
    @Override
    public int getBorderLoopCount() {return this.startingList.getRequiredLoops();}
    @Override
    public int getRequiredLoops(){return super.getRequiredLoops() + this.repStatement.getRequiredLoops();}
    @Override
    public int getMultipleOfRequiredLoops(int multiplier){
        int loops = 0;
        if(this.hasStartingList)
            loops += this.startingList.getRequiredLoops();
        loops += multiplier*this.repStatement.getRequiredLoops();
        return loops;
    }
    @Override
    public int matchingWidth(int width, boolean pad){
        int startingReq = 0;
        if(this.hasStartingList)
            startingReq = this.startingList.getRequiredLoops();
        int widthAfterStart = width-startingReq;
        int reqLoops = this.repStatement.getRequiredLoops();
        int leftover = widthAfterStart%reqLoops;
        if(leftover==0)
            return width;
        if(pad)
            return width-leftover;
        int sections = widthAfterStart/reqLoops;
        int min = sections*reqLoops;
        int max = sections*(reqLoops+1);
        int difMin = widthAfterStart-min;
        int difMax = max-widthAfterStart;
        if( difMin<= difMax)
            return min+startingReq;
        return max+startingReq;
    }
    @NotNull
    @Override
    public Course buildLine(@NotNull Course parentCourse, @NotNull Yarn yarn, int id, boolean allowLoopVariance) throws InsufficientLoopsException {
        boolean rightSide = this.idIsRS(id);
        LinkedList<Loop> availableLoops = CourseDefinition.availableParentLoops(parentCourse);
        LinkedList<StitchSet> stitchSets = new LinkedList<>();
        if( this.hasStartingList)
            stitchSets.addAll(this.startingList.buildStitchSetList(yarn, availableLoops, rightSide));
        //note: available loops should not have remaining loops to break up
        int requiredRep = this.repStatement.getRequiredLoops();
        if(availableLoops.size()%requiredRep !=0)
            throw new InvalidRepetitionLoopsException(this.course.getText());
        int requiredRepList = this.repStatement.getRequiredLoops();
        while(availableLoops.size()>=requiredRepList)
            stitchSets.addAll(this.repStatement.buildStitches(availableLoops, yarn, rightSide, -1));
        return new Course(stitchSets, id);
    }
}
