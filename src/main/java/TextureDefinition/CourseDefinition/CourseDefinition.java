package TextureDefinition.CourseDefinition;

import CompilerExceptions.ParsingExceptions.*;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.*;
import TextureDefinition.CourseDefinition.RowDeclarations.LineDeclaration;
import TextureDefinition.CourseDefinition.RowDeclarations.SideDeclaration;
import TextureDefinition.NumConverter;
import TextureDefinition.StitchDefinition.StitchListDefinition;
import TextureDefinition.TextureDefinition;
import TextureGraph.Courses.Course;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by Megan on 12/1/2016.
 * abstraction for a set of instructions in a course
 */
public class CourseDefinition {
    /**
     * the course's knitspeak syntax
     */
    protected KnitSpeakLine course;
    /**
     * the declaration of the row location
     */
    protected LineDeclaration rowDeclaration;
    /**
     * the definition of the texture that this belongs too
     */
    private TextureDefinition textureDefinition;
    /**
     * flags if there is  turn midway through the course
     */
    private boolean turn;
    /**
     * the process definition of the course
     */
    KnitSpeakStitchRowProcess stitchRowProcess;
    /**
     * the list of stitches that come before the repetition
     */
    StitchListDefinition startingList;
    /**
     * flags if stitchs come before the repetition
     */
    boolean hasStartingList;
    /**
     * flags if there is a counter stating when to switch to ending border
     */
    private boolean hasStitchCounter;

    /**
     * @return the loops needed to create a repetition
     */
    @Contract(pure = true)
    public int getRepLoopCount() {return this.startingList.getRequiredLoops();}

    /**
     * @return the loops needed to create the border
     */
    @Contract(pure = true)
    public int getBorderLoopCount() {return 0;}

    /**
     * he loops needed to create a repetition
     */
    private int repReq;
    /**
     * the loops needed to create the border
     */
    private int plusReq;

    /**
     * @param course the course's knitspeak syntax
     * @param rowDeclaration the declaration of the row location
     * @param textureDefinition the definition of the texture that this belongs too
     * @throws LineTypePreviouslyDeclaredException if the
     */
    CourseDefinition(@NotNull KnitSpeakLine course, @NotNull LineDeclaration rowDeclaration, TextureDefinition textureDefinition) throws LineTypePreviouslyDeclaredException, OnlyCommonStitchesInOneStitchException, TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        this.course = course;
        this.textureDefinition = textureDefinition;
        KnitSpeakSideDec sideDec = this.course.getSideDec();
        this.rowDeclaration = rowDeclaration;
        this.turn = course.getTurnAndWrap()!=null;
        if(sideDec !=null) {//Test the declaration against condition in textureGraph
            if(this.rowDeclaration.sideDeclaration == null)
                this.rowDeclaration.sideDeclaration = SideDeclaration.RSOdd;
            boolean ws = !this.rowDeclaration.sideDeclaration.isRS();
            boolean even = !this.rowDeclaration.sideDeclaration.isOdd();
            if(this.textureDefinition.wsDefault) {
                this.textureDefinition.wsDefault = false;
                this.textureDefinition.wsISEven = (ws && even) || (!ws && !even);
            } else{//need to check for consistensy
                boolean check = (ws && even) || (!ws && !even);
                if(check != this.textureDefinition.wsISEven)
                    throw new LineTypePreviouslyDeclaredException(this.rowDeclaration.toString());
            }
        }
        this.stitchRowProcess = course.getStitchRowProcess();
        assert this.stitchRowProcess != null;
        KnitSpeakStitchStatementList startingListElement = this.stitchRowProcess.getStitchStatementList();
        this.hasStartingList = ( startingListElement != null );
        if( this.hasStartingList)
            this.startingList = new StitchListDefinition(startingListElement);
        KnitSpeakStitchCount stitchCountElement = this.course.getStitchCount();
        this.hasStitchCounter = stitchCountElement != null;
        if(this.hasStitchCounter){
            boolean repCounter = stitchCountElement.getMultiplesDeclaration() != null;
            if(repCounter){
                this.repReq = NumConverter.convertNum(stitchCountElement.getMultiplesDeclaration().getStitchesCountList().get(0));
                this.plusReq = NumConverter.convertNum(stitchCountElement.getMultiplesDeclaration().getStitchesCountList().get(1));
            }
            else {
                this.repReq = NumConverter.convertNum(stitchCountElement.getStitchesCount());
                this.plusReq = 0;
            }
        }

    }

    /**
     * @return the number of loops required to make this course
     */
    @Contract(pure = true)
    public int getRequiredLoops(){
        if( !this.hasStartingList)
            return 0;
        return this.startingList.getRequiredLoops();
    }

    /**
     * @param multiplier the number of times to repeat
     * @return the number of loops required to do the repetition
     */
    @Contract(pure = true)
    public int getMultipleOfRequiredLoops(int multiplier){
        if(this.hasStitchCounter){
            return multiplier*this.repReq + this.plusReq;
        }
        return this.getRequiredLoops()*multiplier;
    }

    /**
     * @param width the desired number of stitches
     * @return the closets number of stitches reached by this course abstraction
     */
    @Contract(pure = true)
    public int matchingWidth(int width, boolean pad){
        int reqLoops = this.getRequiredLoops();
        int leftover = width%reqLoops;
        if(leftover==0)
            return width;
        if(pad)
            return width-leftover;
        int sections = width/reqLoops;
        int min = sections*reqLoops;
        int max = sections*(reqLoops+1);
        int difMin = width-min;
        int difMax = max-width;
        if( difMin<= difMax)
            return min;
        return max;
    }

    /**
     * @param parentCourse the course to build up from
     * @param yarn the yarn to build the course from
     * @param id the course number of this new course
     * @param allowLoopVariance true if variance in the textures loop count is legal
     * @return the course that is created from this abstraction
     * @throws InsufficientLoopsException if parentCourse does not have enough loops to parent this course
     */
    @NotNull
    @Contract(pure = true)
    public Course buildLine(@NotNull Course parentCourse, @NotNull Yarn yarn, int id, boolean allowLoopVariance) throws InsufficientLoopsException {
        boolean rightSide = this.idIsRS(id);
        LinkedList<Loop> availableLoops = CourseDefinition.availableParentLoops(parentCourse);
        assert this.hasStartingList;
        int reqLoops = this.getRequiredLoops();
        LinkedList<StitchSet> stitchSets = new LinkedList<>();
        if(!allowLoopVariance && availableLoops.size()<reqLoops)
            throw new InvalidRepetitionLoopsException(""+id);
        stitchSets.addAll(this.startingList.buildStitchSetList(yarn, availableLoops, rightSide));
        if(!this.turn)//If there is a turn, ignore remaininglooops
            while( availableLoops.size()>=reqLoops)
                stitchSets.addAll(this.startingList.buildStitchSetList(yarn, availableLoops, rightSide));
        return new Course(stitchSets, id);
    }
    
    static LinkedList<Loop> availableParentLoops( Course parentCourse){
        LinkedList<Loop> availableLoops = new LinkedList<>();
        Iterator<Loop> reverseIterator = parentCourse.getChildLoops().descendingIterator();
        while (reverseIterator.hasNext())
            availableLoops.add(reverseIterator.next());
        return availableLoops;
    }
    @Contract(pure = true)
    boolean idIsRS(int id){
        if(this.textureDefinition.wsISEven)
            return id%2==1;
        else
            return id%2==0;
    }

    @Override
    public int hashCode(){return this.rowDeclaration.hashCode();}
    @Override
    public String toString() {return this.course.getText();}
}
