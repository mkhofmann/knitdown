package TextureDefinition.StitchDefinition;

import CompilerExceptions.ParsingExceptions.InsufficientLoopsException;
import KnitSpeakParser.psi.KnitSpeakStitch;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 12/1/2016.
 * a stitch element declared in the knitspeak language
 */
public abstract class StitchDefinition {
    /**
     * the psiElement that declared the stitch
     */
    public KnitSpeakStitch stitchElement;
    /**
     * the number of loops required to construct a stitch with this definiton
     */
    protected int requiredLoops;

    /**
     * @return the number of loops required to construct a stitch with this definiton
     */
    @Contract(pure = true)
    public int getRequiredLoops() {return requiredLoops;}

    /**
     * @return the number of loops created by this stitch definition
     */
    public int getResultingLoops() {return resultingLoops;}

    /**
     * the number of loops created by this stitch definition
     */
    protected int resultingLoops;

    /**
     * @param stitchElement the element that sets what type of definition is being created
     */
    public StitchDefinition(@NotNull KnitSpeakStitch stitchElement) {
        this.stitchElement = stitchElement;
    }

    /**
     * @param yarn the yarn to create the stitch set from
     * @param availableLoops the loops available to parent the new stitch set
     * @param rightSide if this is created on right or wrong side
     * @param declaredRep the repetition this belongs to
     * @return the stitch set declared by this abstract stitch.
     */
    @NotNull
    @Contract(pure = true)
    public abstract StitchSet buildStitch(@NotNull Yarn yarn, @NotNull LinkedList<Loop> availableLoops, boolean rightSide, int declaredRep) throws InsufficientLoopsException;

    /**
     * @param availableLoops the loops that are available to be consumed to create the stitch
     *                       This is descuctive to this parameter, removing the loops from the original list
     * @return the loops that will be used as the parents of the stitch
     * @throws InsufficientLoopsException if there are not enough loops in availableLoops to create the stitch
     */
    @NotNull
    @Contract(pure = true)
    protected LinkedList<Loop> getNextRequiredLoops(@NotNull LinkedList<Loop> availableLoops) throws InsufficientLoopsException {
        if(availableLoops.size() < this.requiredLoops)
            throw new InsufficientLoopsException(this.stitchElement.getText());
        if(availableLoops.size() == this.requiredLoops) {
            LinkedList<Loop> reversedLoops = new LinkedList<>();
            while(!availableLoops.isEmpty())
                reversedLoops.add(availableLoops.pop());
            return reversedLoops;
        }
        LinkedList<Loop> parentLoops = new LinkedList<>();
        while(parentLoops.size() != this.requiredLoops)
            parentLoops.add(availableLoops.remove());
        return parentLoops;
    }
}
