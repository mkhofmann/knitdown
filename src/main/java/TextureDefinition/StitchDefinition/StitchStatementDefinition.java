package TextureDefinition.StitchDefinition;

import CompilerExceptions.ParsingExceptions.InsufficientLoopsException;
import KnitSpeakParser.psi.KnitSpeakStitchStatement;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 12/20/2016.
 * abstraction for stitch statement types
 */
public abstract class StitchStatementDefinition {
    /**
     * the element that sets the type of Stitch Statement to be made
     */
    KnitSpeakStitchStatement stitchStatmentElement;

    /**
     * @param stitchStatementElement the element that defines the stitch statement type
     */
    public StitchStatementDefinition(@NotNull KnitSpeakStitchStatement stitchStatementElement) {this.stitchStatmentElement = stitchStatementElement;}

    /**
     * @param availableLoops the loops that can parent the created stitch sets
     * @param yarn the yarn to form the new stitches
     * @param rightSide if this is being built on right or wrong side
     * @param declaredRep the repetition this belongs too.
     * @return an ordered list of stitch sets created from the statment
     */
    @NotNull
    @Contract(pure = true)
    public abstract LinkedList<StitchSet> buildStitches(@NotNull LinkedList<Loop> availableLoops, @NotNull Yarn yarn, boolean rightSide, int declaredRep) throws InsufficientLoopsException;

    /**
     * @return the number of loops needed to parent the abstract stitches
     */
    @Contract(pure = true)
    public abstract int getRequiredLoops();

    @Override
    public String toString(){ return this.stitchStatmentElement.getText();}
}
