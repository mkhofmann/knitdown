package TextureDefinition.StitchDefinition;

import CompilerExceptions.ParsingExceptions.InsufficientLoopsException;
import KnitSpeakParser.psi.KnitSpeakSlipStitchStatement;
import KnitSpeakParser.psi.KnitSpeakStitchStatement;
import TextureDefinition.NumConverter;
import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.StitchInRow.SlipStitch;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 12/20/2016.
 * an abstraction for slip statements
 */
public class SlipStitchDefinitionDefinition extends StitchStatementDefinition {
    /**
     * the repetiton count of the slipping action
     */
    private int numSlips;

    SlipStitchDefinitionDefinition(@NotNull KnitSpeakStitchStatement stitchStatementElement) {
        super(stitchStatementElement);
        KnitSpeakSlipStitchStatement slipStitchStatement = this.stitchStatmentElement.getSlipStitchStatement();
        assert slipStitchStatement != null;
        this.numSlips = NumConverter.convertNum(slipStitchStatement.getNumPossibleSuffixed());
        if(this.numSlips == 0) {
            this.numSlips = NumConverter.convertNum(slipStitchStatement.getStitchesCount());
        }
    }

    /**
     * @throws InsufficientLoopsException if the available loops is less then the set slip count
     */
    @NotNull
    @Override
    public LinkedList<StitchSet> buildStitches(@NotNull LinkedList<Loop> availableLoops, @NotNull Yarn yarn, boolean rightSide, int declaredRep) throws InsufficientLoopsException {
        if(availableLoops.size()<this.numSlips)
            throw new InsufficientLoopsException(this.stitchStatmentElement.getText());
        LinkedList<StitchSet> stitchSets = new LinkedList<>();
        while(stitchSets.size()<this.numSlips)
            stitchSets.add(new Single(new SlipStitch(availableLoops.remove())));
        return stitchSets;
    }

    @Override
    public int getRequiredLoops() {return numSlips;}
}
