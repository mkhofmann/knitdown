package TextureDefinition.StitchDefinition.ParameterizedStitches;

import CompilerExceptions.ParsingExceptions.InsufficientLoopsException;
import KnitSpeakParser.psi.KnitSpeakStitch;
import KnitSpeakParser.psi.KnitSpeakStitchBelow;
import TextureDefinition.NumConverter;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Knit.K;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.Purl.P;
import TextureGraph.Courses.Stitches.Stitch;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 12/1/2016.
 * abstraction for stitches that go into a row below the current row
 */
public class StitchBelowDefinition extends ParameterizedStitchDefinition {
    public StitchBelowDefinition(@NotNull KnitSpeakStitch stitchElement) {
        super(stitchElement);
        KnitSpeakStitchBelow stitchBelowElement = this.parameterizedStitchElement.getStitchBelow();
        assert stitchBelowElement != null;
        this.parameterNum = NumConverter.convertNum(stitchBelowElement.getStitchesCount());
        this.k = stitchBelowElement.getKOrP().getText().equals("k");
        this.requiredLoops=0;
        this.resultingLoops = this.parameterNum;
    }

    /**
     * @throws InsufficientLoopsException if the available loops is less than the loops needed to create the stitch below
     */
    @NotNull
    @Override
    public StitchSet buildStitch(@NotNull Yarn yarn, @NotNull LinkedList<Loop> availableLoops, boolean rightSide, int declaredRep) throws InsufficientLoopsException {
        LinkedList<Stitch> stitches = new LinkedList<>();
        if(availableLoops.size()< this.parameterNum)
            throw new InsufficientLoopsException(this.stitchElement.getText());
        if((rightSide && this.k) || (!this.k && !rightSide)) //k on front or p but on back
            for (int i = 0; i < this.parameterNum; i++)
                stitches.add(new K(availableLoops.remove(), yarn));
        else
            for (int i = 0; i < this.parameterNum; i++)
                stitches.add(new P(availableLoops.remove(), yarn));
        return new StitchSet(stitches);
    }
}
