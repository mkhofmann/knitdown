package TextureDefinition.StitchDefinition.ParameterizedStitches;

import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.KnitSpeakStitch;
import KnitSpeakParser.psi.KnitSpeakStitchMapsIncrease;
import TextureDefinition.NumConverter;
import TextureGraph.Courses.StitchSets.Increases.*;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 1/5/2017.
 * abstracton for a stitch maps increase
 */
public class IncreaseSetDefinition extends ParameterizedStitchDefinition {
    public IncreaseSetDefinition(@NotNull KnitSpeakStitch stitchElement) throws TooWideIncreaseException, TooSmallIncreaseException {
        super(stitchElement);
        KnitSpeakStitchMapsIncrease increaseElement = this.parameterizedStitchElement.getStitchMapsIncrease();
        assert increaseElement != null;
        this.resultingLoops = NumConverter.convertNum(increaseElement.getStitchesCount());
        if(this.resultingLoops<4)
            throw new TooSmallIncreaseException(this.stitchElement);
        if(this.resultingLoops>TooWideIncreaseException.MAX_INCREASE_WIDTH)
            throw new TooWideIncreaseException(stitchElement);
        this.requiredLoops = 1;
    }

    @NotNull
    @Override
    public StitchSet buildStitch(@NotNull Yarn yarn, @NotNull LinkedList<Loop> availableLoops, boolean rightSide, int declaredRep) {
        switch (this.resultingLoops){
            case 4:
                return new Inc4(availableLoops.remove(), yarn);
            case 5:
                return new Inc5(availableLoops.remove(), yarn);
            case  6:
                return new Inc6(availableLoops.remove(), yarn);
            case 7:
                return new Inc7(availableLoops.remove(), yarn);
            case 8:
                return new Inc8(availableLoops.remove(), yarn);
            case 9:
                return new Inc9(availableLoops.remove(), yarn);
            default:
                assert false;
        }
        return null;
    }
}
