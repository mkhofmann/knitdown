package TextureDefinition.StitchDefinition.ParameterizedStitches;

import CompilerExceptions.ParsingExceptions.InsufficientLoopsException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import KnitSpeakParser.psi.KnitSpeakCableParameterized;
import KnitSpeakParser.psi.KnitSpeakStitch;
import TextureDefinition.NumConverter;
import TextureGraph.Courses.StitchSets.Cables.CableStitchMaps;
import TextureGraph.Courses.StitchSets.Cables.CrossSide;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 1/5/2017.
 * A parameterized cable from stitch maps
 */
public class ParameterizedCableDefinition extends ParameterizedStitchDefinition {
    /**
     * the number of stitches that will be heald
     */
    private int heldCount;
    /**
     * the number of stitches that will not be heald
     */
    private int unHeldCount;
    /**
     * the side the unheld stitches will be crossed over
     */
    private CrossSide crossSide;
    /**
     * flags if the background stitches will be purl
     */
    private boolean purl;

    public ParameterizedCableDefinition(@NotNull KnitSpeakStitch stitchElement) throws TooWideCableException, TooSmallCableException {
        super(stitchElement);
        KnitSpeakCableParameterized cableParameterizedElement = this.parameterizedStitchElement.getCableParameterized();
        assert cableParameterizedElement != null;
        int[] cableNums = NumConverter.convertNums(cableParameterizedElement.getStitchesCountList());
        assert cableNums.length==2;
        this.heldCount = cableNums[0];
        this.unHeldCount = cableNums[1];

        this.requiredLoops = this.heldCount + this.unHeldCount;
        if( this.requiredLoops < 3)
            throw new TooSmallCableException(stitchElement);
        if( this.requiredLoops > TooWideCableException.MAX_CABLE_WIDTH)
            throw new TooWideCableException(stitchElement);
        this.resultingLoops = this.requiredLoops;
        String cableName = cableParameterizedElement.getCableName().getText().toLowerCase();
        char sideChar = cableName.charAt(0);
        switch (sideChar){
            case 'r':
                this.crossSide = CrossSide.BACK;
                break;
            case 'l':
                this.crossSide = CrossSide.FRONT;
                break;
            default:
                assert false;
        }
        this.purl = cableName.contains("p");
    }

    @NotNull
    @Override
    public StitchSet buildStitch(@NotNull Yarn yarn, @NotNull LinkedList<Loop> availableLoops, boolean rightSide, int declaredRep) throws InsufficientLoopsException {
        return  new CableStitchMaps(this.crossSide,this.getNextRequiredLoops(availableLoops),this.heldCount, this.unHeldCount,this.purl,yarn,declaredRep);
    }
}
