package TextureDefinition.StitchDefinition.ParameterizedStitches;

import KnitSpeakParser.psi.KnitSpeakParameterizedStitch;
import KnitSpeakParser.psi.KnitSpeakStitch;
import TextureDefinition.StitchDefinition.StitchDefinition;
import org.jetbrains.annotations.NotNull;


/**
 * Created by Megan on 12/1/2016.
 * abstraction for stitches that can be parameterized like cables
 */
public abstract class ParameterizedStitchDefinition extends StitchDefinition {
    /**
     * the element defining the parameterized stitch type
     */
    KnitSpeakParameterizedStitch parameterizedStitchElement;
    /**
     * the number that is set as the stitches parameter
     */
    int parameterNum;
    /**
     * distinguishes if the parameterized stitch is a knit type
     */
    protected boolean k;
    ParameterizedStitchDefinition(@NotNull KnitSpeakStitch stitchElement) {
        super(stitchElement);
        this.parameterizedStitchElement = this.stitchElement.getParameterizedStitch();
        assert this.parameterizedStitchElement != null;
    }
}
