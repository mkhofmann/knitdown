package TextureDefinition.StitchDefinition.ParameterizedStitches;

import CompilerExceptions.ParsingExceptions.InsufficientLoopsException;
import KnitSpeakParser.psi.KnitSpeakStitch;
import KnitSpeakParser.psi.KnitSpeakStitchTog;
import TextureDefinition.NumConverter;
import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Knit.KXtog;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.Purl.Pxtog;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 12/1/2016.
 * abstraction for stitch together decrease
 */
public class XTogStitchDefinition extends ParameterizedStitchDefinition {
    public XTogStitchDefinition(@NotNull KnitSpeakStitch stitchElement) {
        super(stitchElement);
        KnitSpeakStitchTog stitchTogElement = this.parameterizedStitchElement.getStitchTog();
        assert stitchTogElement != null;
        this.parameterNum = NumConverter.convertNum(stitchTogElement.getStitchesCount());
        this.k = stitchTogElement.getKOrP().getText().equals("k");
        this.requiredLoops=this.parameterNum;
        this.resultingLoops =1;
    }

    @NotNull
    @Override
    public StitchSet buildStitch(@NotNull Yarn yarn, @NotNull LinkedList<Loop> availableLoops, boolean rightSide, int declaredRep) throws InsufficientLoopsException {
        if(this.k || (rightSide))//k on front or p but on back
            return new Single(new KXtog(yarn, this.getNextRequiredLoops(availableLoops)));
        return new Single(new Pxtog(yarn, this.getNextRequiredLoops(availableLoops)));
    }
}
