package TextureDefinition.StitchDefinition;

import CompilerExceptions.ParsingExceptions.InsufficientLoopsException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.KnitSpeakCommonStitchStatement;
import KnitSpeakParser.psi.KnitSpeakNumStitch;
import KnitSpeakParser.psi.KnitSpeakStitch;
import KnitSpeakParser.psi.KnitSpeakStitchStatement;
import TextureDefinition.NumConverter;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 12/1/2016.
 * abstraction for common key word stitches
 */
public class CommonStitchDefinitionDefinition extends StitchStatementDefinition {
    /**
     * the number of repetitions of the stitch type
     */
    private int numReps;
    /**
     * the element that defines the specific stitch construction
     */
    protected KnitSpeakStitch stitchElement;
    /**
     * the definition of the constructed stitches
     */
    public StitchDefinition stitch;

    CommonStitchDefinitionDefinition(@NotNull KnitSpeakStitchStatement stitchStatementElement) throws TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        super(stitchStatementElement);
        KnitSpeakCommonStitchStatement commonStitchStatementElement = this.stitchStatmentElement.getCommonStitchStatement();
        assert commonStitchStatementElement != null;
        KnitSpeakNumStitch numStitchElement = commonStitchStatementElement.getNumStitch();
        this.numReps = NumConverter.convertNum(numStitchElement.getNumPossibleSuffixed());
        if(this.numReps ==0)
            this.numReps =1;
        this.stitchElement = numStitchElement.getStitch();
        this.stitch = StitchDefinitionFactory.buildStitchDefinition(this.stitchElement);
    }

    @NotNull
    @Contract(pure = true)
    @Override
    public LinkedList<StitchSet> buildStitches(@NotNull LinkedList<Loop> availableLoops, @NotNull Yarn yarn, boolean rightSide, int declaredRep) throws InsufficientLoopsException {
        LinkedList<StitchSet> stitchSets = new LinkedList<>();
        for(int i=0; i < this.numReps; i++)
            stitchSets.add(this.stitch.buildStitch(yarn, availableLoops, rightSide, declaredRep));
        return stitchSets;
    }
    @Contract(pure = true)
    @Override
    public int getRequiredLoops(){return this.numReps* this.stitch.getRequiredLoops();}

}
