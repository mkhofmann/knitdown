package TextureDefinition.StitchDefinition;

import CompilerExceptions.ParsingExceptions.*;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.KnitSpeakStitchStatementList;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * A Stitch list that represents the tile repeat in the course
 */
public class RepStitchListDefinition extends StitchListDefinition {
    /**
     * the number of loops in the repeat section
     */
    private int repCount = 0;
    public RepStitchListDefinition(@NotNull KnitSpeakStitchStatementList listElement) throws OnlyCommonStitchesInOneStitchException, TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        super(listElement);
        this.isRepSection = true;
    }

    @Override
    @NotNull
    public LinkedList<StitchSet> buildStitchSetList(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents, boolean rightSide) throws InsufficientLoopsException {
        this.repCount++;
        LinkedList<StitchSet> stitchSets = new LinkedList<>();
        for(StitchStatementDefinition stitchStatement: this.stitchStatements)
            stitchSets.addAll(stitchStatement.buildStitches(parents, yarn, rightSide, this.repCount));
        return stitchSets;
    }
}
