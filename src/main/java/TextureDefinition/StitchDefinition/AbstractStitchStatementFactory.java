package TextureDefinition.StitchDefinition;

import CompilerExceptions.ParsingExceptions.OnlyCommonStitchesInOneStitchException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.KnitSpeakStitchStatement;
import org.jetbrains.annotations.NotNull;

/**
 * Created by Megan on 12/20/2016.
 * creates the specific abstraction needed for each stitch statement
 */
class AbstractStitchStatementFactory {
    /**
     *
     * @param stitchStatement the stitch statement that determines type
     * @return the definition based on the stitch statement type
     */
    @NotNull
    static StitchStatementDefinition buildAbstractStitchStatement(@NotNull KnitSpeakStitchStatement stitchStatement) throws OnlyCommonStitchesInOneStitchException, TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        if(stitchStatement.getCommonStitchStatement() != null)
            return new CommonStitchDefinitionDefinition(stitchStatement);
        else if (stitchStatement.getSlipStitchStatement() !=null)
            return new SlipStitchDefinitionDefinition(stitchStatement);
        else if (stitchStatement.getAllInXStitchStatement() !=null)
            return new AllIn1StitchDefinitionDefinition(stitchStatement);
        else if (stitchStatement.getRepStatement() != null)
            return new RepStitchStatementDefinition(stitchStatement);
        assert false;
        return null;
    }
}
