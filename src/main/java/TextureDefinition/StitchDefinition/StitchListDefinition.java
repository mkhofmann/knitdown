package TextureDefinition.StitchDefinition;

import CompilerExceptions.ParsingExceptions.*;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.KnitSpeakStitchStatement;
import KnitSpeakParser.psi.KnitSpeakStitchStatementList;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Megan on 12/1/2016.
 * abstraction for a list of stitch abstractions
 */
public class StitchListDefinition {
    /**
     * flags if this is a repeat, defining a whole tile
     */
    boolean isRepSection = false;
    /**
     * list of the statements that will be constructed in this list
     */
    LinkedList<StitchStatementDefinition> stitchStatements;

    /**
     * @param listElement the element that can be broken down into the relevant definitions
     */
    public StitchListDefinition(@NotNull KnitSpeakStitchStatementList listElement) throws OnlyCommonStitchesInOneStitchException, TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        List<KnitSpeakStitchStatement> stitchStatementElements = listElement.getStitchStatementList();
        this.stitchStatements = new LinkedList<>();
        for(KnitSpeakStitchStatement sse: stitchStatementElements)
            this.stitchStatements.add(AbstractStitchStatementFactory.buildAbstractStitchStatement(sse));
    }

    /**
     * @return the number of abstractions in the list
     */
    @Contract(pure = true)
    public int size(){return this.stitchStatements.size();}

    /**
     * @param index the index of abstraction desired
     * @return the abstractation at index
     */
    @Contract(pure = true)
    @Nonnull
    public StitchStatementDefinition get(int index){return this.stitchStatements.get(index);}

    /**
     * @param index the index of the abstratction to remove
     * @return the abstraction removed
     */
    @Nonnull
    public StitchStatementDefinition remove(int index){return this.stitchStatements.remove(index);}

    /**
     * @return the removed last abstraction in the list
     */
    @Nonnull
    public StitchStatementDefinition removeLast(){return this.stitchStatements.removeLast();}

    /**
     * @param yarn the yarn to create new sets from
     * @param parents the available loops to parent this stitchset
     * @param rightSide if this is to be made on the right or wrong side
     * @return an ordered set of stitch sets created from this list of abstractions
     */
    @NotNull
    @Contract(pure = true)
    public LinkedList<StitchSet> buildStitchSetList(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents, boolean rightSide) throws InsufficientLoopsException {
        LinkedList<StitchSet> stitchSets = new LinkedList<>();
        for(StitchStatementDefinition stitchStatement: this.stitchStatements)
            stitchSets.addAll(stitchStatement.buildStitches(parents, yarn, rightSide, -1));
        return stitchSets;
    }

    /**
     * @return the number of parent loops needed to make this set.
     */
    @Contract(pure = true)
    public int getRequiredLoops(){
        int requiredLoops = 0;
        for(StitchStatementDefinition ss: this.stitchStatements)
            requiredLoops += ss.getRequiredLoops();
        return requiredLoops;
    }
}
