package TextureDefinition.StitchDefinition;

import CompilerExceptions.ParsingExceptions.*;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.KnitSpeakRepStatement;
import KnitSpeakParser.psi.KnitSpeakStitchStatement;
import KnitSpeakParser.psi.KnitSpeakStitchStatementList;
import TextureDefinition.NumConverter;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 12/20/2016.
 * abstraction for stitch statement that supports repetition
 */
public class RepStitchStatementDefinition extends StitchStatementDefinition {
    /**
     * the number of repetitions of the stitch grouping
     */
    private int numReps;
    private StitchListDefinition stitchList;
    RepStitchStatementDefinition(@NotNull KnitSpeakStitchStatement stitchStatementElement) throws OnlyCommonStitchesInOneStitchException, TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        super(stitchStatementElement);
        KnitSpeakRepStatement repStatementElement = this.stitchStatmentElement.getRepStatement();
        assert repStatementElement != null;
        this.numReps = NumConverter.convertNum(repStatementElement.getRepTimes());
        KnitSpeakStitchStatementList stitchStatementListElement = repStatementElement.getStitchStatementList();
        this.stitchList = new StitchListDefinition(stitchStatementListElement);
    }

    @NotNull
    @Override
    public LinkedList<StitchSet> buildStitches(@NotNull LinkedList<Loop> availableLoops, @NotNull Yarn yarn, boolean rightSide, int declaredRep) throws InsufficientLoopsException {
        LinkedList<StitchSet> stitchSets = new LinkedList<>();
        for(int i=0; i<this.numReps; i++)
            stitchSets.addAll(this.stitchList.buildStitchSetList(yarn,availableLoops, rightSide));
        return stitchSets;
    }

    @Override
    public int getRequiredLoops() {return this.stitchList.getRequiredLoops()*this.numReps;}
}
