package TextureDefinition.StitchDefinition.Primitives;

import KnitSpeakParser.psi.KnitSpeakPrimitiveStitch;
import KnitSpeakParser.psi.KnitSpeakStitch;
import TextureDefinition.StitchDefinition.StitchDefinition;
import org.jetbrains.annotations.NotNull;

/**
 * Created by Megan on 12/1/2016.
 * A basic stitch type declared in the knitspeak language
 */
abstract class PrimitiveStitchDefinition extends StitchDefinition {
    /**
     * the element that sets the definition type
     */
    KnitSpeakPrimitiveStitch primitiveStitchElement;

    /**
     * @param stitchElement the element that sets the definition type
     */
    PrimitiveStitchDefinition(@NotNull KnitSpeakStitch stitchElement) {
        super(stitchElement);
        this.primitiveStitchElement = this.stitchElement.getPrimitiveStitch();
        assert this.primitiveStitchElement != null;
    }
}
