package TextureDefinition.StitchDefinition.Primitives;

import CompilerExceptions.ParsingExceptions.InsufficientLoopsException;
import KnitSpeakParser.psi.KnitSpeakPPrimitive;
import KnitSpeakParser.psi.KnitSpeakStitch;
import TextureGraph.Courses.StitchSets.Increases.KFB;
import TextureGraph.Courses.StitchSets.Increases.PFB;
import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Knit.K;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.Purl.P;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 12/1/2016.
 * A primitive definition of a simple Purl stitch or PFB.
 * PFB definition is based on nuance of the grammar, including FB as a modifier to a stitch not a seperate token
 */
public class PPrimitiveDefinition extends PrimitiveStitchDefinition {
    /**
     * flags if the tfl modifier followed the stitch element
     */
    public boolean pfb;

    public PPrimitiveDefinition(@NotNull KnitSpeakStitch stitchElement)  {
        super(stitchElement);
        KnitSpeakPPrimitive pPrimitiveElement = this.primitiveStitchElement.getPPrimitive();
        assert pPrimitiveElement != null;
        this.pfb = pPrimitiveElement.getText().equals("pfb");
        this.requiredLoops = 1;
        this.resultingLoops = 1;
    }

    @NotNull
    @Override
    public StitchSet buildStitch(@NotNull Yarn yarn, @NotNull LinkedList<Loop> availableLoops, boolean rightSide, int declaredRep) throws InsufficientLoopsException {
        if(rightSide) {
            if (this.pfb)
                return new PFB(this.getNextRequiredLoops(availableLoops).remove(), yarn);
            return new Single(new P(this.getNextRequiredLoops(availableLoops).remove(), yarn));
        }else{
            if(this.pfb)
                return new KFB(this.getNextRequiredLoops(availableLoops).remove(), yarn);
            return new Single(new K(this.getNextRequiredLoops(availableLoops).remove(), yarn));
        }
    }
}
