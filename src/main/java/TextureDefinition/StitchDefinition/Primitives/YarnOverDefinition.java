package TextureDefinition.StitchDefinition.Primitives;

import KnitSpeakParser.psi.KnitSpeakStitch;
import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.YO;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 12/1/2016.
 * abstraction for yarn over increase
 */
public class YarnOverDefinition extends PrimitiveStitchDefinition {
    public YarnOverDefinition(@NotNull KnitSpeakStitch stitchElement) {
        super(stitchElement);
        this.requiredLoops =0;
        this.resultingLoops = 0;
    }

    @NotNull
    @Override
    public StitchSet buildStitch(@NotNull Yarn yarn, @NotNull LinkedList<Loop> availableLoops, boolean rightSide, int declaredRep){
        return new Single(new YO(yarn));
    }
}
