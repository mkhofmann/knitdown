package TextureDefinition.StitchDefinition.Primitives;

import CompilerExceptions.ParsingExceptions.InsufficientLoopsException;
import KnitSpeakParser.psi.KnitSpeakCablePrimitive;
import KnitSpeakParser.psi.KnitSpeakStitch;
import TextureGraph.Courses.StitchSets.Cables.Cable;
import TextureGraph.Courses.StitchSets.Cables.CrossSide;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 12/1/2016.
 * an abstraction that represents a stitch essentails cable
 */
public class CableStitchDefinition extends PrimitiveStitchDefinition {
    /**
     * flags if the constructed cable has purls
     */
    protected boolean purl;
    /**
     * defines the crossSide of the resulting cable
     */
    private CrossSide crossSide;
    /**
     * count of the stitches in the resulting cable
     */
    private int stitches;
    public CableStitchDefinition(@NotNull KnitSpeakStitch stitchElement) {
        super(stitchElement);
        KnitSpeakCablePrimitive cablePrimitiveElement = this.primitiveStitchElement.getCablePrimitive();
        assert cablePrimitiveElement != null;
        String cableText = cablePrimitiveElement.getText();
        this.purl = (cableText.length()==4);
        if(cableText.charAt(2)=='f')
            this.crossSide = CrossSide.FRONT;
        else
            this.crossSide = CrossSide.BACK;
        this.stitches = Integer.parseInt(cableText.substring(1,2));
        this.requiredLoops = this.stitches;
        this.resultingLoops = this.stitches;
    }

    @NotNull
    @Override
    public StitchSet buildStitch(@NotNull Yarn yarn, @NotNull LinkedList<Loop> availableLoops, boolean rightSide, int declaredRep) throws InsufficientLoopsException {
        return new Cable(this.crossSide, this.getNextRequiredLoops(availableLoops), this.stitches, this.purl, yarn);
    }
}
