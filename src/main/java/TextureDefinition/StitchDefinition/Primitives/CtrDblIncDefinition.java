package TextureDefinition.StitchDefinition.Primitives;

import KnitSpeakParser.psi.KnitSpeakStitch;
import TextureGraph.Courses.StitchSets.Increases.Ctrdblinc;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 1/5/2017.
 * the abstraction that represents a centered double increase from stitch maps
 */
public class CtrDblIncDefinition extends PrimitiveStitchDefinition {
    public CtrDblIncDefinition(@NotNull KnitSpeakStitch stitchElement) {
        super(stitchElement);
        assert this.primitiveStitchElement.getText().equals("ctr dbl inc");
        this.requiredLoops = 1;
        this.resultingLoops = 3;

    }

    @NotNull
    @Override
    public StitchSet buildStitch(@NotNull Yarn yarn, @NotNull LinkedList<Loop> availableLoops, boolean rightSide, int declaredRep){
        return new Ctrdblinc(availableLoops.remove(), yarn);
    }
}
