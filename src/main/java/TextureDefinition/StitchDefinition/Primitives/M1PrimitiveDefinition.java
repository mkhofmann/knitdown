package TextureDefinition.StitchDefinition.Primitives;

import KnitSpeakParser.psi.KnitSpeakStitch;
import org.jetbrains.annotations.NotNull;

/**
 * Created by Megan on 12/1/2016.
 * an abstraction that represents make 1 increases as we machine knit them, as YOs
 */
public class M1PrimitiveDefinition extends YarnOverDefinition {
    public M1PrimitiveDefinition(@NotNull KnitSpeakStitch stitchElement) {
        super(stitchElement);
    }
}
