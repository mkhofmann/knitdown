package TextureDefinition.StitchDefinition.Primitives;

import CompilerExceptions.ParsingExceptions.InsufficientLoopsException;
import KnitSpeakParser.psi.KnitSpeakStitch;
import KnitSpeakParser.psi.KnitSpeakTiltPrimitive;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.StitchSets.Twists.T2L;
import TextureGraph.Courses.StitchSets.Twists.T2LP;
import TextureGraph.Courses.StitchSets.Twists.T2R;
import TextureGraph.Courses.StitchSets.Twists.T2RP;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 12/1/2016.
 * abstraction for tilted switches
 */
public class TwistPrimitiveDefinition extends PrimitiveStitchDefinition {
    /**
     * flags if the twist leans left
     */
    protected boolean left;
    /**
     * flags if the twist contains purls
     */
    protected boolean purl;
    public TwistPrimitiveDefinition(@NotNull KnitSpeakStitch stitchElement){
        super(stitchElement);
        KnitSpeakTiltPrimitive tiltPrimitiveElement = this.primitiveStitchElement.getTiltPrimitive();
        assert tiltPrimitiveElement != null;
        this.purl = tiltPrimitiveElement.getText().length()==4;
        this.left = tiltPrimitiveElement.getText().charAt(2) == 'l';
        this.requiredLoops = 2;
        this.resultingLoops = 2;
    }

    @NotNull
    @Override
    public StitchSet buildStitch(@NotNull Yarn yarn, @NotNull LinkedList<Loop> availableLoops, boolean rightSide, int declaredRep) throws InsufficientLoopsException {
        if(this.purl){
            if(this.left)
                return new T2LP(this.getNextRequiredLoops(availableLoops), yarn);
            return new T2RP(this.getNextRequiredLoops(availableLoops), yarn);
        }
        if(this.left)
            return new T2L(this.getNextRequiredLoops(availableLoops), yarn);
        return new T2R(this.getNextRequiredLoops(availableLoops), yarn);
    }
}
