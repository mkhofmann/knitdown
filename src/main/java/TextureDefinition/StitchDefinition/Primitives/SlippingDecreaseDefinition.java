package TextureDefinition.StitchDefinition.Primitives;

import CompilerExceptions.ParsingExceptions.InsufficientLoopsException;
import KnitSpeakParser.psi.KnitSpeakSDecPrimitive;
import KnitSpeakParser.psi.KnitSpeakStitch;
import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Knit.KXtog;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.Purl.Pxtog;
import TextureGraph.Courses.Stitches.SlippingDecreases.S2KPO;
import TextureGraph.Courses.Stitches.SlippingDecreases.SK2PO;
import TextureGraph.Courses.Stitches.SlippingDecreases.Skpo;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 12/1/2016.
 * abstraction for slipping decreases
 */
public class SlippingDecreaseDefinition extends PrimitiveStitchDefinition {
    /**
     * flags if this is a sk2po stitch
     */
    private boolean sk2po;
    /**
     * flags if this is a skpo stitch
     */
    protected boolean skpo;
    /**
     * flags if this is a ssk stitch
     */
    private boolean ssk;/**
     * flags if this is a ssp stitch
     */
    private boolean ssp;/**
     * flags if this is a sssp stitch
     */
    private boolean sssp;
    public SlippingDecreaseDefinition(@NotNull KnitSpeakStitch stitchElement) {
        super(stitchElement);
        KnitSpeakSDecPrimitive sDecPrimitiveElement = this.primitiveStitchElement.getSDecPrimitive();
        assert sDecPrimitiveElement != null;
        this.sk2po = sDecPrimitiveElement.getText().equals("sk2po");
        this.skpo = sDecPrimitiveElement.getText().equals("skpo");
        this.ssk = sDecPrimitiveElement.getText().equals("ssk");
        this.ssp = sDecPrimitiveElement.getText().equals("ssp");
        this.sssp = sDecPrimitiveElement.getText().equals("sssp");
        this.requiredLoops = 3;
        if(this.skpo || this.ssk || this.ssp)
            this.requiredLoops =2;
        else if(this.sssp)
            this.requiredLoops = 4;
        this.resultingLoops = 1;
    }

    @NotNull
    @Override
    public StitchSet buildStitch(@NotNull Yarn yarn, @NotNull LinkedList<Loop> availableLoops, boolean rightSide, int declaredRep) throws InsufficientLoopsException {
        if(this.skpo){
            return new Single(new Skpo(this.getNextRequiredLoops(availableLoops),yarn));
        } else if(this.sk2po){
            return new Single(new SK2PO(this.getNextRequiredLoops(availableLoops), yarn));
        } else if(this.sssp){
            return new Single(new Pxtog(yarn, this.getNextRequiredLoops(availableLoops)));
        } else if(this.ssk){
            return new Single(new KXtog(yarn, this.getNextRequiredLoops(availableLoops)));
        } else if(this.ssp){
            return new Single(new Pxtog(yarn, this.getNextRequiredLoops(availableLoops)));
        }
        return new Single(new S2KPO(this.getNextRequiredLoops(availableLoops),yarn));
    }
}
