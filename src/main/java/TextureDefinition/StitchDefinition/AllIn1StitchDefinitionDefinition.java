package TextureDefinition.StitchDefinition;

import CompilerExceptions.ParsingExceptions.*;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.KnitSpeakAllInXStitchStatement;
import KnitSpeakParser.psi.KnitSpeakStitchStatement;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Megan on 12/20/2016.
 * abstraction to describe stitches that come from one shared parent
 */
public class AllIn1StitchDefinitionDefinition extends StitchStatementDefinition {
    /**
     * The list of Stitch Definitions that make up the all in one grouping
     */
    private LinkedList<CommonStitchDefinitionDefinition> stitchList;

    /**
     * @throws OnlyCommonStitchesInOneStitchException the stitch statement was not of a common type
     */
    AllIn1StitchDefinitionDefinition(@NotNull KnitSpeakStitchStatement stitchStatementElement) throws OnlyCommonStitchesInOneStitchException, TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        super(stitchStatementElement);
        KnitSpeakAllInXStitchStatement allInXStitchStatement = this.stitchStatmentElement.getAllInXStitchStatement();
        assert allInXStitchStatement != null;
        List<KnitSpeakStitchStatement> stitchStatementElementList = allInXStitchStatement.getStitchStatementList().getStitchStatementList();
        this.stitchList = new LinkedList<>();
        for( KnitSpeakStitchStatement sse: stitchStatementElementList){
            if(sse.getCommonStitchStatement() == null)
                throw new OnlyCommonStitchesInOneStitchException(this.stitchStatmentElement.getText());
            CommonStitchDefinitionDefinition commonStitchStatement = new CommonStitchDefinitionDefinition(sse);
            if(commonStitchStatement.getRequiredLoops() > 1)
                throw new OnlyCommonStitchesInOneStitchException(this.stitchStatmentElement.getText());
            this.stitchList.add(commonStitchStatement);
        }
    }

    @NotNull
    @Override
    public LinkedList<StitchSet> buildStitches(@NotNull LinkedList<Loop> availableLoops, @NotNull Yarn yarn, boolean rightSide, int declaredRep) throws InsufficientLoopsException {
        Loop parent = availableLoops.remove();
        LinkedList<StitchSet> stitchSets = new LinkedList<>();
        for(CommonStitchDefinitionDefinition cSS: this.stitchList){
            StitchDefinition stitchDefinition = cSS.stitch;
            LinkedList<Loop> parentList = new LinkedList<>();
            parentList.add(parent);
            stitchSets.add(stitchDefinition.buildStitch(yarn, parentList, rightSide, -1));
        }
        return stitchSets;
    }

    @Override
    public int getRequiredLoops() {return 1;}
}
