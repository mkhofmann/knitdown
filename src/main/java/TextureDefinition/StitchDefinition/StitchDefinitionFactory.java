package TextureDefinition.StitchDefinition;

import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.*;
import TextureDefinition.StitchDefinition.ParameterizedStitches.IncreaseSetDefinition;
import TextureDefinition.StitchDefinition.ParameterizedStitches.ParameterizedCableDefinition;
import TextureDefinition.StitchDefinition.ParameterizedStitches.StitchBelowDefinition;
import TextureDefinition.StitchDefinition.ParameterizedStitches.XTogStitchDefinition;
import TextureDefinition.StitchDefinition.Primitives.*;
import org.jetbrains.annotations.NotNull;

/**
 * Created by Megan on 12/1/2016.
 * creates abstraction based on passed stitch element
 */
class StitchDefinitionFactory {
    /**
     * @param stitchElement the element to construct a definition from
     * @return the resulting definition
     */
    @NotNull
    static StitchDefinition buildStitchDefinition(@NotNull KnitSpeakStitch stitchElement) throws TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        KnitSpeakPrimitiveStitch primitiveStitchElement = stitchElement.getPrimitiveStitch();
        if(primitiveStitchElement != null){
            KnitSpeakTiltPrimitive tiltPrimitiveElement = primitiveStitchElement.getTiltPrimitive();
            if(tiltPrimitiveElement != null)
                return new TwistPrimitiveDefinition(stitchElement);
            KnitSpeakM1Primitive m1Primitive = primitiveStitchElement.getM1Primitive();
            if(m1Primitive != null)
                return new M1PrimitiveDefinition(stitchElement);
            KnitSpeakKPrimitive kPrimitive = primitiveStitchElement.getKPrimitive();
            if(kPrimitive != null)
                return new KPrimitiveDefinition(stitchElement);
            KnitSpeakPPrimitive pPrimitive = primitiveStitchElement.getPPrimitive();
            if(pPrimitive != null)
                return new PPrimitiveDefinition(stitchElement);
            KnitSpeakSDecPrimitive sDecPrimitive = primitiveStitchElement.getSDecPrimitive();
            if(sDecPrimitive != null)
                return new SlippingDecreaseDefinition(stitchElement);
            KnitSpeakCablePrimitive cablePrimitive = primitiveStitchElement.getCablePrimitive();
            if(cablePrimitive != null)
                return new CableStitchDefinition(stitchElement);
            if(primitiveStitchElement.getText().equals("ctr dbl inc")){
                return new CtrDblIncDefinition(stitchElement);
            }
            else
                return new YarnOverDefinition(stitchElement);
        } else{
            KnitSpeakParameterizedStitch parameterizedStitch = stitchElement.getParameterizedStitch();
            assert parameterizedStitch != null;
            KnitSpeakStitchBelow stitchBelow = parameterizedStitch.getStitchBelow();
            if(stitchBelow != null)
                return new StitchBelowDefinition(stitchElement);
            KnitSpeakCableParameterized cableParameterized = parameterizedStitch.getCableParameterized();
            if(cableParameterized != null)
                return new ParameterizedCableDefinition(stitchElement);
            KnitSpeakStitchMapsIncrease stitchMapsIncrease = parameterizedStitch.getStitchMapsIncrease();
            if(stitchMapsIncrease != null)
                return new IncreaseSetDefinition(stitchElement);
            else
                return new XTogStitchDefinition(stitchElement);
        }
    }
}
