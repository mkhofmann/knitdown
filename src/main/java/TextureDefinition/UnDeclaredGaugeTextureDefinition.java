package TextureDefinition;

import CompilerExceptions.ParsingExceptions.*;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.KnitSpeakPattern;
import TextureDefinition.CourseDefinition.RowDeclarations.LineDeclaration;
import org.jetbrains.annotations.NotNull;

/**
 * Created by Megan on 12/1/2016.
 * A gamrent where gauge must be found from the pattern description
 */
public class UnDeclaredGaugeTextureDefinition extends TextureDefinition {
    /**
     * @param pattern the pattern to parse
     */
    UnDeclaredGaugeTextureDefinition(@NotNull KnitSpeakPattern pattern) throws UnDefinedLineException, InvalidRepetitionLoopsException, LineTypePreviouslyDeclaredException, OnlyCommonStitchesInOneStitchException, TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        super(pattern);
    }

    @Override
    protected int countCastOnByReps(int reps) {
        return this.lineDefinitionsMap.get(this.getMaxRepSizeLineDeclaration()).getMultipleOfRequiredLoops(reps);
    }

    @Override
    protected int closestCastOnToWidth(int stitchCount) {return this.lineDefinitionsMap.get(this.lineDeclarations.get(0)).matchingWidth(stitchCount, false);}

    @Override
    public int getBorderSize() {
        return this.lineDefinitionsMap.get(this.lineDeclarations.get(0)).getBorderLoopCount();
    }

    @Override
    public int getRepSize() {
        return this.lineDefinitionsMap.get(this.getMaxRepSizeLineDeclaration()).getRepLoopCount();
    }

    /**
     * @return the course declaration with the largest repeated section which should define the repcount for the whole texture
     */
    @NotNull
    private LineDeclaration getMaxRepSizeLineDeclaration(){
        int maxRep = 0;
        LineDeclaration maxDeclaration=null;
        for(LineDeclaration lineDeclaration: this.lineDefinitionsMap.keySet()) {
            int repLoopCount = this.lineDefinitionsMap.get(lineDeclaration).getRepLoopCount();
            if(repLoopCount >maxRep){
                maxDeclaration = lineDeclaration;
                maxRep = repLoopCount;
            }
        }
        assert maxDeclaration != null;
        return maxDeclaration;
    }

}
