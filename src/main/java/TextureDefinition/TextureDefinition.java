package TextureDefinition;

import CompilerExceptions.ParsingExceptions.*;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.*;
import TextureDefinition.CourseDefinition.CourseDefinition;
import TextureDefinition.CourseDefinition.CourseDefinitionFactory;
import TextureDefinition.CourseDefinition.RowDeclarations.LineDeclaration;
import TextureDefinition.CourseDefinition.RowDeclarations.SideDeclaration;
import TextureGraph.Courses.CastOnCourse;
import TextureGraph.Courses.Course;
import TextureGraph.Courses.PurlWSCourse;
import TextureGraph.Courses.Stitches.Loops.TurnSection;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.TextureBuiltInRow;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Megan on 12/20/2016.
 * an abstraction for a textureGraph graph created from a knitspeak file
 */
public abstract class TextureDefinition {
    /**
     * The knitspeak pattern to build the texture definition from
     */
    protected KnitSpeakPattern pattern;
    /**
     * A map from course declarations to the definition of those courses
     */
    HashMap<LineDeclaration, CourseDefinition> lineDefinitionsMap;
    /**
     * The declaration of RS rows
     */
    private LineDeclaration rsDeclaration;
    /**
     * The declaration of WS rows
     */
    private LineDeclaration wsDeclaration;
    /**
     * Indexable list of course declarations in the order they are assigned to courses in one repeat of the pattern
     */
    ArrayList<LineDeclaration> lineDeclarations;
    /**
     * flags if the declared WS is on even rows
     */
    public boolean wsISEven =true;
    /**
     * flags that the ws is the default, all even rows
     */
    public boolean wsDefault = true;
    /**
     * true if the knitspeak has a cast on declartion, stating how many loops to create
     */
    private boolean hasCastOnDeclaration;
    /**
     * number of stitches needed to caston for one repeat
     */
    private int castOnSts = 0;

    /**
     * Build the texture definition from a valid pattern
     * @param pattern the knitspeak pattern that defines this texture
     * @throws UnDefinedLineException If there is a missing definition
     * @throws InvalidRepetitionLoopsException repetition between courses will not work
     */
    public TextureDefinition(@NotNull KnitSpeakPattern pattern) throws UnDefinedLineException, InvalidRepetitionLoopsException, LineTypePreviouslyDeclaredException, OnlyCommonStitchesInOneStitchException, TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        this.pattern = pattern;
        KnitSpeakRowCounter rowCounter = this.pattern.getRowCounter();
        KnitSpeakCastOnLine castOnLine = this.pattern.getCastOnLine();
        this.hasCastOnDeclaration = castOnLine != null;
        if(this.hasCastOnDeclaration)
            this.castOnSts = NumConverter.convertNum(castOnLine.getStitchesCount());
        List<KnitSpeakLine> lines = this.pattern.getLineList();
        this.lineDeclarations = new ArrayList<>();
        if(rowCounter != null) {
            List<KnitSpeakStitchesCount> numList = rowCounter.getStitchesCountList();
            int lastRow = NumConverter.convertNum(numList.get(numList.size() - 1));
            for(int i = 0; i< lastRow; i++)
                this.lineDeclarations.add(null);
        }
        KnitSpeakStitchCount firstDeclaredStitchCount = null;
        for(KnitSpeakLine line: lines) {
            if(firstDeclaredStitchCount == null && line.getStitchCount() != null) {//found some declaration to compare against
                firstDeclaredStitchCount = line.getStitchCount();
                if (firstDeclaredStitchCount.getStitchesCount() == null && firstDeclaredStitchCount.getMultiplesDeclaration() == null)//one more stitch is invalid
                    throw new InvalidRepetitionLoopsException(line.getRowDeclaration().toString());
            } else if(firstDeclaredStitchCount != null && line.getStitchCount() != null){// there are two stitch counts to compare
                KnitSpeakStitchesCount lineStitchCount = line.getStitchCount().getStitchesCount();
                KnitSpeakMultiplesDeclaration lineMultiplesDeclaration = line.getStitchCount().getMultiplesDeclaration();
                KnitSpeakMultiplesDeclaration firstMultiplesDeclaration = firstDeclaredStitchCount.getMultiplesDeclaration();
                KnitSpeakStitchesCount firstStitchCount = firstDeclaredStitchCount.getStitchesCount();
                boolean stitchCountsMatch;
                boolean multiplesMatch = false;
                if (firstStitchCount == null && lineStitchCount == null) {
                    stitchCountsMatch = true;
                    assert lineMultiplesDeclaration != null;
                    assert firstMultiplesDeclaration != null;
                    multiplesMatch = (NumConverter.convertNum(firstMultiplesDeclaration.getStitchesCountList().get(0)) == NumConverter.convertNum(lineMultiplesDeclaration.getStitchesCountList().get(0))) && (NumConverter.convertNum(firstMultiplesDeclaration.getStitchesCountList().get(1)) == NumConverter.convertNum(lineMultiplesDeclaration.getStitchesCountList().get(1)));
                } else
                    stitchCountsMatch = firstStitchCount != null && lineStitchCount != null && NumConverter.convertNum(firstStitchCount) == NumConverter.convertNum(lineStitchCount);
                if(!(stitchCountsMatch && multiplesMatch))
                    throw new InvalidRepetitionLoopsException(line.getRowDeclaration().getText());
            }
            KnitSpeakRowDeclaration rowDeclaration = line.getRowDeclaration();
            this.addRowDeclarationAsFound(new LineDeclaration(rowDeclaration));
        }
        lineDefinitionsMap = new HashMap<>(lines.size());
        for(KnitSpeakLine l: lines){
            LineDeclaration rowDeclaration = new LineDeclaration(l.getRowDeclaration());
            KnitSpeakAsRowProcess asRowProcess = l.getAsRowProcess();
            if(asRowProcess != null){
                int asRow = NumConverter.convertNum(asRowProcess.getNumPossibleSuffixed());
                try {
                    CourseDefinition asLine = this.lineDefinitionsMap.get(this.lineDeclarations.get(asRow-1));
                    if( asLine == null)
                        throw new UnDefinedLineException(rowDeclaration.toString());
                    lineDefinitionsMap.put(rowDeclaration, asLine );
                } catch (IndexOutOfBoundsException iob) {
                    throw new UnDefinedLineException(rowDeclaration.toString());
                }
            } else
                lineDefinitionsMap.put(rowDeclaration, CourseDefinitionFactory.buildCourseDefinition(l,rowDeclaration, this));
        }
        if(this.rsDeclaration != null)
            this.fillRSRows();
        if(this.wsDeclaration != null)
            this.fillWSRows();
        for(LineDeclaration rowDeclaration: this.lineDeclarations){
            if(rowDeclaration == null)
                throw new UnDefinedLineException("Can't find the row");
        }
    }

    /**
     * @return the definition of the first non cast on course in the pattern
     */
    @Contract(pure = true)
    public CourseDefinition getBottomLine() {return this.lineDefinitionsMap.get(this.lineDeclarations.get(0));}

    /**
     * @return the number of non-cast on courses in the pattern
     */
    @Contract(pure = true)
    public int getHeight(){return this.lineDeclarations.size();}

    /**
     * @param reps the number of pattern reps desired for a specific textureGraph
     * @return the number of castons needed to start that repeating pattern
     */
    @Contract(pure = true)
    protected abstract int countCastOnByReps(int reps);

    /**
     * @param stitchCount the desired stitchCount
     * @return the cast on stitchCount created when trying to approach a specific stitchCount
     */
    @Contract(pure = true)
    protected abstract int closestCastOnToWidth(int stitchCount);

    /**
     * @param reps the number of width repetitions of the pattern
     * @param yarn the yarn to build the cast on course from
     * @return a cast on course with the needed stitches
     */
    @NotNull
    private CastOnCourse castOnByReps(int reps, @NotNull Yarn yarn){return new CastOnCourse(this.countCastOnByReps(reps), yarn);}

    /**
     * @param stitchCount the width in stitches of the needed cast on course
     * @param yarn the yarn to build the cast on course from
     * @return the cast on course with the needed stitches
     */
    @NotNull
    private CastOnCourse castOnByWidth(int stitchCount, @NotNull Yarn yarn){
        return new CastOnCourse(this.closestCastOnToWidth(stitchCount), yarn);
    }

    /**
     * @param reps the reps of the textureGraph in width
     * @param textureOnly flag if the texture being read is only meant to construct a textured rectangle, or more complex structure that may not be machine knittable or be 3D
     * @return a textureGraph built in row with one height repetition of this pattern and reps to match the desired stitch count
     */
    @NotNull
    public TextureBuiltInRow buildGarmentGraphByWidthReps(int reps, boolean textureOnly) throws InsufficientLoopsException {
        Yarn yarn = new Yarn();
        LinkedList<Course> courses = new LinkedList<>();
        if(this.hasCastOnDeclaration) {
            assert this.castOnSts % reps == 0;
            courses.add(this.castOnByWidth(this.castOnSts, yarn));
        }
        else
            courses.add(this.castOnByReps(reps, yarn));
        for(int i = 0; i<this.lineDeclarations.size(); i++){
            LineDeclaration rowDeclaration = this.lineDeclarations.get(i);
            CourseDefinition courseDefinition = this.lineDefinitionsMap.get(rowDeclaration);
            Course nextCourse = courseDefinition.buildLine(courses.getLast(), yarn, i+1, textureOnly);
            if(textureOnly){
                if(nextCourse.getParentLoops().size() != nextCourse.getChildLoops().size())
                    throw new InvalidRepetitionLoopsException(rowDeclaration.toString());
            }
            courses.add(nextCourse);
            if(!(nextCourse.getYarn().sections.getLast() instanceof TurnSection))
                nextCourse.turnAtEndOfLine();
        }
        if(this.wsDeclaration != null && courses.size()%2!=1) {
            Course nextCourse = this.lineDefinitionsMap.get(this.wsDeclaration).buildLine(courses.getLast(), yarn, courses.size(), false);
            courses.add(nextCourse);
            nextCourse.turnAtEndOfLine();
        }
        return new TextureBuiltInRow(courses);
    }

    /**
     * @param garmentGraph to extend
     * @param rep the number of height reps to extend by
     * @return the textureGraph graph extended by reps
     */
    @NotNull
    public TextureBuiltInRow extendGarmentGraph(@NotNull TextureBuiltInRow garmentGraph, int rep) throws InsufficientLoopsException {
        return this.extendGarmentGraph(garmentGraph, garmentGraph.getYarn(), rep);
    }
    /**
     * @param garmentGraph to extend
     * @param yarn the yarn to extend the textureGraph with
     * @param rep the number of height reps to extend by
     * @return the textureGraph graph extended by reps
     */
    @NotNull
    private TextureBuiltInRow extendGarmentGraph(@NotNull TextureBuiltInRow garmentGraph, @NotNull Yarn yarn, int rep) throws InsufficientLoopsException {
        for(int r=0; r<rep; r++) {
            int start = garmentGraph.courses.size();
            for (int i = 0; i < this.lineDeclarations.size(); i++) {
                LineDeclaration rowDeclaration = this.lineDeclarations.get(i);
                CourseDefinition courseDefinition = this.lineDefinitionsMap.get(rowDeclaration);
                garmentGraph.addLine(courseDefinition.buildLine(garmentGraph.getLastLine(), yarn, start + i, false));
            }
            if (this.wsDeclaration != null && garmentGraph.courses.size() % 2 != 1)
                garmentGraph.addLine(this.lineDefinitionsMap.get(this.wsDeclaration).buildLine(garmentGraph.courses.getLast(), yarn, garmentGraph.courses.size(), false));
        }
        if(garmentGraph.courses.size()%2!=1)//even rows + caston
            garmentGraph.addLine(new PurlWSCourse(garmentGraph.getLastLine().getCourseID(), garmentGraph.getLastLine(), yarn));
        return garmentGraph;
    }

    /**
     * @param garmentGraph the textureGraph graph to be extended
     * @param extension the number of courses to extend it, the extension should be less than the height of this pattern
     * @return the textureGraph graph extended by extension courses
     */
    @NotNull
    public TextureBuiltInRow extendGarmentGraphPartialRep(@NotNull TextureBuiltInRow garmentGraph, int extension) throws InsufficientLoopsException {
        return this.extendGarmentGraphPartialRep(garmentGraph, garmentGraph.getYarn(), extension);
    }

    /**
     *
     * @param garmentGraph the textureGraph graph to be extended
     * @param yarn the yarn to extend the textureGraph by
     * @param extension the number of courses to extend it, the extension should be less than the height of this pattern
     * @return the textureGraph graph extended by extension courses
     */
    @NotNull
    private TextureBuiltInRow extendGarmentGraphPartialRep(@NotNull TextureBuiltInRow garmentGraph, @NotNull Yarn yarn, int extension) throws InsufficientLoopsException {
        int start = garmentGraph.courses.size();
        assert extension< this.lineDeclarations.size();
        for(int i=0; i<extension; i++){
            LineDeclaration rowDeclaration = this.lineDeclarations.get(i);
            CourseDefinition courseDefinition = this.lineDefinitionsMap.get(rowDeclaration);
            garmentGraph.addLine(courseDefinition.buildLine(garmentGraph.getLastLine(), yarn, start+i, false));
        }
        if(this.wsDeclaration != null && garmentGraph.courses.size()%2!=1)//todo should we be adding this extra row here?
            garmentGraph.addLine(this.lineDefinitionsMap.get(this.wsDeclaration).buildLine(garmentGraph.courses.getLast(), yarn, garmentGraph.courses.size(), false));
        return garmentGraph;
    }

    /**
     * Filles in the RS rows that are missing course definitions
     * @throws LineTypePreviouslyDeclaredException thrown if a course is over declared
     */
    private void fillRSRows() throws LineTypePreviouslyDeclaredException {
        for(int i = 0; i<this.lineDeclarations.size(); i+=2){
            if(lineDeclarations.get(i) != null)
                throw new LineTypePreviouslyDeclaredException(""+i);
            lineDeclarations.set(i, this.rsDeclaration);
        }
    }

    /**
     * filles the WS rows that are missing course definitions
     * @throws LineTypePreviouslyDeclaredException thrown if a course is over declared
     */
    private void fillWSRows() throws  LineTypePreviouslyDeclaredException {
        for(int i = 1; i<this.lineDeclarations.size(); i+=2){
            if(lineDeclarations.get(i) != null) {
                if(i==this.lineDeclarations.size()-1)//may declare last row
                    continue;
                if (lineDeclarations.get(i).equals(this.wsDeclaration))
                    continue;
                throw new LineTypePreviouslyDeclaredException(""+i);
            }
            lineDeclarations.set(i, this.wsDeclaration);
        }
    }

    /**
     * Adds a row declaration to indexable list in its proper index
     * @param rowDeclaration the row declaration to add to list
     * @throws LineTypePreviouslyDeclaredException thrown if a declaration is repeated
     */
    private void addRowDeclarationAsFound(@NotNull LineDeclaration rowDeclaration) throws LineTypePreviouslyDeclaredException {
        if(rowDeclaration.sideDeclaration == SideDeclaration.RSOdd || rowDeclaration.sideDeclaration == SideDeclaration.RSEven)
            this.setRsDeclaration(rowDeclaration);
        else if(rowDeclaration.sideDeclaration == SideDeclaration.WSOdd || rowDeclaration.sideDeclaration == SideDeclaration.WSEven)
            this.setWsDeclaration(rowDeclaration);
        while(this.lineDeclarations.size()<rowDeclaration.getLastRow())
            this.lineDeclarations.add(null);
        for(int id: rowDeclaration.getRow_ids()){
            assert id - 1 <= lineDeclarations.size() && id - 1 >= 0;
            if(lineDeclarations.get(id-1) != null)
                throw new LineTypePreviouslyDeclaredException(rowDeclaration.toString());
            lineDeclarations.set(id-1, rowDeclaration);
        }
    }

    /**
     * sets the rsDeclaration to the row declaration
     * @param rowDeclaration the row declaration that defines RS rows
     * @throws LineTypePreviouslyDeclaredException if the RS declaration was already set.
     */
    private void setRsDeclaration(@NotNull LineDeclaration rowDeclaration) throws LineTypePreviouslyDeclaredException {
        if(rsDeclaration != null)
            throw new LineTypePreviouslyDeclaredException(rowDeclaration.toString());
        rsDeclaration = rowDeclaration;
    }

    /**
     * sets the wsDeclaration to the row declaration
     * @param rowDeclaration the row declaration taht defines WS rows
     * @throws LineTypePreviouslyDeclaredException if the Ws declaration was already set
     */
    private void setWsDeclaration(@NotNull LineDeclaration rowDeclaration) throws LineTypePreviouslyDeclaredException {
        if(wsDeclaration != null)
            throw new LineTypePreviouslyDeclaredException(rowDeclaration.toString());
        wsDeclaration = rowDeclaration;
    }

    @Override
    public String toString(){return this.pattern.getContainingFile().getName();}
    @Contract(pure = true)
    public abstract int getBorderSize();
    @Contract(pure = true)
    public abstract int getRepSize();
}
