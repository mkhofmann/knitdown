package TextureDefinition;

import CompilerExceptions.ParsingExceptions.*;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.KnitSpeakGaugeReqDeclaration;
import KnitSpeakParser.psi.KnitSpeakPattern;
import KnitSpeakParser.psi.KnitSpeakStitchesCount;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Megan on 12/20/2016.
 *
 *
 */
public class StitchCountDeclaredTextureDefinition extends TextureDefinition {
    /**
     * Flags if the stitch count can be any integer
     */
    private boolean anyNumber;
    /**
     * the number of stitches in a repeat tile
     */
    private int repCount;
    /**
     * the number of stitches at the border of the textrue
     */
    private int borderCount;

    /**
     * @param pattern the knitspeak that the pattern is construted from
     */
    StitchCountDeclaredTextureDefinition(@NotNull KnitSpeakPattern pattern) throws UnDefinedLineException, InvalidRepetitionLoopsException, LineTypePreviouslyDeclaredException, OnlyCommonStitchesInOneStitchException, TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        super(pattern);
        KnitSpeakGaugeReqDeclaration gaugeReqDeclaration = pattern.getGaugeReqDeclaration();
        assert gaugeReqDeclaration != null;
        this.anyNumber = gaugeReqDeclaration.getMultiplesDeclaration() == null;
        if(!this.anyNumber){
            List<KnitSpeakStitchesCount> nums = gaugeReqDeclaration.getMultiplesDeclaration().getStitchesCountList();
            this.repCount = NumConverter.convertNum(nums.get(0));
            if(nums.size()<2)
                this.borderCount = 0;
            else
                this.borderCount = NumConverter.convertNum(nums.get(1));
        }
    }

    @Override
    protected int countCastOnByReps(int reps) {
        if(this.anyNumber)
            return reps;
        return repCount *reps+ borderCount;
    }

    @Override
    protected int closestCastOnToWidth(int stitchCount) {
        if(this.anyNumber)
            return stitchCount;
        int newWidth = stitchCount -this.borderCount;
        int leftover = newWidth%this.repCount;
        if(leftover ==0)
            return stitchCount;
        int sections = newWidth/this.repCount;
        int min = sections*this.repCount;
        int max = sections*(this.repCount +1);
        int difMin = newWidth-min;
        int difMax = max -newWidth;
        if( difMax <= difMin)
            return min+ borderCount;
        return max+ borderCount;
    }

    @Override
    public int getBorderSize() {
        return this.borderCount;
    }

    @Override
    public int getRepSize() {
        return this.repCount;
    }
}
