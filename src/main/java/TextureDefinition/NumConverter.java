package TextureDefinition;


import KnitSpeakParser.psi.KnitSpeakNumPossibleSuffixed;
import KnitSpeakParser.psi.KnitSpeakRepTimes;
import KnitSpeakParser.psi.KnitSpeakStitchesCount;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * Created by Megan on 12/1/2016.
 * converts num definitions from knitspeak into integers
 */
public class NumConverter {
    /**
     * @param repTimes the KnitSpeak token to conver to an integer
     * @return the interger value
     */
    public static int convertNum(@Nullable KnitSpeakRepTimes repTimes){
        if(repTimes == null)
            return 0;
        if(repTimes.getStitchesCount() == null)
            return 2;
        return NumConverter.convertNum(repTimes.getStitchesCount());
    }
    /**
     * @param num the KnitSpeak token to conver to an integer
     * @return the interger value
     */
    public static int convertNum(@Nullable KnitSpeakStitchesCount num){
        if(num == null)
            return 0;
        String digits = num.getText();
        return Integer.parseInt(digits);
    }
    /**
     * @param num the KnitSpeak token to conver to an integer
     * @return the interger value
     */
    public static int convertNum( @Nullable KnitSpeakNumPossibleSuffixed num){
        if( num ==null)
            return 0;
        return NumConverter.convertNum(num.getStitchesCount());
    }
    /**
     * @param nums the KnitSpeak tokens to convert to integers
     * @return the interger values
     */
    @NotNull
    public static int[] convertNums(@Nullable List<KnitSpeakStitchesCount> nums){
        if( nums == null || nums.size()==0)
            return new int[0];
        int[] ints = new int[nums.size()];
        for(int i =0; i<nums.size(); i++)
            ints[i] = NumConverter.convertNum(nums.get(i));
        return ints;
    }
}
