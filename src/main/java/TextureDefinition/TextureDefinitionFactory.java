package TextureDefinition;

import CompilerExceptions.ParsingExceptions.*;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooSmallIncreaseException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideCableException;
import CompilerExceptions.ParsingExceptions.SizeExceptions.TooWideIncreaseException;
import KnitSpeakParser.psi.KnitSpeakPattern;
import org.jetbrains.annotations.NotNull;

/**
 * Created by Megan on 12/20/2016.
 * creates the correct abstraction depending on how gauge was declared
 */
public class TextureDefinitionFactory {
    /**
     * @param pattern the pattern to parse
     * @return an abstract textureGraph of the coprrect type
     */
    @NotNull
    public static TextureDefinition buildTextureDefinition(@NotNull KnitSpeakPattern pattern) throws UnDefinedLineException, LineTypePreviouslyDeclaredException, InvalidRepetitionLoopsException, OnlyCommonStitchesInOneStitchException, TooWideCableException, TooSmallCableException, TooWideIncreaseException, TooSmallIncreaseException {
        if(pattern.getGaugeReqDeclaration() != null)
            return new StitchCountDeclaredTextureDefinition(pattern);
        else
            return new UnDeclaredGaugeTextureDefinition(pattern);
    }
}
