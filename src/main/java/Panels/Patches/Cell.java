package Panels.Patches;

import Panels.Panel;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.Iterator;

/**
 * Created by Megan on 4/26/2017.
 * a height and width unit on as panel
 */
public class Cell implements Comparable<Cell>{
    /**
     * @return cell index by width
     */
    @Contract(pure=true)
    int getX() {
        return x;
    }
    /**
     * @return cell index by height
     */
    public int getY() {
        return y;
    }
    /**
     * index by width
     */
    private int x;
    /**
     * index by height
     */
    private int y;
    /**
     * owning panel
     */
    private Panel panel;
    /**
     * @param x the index by width
     * @param y the index by height
     * @param panel the assigned panel
     */
    public Cell(int x, int y, @NotNull Panel panel){
        this.panel = panel;
        this.x = x;
        this.y = y;
        assert x>=0;
        assert x<panel.cellsInWidth;
        assert y>=0;
        assert y<panel.cellsInHeight;
    }
    /**
     * @return the value width location of the cell in real world units
     */
    @Contract(pure=true)
    double getRealX(){
        return ((double)this.x)/((double) Panel.CELL_PER_UNIT);
    }
    /**
     * @return the value width location of the cell in real world units
     */
    @Contract(pure=true)
    double getRealY(){
        return ((double)this.y)/((double) Panel.CELL_PER_UNIT);
    }
    @Override
    public int compareTo(@NotNull Cell o) {return Cell.xThenY.compare(this, o);}

    private static Comparator<Cell> xThenY = (o1, o2) -> {
        int compareX = Cell.xPosComparator.compare(o1, o2);
        if(compareX == 0)
            return Cell.yPosComparator.compare(o1, o2);
        return compareX;
    };

    static Comparator<Cell> xPosComparator = Comparator.comparingInt(o -> o.x);
    static Comparator<Cell> yPosComparator = Comparator.comparingInt(o -> o.y);

    static Comparator<Cell> rightMost = (o1, o2) -> xPosComparator.compare(o2, o1);
    static Comparator<Cell> leftMost = (o1, o2) -> xPosComparator.compare(o1, o2);
    static Comparator<Cell> topMost = (o1, o2) -> yPosComparator.compare(o1, o2);
    static Comparator<Cell> bottomMost = (o1, o2) -> yPosComparator.compare(o2, o1);

    /**
     * @return true if this cell is the highest in the panel
     */
    @Contract(pure=true)
    boolean isTop(){return this.y+1>=this.panel.cellsInHeight;}

    /**
     * @return the cell directly above this, or null if none exists
     */
    @Nullable
    @Contract(pure=true)
    Cell aboveCell(){
        if(this.isTop())
            return null;
        return new Cell(this.x, this.y+1, this.panel);
    }

    /**
     * @return true if this cell is the lowest in the panel
     */
    @Contract(pure=true)
    boolean isBottom(){return this.y-1<0;}
    /**
     * @return the cell directly below this, or null if none exists
     */
    @Nullable
    @Contract(pure=true)
    Cell belowCell(){
        if(this.isBottom())
            return null;
        return new Cell(this.x, this.y-1, this.panel);
    }

    /**
     * @return true if this is the cell furthest to the right in the panel
     */
    @Contract(pure=true)
    public boolean isRight(){return this.x-1<0;}
    /**
     * @return the cell directly right of this, or null if none exists
     */
    @Nullable
    @Contract(pure=true)
    Cell rightCell(){
        if(this.isRight())
            return null;
        return new Cell(this.x-1, this.y, this.panel);
    }

    /**
     * @return true if this is the cell furthest to the left in the panel
     */
    @Contract(pure=true)
    public boolean isLeft(){
        assert this.panel != null;
        return  this.x+1>=this.panel.cellsInWidth;}
    /**
     * @return the cell directly left of this, or null if none exists
     */
    @Nullable
    @Contract(pure=true)
    Cell leftCell(){
        if(this.isLeft())
            return null;
        return new Cell(this.x+1, this.y, this.panel);
    }
    /**
     * @return iterator that moves left, starting with the left neighbor of this
     */
    Iterator<Cell> leftRowIterator(){return new LeftRowIterator(this);}
    /**
     * @return iterator that moves right, starting with the right neighbor of this
     */
    public Iterator<Cell> rightRowIterator(){return new RightRowIterator(this);}
    /**
     * @return iterator that moves up, starting with the above neighbor of this
     */
    Iterator<Cell> upColumnIterator(){return new UpColumnIterator(this);}
    /**
     * @return iterator that moves down, starting with the below neighbor of this
     */
    public Iterator<Cell> downColumnIterator(){return new DownColumnIterator(this);}

    private class LeftRowIterator implements Iterator<Cell>{
        Cell lastCell;
        LeftRowIterator(Cell start){this.lastCell =  start;}
        @Override
        public boolean hasNext() {return !this.lastCell.isLeft();}
        @Override
        public Cell next() {
            assert this.hasNext();
            return this.lastCell = this.lastCell.leftCell();
        }
    }
    private class RightRowIterator implements Iterator<Cell>{
        Cell lastCell;
        RightRowIterator(Cell start){this.lastCell =  start;}
        @Override
        public boolean hasNext() {return !this.lastCell.isRight();}
        @Override
        public Cell next() {
            assert this.hasNext();
            return this.lastCell = this.lastCell.rightCell();
        }
    }
    private class UpColumnIterator implements Iterator<Cell> {
        Cell lastCell;
        UpColumnIterator(Cell start){this.lastCell =  start;}
        @Override
        public boolean hasNext() {return !this.lastCell.isTop();}
        @Override
        public Cell next() {
            assert this.hasNext();
            return this.lastCell = this.lastCell.aboveCell();
        }
    }
    private class DownColumnIterator implements Iterator<Cell> {
        Cell lastCell;
        DownColumnIterator(Cell start){this.lastCell =  start;}
        @Override
        public boolean hasNext() {return !this.lastCell.isBottom();}
        @Override
        public Cell next() {
            assert this.hasNext();
            return this.lastCell = this.lastCell.belowCell();
        }
    }
    @Override
    public int hashCode(){return this.y*this.panel.cellsInWidth+this.x;}
    @Override
    public String toString(){return "("+this.x+","+this.y+")";}
    @Override
    public boolean equals(Object o){
        if(o instanceof Cell){
            Cell other = (Cell) o;
            return this.x==other.x && this.y ==other.y;
        }
        return false;
    }
}
