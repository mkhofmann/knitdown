package Panels.Patches;

import CompilerExceptions.ParsingExceptions.InsufficientLoopsException;
import CompilerExceptions.StitchManipulationExceptions.NoTextureCarvesAvailableException;
import Panels.Panel;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.TextureBuiltInRound;
import TextureGraph.TextureBuiltInRow;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * Created by Megan on 4/26/2017.
 * A patch sets the position, stitchCount and texture of a portion of the panel
 */
public class Patch implements Comparable<Patch>, Iterable<Cell>{
    /**
     * Weight applied to width errors
     */
    private static final double WIDTH_WEIGHT = 10.0;
    /**
     * Weight applied to Texture carving errors.
     */
    private static final double TEXTURE_CARVING_WEIGHT = 1.0;
    /**
     * Weight applied to glue joint errors.
     */
    private static final double GLUE_JOINT_WEIGHT = 2.0;
    /**
     * the texture that makes up this patch
     */
    public Texture texture;
    /**
     * the last course that was read from this textureGraph, used to merge the patches
     */
    public int lastReadLine=1;
    /**
     * handles rounding error in patch easing
     */
    private boolean addEvenLineRight = true;
    private boolean addEvenLineLeft = true;
    private boolean addEvenLine = true;

    /**
     * @return true if this textureGraph has been fully merged into the panel
     */
    @Contract(pure=true)
    public boolean fullyRead(){
        return lastReadLine> lastMadeTextureGraph.getHeight();
    }

    /**
     * the textureGraph that was last built from this patch
     */
    public TextureBuiltInRound lastMadeTextureGraph;
    /**
     * the position and stitchCount of this textureGraph
     */
    public PatchPosition position;
    /**
     * the panel that relates this patch to others
     */
    private Panel panel;
    /**
     * The assigned stitch count at both edges of the patch
     */
    public int edgeStitchCount;
    /**
     * The number of loops leaving the patch
     */
    public int topEdgeLoopCount=-1;
    /**
     * The number of loops entering the aptch
     */
    public int botEdgeLoopCount=-1;
    /**
     * the number of courses in the patch
     */
    private int lineCount;
    /**
     * flags if the patch has already had its stitches adjusted
     */
    private boolean stitchAdjusted = false;
    /**
     * flags if the patch is having its stitches adjusted somewhere in the stack
     */
    private boolean stitchAdjusting = false;
    /**
     * flags if the patch has already had its courses adjusted
     */
    private boolean lineAdjusted = false;
    /**
     * flags if the patch is having its courses adjusted somewhere in the stack
     */
    private boolean lineAdjusting = false;

    private boolean roundStitchCountUpWhenPropegatingFromLowerNeighbors = false;
    private boolean roundStitchCountUpWhenPropegatingFromUpperNeighbors = false;
    private boolean roundLineCountUpWhenPropegatingFromRightNeighbors = false;
    private boolean roundLineCountUpWhenPropegatingFromLeftNeighbors = false;
    /**
     * the significance of the patch in the whole panel
     */
    public int significance;
    /**
     * Maps patches neighboring above to the number of loops dedicated to feed into it
     */
    public HashMap<Patch, Integer> dedicatedStitchesToTopNeighbor = new HashMap<>();
    /**
     * Set of above neighboring above patches that have already traded their glue joints with this patch
     */
    private HashSet<Patch> topTradesMade = new HashSet<>();
    /**
     * Maps patches neighboring below to the number of loops dedicated to feed from them
     */
    public HashMap<Patch, Integer> dedicatedStitchesToBotNeighbor = new HashMap<>();
    /**
     * Set of above neighboring below patches that have already traded their glue joints with this patch
     */
    private HashSet<Patch> botTradesMade = new HashSet<>();
    /**
     * The discovered optimum body stitch count that balences cost of texture carving and width
     */
    public int optBodyStitchCount;

    /**
     * @param texture the texture to construct this patch from
     * @param position the location of this patch in the panel
     * @param significance the significance of this patch in the panel
     */
    public Patch(@NotNull Texture texture, @NotNull PatchPosition position, int significance)  {
        this.texture = texture;
        this.position = position;
        this.panel = position.panel;
        this.setToMatchWidth();
        this.setToMatchHeight();
        this.significance = significance;
    }
    /**
     * @return true if all of the neighbors to the left are already adjusted, forcing this patch to meet their specs
     */
    @Contract(pure=true)
    private boolean leftNeighborsNotFullyAdjusted(){
        TreeSet<Patch> neighbors = this.leftNeighbors();
        if(neighbors.isEmpty())
            return true;
        for(Patch neighbor: neighbors)
            if(!neighbor.lineAdjusted)
                return true;
        return false;
    }
    /**
     * @return true if all of the neighbors to the right are already adjusted, forcing this patch to meet their specs
     */
    @Contract(pure=true)
    private boolean rightNeighborsNotFullyAdjusted(){
        TreeSet<Patch> neighbors = this.rightNeighbors();
        if(neighbors.isEmpty())
            return true;
        for(Patch neighbor: neighbors)
            if(!neighbor.lineAdjusted)
                return true;
        return false;
    }
    /**
     * @return true if any of the neighbors to the left are already adjusted, forcing this patch to meet their specs
     */
    @Contract(pure=true)
    private boolean leftNeighborsPartiallyAdjusted(){
        TreeSet<Patch> neighbors = this.leftNeighbors();
        if(neighbors.isEmpty())
            return false;
        for(Patch neighbor: neighbors)
            if(neighbor.lineAdjusted)
                return true;
        return false;
    }
    /**
     * @return true if any of the neighbors to the right are already adjusted, forcing this patch to meet their specs
     */
    @Contract(pure=true)
    private boolean rightNeighborsPartiallyAdjusted(){
        TreeSet<Patch> neighbors = this.rightNeighbors();
        if(neighbors.isEmpty())
            return false;
        for(Patch neighbor: neighbors)
            if(neighbor.lineAdjusted)
                return true;
        return false;
    }
    /**
     * @return true if any of the neighbors below the patch are already adjusted, forcing this patch to meet their specs
     */
    @Contract(pure=true)
    private boolean bottomNeighborsPartiallyAdjusted(){
        TreeSet<Patch> neighbors = this.bottomNeighbors();
        if(neighbors.isEmpty())
            return false;
        for(Patch neighbor: neighbors)
            if(neighbor.stitchAdjusted)
                return true;
        return false;
    }
    /**
     * @return true if all of the neighbors below the paatch are already adjusted, forcing this patch to meet their specs
     */
    @Contract(pure=true)
    private boolean bottomNeighborsNotFullyAdjusted(){
        TreeSet<Patch> neighbors = this.bottomNeighbors();
        if(neighbors.isEmpty())
            return true;
        for(Patch neighbor: neighbors)
            if(!neighbor.stitchAdjusted)
                return true;
        return false;
    }
    /**
     * @return true if any of the neighbors above the patch are already adjusted, forcing this patch to meet their specs
     */
    @Contract(pure=true)
    private boolean topNeighborsPartiallyAdjusted(){
        TreeSet<Patch> neighbors = this.topNeighbors();
        if(neighbors.isEmpty())
            return false;
        for(Patch neighbor: neighbors)
            if(neighbor.stitchAdjusted)
                return true;
        return false;
    }
    /**
     *
     * @return false if all of the neighbors above the patch are already adjusted, forcing this patch to meet their specs
     */
    @Contract(pure=true)
    private boolean topNeighborsNotFullyAdjusted(){
        TreeSet<Patch> neighbors = this.topNeighbors();
        if(neighbors.isEmpty())
            return true;
        for(Patch neighbor: neighbors)
            if(!neighbor.stitchAdjusted)
                return true;
        return false;
    }
    /**
     * @return builds a textureGraph with precomputed stitchCount
     */
    private TextureBuiltInRow buildGarmentInRow() throws NoTextureCarvesAvailableException, InsufficientLoopsException {
        int widthReps = this.texture.closestStitchRepCount(this.optBodyStitchCount);
        int textureCarvingDelta = this.texture.stitchDelta(this.optBodyStitchCount);
        int heightReps = this.texture.closestHeightRepCount(this.lineCount);
        int heightDelta = this.texture.lineDelta(this.lineCount);
        if(this.topEdgeLoopCount <=0 || this.botEdgeLoopCount<=0){
            this.topEdgeLoopCount = this.edgeStitchCount;
            this.botEdgeLoopCount = this.edgeStitchCount;
        }
        return this.texture.buildTexture(widthReps, textureCarvingDelta, heightReps, heightDelta, this.topEdgeLoopCount, this.botEdgeLoopCount);
    }

    /**
     * builds a textureGraph with precomputed stitchCount in rounds
     * @param yarn to construct the textureGraph from
     */
    public void buildTextureInRound(@NotNull Yarn yarn) throws NoTextureCarvesAvailableException, InsufficientLoopsException {
        this.lastMadeTextureGraph = (TextureBuiltInRound) this.buildGarmentInRow().switchRowRounds(yarn);
    }
    /**
     * set defaults stitch edge count based on width
     */
    private void setToMatchWidth(){
        this.edgeStitchCount = this.optBodyStitchCount;
        if(this.edgeStitchCount == 0)
            this.edgeStitchCount = this.texture.idealStitchCount(this.position.width());
    }
    /**
     * Use hill climbing to minimize the error between inaccurate width and texture carving.
     */
    public void optimizeWidth(){
        double inchesPerStitchNatural = 1.0 / this.texture.stitchesPerInch;
        int repStitches = this.texture.repStitches();
        double repWidth = inchesPerStitchNatural * (double)repStitches;
        double width = this.position.width();
        int optSC = this.texture.idealStitchCount(width);
        double lowerWidth = width - repWidth;//only need to check up to one rep away
        int curStitchCount;
        double curError;
        double minError = Double.MAX_VALUE;
        for(double curWidth = width; curWidth>=lowerWidth; curWidth-= inchesPerStitchNatural){
           curStitchCount = this.texture.idealStitchCount(curWidth);
           curError = this.widthDeltaError(curWidth)+ this.textureCarvingError(curStitchCount);
           if(curError == 0.0){
               this.optBodyStitchCount = curStitchCount;
               return;
           }else if(curError<minError){
               minError = curError;
               optSC = curStitchCount;
           }
        }
        this.optBodyStitchCount = optSC;
    }
    /**
     * Trade glue joints between neighboring patches so that we are not unneccissarily increasing then immediately decreaasing or visa versa
     */
    public void tradeGlueJoints(){
        if(this.dedicatedStitchesToTopNeighbor.keySet().size()==0)
            this.topEdgeLoopCount = this.optBodyStitchCount;
        else if(this.topEdgeLoopCount<0)
            this.topEdgeLoopCount = this.edgeStitchCount;
        if(this.dedicatedStitchesToBotNeighbor.keySet().size() ==0)
            this.botEdgeLoopCount = this.optBodyStitchCount;
        else if(this.botEdgeLoopCount<0)
            this.botEdgeLoopCount = this.edgeStitchCount;
        int curTopGlueJoints = this.glueJointTopDelta();
        int curBotGlueJoints = this.glueJointBotDelta();
        for(Patch topPatch: this.dedicatedStitchesToTopNeighbor.keySet()){
            if(!this.topTradesMade.contains(topPatch)){// if the trade wasn't already made
                if(topPatch.botEdgeLoopCount <0)
                    topPatch.botEdgeLoopCount = topPatch.edgeStitchCount;
                int topPatchGlueJoints = topPatch.glueJointBotDelta();
                if(topPatchGlueJoints>0 && curTopGlueJoints>0){
                    int glueJointReduction = Math.min(topPatchGlueJoints, curTopGlueJoints);
                    topPatch.botEdgeLoopCount+=glueJointReduction;
                    this.topEdgeLoopCount+= glueJointReduction;
                    curTopGlueJoints= this.glueJointTopDelta();
                    this.dedicatedStitchesToTopNeighbor.put(topPatch, this.dedicatedStitchesToTopNeighbor.get(topPatch)+glueJointReduction);
                    topPatch.dedicatedStitchesToBotNeighbor.put(this, topPatch.dedicatedStitchesToBotNeighbor.get(this)+glueJointReduction);
                }else if(topPatchGlueJoints<0 && curTopGlueJoints<0){
                    int glueJointReduction = Math.max(topPatchGlueJoints, curTopGlueJoints);
                    topPatch.botEdgeLoopCount+=glueJointReduction;
                    this.topEdgeLoopCount+= glueJointReduction;
                    curTopGlueJoints= this.glueJointTopDelta();
                    this.dedicatedStitchesToTopNeighbor.put(topPatch, this.dedicatedStitchesToTopNeighbor.get(topPatch)+glueJointReduction);
                    topPatch.dedicatedStitchesToBotNeighbor.put(this, topPatch.dedicatedStitchesToBotNeighbor.get(this)+glueJointReduction);
                }
                this.topTradesMade.add(topPatch);
                topPatch.botTradesMade.add(this);
            }
        }
        for(Patch botPatch: this.dedicatedStitchesToBotNeighbor.keySet()){
            if(!this.botTradesMade.contains(botPatch)) {
                if (botPatch.topEdgeLoopCount < 0)
                    botPatch.topEdgeLoopCount = botPatch.edgeStitchCount;
                int botPatchGlueJoints = botPatch.glueJointTopDelta();
                if (botPatchGlueJoints > 0 && curBotGlueJoints > 0) {
                    int glueJointReduction = Math.min(botPatchGlueJoints, curBotGlueJoints);
                    botPatch.topEdgeLoopCount -= glueJointReduction;
                    this.botEdgeLoopCount -= glueJointReduction;
                    curBotGlueJoints = this.glueJointBotDelta();
                    this.dedicatedStitchesToBotNeighbor.put(botPatch, this.dedicatedStitchesToBotNeighbor.get(botPatch) - glueJointReduction);
                    botPatch.dedicatedStitchesToTopNeighbor.put(this, botPatch.dedicatedStitchesToTopNeighbor.get(this) - glueJointReduction);
                }else if(botPatchGlueJoints < 0 && curBotGlueJoints < 0){
                    int glueJointReduction = Math.max(botPatchGlueJoints, curBotGlueJoints);
                    botPatch.topEdgeLoopCount += glueJointReduction;
                    this.botEdgeLoopCount += glueJointReduction;
                    curBotGlueJoints = this.glueJointBotDelta();
                    this.dedicatedStitchesToBotNeighbor.put(botPatch, this.dedicatedStitchesToBotNeighbor.get(botPatch) + glueJointReduction);
                    botPatch.dedicatedStitchesToTopNeighbor.put(this, botPatch.dedicatedStitchesToTopNeighbor.get(this) + glueJointReduction);
                }
                this.botTradesMade.add(botPatch);
                botPatch.topTradesMade.add(this);
            }
        }
    }
    /**
     * Trade off between texture carves and glue joints to minimize overall error funtion
     */
    public void optimizeTextureCarvingOrGlueJoints(){
        int startingTextureCarves = this.texture.stitchDelta(this.optBodyStitchCount);
        int startingGlueJoints = this.glueJointDelta();
        if(startingGlueJoints<0 && startingTextureCarves<0) {//we are adding loops at the edges that are being removed by texture carving, search for trade off
            double minError = Double.MAX_VALUE;
            int minimizingBodyCount = this.optBodyStitchCount;
            for(int difOnTextureCarves=0;difOnTextureCarves<Math.abs(startingTextureCarves); difOnTextureCarves++){
                int curStitchCount = this.optBodyStitchCount+difOnTextureCarves;
                double estimatedWidth = this.estimateWidth(curStitchCount);
                int curTextureCarves = startingTextureCarves+difOnTextureCarves;
                int curGlueJoints = startingGlueJoints+difOnTextureCarves;
                double curError = this.widthDeltaError(estimatedWidth)+this.glueJointError(curGlueJoints)+ this.textureCarvingError(curStitchCount, curTextureCarves);
                if(curError<minError){
                    minError= curError;
                    minimizingBodyCount = curStitchCount;
                }
            }
            this.optBodyStitchCount = minimizingBodyCount;
        }
    }
    /**
     * @param curStitchCount the stitch count that is currently used to determined width
     * @return the estimated width based on texture natural gauge and the curStitchCount
     */
    @Contract(pure=true)
    private double estimateWidth(int curStitchCount) {
        return ((double)curStitchCount)/this.texture.stitchesPerInch;
    }
    /**
     * set stitchCount data to match the neighbors above this patch
     */
    private void setStitchesToMatchTopNeighbors(){
        this.setStitchesToMatchNeighbors(this.topNeighbors(), true);
    }
    /**
     * set stitchCount data to match the neighbors below this patch
     */
    private void setStitchesToMatchBottomNeighbors(){
        this.setStitchesToMatchNeighbors(this.bottomNeighbors(), false);
    }
    /**
     * set stitchCount data to match the neighbors
     * @param neighbors the neighbors to propegate from
     * @param topNeighbors true if the neighbor set is above the patch
     */
    private void setStitchesToMatchNeighbors(@NotNull Collection<Patch> neighbors, boolean topNeighbors){
        this.edgeStitchCount =0;
        for(Patch patch: neighbors){
            assert patch.stitchAdjusted;
            int overLappingWidth = this.cellOverLappingWidth(patch);
            double widthFraction = ((double)overLappingWidth)/((double)patch.position.cellWidth());
            double exactCount = widthFraction*patch.edgeStitchCount;
            int count = (int) exactCount;
            double inexact = exactCount-count;
            if(inexact==.5){
                if(PatchPosition.bottomComparator.compare(patch.position, this.position)>0) {//if patch is a below neighbor
                    if(patch.roundStitchCountUpWhenPropegatingFromLowerNeighbors)
                        inexact += .01;
                    patch.roundStitchCountUpWhenPropegatingFromLowerNeighbors = !patch.roundStitchCountUpWhenPropegatingFromLowerNeighbors;
                }else{//if patch is a above neighbor
                    if(patch.roundStitchCountUpWhenPropegatingFromUpperNeighbors)
                        inexact += .01;
                    patch.roundStitchCountUpWhenPropegatingFromUpperNeighbors = !patch.roundStitchCountUpWhenPropegatingFromUpperNeighbors;
                }
            }
            if(inexact>.5)
                count++;//round up
            if(topNeighbors) {
                this.dedicatedStitchesToTopNeighbor.put(patch, count);
                patch.dedicatedStitchesToBotNeighbor.put(this, count);
            }else {
                this.dedicatedStitchesToBotNeighbor.put(patch, count);
                patch.dedicatedStitchesToTopNeighbor.put(this, count);
            }
            this.edgeStitchCount += count;//recurse
        }
        if(this.significance==0)//if the patch is insignificant then just set the body count and remove the glue joints.
            this.optBodyStitchCount = this.edgeStitchCount;
    }
    /**
     * sets default course dimensions based on height
     */
    private void setToMatchHeight(){
        double height = this.position.height();
        this.lineCount = this.texture.idealLineCount(height);
        if(this.lineCount%2 !=0) {
            if (this.addEvenLine)
                this.lineCount++;
            else
                this.lineCount--;
            this.addEvenLine = !this.addEvenLine;
        }
    }
    /**
     * set course count data to match the neighbors to the left of this patch
     */
    private void setLinesToMatchLeftNeighbors(){
        this.setLinesToMatchNeighbors(this.leftNeighbors(), true);
    }
    /**
     * set course count data to match the neighbors to the right of this patch
     */
    private void setLinesToMatchRightNeighbors(){
        this.setLinesToMatchNeighbors(this.rightNeighbors(), false);
    }
    /**
     * set course count data to match the neighbors
     * @param neighbors the neighbors to propegate from
     * @param fromLeft true if the neighbors to match are on the left
     */
    private void setLinesToMatchNeighbors(@NotNull Collection<Patch> neighbors, boolean fromLeft){
        this.lineCount =0;
        for(Patch patch: neighbors){
            int overlappingHeight = this.cellOverlappingHeight(patch);
            int patchHeight = patch.position.cellHeight();
            double heightFraction = ((double)overlappingHeight)/ ((double)patchHeight);
            double exactCount = heightFraction*patch.lineCount;
            int count = (int) exactCount;
            double inexact = exactCount-count;
            if(inexact==.5){
                if(PatchPosition.rightComparator.compare(patch.position, this.position)>0) {//if patch is a right of neighbor
                    if(patch.roundLineCountUpWhenPropegatingFromRightNeighbors)
                        inexact += .01;
                    patch.roundLineCountUpWhenPropegatingFromRightNeighbors = !patch.roundLineCountUpWhenPropegatingFromRightNeighbors;
                }else{//if patch is a left of neighbor
                    if(patch.roundLineCountUpWhenPropegatingFromLeftNeighbors)
                        inexact += .01;
                    patch.roundLineCountUpWhenPropegatingFromLeftNeighbors = !patch.roundLineCountUpWhenPropegatingFromLeftNeighbors;
                }
            }
            if(inexact>.5)
                count++;//round up
            if(count%2!=0) {
                if(!fromLeft) {//not even set of WS RS
                    if (patch.addEvenLineRight)
                        count++;
                    else
                        count--;
                    patch.addEvenLineRight = !patch.addEvenLineRight;
                }else{
                    if (patch.addEvenLineLeft)
                        count++;
                    else
                        count--;
                    patch.addEvenLineLeft = !patch.addEvenLineLeft;
                }
            }
            this.lineCount+= count;
            assert this.lineCount%2==0;
        }
    }
    /**
     * adjusts the stitches of this patch to be set from neighbors or set on its own, then propegates those changes
     * @return a set of patches that were effected by the propegation process
     */
    @Nonnull
    public LinkedHashSet<Patch> adjustStitches(){
        return this.adjustStitches(new LinkedHashSet<>());
    }
    @Nonnull
    private LinkedHashSet<Patch> adjustStitches(@Nonnull LinkedHashSet<Patch> effectedPatches){
        if(this.stitchAdjusted || this.stitchAdjusting)
            return effectedPatches;
        this.stitchAdjusting = true;
        boolean topPartial = this.topNeighborsPartiallyAdjusted();
        boolean bottomPartial = this.bottomNeighborsPartiallyAdjusted();
        assert !(topPartial && bottomPartial);
        if(topPartial){
            if(this.topNeighborsNotFullyAdjusted()) {//need to  fill in failed neighbors
                for (Patch neighbor : Patch.patchesByStitchDominance(this.topNeighbors()))
                    effectedPatches.addAll(neighbor.adjustStitches(effectedPatches));
                if (this.topNeighborsNotFullyAdjusted()) {//one of the neighbors is being adjusted, so return and propegate this up the stack
                    this.stitchAdjusting = false;
                    return effectedPatches;
                }
            }
            this.setStitchesToMatchTopNeighbors();
            this.stitchAdjusted = true;
            effectedPatches.addAll(this.propagateDown(effectedPatches));
        } else if(bottomPartial){
            if(this.bottomNeighborsNotFullyAdjusted()) {
                for (Patch neighbor : Patch.patchesByStitchDominance(this.bottomNeighbors()))
                    effectedPatches.addAll(neighbor.adjustStitches(effectedPatches));
                if (this.bottomNeighborsNotFullyAdjusted()) {//one of the neighbors is being adjusted, so return and propegate this up the stack
                    this.stitchAdjusting = false;
                    return effectedPatches;
                }
            }
            this.setStitchesToMatchBottomNeighbors();
            this.stitchAdjusted = true;
            effectedPatches.addAll(this.propagateUp(effectedPatches));
        }else {
            LinkedHashSet<Patch> adjustedInCols = this.getAdjustedPatchesInColumns();
            if(adjustedInCols.isEmpty()) {
                this.setToMatchWidth();
                this.stitchAdjusted = true;
                effectedPatches.addAll(this.propagateStitches(effectedPatches));
            } else {
                this.stitchAdjusting = false;
                return effectedPatches;
            }
        }
        this.stitchAdjusting = false;
        effectedPatches.add(this);
        return effectedPatches;
    }
    /**
     * propagate the stitch stitchCount of this patch down and upward
     * @param effectedPatches the set of patches that have all been effected
     * @return effectedPatches from this propagation
     */
    @Nonnull
    private LinkedHashSet<Patch> propagateStitches(@Nonnull LinkedHashSet<Patch> effectedPatches){
        TreeSet<Patch> neighbors = Patch.patchesByStitchDominance(this.topNeighbors());
        neighbors.addAll(this.bottomNeighbors());
        return Patch.propagateStitchesToNeighbors(effectedPatches, neighbors);
    }
    /**
     * propagate the stitch stitchCount of this patch
     * @param effectedPatches the set of patches that have all been effected
     * @param neighbors the neighbors to propagate to
     * @return effectedPatches from this propagation
     */
    @Nonnull
    private static LinkedHashSet<Patch> propagateStitchesToNeighbors(@NotNull LinkedHashSet<Patch> effectedPatches, @NotNull Collection<Patch> neighbors){
        for(Patch neighbor: neighbors)
            effectedPatches.addAll(neighbor.adjustStitches(effectedPatches));
        return effectedPatches;
    }
    /**
     * propagate the stitch stitchCount of this patch downward
     * @param effectedPatches the set of patches that have all been effected
     * @return effectedPatches from this propagation
     */
    @Nonnull
    private LinkedHashSet<Patch> propagateDown(@Nonnull LinkedHashSet<Patch> effectedPatches){
        return Patch.propagateStitchesToNeighbors(effectedPatches, this.bottomNeighbors());
    }
    /**
     * propagate the stitch stitchCount of this patch upward
     * @param effectedPatches the set of patches that have all been effected
     * @return effectedPatches from this propagation
     */
    @Nonnull
    private LinkedHashSet<Patch> propagateUp(@Nonnull LinkedHashSet<Patch> effectedPatches){
        return Patch.propagateStitchesToNeighbors(effectedPatches, this.topNeighbors());
    }
    /**
     * @return the set of patches above this that have been predefined, constraining this patch
     */
    @Nonnull
    private LinkedHashSet<Patch> adjustedNeighborsInAboveColumns(){
        LinkedHashSet<Patch> abovePatches = new LinkedHashSet<>();
        if(this.position.topLeft.isTop())
            return abovePatches;
        Cell aboveTLCell = this.position.topLeft.aboveCell();
        assert aboveTLCell != null;
        Patch cellPatch = this.panel.getPatchOnCell(aboveTLCell);
        assert cellPatch != null;
        if(cellPatch.stitchAdjusted)
            abovePatches.add(cellPatch);
        for (Iterator<Cell> it1 = aboveTLCell.upColumnIterator(); it1.hasNext(); ){
            cellPatch = this.panel.getPatchOnCell(it1.next());
            assert cellPatch != null;
            if(cellPatch.stitchAdjusted)
                abovePatches.add(cellPatch);
        }
        for (Iterator<Cell> it = aboveTLCell.rightRowIterator(); it.hasNext(); ) {
            Cell aboveNeighborCell = it.next();
            if(Cell.rightMost.compare(aboveNeighborCell, this.position.bottomRight)>0)
                break;
            cellPatch = this.panel.getPatchOnCell(aboveNeighborCell);
            assert cellPatch != null;
            if(cellPatch.stitchAdjusted)
                abovePatches.add(cellPatch);
            for (Iterator<Cell> it1 = aboveNeighborCell.upColumnIterator(); it1.hasNext(); ){
                cellPatch = this.panel.getPatchOnCell(it1.next());
                assert cellPatch != null;
                if(cellPatch.stitchAdjusted)
                    abovePatches.add(cellPatch);
            }
        }
        return abovePatches;
    }
    /**
     * @return the set of patches below this that have been predefined, constraining this patch
     */
    @Nonnull
    private LinkedHashSet<Patch> adjustedNeighborsInBelowColumns(){
        LinkedHashSet<Patch> belowPatches = new LinkedHashSet<>();
        if(this.position.bottomRight.isBottom())
            return belowPatches;
        Cell belowBRCell = this.position.topLeft.belowCell();
        assert belowBRCell != null;
        Patch cellPatch = this.panel.getPatchOnCell(belowBRCell);
        assert cellPatch != null;
        if(cellPatch.stitchAdjusted)
            belowPatches.add(cellPatch);
        for (Iterator<Cell> it1 = belowBRCell.downColumnIterator(); it1.hasNext(); ){
            cellPatch = this.panel.getPatchOnCell(it1.next());
            assert cellPatch != null;
            if(cellPatch.stitchAdjusted)
                belowPatches.add(cellPatch);
        }
        for (Iterator<Cell> it = belowBRCell.leftRowIterator(); it.hasNext(); ) {
            Cell belowNeighborCell = it.next();
            if(Cell.leftMost.compare(belowNeighborCell, this.position.topLeft)>0)
                break;
            cellPatch = this.panel.getPatchOnCell(belowNeighborCell);
            assert cellPatch != null;
            if(cellPatch.stitchAdjusted)
                belowPatches.add(cellPatch);
            for (Iterator<Cell> it1 = belowNeighborCell.downColumnIterator(); it1.hasNext(); ){
                cellPatch = this.panel.getPatchOnCell(it1.next());
                assert cellPatch != null;
                if(cellPatch.stitchAdjusted)
                    belowPatches.add(cellPatch);
            }
        }
        return belowPatches;
    }
    /**
     * @return the set of patches left this that have been predefined, constraining this patch
     */
    @Nonnull
    private LinkedHashSet<Patch> adjustedNeighborsInLeftRows(){
        LinkedHashSet<Patch> leftPatches = new LinkedHashSet<>();
        if(this.position.topLeft.isLeft())
            return leftPatches;
        Cell leftTLCell = this.position.topLeft.leftCell();
        assert leftTLCell != null;
        Patch cellPatch = this.panel.getPatchOnCell(leftTLCell);
        assert cellPatch != null;
        if(cellPatch.lineAdjusted)
            leftPatches.add(cellPatch);
        for (Iterator<Cell> it1 = leftTLCell.leftRowIterator(); it1.hasNext(); ){
            cellPatch = this.panel.getPatchOnCell(it1.next());
            assert cellPatch != null;
            if(cellPatch.lineAdjusted)
                leftPatches.add(cellPatch);
        }

        for (Iterator<Cell> it = leftTLCell.downColumnIterator(); it.hasNext(); ) {
            Cell leftNeighborCell = it.next();

            if(Cell.bottomMost.compare(leftNeighborCell, this.position.bottomRight)>0)
                break;
            cellPatch = this.panel.getPatchOnCell(leftNeighborCell);
            assert cellPatch != null;
            if(cellPatch.lineAdjusted)
                leftPatches.add(cellPatch);
            for (Iterator<Cell> it1 = leftNeighborCell.leftRowIterator(); it1.hasNext(); ){
                cellPatch = this.panel.getPatchOnCell(it1.next());
                assert cellPatch != null;
                if(cellPatch.lineAdjusted)
                    leftPatches.add(cellPatch);
            }
        }
        return leftPatches;
    }
    /**
     * @return the set of patches right this that have been predefined, constraining this patch
     */
    @Nonnull
    private LinkedHashSet<Patch> adjustedNeighborsInRightRows(){
        LinkedHashSet<Patch> rightPatches = new LinkedHashSet<>();
        if(this.position.bottomRight.isRight())
            return rightPatches;
        Cell rightBRCell = this.position.bottomRight.rightCell();
        assert rightBRCell != null;
        Patch cellPatch = this.panel.getPatchOnCell(rightBRCell);
        assert cellPatch != null;
        if(cellPatch.lineAdjusted)
            rightPatches.add(cellPatch);
        for (Iterator<Cell> it1 = rightBRCell.rightRowIterator(); it1.hasNext(); ){
            cellPatch = this.panel.getPatchOnCell(it1.next());
            assert cellPatch != null;
            if(cellPatch.lineAdjusted)
                rightPatches.add(cellPatch);
        }

        for (Iterator<Cell> it = rightBRCell.upColumnIterator(); it.hasNext(); ) {
            Cell rightNeighborCell = it.next();
            if(Cell.topMost.compare(rightNeighborCell, this.position.topLeft)>0)
                break;
            cellPatch = this.panel.getPatchOnCell(rightNeighborCell);
            assert cellPatch != null;
            if(cellPatch.lineAdjusted)
                rightPatches.add(cellPatch);
            for (Iterator<Cell> it1 = rightNeighborCell.rightRowIterator(); it1.hasNext(); ){
                cellPatch = this.panel.getPatchOnCell(it1.next());
                assert cellPatch != null;
                if(cellPatch.lineAdjusted)
                    rightPatches.add(cellPatch);
            }
        }
        return rightPatches;
    }
    /**
     * @return the set of patches above and below this that have been predefined, constraining this patch
     */
    @Nonnull
    @Contract(pure=true)
    private LinkedHashSet<Patch> getAdjustedPatchesInColumns(){
        LinkedHashSet<Patch> patches = this.adjustedNeighborsInAboveColumns();
        patches.addAll(this.adjustedNeighborsInBelowColumns());
        return patches;
    }
    /**
     * @return the set of patches to the left and right of this that have been predefined, constraining this patch
     */
    @Nonnull
    @Contract(pure=true)
    private LinkedHashSet<Patch> getAdjustedPatchesInRows(){
        LinkedHashSet<Patch> patches = this.adjustedNeighborsInLeftRows();
        patches.addAll(this.adjustedNeighborsInRightRows());
        return patches;
    }
    /**
     * adjust the course count in this patch, propegating it forward patches who neighbor this.
     * @return the set of patches that have been effected thus far on the stack.
     */
    @Nonnull
    public LinkedHashSet<Patch> adjustLines(){
        return this.adjustLines(new LinkedHashSet<>());
    }
    @Nonnull
    private LinkedHashSet<Patch> adjustLines(@Nonnull LinkedHashSet<Patch> effectedPatches){
        if(this.lineAdjusted || this.lineAdjusting)
            return effectedPatches;
        this.lineAdjusting = true;
        boolean leftPartial = this.leftNeighborsPartiallyAdjusted();
        boolean rightPartial = this.rightNeighborsPartiallyAdjusted();
        assert !(leftPartial && rightPartial);
        if(leftPartial){
            if(this.leftNeighborsNotFullyAdjusted()){//need to  fill in failed neighbors
                TreeSet<Patch> leftNeighbors = Patch.patchesByLineDominance(this.leftNeighbors());
                for(Patch neighbor: leftNeighbors)
                    effectedPatches.addAll(neighbor.adjustLines(effectedPatches));
                if (this.leftNeighborsNotFullyAdjusted()) {//one of the neighbors is being adjusted, so return and propegate this up the stack
                    this.lineAdjusting = false;
                    return effectedPatches;
                }
            }
            this.setLinesToMatchLeftNeighbors();
            this.lineAdjusted = true;
            effectedPatches.addAll(this.propagateRight(effectedPatches));
        } else if(rightPartial){
            if(this.rightNeighborsNotFullyAdjusted()){
                for(Patch neighbor: Patch.patchesByLineDominance(this.rightNeighbors()))
                    effectedPatches.addAll(neighbor.adjustLines(effectedPatches));
                if (this.rightNeighborsNotFullyAdjusted()) {//one of the neighbors is being adjusted, so return and propegate this up the stack
                    this.lineAdjusting = false;
                    return effectedPatches;
                }
            }
            this.setLinesToMatchRightNeighbors();
            this.lineAdjusted = true;
            effectedPatches.addAll(this.propagateLeft(effectedPatches));
        }else {
            LinkedHashSet<Patch> adjustedInRows = this.getAdjustedPatchesInRows();
            if(adjustedInRows.isEmpty()) {
                this.setToMatchHeight();
                this.lineAdjusted = true;
                effectedPatches.addAll(this.propagateLines(effectedPatches));
            } else {
                this.lineAdjusting = false;
                return effectedPatches;
            }
        }
        this.lineAdjusting = false;
        effectedPatches.add(this);
        return effectedPatches;
    }
    /**
     * propegates the course count to neighboring patches
     * @param effectedPatches the patches that have already been impacted
     * @param neighbors the neighbors to propegate course count too, note that these must be neighbors
     * @return the set of patches that have been impacted thus far on the stack
     */
    @Nonnull
    private static LinkedHashSet<Patch> propagateLinesToNeighbors(@Nonnull LinkedHashSet<Patch> effectedPatches, @Nonnull Collection<Patch> neighbors){
        for(Patch neighbor: neighbors)
            effectedPatches.addAll(neighbor.adjustLines(effectedPatches));
        return effectedPatches;
    }
    /**
     * propegates the course count to all left and right neighboring patches
     * @param effectedPatches the patches that have already been impacted
     * @return the set of patches that have been impacted thus far on the stack
     */
    @Nonnull
    private LinkedHashSet<Patch> propagateLines(@Nonnull LinkedHashSet<Patch> effectedPatches){
        TreeSet<Patch> neighbors = Patch.patchesByLineDominance(this.leftNeighbors());
        neighbors.addAll(this.rightNeighbors());
        return Patch.propagateLinesToNeighbors(effectedPatches, neighbors);
    }
    /**
     * propegates the course count to all left neighboring patches
     * @param effectedPatches the patches that have already been impacted
     * @return the set of patches that have been impacted thus far on the stack
     */
    @Nonnull
    private LinkedHashSet<Patch> propagateLeft(@Nonnull LinkedHashSet<Patch> effectedPatches){
        return Patch.propagateLinesToNeighbors(effectedPatches, this.leftNeighbors());
    }
    /**
     * propegates the course count to all right neighboring patches
     * @param effectedPatches the patches that have already been impacted
     * @return the set of patches that have been impacted thus far on the stack
     */
    @Nonnull
    private LinkedHashSet<Patch> propagateRight(@Nonnull LinkedHashSet<Patch> effectedPatches){
        return Patch.propagateLinesToNeighbors(effectedPatches, this.rightNeighbors());
    }
    /**
     * @return an ordered set of patches that neighbor this patch to the left
     */
    @NotNull
    @Contract(pure=true)
    private TreeSet<Patch> leftNeighbors(){
        TreeSet<Patch> neighbors = new TreeSet<>();
        if(!this.position.topLeft.isLeft()) {
            Cell leftNeighbor = this.position.topLeft.leftCell();
            assert leftNeighbor != null;
            Patch neighborPatch = this.panel.getPatchOnCell(leftNeighbor);
            if(neighborPatch != null)
                neighbors.add(neighborPatch);
            for (Iterator<Cell> it = leftNeighbor.downColumnIterator(); it.hasNext(); ) {
                Cell neighbor = it.next();
                if(Cell.yPosComparator.compare(this.position.bottomRight, neighbor)>0)//if cell is below this base
                    break;
                neighborPatch = this.panel.getPatchOnCell(neighbor);
                if(neighborPatch != null)
                    neighbors.add(neighborPatch);
            }
        }
        return neighbors;
    }
    /**
     * @return an ordered set of patches that neighbor this patch to the right
     */
    @NotNull
    @Contract(pure=true)
    private TreeSet<Patch> rightNeighbors(){
        TreeSet<Patch> neighbors = new TreeSet<>();
        if(!this.position.bottomRight.isRight()) {
            Cell rightNeighbor = this.position.bottomRight.rightCell();
            assert rightNeighbor != null;
            Patch neighborPatch = this.panel.getPatchOnCell(rightNeighbor);
            if(neighborPatch != null)
                neighbors.add(neighborPatch);
            for (Iterator<Cell> it = rightNeighbor.upColumnIterator(); it.hasNext(); ) {
                Cell neighbor = it.next();
                if(Cell.yPosComparator.compare(this.position.topLeft, neighbor)<0)//if cell is below this base
                    break;
                neighborPatch = this.panel.getPatchOnCell(neighbor);
                if(neighborPatch != null)
                    neighbors.add(neighborPatch);
            }
        }
        return neighbors;
    }
    /**
     * @return an ordered set of patches that neighbor below this patch
     */
    @NotNull
    @Contract(pure=true)
    public TreeSet<Patch> bottomNeighbors(){
        TreeSet<Patch> neighbors = new TreeSet<>();
        if(!this.position.bottomRight.isBottom()) {
            Cell bottomNeighbor = this.position.bottomRight.belowCell();
            assert bottomNeighbor != null;
            Patch neighborPatch = this.panel.getPatchOnCell(bottomNeighbor);
            if(neighborPatch != null)
                neighbors.add(neighborPatch);
            for (Iterator<Cell> it = bottomNeighbor.leftRowIterator(); it.hasNext(); ) {
                Cell neighbor = it.next();
                if(Cell.xPosComparator.compare(this.position.topLeft, neighbor)<0)//if cell is below this base
                    break;
                neighborPatch = this.panel.getPatchOnCell(neighbor);
                if(neighborPatch != null)
                    neighbors.add(neighborPatch);
            }
        }
        return neighbors;
    }
    /**
     * @return an ordered set of patches that neighbor above this patch
     */
    @NotNull
    @Contract(pure=true)
    public TreeSet<Patch> topNeighbors(){
        TreeSet<Patch> neighbors = new TreeSet<>();
        if(!this.position.topLeft.isTop()) {
            Cell topNeighbor = this.position.topLeft.aboveCell();
            assert topNeighbor != null;
            Patch neighborPatch = this.panel.getPatchOnCell(topNeighbor);
            if(neighborPatch != null)
                neighbors.add(neighborPatch);
            for (Iterator<Cell> it = topNeighbor.rightRowIterator(); it.hasNext(); ) {
                Cell neighbor = it.next();
                if(Cell.xPosComparator.compare(this.position.bottomRight, neighbor)>0)//if cell is below this base
                    break;
                neighborPatch = this.panel.getPatchOnCell(neighbor);
                if(neighborPatch != null)
                    neighbors.add(neighborPatch);
            }
        }
        return neighbors;
    }
    /**
     * @param patch the patch to compare overlap to
     * @return the number of cells that overlap between this and patch by width
     */
    @Contract(pure=true)
    private int cellOverLappingWidth(@NotNull Patch patch){
        Cell start;
        Cell end;
        if(PatchPosition.rightComparator.compare(this.position, patch.position)<0)//if this is further left than the patch
            start = patch.position.bottomRight;
        else
            start = this.position.bottomRight;
        if(PatchPosition.leftComparator.compare(this.position, patch.position)<0)//if this is further left than the patch
            end = this.position.topLeft;
        else
            end = patch.position.topLeft;
        return end.getX()-start.getX()+1;
    }
    /**
     * @param patch the patch to compare overlap to
     * @return the number of cells that overlap between this and patch by height
     */
    @Contract(pure=true)
    private int cellOverlappingHeight(@NotNull Patch patch){
        Cell start;
        Cell end;
        if(PatchPosition.bottomComparator.compare(this.position, patch.position)<0)//if this is lower than the patch
            start = patch.position.bottomRight;
        else
            start = this.position.bottomRight;
        if(PatchPosition.topComparator.compare(this.position, patch.position)<0)//if this is lower than the patch
            end = this.position.topLeft;
        else
            end = patch.position.topLeft;
        return end.getY()- start.getY()+1;
    }
    /**
     * @param stitchCount the number of stitches to make the patch from
     * @return the error introduced by texture carving to that amount of stitches
     */
    @Contract(pure=true)
    private double textureCarvingError(int stitchCount){
        int seamCarvingDelta = this.texture.stitchDelta(stitchCount);
        if(seamCarvingDelta==0)
            return 0.0;
        return this.textureCarvingError(stitchCount, seamCarvingDelta);
    }
    /**
     * @param stitchCount the number of stitches to make the patch from
     * @param seamCarvingDelta the number of carves to make from this patch
     * @return the error introduced by texture carving to that amount of stitches
     */
    @Contract(pure=true)
    private double textureCarvingError(double stitchCount, double seamCarvingDelta) {
        return ((seamCarvingDelta * seamCarvingDelta)/ stitchCount)*Patch.TEXTURE_CARVING_WEIGHT*this.significance;
    }
    /**
     * @param adjustedWidth the width resulting from the actual stitch count
     * @return the error introduced by variance from the desired width
     */
    @Contract(pure=true)
    private double widthDeltaError(double adjustedWidth){
        double errRoot = this.position.width() - adjustedWidth;
        return this.significance*errRoot*errRoot*Patch.WIDTH_WEIGHT;
    }
    /**
     * @return the number of loops to be added (negative) or subtracted (positive) to the outgoing edges
     */
    @Contract(pure=true)
    private int glueJointDelta(){
        return this.optBodyStitchCount-this.edgeStitchCount;
    }
    /**
     * @return the number of loops added (negative) or subtracted (positive) from the top edge
     */
    @Contract(pure=true)
    private int glueJointTopDelta(){
        return this.optBodyStitchCount - this.topEdgeLoopCount;
    }
    /**
     * @return the number of loops added (negative) or subtracted (positive) from the bottom edge
     */
    @Contract(pure=true)
    private int glueJointBotDelta(){
        return this.optBodyStitchCount - this.botEdgeLoopCount;
    }
    /**
     * @param joints the number of loops being added/subtracted from edges
     * @return the error introduced by adding glue joints to the patch
     */
    @Contract(pure=true)
    private double glueJointError(double joints) {
        if(joints ==0.0)
            return 0.0;
        return ((joints*joints)/((double)this.edgeStitchCount))*Patch.GLUE_JOINT_WEIGHT*this.significance;
    }
    @Override
    public int compareTo(@NotNull Patch o) {
        return Patch.positionComparatorXThenY.compare(this, o);
    }
    private static Comparator<Patch> positionComparatorXThenY = Comparator.comparing(o -> o.position);
    /**
     * which patch has a dominant stitch count. Decided by how  stretchy it is (stretchy==less dominant) then its width
     */
    private static Comparator<Patch> stitchDominance = (o1, o2) -> {
        int compare = Integer.compare(o1.significance, o2.significance);
        if(compare == 0){
            compare = PatchPosition.widthComparator.compare(o1.position, o2.position);
            if(compare == 0) {
                compare= PatchPosition.heightComparator.compare(o1.position, o2.position);
                if (compare == 0) {
                    compare = PatchPosition.rightComparator.compare(o1.position, o2.position);
                    if (compare == 0) {
                        compare = PatchPosition.topComparator.compare(o1.position, o2.position);
                        if (compare == 0) {
                            compare = PatchPosition.leftComparator.compare(o1.position, o2.position);
                            if (compare == 0)
                                return PatchPosition.bottomComparator.compare(o1.position, o2.position);
                        }
                    }
                }
            }
        }
        return compare;
    };
    public static TreeSet<Patch> patchesByStitchDominance(@NotNull Collection<Patch> patches){
        TreeSet<Patch> sortedPatches =new TreeSet<>(Patch.reverseStitchDominance);
        sortedPatches.addAll(patches);
        return sortedPatches;
    }
    public static TreeSet<Patch> patchesByLineDominance(@NotNull Collection<Patch> patches){
        TreeSet<Patch> sortedPatches =new TreeSet<>(Patch.reverseLineDominance);
        sortedPatches.addAll(patches);
        return sortedPatches;
    }
    /**
     * which patch has a dominant course count, Decided by how stretchy it is then its height
     */
    private static Comparator<Patch> lineDominance = (o1, o2) -> {
        int compare = Integer.compare(o1.significance, o2.significance);
        if(compare == 0){
            compare = PatchPosition.heightComparator.compare(o1.position, o2.position);
            if(compare == 0) {
                compare= PatchPosition.widthComparator.compare(o1.position, o2.position);
                if (compare == 0) {
                    compare = PatchPosition.bottomComparator.compare(o1.position, o2.position);
                    if (compare == 0) {
                        compare = PatchPosition.topComparator.compare(o1.position, o2.position);
                        if (compare == 0) {
                            compare = PatchPosition.leftComparator.compare(o1.position, o2.position);
                            if (compare == 0)
                                return PatchPosition.rightComparator.compare(o1.position, o2.position);
                        }
                    }
                }
            }
        }
        return compare;
    };
    private static Comparator<Patch> reverseLineDominance = (o1, o2) -> Patch.lineDominance.compare(o2, o1);
    private static Comparator<Patch> reverseStitchDominance = (o1, o2) -> Patch.stitchDominance.compare(o2, o1);
    @NotNull
    @Override
    public Iterator<Cell> iterator() {
        return new CellIterator();
    }
    public class CellIterator implements Iterator<Cell>{
        Cell rightInRow;
        Cell lastCell;
        CellIterator(){
            this.rightInRow = position.bottomRight;
            this.lastCell = this.rightInRow;
        }
        @Override
        public boolean hasNext() {return !this.lastCell.isLeft() || !this.rightInRow.isTop();}
        @Override
        public Cell next() {
            if(!this.lastCell.isLeft())
                return this.lastCell = this.lastCell.leftCell();
            else {
                assert !this.rightInRow.isTop();
                return this.rightInRow= this.lastCell = this.rightInRow.aboveCell();
            }
        }
    }
    @Override
    public String toString(){ return this.texture.toString()+"_"+this.position.toString()+"_s("+this.significance+")_wb("+this.optBodyStitchCount+")_we("+this.edgeStitchCount+",t"+this.topEdgeLoopCount+",b"+this.botEdgeLoopCount+")_h("+this.lineCount+")";}
}
