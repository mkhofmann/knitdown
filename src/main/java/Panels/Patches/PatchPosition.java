package Panels.Patches;

import Panels.Panel;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * Created by Megan on 4/26/2017.
 * the rectangular patch positioning on a panel
 */
public class PatchPosition implements Comparable<PatchPosition>, Iterable<Cell>{
    /**
     * the cell at the lower right corner of the position
     */
    Cell bottomRight;
    /**
     * the cell at the upper left corner of the position
     */
    Cell topLeft;
    /**
     * the panel that heald this position
     */
    Panel panel;
    /**
     * The set of cells in the panel that are covered by this
     */
    private TreeSet<Cell> coveredCells = new TreeSet<>();
    /**
     * @param brX the width index of the bottom right cell
     * @param brY the height index of the bottom right cell
     * @param width the actual width of the cell
     * @param height the actual height of the cell
     * @param panel the assigned panel
     */
    public PatchPosition(double brX, double brY, double width, double height,@NotNull Panel panel){
        this.panel = panel;
        this.bottomRight = this.panel.getClosestCell(brX, brY);
        this.topLeft = this.panel.getClosestCell(this.bottomRight.getRealX()+width,this.bottomRight.getRealY()+height);
        this.coverCells();
    }
    /**
     * makes the interming cells in this patch as covered on the panel
     */
    private void coverCells(){
        this.coveredCells.add(this.bottomRight);
        for (Iterator<Cell> it = this.bottomRight.leftRowIterator(); it.hasNext(); ) {
            Cell bottomRowCell = it.next();
            if(Cell.xPosComparator.compare(bottomRowCell, topLeft)>0)//past left side
                break;
            coveredCells.add(bottomRowCell);
        }
        for (Iterator<Cell> it = this.bottomRight.upColumnIterator(); it.hasNext(); ) {
            Cell rightSideCell = it.next();
            if(Cell.yPosComparator.compare(rightSideCell, topLeft)>0)
                break;
            coveredCells.add(rightSideCell);
            for (Iterator<Cell> it1 = rightSideCell.leftRowIterator(); it1.hasNext(); ) {
                Cell rowCell = it1.next();
                if(Cell.xPosComparator.compare(rowCell, topLeft)>0)//past left side
                    break;
                coveredCells.add(rowCell);
            }
        }
    }
    /**
     * @return the width in real world units of this patch position
     */
    @Contract(pure = true)
    public double width(){
        double inchX = this.topLeft.getRealX();
        double inchX1 = this.bottomRight.getRealX();
        return inchX - inchX1 +(1.0/(double) Panel.CELL_PER_UNIT);
    }
    /**
     * @return the number of cells width wise
     */
    @Contract(pure = true)
    int cellWidth(){
        int left = this.topLeft.getX();
        int right = this.bottomRight.getX();
        return left-right+1;
    }
    /**
     * @return the height in inches of this patch position
     */
    @Contract(pure = true)
    double height(){
        double inchYT = this.topLeft.getRealY();
        double inchYB = this.bottomRight.getRealY();
        return inchYT - inchYB +(1.0/(double) Panel.CELL_PER_UNIT);
    }

    /**
     * @return the number of cells height wise
     */
    @Contract(pure = true)
    int cellHeight(){return this.topLeft.getY()-this.bottomRight.getY()+1;}
    static Comparator<PatchPosition> widthComparator = Comparator.comparingDouble(PatchPosition::width);
    static Comparator<PatchPosition> heightComparator = Comparator.comparingDouble(PatchPosition::height);
    static Comparator<PatchPosition> bottomComparator = (o1, o2) -> Cell.yPosComparator.compare(o1.bottomRight, o2.bottomRight);
    static Comparator<PatchPosition> topComparator = (o1, o2) -> Cell.yPosComparator.compare(o1.topLeft, o2.topLeft);
    static Comparator<PatchPosition> rightComparator = (o1, o2) -> Cell.xPosComparator.compare(o1.bottomRight, o2.bottomRight);
    static Comparator<PatchPosition> leftComparator = (o1, o2) -> Cell.xPosComparator.compare(o1.topLeft, o2.topLeft);
    private static Comparator<PatchPosition> xThenY = (o1, o2) -> {
        int compare = PatchPosition.rightComparator.compare(o1, o2);
        if(compare ==0){
            compare = PatchPosition.bottomComparator.compare(o1,o2);
            if(compare==0){
                compare= PatchPosition.leftComparator.compare(o1, o2);
                if(compare==0)
                    compare=PatchPosition.topComparator.compare(o1, o2);
            }
        }
        return compare;
    };
    /**
     * compare based on bottom right corner, then top left
     * @param o patch position to be compared to
     * @return <0 if this is closer to origin than o, or >0 if its closer to the top left corner.
     */
    @Override
    public int compareTo(@NotNull PatchPosition o) {return PatchPosition.xThenY.compare(this, o);}
    @NotNull
    @Override
    public Iterator<Cell> iterator() {return this.coveredCells.iterator();}
    @Override
    public String toString(){return "["+bottomRight+":"+topLeft+"]";}
    @Override
    public boolean equals(Object o){
        if(o instanceof PatchPosition){
            PatchPosition other = (PatchPosition) o;
            return other.bottomRight.equals(this.bottomRight) && other.topLeft.equals(this.topLeft);
        }
        return false;
    }
}
