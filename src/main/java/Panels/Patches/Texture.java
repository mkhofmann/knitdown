package Panels.Patches;

import CompilerExceptions.ParsingExceptions.InsufficientLoopsException;
import CompilerExceptions.StitchManipulationExceptions.NoTextureCarvesAvailableException;
import TextureDefinition.TextureDefinition;
import TextureGraph.TextureBuiltInRow;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

/**
 * Created by Megan on 4/26/2017.
 * heald the abstract textureGraph structure to build textureGraph graphs in a stitchCount patch from.
 */
public class Texture {
    /**
     * the stitches per inch when not stretched of the texture
     */
    public double stitchesPerInch;
    /**
     * the courses per inch when not streitch of the texture
     */
    private double linesPerInch;
    /**
     * the texture definition that describes what will fill the patch of this texture
     */
    private TextureDefinition textureDefinition;

    /**
     * @param textureDefinition the texture definition of this panel
     * @param stitchesPerInch the stitches per inch measured gauge of this texture
     * @param linesPerInch the courses per inch measured gauge of this texture
     */
    public Texture(@NotNull TextureDefinition textureDefinition, double stitchesPerInch, double linesPerInch){
        this.linesPerInch = linesPerInch;
        this.stitchesPerInch = stitchesPerInch;
        this.textureDefinition = textureDefinition;
    }

    /**
     * @param widthReps the number of reps widthwise of the texture definition
     * @param textureCarvingDelta the number of texture carves to be added to the texture
     * @param lineReps the repetitions of the pattern height wise
     * @param lineDelta the number of addition or subtracted courses
     * @param topEdgeLoopCount the number of loops leaving the resulting graph
     * @param botEdgeLoopCount the number of loops entering the resulting graph
     * @return a texture graph built in row fashion constructed to meet the specifications
     * @throws NoTextureCarvesAvailableException if there are no possible narrowed paths from this course
     */
    TextureBuiltInRow buildTexture(int widthReps, int textureCarvingDelta, int lineReps, int lineDelta, int topEdgeLoopCount, int botEdgeLoopCount) throws NoTextureCarvesAvailableException, InsufficientLoopsException {
        boolean noWidth = false;
        if(widthReps==0) {
            widthReps = 1;
            noWidth= true;
        }
        TextureBuiltInRow textureGraph = textureDefinition.buildGarmentGraphByWidthReps(widthReps, false);
        if(noWidth) textureCarvingDelta = textureCarvingDelta - textureGraph.getFirstStitchSetCount();
        if(lineReps==0)
            textureGraph.removeXCourses(textureGraph.getHeight() - lineDelta, true);
        else if(lineReps>1)
            textureGraph = this.textureDefinition.extendGarmentGraph(textureGraph,lineReps-1);
        if(lineDelta<0 && lineReps>0)
            textureGraph.removeXCourses(Math.abs(lineDelta), true);
        else if(lineDelta>0 && lineReps>0)
            textureGraph = this.textureDefinition.extendGarmentGraphPartialRep(textureGraph, lineDelta);
        if(textureCarvingDelta<0){//narrow
            assert Math.abs(textureCarvingDelta) < textureGraph.getFirstStitchSetCount();
            textureGraph.narrowTextureGraph(Math.abs(textureCarvingDelta), 0);
        } else if(textureCarvingDelta>0)//widen
            textureGraph.widenWithSTST(textureCarvingDelta, 0);
        int actualTopLoopCount = textureGraph.getLastLine().getChildLoops().size();
        if(actualTopLoopCount<topEdgeLoopCount)
            textureGraph.increaseTopEdge(topEdgeLoopCount - actualTopLoopCount);
        else if(actualTopLoopCount>topEdgeLoopCount)
            textureGraph.decreaseTopEdge(actualTopLoopCount-topEdgeLoopCount);
        int actualBotLoopCount = textureGraph.getFirstLine().getParentLoops().size();
        if(actualBotLoopCount<botEdgeLoopCount)
            textureGraph.decreaseBottomEdge(botEdgeLoopCount-actualBotLoopCount);
        else if(actualBotLoopCount>botEdgeLoopCount)
            textureGraph.increaseBottomEdge(actualBotLoopCount-botEdgeLoopCount);
        return textureGraph;
    }
    /**
     * @return the base repition count of this texture
     */
    @Contract(pure = true)
    public int repStitches(){return textureDefinition.getBottomLine().getRepLoopCount();}
    /**
     * @return the stitchCount of the border around the repeated texture
     */
    @Contract(pure = true)
    public int borderStitches(){return textureDefinition.getBottomLine().getBorderLoopCount();}
    /**
     * @return height of a repetition of the texture
     */
    @Contract(pure = true)
    private int repLines(){
        int height = textureDefinition.getHeight();
        if(height%2==1)
            height++;
        return height;}
    /**
     * @param idealWidth the desired width of this texture
     * @return the number of stitches needed to approximate this width
     */
    @Contract(pure = true)
    int idealStitchCount(double idealWidth){return (int) (idealWidth*this.stitchesPerInch);}
    /**
     * @param idealHeight the desired height of this texture
     * @return the number of courses needed to approximate this height
     */
    @Contract(pure = true)
    int idealLineCount(double idealHeight){
        double v = idealHeight * this.linesPerInch;
        return (int) v;
    }
    /**
     * @param idealStitchCount the number of stitches desired
     * @return the number of repetitions that best approximate the desired stitch count
     */
    @Contract(pure = true)
    int closestStitchRepCount(int idealStitchCount){
        int borderStitches = this.borderStitches();
        int idealMinusBorder = idealStitchCount- borderStitches;
        int repStitches = this.repStitches();
        int underReps = idealMinusBorder/ repStitches;
        int overReps = underReps+1;
        int underDifference = idealMinusBorder% repStitches;
        int overDifference = (repStitches *overReps)-idealMinusBorder;
        if(underDifference<=overDifference)
            return underReps;
        else
            return overReps;
    }
    /**
     * @param idealLineCount the desired number of courses
     * @return the number of height repetitions that best approximates the desired course count
     */
    @Contract(pure = true)
    int closestHeightRepCount(int idealLineCount){
        int repLines = this.repLines();
        int underHeightReps = idealLineCount/ repLines;
        int overHeightReps = underHeightReps+1;
        int underHeightDiff = idealLineCount% repLines;
        int overHeightDiff = (overHeightReps* repLines)-idealLineCount;
        if(underHeightDiff<=overHeightDiff)
            return underHeightReps;
        else
            return overHeightReps;
    }
    /**
     * the number of stitch changes need to match the given stitchCount
     * @param stitchCount being approached
     * @return positive stitchDelta to widen or negative stitchDelta to narrow
     */
    @Contract(pure = true)
    int stitchDelta(int stitchCount){
        int repSection = this.repSectionOfStitchCount(stitchCount);
        int reps = this.closestStitchRepCount(stitchCount);
        int repStitches = this.repStitches();
        return repSection-reps*repStitches;
    }
    /**
     * the number of course changes need to match the given lineCount
     * @param lineCount being approached
     * @return positive lineDelta to widen or negative lineDelta to narrow
     */
    @Contract(pure = true)
    int lineDelta(int lineCount){
        if(lineCount%2!=0)//not even, must make even to finish texture on WS
            lineCount++;
        int reps = this.closestHeightRepCount(lineCount);
        return lineCount-this.repLines()*reps;
    }
    /**
     * @param stitchCount to break up
     * @return the number of stitches in stitch count that are handled by the repeating pattern
     */
    @Contract(pure = true)
    private int repSectionOfStitchCount(int stitchCount){return stitchCount-this.borderStitches();}

    @Override
    public String toString(){return this.textureDefinition.toString();}
    @Override
    @Contract("null->false")
    public boolean equals(Object o){
        if(o instanceof Texture){
            Texture other = (Texture) o;
            return this.stitchesPerInch == other.stitchesPerInch &&
                    this.linesPerInch == other.linesPerInch &&
                    this.textureDefinition.equals(other.textureDefinition);
        }
        return false;
    }

}
