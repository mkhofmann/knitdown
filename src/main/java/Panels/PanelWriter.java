package Panels;

import Panels.Patches.Cell;
import Panels.Patches.Patch;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * This class outputs a file describing the panel. Its over simplified and could be made more general
 */
public class PanelWriter extends Writer {
    private boolean closed = false;
    private FileWriter writer;
    private Panel panel;
    protected File file;

    private PanelWriter(File file, FileWriter writer, Panel panel){
        this.file = file;
        this.writer = writer;
        this.panel =panel;
    }
    private PanelWriter(File file, Panel panel) throws IOException {
        this(file, new FileWriter(file), panel);
    }

    public PanelWriter(String pathName, Panel panel) throws IOException {
        this(new File(pathName), panel);
    }

    public  void writeFile(boolean adjustPanel) throws IOException {
        assert this.panel.patches.size()<24;//I just don't want to deal with names for patches greater than one letter
        if(adjustPanel)
            this.panel.adjustPanel();
        HashMap<Patch, String> namedPatches = new HashMap<>();
        StringBuilder data = new StringBuilder(Panel.CELL_PER_UNIT + "\n\n");
        String[] alphabet ={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
        int alphabetIndex = 0;

        for(Patch patch: this.panel.patches){
            String patchLetter = alphabet[alphabetIndex];
            namedPatches.put(patch, " "+ patchLetter);
            data.append(patchLetter).append(" ").append(patch.texture.stitchesPerInch).append(" ").append(patch.texture.repStitches()).append(" ").append(patch.texture.borderStitches()).append(" ").append(patch.significance).append("\n");
            alphabetIndex++;
            assert alphabetIndex<alphabet.length;
        }
        data.append("\n");
        Cell topLeft = panel.cells.lastKey();
        Patch topLeftPatch = panel.cells.get(topLeft);
        String topLeftName = namedPatches.get(topLeftPatch);
        data.append(topLeftName);
        for (Iterator<Cell> it = topLeft.rightRowIterator(); it.hasNext(); ) {
            Cell nextRight = it.next();
            Patch cellPatch = panel.cells.get(nextRight);
            String name = namedPatches.get(cellPatch);
            data.append(name);
        }
        data.append("\n");
        for (Iterator<Cell> downIter = topLeft.downColumnIterator(); downIter.hasNext(); ) {
            Cell leftCell = downIter.next();
            Patch leftPatch = panel.cells.get(leftCell);
            String leftName = namedPatches.get(leftPatch);
            data.append(leftName);
            for (Iterator<Cell> rightIter = leftCell.rightRowIterator(); rightIter.hasNext(); ) {
                Cell nextRight = rightIter.next();
                Patch rightPatch = panel.cells.get(nextRight);
                String rightName = namedPatches.get(rightPatch);
                data.append(rightName);
            }
            data.append("\n");
        }
        data.append("\nlinking counts\n");
        for(Patch patch: panel.patches)
            for (Patch upNeighbor : patch.topNeighbors()) {
                Integer integer = patch.dedicatedStitchesToTopNeighbor.get(upNeighbor);
                data.append(namedPatches.get(patch)).append("->").append(namedPatches.get(upNeighbor)).append(" ").append(integer).append("\n");
            }
        data.append("\nin counts\n");
        for(Patch patch: panel.patches){
            double patchEdgeCount = patch.edgeStitchCount;
            double patchBodyCount = patch.optBodyStitchCount;
            TreeSet<Patch> topNeighbors = patch.topNeighbors();
            if(topNeighbors.isEmpty())
                data.append(namedPatches.get(patch)).append("-> ").append((int) patchBodyCount).append("\n");
            int neighborCount = topNeighbors.size();
            boolean add = false;
            for(Patch upNeighbor: topNeighbors) {
                double linkingStitches = patch.dedicatedStitchesToTopNeighbor.get(upNeighbor);
                double portionOfEdge = linkingStitches/patchEdgeCount;
                if(neighborCount!= 1.0 && portionOfEdge== (1.0/(double)neighborCount)){
                    if(add)
                        portionOfEdge += .001;
                    else
                        portionOfEdge -= .001;
                    add = !add;
                }
                data.append(namedPatches.get(patch)).append("->").append(namedPatches.get(upNeighbor)).append(" ").append(Math.round(portionOfEdge * patchBodyCount)).append("\n");
            }
        }
        data.append("\nout counts\n");
        for(Patch patch: panel.patches){
            double patchEdgeCount = patch.edgeStitchCount;
            double patchBodyCount = patch.optBodyStitchCount;
            TreeSet<Patch> botNeighbors = patch.bottomNeighbors();
            int neighborCount = botNeighbors.size();
            boolean add = false;
            if(botNeighbors.isEmpty())
                data.append("null->").append(namedPatches.get(patch)).append(" ").append((int) patchBodyCount).append("\n");
            for(Patch downNeighbor: botNeighbors) {
                double linkingStitches = patch.dedicatedStitchesToBotNeighbor.get(downNeighbor);
                double portionOfEdge = linkingStitches/patchEdgeCount;
                if(neighborCount!= 1.0 && portionOfEdge== (1.0/(double)neighborCount)){
                    if(add)
                        portionOfEdge += .001;
                    else
                        portionOfEdge -= .001;
                    add = !add;
                }
                data.append(namedPatches.get(downNeighbor)).append("->").append(namedPatches.get(patch)).append(" ").append(Math.round(portionOfEdge * patchBodyCount)).append("\n");
            }
        }
        this.write(data.toString());
        this.flush();
    }

    @Override
    public void write(@NotNull char[] cbuf, int off, int len) throws IOException {
        if(this.closed)
            this.buildDummyFile();
        try {
            this.writer.write(cbuf, off, len);
        } catch (IOException io){
            System.out.println("Should have printed dummy file");
            io.printStackTrace();
        }
    }

    public void write(@NotNull String str) throws IOException {
        if(this.closed)
            this.buildDummyFile();
        try {
            this.writer.write(str);
        } catch (IOException io){
            System.out.println("Should have printed dummy file");
            io.printStackTrace();
        }
    }

    private void buildDummyFile() throws IOException {
        String dummyPathName = this.file.getPath();
        int dotIndex = dummyPathName.lastIndexOf('.');
        if(dotIndex<0){
            String fileType = ".txt";
            dummyPathName = dummyPathName.concat("-dummy").concat(fileType);
        } else{
            String path = dummyPathName.substring(0, dotIndex);
            String type = dummyPathName.substring(dotIndex);
            dummyPathName = path.concat("-dummy").concat(type);
        }
        this.file = new File(dummyPathName);
        this.writer = new FileWriter(this.file);
        this.closed = false;
    }

    @Override
    public void flush() throws IOException {this.writer.flush();}

    @Override
    public void close() throws IOException {
        this.writer.close();
        this.closed = true;
    }
}
