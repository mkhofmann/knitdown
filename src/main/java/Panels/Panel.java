package Panels;

import CompilerExceptions.ParsingExceptions.InsufficientLoopsException;
import CompilerExceptions.PanelExceptions.PreviouslyCoveredCellException;
import CompilerExceptions.StitchManipulationExceptions.NoTextureCarvesAvailableException;
import Panels.Patches.Cell;
import Panels.Patches.Patch;
import Panels.Patches.PatchPosition;
import Panels.Patches.Texture;
import TextureGraph.Courses.CastOnCourse;
import TextureGraph.Courses.Course;
import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.*;
import TextureGraph.Courses.Stitches.Stitch;
import TextureGraph.Courses.Stitches.YO;
import TextureGraph.TextureBuiltInRound;
import TextureGraph.TextureBuiltInRow;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.*;

/**
 * Created by Megan on 4/26/2017.
 * the structure that organizes patches into a larger rectangle that can be made into a textureGraph graph
 */
public class Panel {
    /**
     * fidelity of the grid structure, defaults to 1/8 inch, int is given per inch
     */
    public static final int CELL_PER_UNIT = 8;
    /**
     * the number of cells width wise
     */
    public int cellsInWidth;
    /**
     * the number of cells height wise
     */
    public int cellsInHeight;
    /**
     * the set of patches in this panel
     */
    TreeSet<Patch> patches= new TreeSet<>();
    /**
     * the relationship of cells to a patch
     */
    TreeMap<Cell, Patch> cells = new TreeMap<>();
    /**
     * @param width the width of the panel in real world units
     * @param height the height of the panel in real world units
     */
    public Panel(double width, double height){
        this.cellsInWidth = (int)(width* CELL_PER_UNIT);
        this.cellsInHeight = (int)(height* CELL_PER_UNIT);
    }

    /**
     * @return the rel world width of the panel
     */
    @Contract(pure = true)
    public double width(){
        return ((double)this.cellsInWidth)*(1.0/((double) CELL_PER_UNIT));
    }

    /**
     * Set the panels loop and course counts based on read in values from a optimized panel format
     * @param panelInput the file containing the specified optimized panel
     */
    public void easePanel(OptimizedPanelReader panelInput){
        HashMap<String, Patch> patchesByName = new HashMap<>();
        Iterator<Patch> patchIterator = this.patches.iterator();
        for(String patchName: panelInput.bodyStitchesPerPatch.keySet()){
            assert patchIterator.hasNext();
            patchesByName.put(patchName, patchIterator.next());
        }
        for(String patchName: panelInput.bodyStitchesPerPatch.keySet()){
            Patch patch = patchesByName.get(patchName);
            patch.optBodyStitchCount = panelInput.bodyStitchesPerPatch.get(patchName);
            patch.botEdgeLoopCount = patch.optBodyStitchCount;
            patch.topEdgeLoopCount = panelInput.topStitchesPerPatch.get(patchName);
            patch.edgeStitchCount = -1;
        }
    }
    /**
     * @return a textureGraph built in the round that  merges each patches, the patches should already be eased
     */
    private TextureBuiltInRound buildPanelInRound() throws NoTextureCarvesAvailableException, InsufficientLoopsException {
        Yarn yarn = new Yarn();
        this.buildPatchGarmentGraphs();
        LinkedList<Course> courses = new LinkedList<>();
        courses.add(this.buildCastOnLine(yarn));
        TreeSet<Patch> unConsumedPatches = new TreeSet<>(patches);
        TreeSet<Patch> lowestPatches = this.getLinesInBottomRow();
        while(!unConsumedPatches.isEmpty()){
            courses.size();
            LinkedHashSet<Course> linesToMerge = new LinkedHashSet<>();
            TreeMap<Patch, TreeSet<Patch>> patchesToReplace = new TreeMap<>();
            for (Patch lowestPatch: lowestPatches) {
                if(lowestPatch.fullyRead()){
                    unConsumedPatches.remove(lowestPatch);
                    TreeSet<Patch> neighbors = lowestPatch.topNeighbors();
                    if(!neighbors.isEmpty()) {
                        patchesToReplace.put(lowestPatch, neighbors);
                        for (Patch neighbor : neighbors)
                            linesToMerge.add(neighbor.lastMadeTextureGraph.getLine(neighbor.lastReadLine));
                    }
                }
                else
                    linesToMerge.add(lowestPatch.lastMadeTextureGraph.getLine(lowestPatch.lastReadLine++));
            }
            if(!patchesToReplace.isEmpty()){
                LinkedHashSet<Patch> replacements = new LinkedHashSet<>();
                for(Patch replace: patchesToReplace.keySet()) {
                    assert lowestPatches.remove(replace);
                    replacements.addAll(patchesToReplace.get(replace));
                }
                lowestPatches.addAll(replacements);
                for (Patch replacement : replacements)
                    replacement.lastReadLine++;
                patchesToReplace.clear();
            }
            if(!linesToMerge.isEmpty())
                courses.add(Panel.mergeLines(linesToMerge, yarn, courses.size(), courses.getLast()));
        }

        for(Course course : courses){
            Loop lastLoop = course.stitchSets.getLast().getChildLoops().getLast();
            YarnSection lastSection = lastLoop.yarnSection.getNextSection();
            if(lastSection!=null && !(lastSection instanceof TurnSection))
                lastSection.parentYarn.newTurn(lastSection.indexInYarn()-1);
        }
        return new TextureBuiltInRound(courses);
    }
    /**
     * @return a textureGraph built in rows that  merges each patches, the patches should already be eased
     **/
    public TextureBuiltInRow buildPanel() throws NoTextureCarvesAvailableException, InsufficientLoopsException {
        return (TextureBuiltInRow) this.buildPanelInRound().switchRowRounds(new Yarn());
    }
    /**
     * @param courses the courses to be merged in order
     * @param yarn the yarn to construct the meared courses from
     * @param lineId the id of the new course
     * @param parentCourse the course to get parent loops from
     * @return the new Course from the merge
     */
    @NotNull
    private static Course mergeLines(@NotNull LinkedHashSet<Course> courses, @NotNull Yarn yarn, int lineId, @Nonnull Course parentCourse) {
        LinkedList<StitchSet> newSets = new LinkedList<>();
        int neededLoopCount = 0;
        for(Course oldCourse : courses) {
            assert !(oldCourse instanceof CastOnCourse);
            neededLoopCount+= oldCourse.getParentLoops().size();
        }
        LinkedList<Loop> childLoops = parentCourse.getChildLoops();
        int diff = neededLoopCount - childLoops.size();
        if(diff >0){//todo this is a crappy hack patch
            TreeMap<Integer, Single> indexOfYo = new TreeMap<>();
            int i = 2;
            for(int l =0; l<diff; l++, i+=2){
                assert i < parentCourse.stitchSets.size();
                Stitch stitch = parentCourse.stitchSets.get(i).getFirstStitch();
                YO newYo = new YO(stitch.childLoop.insertPriorLoop(LoopDirection.BtF));
                indexOfYo.put(i-1, new Single(newYo));
            }
            for(int index: indexOfYo.descendingKeySet())
                parentCourse.stitchSets.add(index, indexOfYo.get(index));
            childLoops = parentCourse.getChildLoops();
        }
        LinkedList<Loop> parents = childLoops;
        for(Course oldCourse : courses)
            for (StitchSet oldSet : oldCourse)
                newSets.add(oldSet.copyFromParents(yarn, parents));
        return new Course(newSets, lineId);
    }
    /**
     * @return set of courses in the lowest patches, to build the foundation of the textureGraph
     */
    @Nonnull
    @Contract(pure = true)
    private TreeSet<Patch> getLinesInBottomRow(){
        TreeSet<Patch> patchesInRow = new TreeSet<>();
        for(Cell cell: this.cells.keySet())
            if (cell.getY() == 0)
                patchesInRow.add(this.cells.get(cell));
        return patchesInRow;
    }
    /**
     * @param yarn the yarn to build the cast ons from
     * @return a caston course that sets the base of the textureGraph
     */
    @NotNull
    @Contract(pure = true)
    private CastOnCourse buildCastOnLine(@NotNull Yarn yarn){
        int castOns = 0;
        for(Patch base: this.getLinesInBottomRow())
            castOns+= base.lastMadeTextureGraph.getFirstStitchSetCount();
        return new CastOnCourse(castOns, yarn);
    }

    /**
     * build the textureGraph graphs for each patch in the panel
     */
    private void buildPatchGarmentGraphs() throws NoTextureCarvesAvailableException, InsufficientLoopsException {
        for(Patch patch: this.patches) {
            System.out.println("Building patch: "+patch.toString());
            patch.buildTextureInRound(new Yarn());
            System.out.println("Built!");
        }
    }
    /**
     * find closest coordinate, rounding to floor integer cell
     * @param inchX number of inches along stitch course from origin
     * @param inchY number of inches along courses form origin
     * @return the coordinate that is closest to this point
     */
    @NotNull
    @Contract(pure = true)
    public Cell getClosestCell(double inchX, double inchY){
        Double cellX = inchX* CELL_PER_UNIT;
        Double cellY = inchY* CELL_PER_UNIT;
        if(cellX==this.cellsInWidth)
            cellX = (double) this.cellsInWidth-1;
        if(cellY == this.cellsInHeight)
            cellY = (double) this.cellsInHeight-1;
        return this.getCell(cellX.intValue(),cellY.intValue());
    }
    /**
     * @param x the index width wise of the cell
     * @param y the index height wise of the cell
     * @return the patch at coordinate (x,y)
     */
    @NotNull
    @Contract(pure = true)
    private Cell getCell(int x, int y){
        return new Cell(x,y,this);
    }
    /**
     * @param cell the cell to find the patch by
     * @return the patch that owns the cell
     */
    @Nullable
    @Contract(pure = true)
    public Patch getPatchOnCell(@NotNull Cell cell){
        if(this.cells.containsKey(cell))
            return this.cells.get(cell);
        return null;
    }
    /**
     * add the patch to the panel
     * @param patch the patch to be added
     * @param overlap true if the patch should be allowed to overload the covered cells, false if it should throw an error
     * @throws PreviouslyCoveredCellException, if something already covers a cell on for this patch
     */
    private void addPatch(@NotNull Patch patch, boolean overlap) throws PreviouslyCoveredCellException {
        for(Cell coveredCell: patch.position){
            if(!overlap && this.cells.containsKey(coveredCell))
                throw new PreviouslyCoveredCellException("Cannot place new texture on cell" + coveredCell+": "+ Objects.requireNonNull(this.getPatchOnCell(coveredCell)).texture.toString());
            this.cells.put(coveredCell, patch);
        }
        patches.add(patch);
    }

    /**
     * add the patch to the panel
     * @param texture the texture of the patch
     * @param brx the bottom right width index of the patch
     * @param bry the bottom right height index of the patch
     * @param width the width of the patch in real world units
     * @param height the height of the patch in real world units
     * @param significance the significance of the patch
     * @param overlap true if the patch should be allowed to overload the covered cells, false if it should throw an error
     * @throws PreviouslyCoveredCellException, if something already covers a cell on for this patch
     */
    public void addPatch(@NotNull Texture texture, double brx, double bry, double width, double height, int significance, boolean overlap) throws PreviouslyCoveredCellException {
        this.addPatch(new Patch(texture, new PatchPosition(brx, bry, width, height, this), significance), overlap);
    }

    /**
     * adjusts the panel with heuristic based loop and course counts and outputs it to a file
     * @param panelOutputName the file name of the output panel
     */
    @SuppressWarnings("SameParameterValue")
    private void adjustPanel(String panelOutputName){
        if(this.patches.size()==1)
            return;
        this.adjustWidths();
        TreeSet<Patch> dominanceSortedPatches = Patch.patchesByStitchDominance(this.patches);
        assert dominanceSortedPatches.size()== this.patches.size();
        while(!dominanceSortedPatches.isEmpty()){
            Patch patch = dominanceSortedPatches.first();//should have highest dominance value
            LinkedHashSet<Patch> effectedPatches = patch.adjustStitches();
            boolean b = dominanceSortedPatches.removeAll(effectedPatches);
            assert b;
        }
        dominanceSortedPatches = Patch.patchesByLineDominance(this.patches);
        while(!dominanceSortedPatches.isEmpty()){
            Patch patch = dominanceSortedPatches.first();//should have highest dominance value
            LinkedHashSet<Patch> effectedPatches = patch.adjustLines();
            dominanceSortedPatches.removeAll(effectedPatches);
        }
        this.balanceTextureCarvingAndGlueJoints();
        try {
            PanelWriter panelWriter = new PanelWriter(panelOutputName+".panel",this);
            panelWriter.writeFile(false);
        } catch (IOException e) {
            assert false;
        }
    }
    /**
     * adjusts the panel with heuristic based loop and course counts and outputs it to a file
     */
    public void adjustPanel(){
        this.adjustPanel("testPanel");
    }

    /**
     * balance the relationship between widths and texture carves to minimize error
     */
    private void adjustWidths() {
        for(Patch patch: this.patches)
            patch.optimizeWidth();
    }

    /**
     * balances the error trade off between texture carves and glue joints assuming all edges have the same loop counts
     */
    private void balanceTextureCarvingAndGlueJoints(){
        for(Patch patch: this.patches)
            patch.optimizeTextureCarvingOrGlueJoints();
        for(Patch patch: this.patches)
            patch.tradeGlueJoints();
        for(Patch patch: this.patches){
            if(patch.botEdgeLoopCount>0 && patch.topEdgeLoopCount<0)
                patch.topEdgeLoopCount = patch.optBodyStitchCount;
            if(patch.topEdgeLoopCount>0 && patch.botEdgeLoopCount<0)
                patch.botEdgeLoopCount = patch.optBodyStitchCount;
        }
    }

    @Override
    public String  toString(){
        StringBuilder str = new StringBuilder();
        for(Patch patch: patches)
            str.append("(").append(patch.position.toString()).append(":").append(patch.significance).append("), ");
        return str.toString();
    }
    @Override
    public int hashCode(){return this.toString().hashCode();}



}
