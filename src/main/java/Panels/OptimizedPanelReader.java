package Panels;

import java.io.*;
import java.util.HashMap;

public class OptimizedPanelReader {

    HashMap<String, Integer> bodyStitchesPerPatch = new HashMap<>();
    HashMap<String, Integer> topStitchesPerPatch = new HashMap<>();
    public OptimizedPanelReader(File file) throws IOException {
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        bufferedReader.readLine();
        String line = bufferedReader.readLine().trim();
        while(line != null){
            String firstPatch=null;
            String secondPatch=null;
            String inCount;
            String outCount;
            line = line.trim();
            int hyphenIndex = line.indexOf('-');
            if(hyphenIndex>0)
                firstPatch = line.substring(0, hyphenIndex).trim();
            line = line.substring(hyphenIndex+2).trim();
            String[] strings = line.split(" ");
            if(strings.length==2){
                inCount = strings[0];
                outCount = strings[1];
            }else {
                secondPatch = strings[0];
                inCount = strings[1];
                outCount = strings[2];
            }
            line = bufferedReader.readLine();
            int bodyStitches = Integer.parseInt(outCount);
            int inStitches = Integer.parseInt(inCount);
            if(secondPatch != null) {
                assert bodyStitches >0;
                if (!bodyStitchesPerPatch.containsKey(secondPatch))
                    bodyStitchesPerPatch.put(secondPatch, bodyStitches);
                else
                    bodyStitchesPerPatch.put(secondPatch, bodyStitches + bodyStitchesPerPatch.get(secondPatch));
            }
            if(firstPatch != null){
                assert inStitches>0;
                if(!topStitchesPerPatch.containsKey(firstPatch))
                    topStitchesPerPatch.put(firstPatch, inStitches);
                else
                    topStitchesPerPatch.put(firstPatch, inStitches+topStitchesPerPatch.get(firstPatch));
            }
        }
    }
}
