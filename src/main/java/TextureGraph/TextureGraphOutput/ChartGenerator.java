package TextureGraph.TextureGraphOutput;

import CompilerExceptions.ChartGeneratorBugException;
import TextureGraph.Courses.Course;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.YarnSection;
import TextureGraph.Courses.Stitches.Stitch;
import TextureGraph.Courses.Stitches.StitchInRow.CastOn;
import TextureGraph.TextureGraph;
import com.intellij.openapi.util.Pair;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by Lea starting on 11/18/2016
 * Outputs a .chart file describing the texture graph
 */
public class ChartGenerator extends KnitSpeakOutputGenerator {


    private HashMap<YarnSection, String> loopToID = new HashMap<>();
    private HashSet<String> forceUniqueIds = new HashSet<>();
    /**
     * @param pathName the path of the output file
     * @param textureGraph the texture graph to output
     */
    public ChartGenerator(@NotNull String pathName, @NotNull TextureGraph textureGraph) throws IOException {super(pathName, textureGraph);}

    @Override
    public void writeFile() throws IOException, ChartGeneratorBugException {
        this.loopToID = new HashMap<>();
        this.forceUniqueIds = new HashSet<>();
        StringBuilder dataBuilder = new StringBuilder("[");
        int uniqueCount = 0;
        int line = 0;
        for(TextureGraph texture: this.textureGraphs) {
            for (Course l : texture) {
                if (l.stitchSets.size() == 0)
                    System.err.println("warning:" + this.file.getName() + " course " + l.getCourseID() + " has no stitch sets");
                else {
                    dataBuilder.append("[");
                    int stitch = 0;
                    for (StitchSet ss : l) {
                        if (ss.stitchCount() == 0)
                            System.err.println("warning: empty stitch set on course " + l.getCourseID());
                        else
                            for (Stitch s : ss) {
                                Pair<String, Integer> idPair = buildId(line, stitch, uniqueCount, s.childLoop.yarnSection);
                                //dataBuilder.append(ChartGenerator.writeStitch(s, idPair.first, loopToID, false));
                                dataBuilder.append(this.writeStitch(s, idPair.first));
                                uniqueCount = idPair.second;
                                stitch++;
                            }
                    }
                    dataBuilder = new StringBuilder(dataBuilder.substring(0, dataBuilder.length() - 2)); // get rid of last comma and space (wish I'd had something like array.join);
                }
                dataBuilder.append("],\n");
                line++;
            }
        }
        String data = dataBuilder.toString();
        data = data.substring(0, data.length() - 2); // get rid of last comma and space (wish I'd had something like array.join)
        data = data + "\n]";
        this.write(data);
        this.flush();
        this.close();
        System.out.println("Non unique loop count: "+uniqueCount);
    }

    @NotNull
    @Contract(pure = true)
    private Pair<String, Integer> buildId(int line, int stitch, int uniqueCount, YarnSection section) throws ChartGeneratorBugException {
       String id = line+"_"+stitch;
       if(this.forceUniqueIds.contains(id)) {
           id+="_"+uniqueCount;
           uniqueCount++;
       }
       assert !this.forceUniqueIds.contains(id);
       this.forceUniqueIds.add(id);
       if(this.loopToID.containsKey(section))
           throw new ChartGeneratorBugException();
       assert !this.loopToID.containsKey(section);
       loopToID.put(section, id);
       return Pair.create(id, uniqueCount);
    }

    @NotNull
    private String writeStitch(@NotNull Stitch s, String id) throws ChartGeneratorBugException {
        StringBuilder desc = new StringBuilder("{");
        desc.append("\n\t\"id\": \"").append(id);
        desc.append("\",\n\t\"parents\": [");
        if(!(s instanceof CastOn || s.parentLoops.size()<=0)) {
            for (Loop l : s.parentLoops) {
                String parentStr = loopToID.get(l.yarnSection);
                if(parentStr == null)
                    throw new ChartGeneratorBugException();//todo: very strange bug here. Rerunning the chart sometimes works
//                String substring = parentStr.substring(0, parentStr.indexOf('_'));
//                int parentLine = Integer.parseInt(substring);
////                if(parentLine != s.owningStitchSet.owningCourse.getCourseID()-1)
////                    assert parentLine == s.owningStitchSet.owningCourse.getCourseID()-1;
                desc.append("\"").append(parentStr).append("\", ");
            }
            desc = new StringBuilder(desc.substring(0, desc.length() - 2)); // get rid of last comma and space (wish I'd had something like array.join)
        }
        desc.append("],");
        desc.append("\n\t\"yarnName\": \"").append(s.getYarn()).append("\"");
        desc.append(",\n\t\"stitchOrder\": ").append(s.stitchOrder);//grabs the order relative to overlapping stitches
        desc.append(",\n\t\"pullDir\": \"").append(s.childLoop.direction);
        desc.append("\",\n\t\"tuckType\": \"knit");
        desc.append("\",\n\t\"isBO\": ").append("false");
        desc.append("\n},\n");
        return desc.toString();
    }

}
