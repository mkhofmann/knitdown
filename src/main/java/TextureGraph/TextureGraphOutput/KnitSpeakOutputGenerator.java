package TextureGraph.TextureGraphOutput;

import CompilerExceptions.ChartGeneratorBugException;
import TextureGraph.TextureGraph;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;

/**
 * Created by Megan on 11/7/2016.
 * Class manages outputing a texture graph to a file format
 */
public abstract class KnitSpeakOutputGenerator extends Writer {
    private boolean closed = false;
    protected File file;
    private FileWriter writer;
    LinkedList<TextureGraph> textureGraphs;
    private KnitSpeakOutputGenerator(File file, FileWriter writer, LinkedList<TextureGraph> textureGraphs){
        this.file = file;
        this.writer = writer;
        this.textureGraphs = textureGraphs;
    }
    private KnitSpeakOutputGenerator(File file, FileWriter writer, TextureGraph textureGraph){
        this(file, writer, new LinkedList<>());
        this.textureGraphs.add(textureGraph);
    }
    private KnitSpeakOutputGenerator(File file, TextureGraph textureGraph) throws IOException {
        this(file, new FileWriter(file), textureGraph);
    }

    KnitSpeakOutputGenerator(String pathName, TextureGraph textureGraph) throws IOException {
        this(new File(pathName), textureGraph);
    }

    private KnitSpeakOutputGenerator(File file, LinkedList<TextureGraph> textureGraphs) throws IOException {
        this(file, new FileWriter(file), textureGraphs);
    }

    public abstract void writeFile() throws IOException, ChartGeneratorBugException;

    @Override
    public void write(@NotNull char[] cbuf, int off, int len) throws IOException {
        if(this.closed)
            this.buildDummyFile();
        try {
            this.writer.write(cbuf, off, len);
        } catch (IOException io){
            System.out.println("Should have printed dummy file");
            io.printStackTrace();
        }
    }

    public void write(@NotNull String str) throws IOException {
        if(this.closed)
            this.buildDummyFile();
        try {
            this.writer.write(str);
        } catch (IOException io){
            System.out.println("Should have printed dummy file");
            io.printStackTrace();
        }
    }

    private void buildDummyFile() throws IOException {
        String dummyPathName = this.file.getPath();
        int dotIndex = dummyPathName.lastIndexOf('.');
        if(dotIndex<0){
            String fileType = ".txt";
            dummyPathName = dummyPathName.concat("-dummy").concat(fileType);
        } else{
            String path = dummyPathName.substring(0, dotIndex);
            String type = dummyPathName.substring(dotIndex);
            dummyPathName = path.concat("-dummy").concat(type);
        }
        this.file = new File(dummyPathName);
        this.writer = new FileWriter(this.file);
        this.closed = false;
    }

    @Override
    public void flush() throws IOException {this.writer.flush();}

    @Override
    public void close() throws IOException {
        this.writer.close();
        this.closed = true;
    }
}
