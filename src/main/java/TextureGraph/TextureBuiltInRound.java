package TextureGraph;

import TextureGraph.Courses.CastOnCourse;
import TextureGraph.Courses.Course;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.Stitch;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by Megan on 5/23/2017.
 * for textures when the courses meet in rounds
 */
public class TextureBuiltInRound extends TextureGraph {

    public TextureBuiltInRound(@NotNull LinkedList<Course> courses){super(courses);}

    @NotNull
    @Override
    public TextureGraph switchRowRounds(@NotNull Yarn yarn){
        LinkedList<Course> newCourses = new LinkedList<>();
        newCourses.add(new CastOnCourse(this.getFirstStitchSetCount(), yarn));
        LinkedList<Loop> parents = new LinkedList<>();
        for(int l = 1; l<this.courses.size(); l++){
            Course lastCourse = newCourses.getLast();
            for (Iterator<StitchSet> it = lastCourse.stitchSets.descendingIterator(); it.hasNext(); ) {
                StitchSet set = it.next();
                for (Iterator<Stitch> it1 = set.stitches.descendingIterator(); it1.hasNext(); ) {
                    Stitch stitch = it1.next();
                    parents.add(stitch.childLoop);
                }
            }
            Course thisCourse = this.getLine(l);
            Iterator<StitchSet> setIterator;
            if(thisCourse.isStandardRightSide())
                setIterator = thisCourse.stitchSets.iterator();
            else
                setIterator = thisCourse.stitchSets.descendingIterator();
            LinkedList<StitchSet> newSets = new LinkedList<>();
            for (Iterator<StitchSet> it = setIterator; it.hasNext(); ) {
                StitchSet set = it.next();
                newSets.add(set.copyFromParents(yarn,parents));
            }
            Course newCourse = new Course(newSets, l);
            newCourses.add(newCourse);
            newCourse.turnAtEndOfLine();
        }
        return new TextureBuiltInRow(newCourses);
    }


}
