package TextureGraph.Courses;

import TextureGraph.Courses.StitchSets.Cables.Cable;
import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.StitchSets.Twists.Twist;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * A list of connected stitch sets that can all be reduced, thus reducing a path through the texture graph
 */
public class VerticalSetCarve implements Comparable<VerticalSetCarve>, Iterable<StitchSet>{
    /**
     * the sets that can be reduced
     */
    public LinkedList<StitchSet> carve = new LinkedList<>();
    /**
     * a penalty applied to making diagonal moves in the path
     */
    public static final double DIAGONAL_VERTICAL_PENALTY = 2.0;
    /**
     * a penalty applied to making paths near places where old paths had been reduced
     */
    private static final double NEAR_VERTICAL_CARVE_PENALTY = 2.0;

    /**
     * creates a path recursively by building the path of descendant set
     * @param startingSet the first set to create the path from
     */
    public VerticalSetCarve(@NotNull StitchSet startingSet) {
        this.addSet(startingSet);
    }

    /**
     * reduces each set by one child,
     */
    public void removeChildrenFromSets() {
        this.addNeighborCosts();
        for(StitchSet set: this)
            set.reduceWidthOfSet();
    }

    /**
     * adjusts the neighboring cost of all other sets to consider how close they are to old carves
     */
    private void addNeighborCosts(){
        Course firstCourse = this.carve.getFirst().owningCourse;
        int lineIndex = firstCourse.getCourseID();
        Iterator<StitchSet> setPathIterator = this.iterator();
        for (Iterator<Course> it = firstCourse.owningTextureGraph.iterator(lineIndex); it.hasNext(); ) {
            Course course = it.next();
            if(!setPathIterator.hasNext())
                break;
            StitchSet set = setPathIterator.next();
            int setIndex = course.stitchSets.indexOf(set);
            for(int i = 0; i< course.stitchSets.size(); i++){
                StitchSet setAtI = course.stitchSets.get(i);
                if(setIndex!=i)
                    setAtI.neighborReduceWidthCost += 1.0/(Math.pow(NEAR_VERTICAL_CARVE_PENALTY, Math.abs(setIndex-i)));
            }
        }
    }

    /**
     * @return the cost of reducing this whole path
     */
    @Contract(pure = true)
    private double getPathCost(){
        return carve.getFirst().verticalPathCost;
    }

    /**
     * @param others the other stitches that could be carved out
     * @return true if this path does not intersect any of the other stitch sets
     */
    @Contract(pure = true)
    public boolean pathIsExclusive(@NotNull HashSet<StitchSet> others){
        for(StitchSet thisSet: this)
            if(others.contains(thisSet))
                return false;
        return true;
    }

    /**
     * @param set the set to add to this path, effectively adding the path that descends from it
     */
    private void addSet(@NotNull StitchSet set) {
        assert set.verticalPathCost != Double.MAX_VALUE;
//        for(Stitch stitch: set)
//            assert !(stitch instanceof Decrease || stitch instanceof YO);
        this.carve.add(set);
        if(set.verticalPathCost ==Double.MAX_VALUE || set.verticalPathCost ==set.proportionalCostToReduceWidth)
            return;
        double minDescendantPathCost = Double.MAX_VALUE;
        StitchSet minDescendant= null;
        for(StitchSet descendant: set.descendants()){
            assert descendant.pathCalculated;
            double descendantWeight = ((double)descendant.stitchCount()) / ((double)set.stitchCount());
            boolean disSimilarTypes = descendant.getClass() != descendant.getClass();
            if(disSimilarTypes && descendant instanceof Single)
                disSimilarTypes = set.getFirstStitch().getClass() != descendant.getFirstStitch().getClass();
            double dissimilarityCost = 0;
            if(disSimilarTypes)
                dissimilarityCost = DIAGONAL_VERTICAL_PENALTY;
            if(minDescendantPathCost> (descendant.verticalPathCost *descendantWeight)+dissimilarityCost) {
                minDescendant =descendant;
                minDescendantPathCost = (descendant.verticalPathCost *descendantWeight)+dissimilarityCost;
            }
        }
        StitchSet minDiagonalDescendant;
        double minDiagonalPathCost;
        StitchSet descendantNextNeighbor = set.descendantOfNextStitchInLine();
        double nextDiagonalCost = Double.MAX_VALUE;
        StitchSet descendantPriorNeighbor = set.descendantofPriorStitchInLine();
        double priorDiagonalCost = Double.MAX_VALUE;
        if(descendantNextNeighbor != null && !(set instanceof Cable) && !(descendantNextNeighbor instanceof Cable)) {
            double nextPathWeight= ((double)descendantNextNeighbor.stitchCount()) / ((double)set.stitchCount()); //size of set over the size of cur set
            boolean disSimilarTypes = descendantNextNeighbor.getClass() != descendantNextNeighbor.getClass();
            if(disSimilarTypes && descendantNextNeighbor instanceof Single)
                disSimilarTypes = set.getFirstStitch().getClass() != descendantNextNeighbor.getFirstStitch().getClass();
            double dissimilarityCost = 0;
            if(disSimilarTypes)
                dissimilarityCost = DIAGONAL_VERTICAL_PENALTY;
            nextDiagonalCost = (nextPathWeight*descendantNextNeighbor.verticalPathCost) + DIAGONAL_VERTICAL_PENALTY +dissimilarityCost;
        }
        if(descendantPriorNeighbor != null && !(set instanceof Cable) && !(descendantPriorNeighbor instanceof Cable)) {
            double priorPathWeight = ((double)descendantPriorNeighbor.stitchCount()) / ((double)set.stitchCount());
            boolean disSimilarTypes = descendantPriorNeighbor.getClass() != descendantPriorNeighbor.getClass();
            if(disSimilarTypes && descendantPriorNeighbor instanceof Single)
                disSimilarTypes = set.getFirstStitch().getClass() != descendantPriorNeighbor.getFirstStitch().getClass();
            double dissimilarityCost = 0;
            if(disSimilarTypes)
                dissimilarityCost = DIAGONAL_VERTICAL_PENALTY;
            priorDiagonalCost =(priorPathWeight*descendantPriorNeighbor.verticalPathCost)+ DIAGONAL_VERTICAL_PENALTY +dissimilarityCost;
        }
        if(minDescendant != null) {
            nextDiagonalCost += minDescendant.setDiagonalCost;
            priorDiagonalCost += minDescendant.setDiagonalCost;
        }
        if(nextDiagonalCost<priorDiagonalCost) {
            minDiagonalDescendant = descendantNextNeighbor;
            minDiagonalPathCost = nextDiagonalCost;
        }else {
            minDiagonalDescendant = descendantPriorNeighbor;
            minDiagonalPathCost = priorDiagonalCost;
        }
        if(minDescendantPathCost<=minDiagonalPathCost && minDescendantPathCost != Double.MAX_VALUE)
            this.addSet(minDescendant);
        else if( minDiagonalPathCost != Double.MAX_VALUE) {
            assert !(this.carve.getLast() instanceof Cable) && !(this.carve.getLast() instanceof Twist);
            assert minDiagonalDescendant != null;
            this.addSet(minDiagonalDescendant);
        }
    }

    @Override
    public int compareTo(@NotNull VerticalSetCarve o) {
        int cmp = Double.compare(this.getPathCost(), o.getPathCost());
        if(cmp ==0)
            return Integer.compare(o.carve.getFirst().stitches.getFirst().childLoop.hashCode(), this.carve.getFirst().stitches.getFirst().childLoop.hashCode());
        return cmp;
    }

    @NotNull
    @Override
    public Iterator<StitchSet> iterator() {
        return this.carve.iterator();
    }

    @Override
    public String toString(){
        StringBuilder str = new StringBuilder(String.format("%1$,.2f", this.getPathCost()) + ": ");
        for(StitchSet set: this)
            str.append(set.toString()).append(", ");
        return str.toString();
    }
}
