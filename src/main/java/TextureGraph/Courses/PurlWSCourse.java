package TextureGraph.Courses;

import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.Purl.P;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * A wrong side course that is made of only purls, used to even out texture graphs with odd course counts and now WS declaration
 */
public class PurlWSCourse extends Course {
    /**
     * @param id the index of the course in the graph
     * @param previousCourse the course who the parent loops are inherited from
     * @param yarn the yarn to create new purl loops from
     */
    public PurlWSCourse(int id, @NotNull Course previousCourse, @NotNull Yarn yarn) {
        super(id);
        LinkedList<Loop> parents = previousCourse.getChildLoops();
        int i=1;
        for (Iterator<Loop> it = parents.descendingIterator(); it.hasNext(); ) {
            Loop parent = it.next();
            this.addStitchSet(new Single(new P(parent, yarn)));
            i++;
        }
    }
}
