package TextureGraph.Courses.StitchSets;

import TextureGraph.Courses.Lean;
import TextureGraph.Courses.StitchSets.Increases.KFB;
import TextureGraph.Courses.StitchSets.Increases.PFB;
import TextureGraph.Courses.Stitches.Decrease;
import TextureGraph.Courses.Stitches.Knit.K;
import TextureGraph.Courses.Stitches.Knit.KXtog;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.OneToOneStitch;
import TextureGraph.Courses.Stitches.Purl.P;
import TextureGraph.Courses.Stitches.Purl.Pxtog;
import TextureGraph.Courses.Stitches.SlippingDecreases.Skpo;
import TextureGraph.Courses.Stitches.Stitch;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;

/**
 * Created by Megan on 4/18/2017.
 * A stitch set that only has one stitch
 */
public class Single extends StitchSet{
    public Single(@NotNull Stitch stitch){
        super(stitch);
        this.lean = this.stitches.getFirst().lean;
    }

    @Override
    public double proportionOfLocal() {
        Stitch priorStitch = this.getPriorStitchInLine();
        Stitch nextStitch = this.getNextStitchInLine();
        double totalSets = 1.0;
        double matchingSets = 1.0;
        if (nextStitch != null) {
            assert nextStitch.owningStitchSet != this;
            if (nextStitch.owningStitchSet instanceof Single && nextStitch.getClass().equals(this.getFirstStitch().getClass()))
                matchingSets++;
            totalSets++;
        }
        if (priorStitch != null) {
            assert priorStitch.owningStitchSet != this;
            if (priorStitch.owningStitchSet instanceof Single && priorStitch.getClass().equals(this.getFirstStitch().getClass()))
                matchingSets++;
            totalSets++;
        }
        return matchingSets / totalSets;
    }

    /**
     * @return true if this is a K or P or decrease that can be merged into a decrease with the next stitch a KP or decrease.
     */
    @Contract(pure = true)
    public boolean mergableWithNextNeighbor(){
        Stitch firstStitch = this.getFirstStitch();
        if(!(firstStitch instanceof K || firstStitch instanceof P || firstStitch instanceof Decrease))
            return false;
        Stitch nextStitch = this.getNextStitchInLine();
        return nextStitch != null &&  nextStitch.owningStitchSet instanceof Single && (nextStitch instanceof K || nextStitch instanceof P|| nextStitch instanceof Decrease);
    }

    /**
     * @return a decrease inside a single stitch set to replace this and next stitch or null, if the stithces can't be merged
     */
    @Nullable
    public Single mergeToDecreaseWithNextNeighbor(){
        if(!this.mergableWithNextNeighbor())
            return null;
        try {
            Lean predictedLean;
            predictedLean = Lean.right;//TODO: reimplement predictor
            Stitch stitch = this.getFirstStitch();
            Stitch nextStitch = this.getNextStitchInLine();
            assert nextStitch != null;
            if(stitch.childLoop != nextStitch.childLoop)
                stitch.childLoop.removeLoopFromYarn();
            else
                stitch.childLoop = null;
            Loop child = nextStitch.childLoop;
            LinkedList<Loop> parents = new LinkedList<>();
            parents.addAll(stitch.parentLoops);
            parents.addAll(nextStitch.parentLoops);
            Decrease decrease;
            //noinspection ConstantConditions
            if(predictedLean == Lean.right) {
                if(stitch instanceof P || stitch instanceof Pxtog)
                    decrease = new Pxtog(parents, child);
                else
                    decrease = new KXtog(parents, child);
            } else {
                decrease = new Skpo(parents, child);
            }
            return new Single(decrease);
        } catch (Exception e) {
            e.printStackTrace();
            assert false;
        }
        return null;
    }

    /**
     * @return a KFB or PFB to merge this stitch with the next into an increase, decreasing the needed parent loops
     */
    @Nullable
    public StitchSet mergeToIncreaseWithNextNeighbor(){
        if(!this.mergableWithNextNeighbor())
            return null;
        OneToOneStitch stitch = (OneToOneStitch) this.getFirstStitch();
        OneToOneStitch nextStitch = (OneToOneStitch) this.getNextStitchInLine();
        assert nextStitch != null;
        stitch.parentLoops.getFirst().removeLoopFromYarn();
        Loop parent = nextStitch.parentLoops.getFirst();
        stitch.childLoop.removeChildOf(stitch);
        nextStitch.childLoop.removeChildOf(nextStitch);
        parent.removeParentOf(nextStitch);
        nextStitch.parentLoops.getFirst().removeParentOf(nextStitch);
        stitch.owningStitchSet = null;
        nextStitch.owningStitchSet = null;
        if(stitch instanceof K)
            return new KFB(parent, stitch.childLoop, nextStitch.childLoop);
        else
            return new PFB(parent, stitch.childLoop, nextStitch.childLoop);
    }

    @NotNull
    @Override
    public StitchSet copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents){
        return new Single(this.getFirstStitch().copyFromParents(yarn, parents));
    }
    @Override
    @NotNull
    public StitchSet copyFromParents(@NotNull Loop priorLoop, @NotNull LinkedList<Loop> parents){
        return new Single(this.getFirstStitch().copyFromParents(priorLoop,parents));
    }



}
