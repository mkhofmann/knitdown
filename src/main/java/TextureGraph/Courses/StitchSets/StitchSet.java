package TextureGraph.Courses.StitchSets;

import TextureGraph.Courses.Course;
import TextureGraph.Courses.Lean;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.Stitch;
import TextureGraph.Courses.Stitches.StitchInRow.CastOn;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * Created by Megan on 7/18/2016.
 * a set of stitches representing a many to many relationship of loops
 */
public class StitchSet implements Iterable<Stitch> {
    /**
     * the cost to reduce this set based on its type
     */
    public double costToReduceWidthOfSet = Double.MAX_VALUE;
    /**
     * the cost to reduce this set considering the proportional cost in the texture
     */
    public double proportionalCostToReduceWidth = Double.MAX_VALUE;
    /**
     * acumillated cost when a neighbor has recently been reduced so this would be near an existing carve.
     */
    public double neighborReduceWidthCost = 0.0;
    /**
     * The cost of moving diagonally from this stitch set
     */
    public double setDiagonalCost = 0.0;
    /**
     * the total cost of reducing this set based on its cost and the optimal path cost descending from this set
     */
    public double verticalPathCost = Double.MAX_VALUE;
    /**
     * the direction this set leans based on its physical properties
     */
    public Lean lean = Lean.centered;
    /**
     * flags if the path cost has been calculated
     */
    public boolean pathCalculated = false;
    /**
     * the course this set is in
     */
    public Course owningCourse = null;
    /**
     * the ordered set of stitches
     */
    public LinkedList<Stitch> stitches;

    /**
     * creates an empty stitch set
     */
    protected  StitchSet(){
        this.stitches = new LinkedList<>();
    }

    /**
     * @param stitches the stitches in the set
     */
    public StitchSet(@NotNull LinkedList<Stitch> stitches){
        this.setStitches(stitches);
    }

    /**
     * @param stitches the stitches in the set
     */
    public StitchSet(@NotNull Stitch... stitches){
        LinkedList<Stitch> stitchList = new LinkedList<>();
        for( Stitch s: stitches) {
            assert s != null;
            stitchList.addLast(s);
        }
        this.setStitches(stitchList);
    }

    public boolean replaceStitch(Stitch oldStitch, Stitch newStitch){
        if(!this.stitches.contains(oldStitch))
            return false;
        this.stitches.set(this.stitches.indexOf(oldStitch), newStitch);
        newStitch.owningStitchSet =this;
        return true;
    }

    /**
     * set the reduce cost for this stitch set
     * @return the set cost
     */
    private double setReduceWidthCost(){
        double localCost;
        double parentLoops = this.getParentLoops().size();
        if(parentLoops ==0)
            localCost=Double.MAX_VALUE;
        else
            localCost = (parentLoops-this.stitches.size()) / parentLoops;
        double cost = Math.min(localCost, 0);
        if(cost>0)
            cost= Double.MAX_VALUE;
        return cost;
    }

    /**
     * sets the proportional cost for this based on how common it is in the texture
     */
    public void setProportionalCost(){
        if(this.getFirstStitch() instanceof CastOn) {
            this.proportionalCostToReduceWidth = 0.0;
            return;
        }
        double setReduceWidthCost = this.setReduceWidthCost();
        if(setReduceWidthCost ==Double.MAX_VALUE)
            this.proportionalCostToReduceWidth = Double.MAX_VALUE;
        else {
            this.proportionalCostToReduceWidth = this.getProportionalCost() + setReduceWidthCost + this.neighborReduceWidthCost;
            assert !(this.proportionalCostToReduceWidth < 0);
        }
        assert !(this.proportionalCostToReduceWidth < 0);
    }

    private double getProportionalCost() {
        return this.proportionOfTexture() + this.proportionOfLine() + this.proportionOfLocal() + this.proportionOfWale();
    }

    /**
     * reduce this set by removing a child loop and reseting things so that it is a valid set
     */
    public void reduceWidthOfSet(){
        double minCost = Double.MAX_VALUE;
        Stitch minStitch=null;
        for(Stitch stitch: this) {
            if (stitch.costToReduceStitch() < minCost) {
                minCost = stitch.costToReduceStitch();
                minStitch = stitch;
            }
        }
        assert minStitch!=null;
        minStitch.removeChildFromYarn();
    }

    /**
     * @return the proportion of the texture that is of this type
     */
    @Contract(pure = true)
    public double proportionOfTexture(){
        double sum = 0.0;
        for(Stitch stitch: this)
            sum+= stitch.proportionOfTexture();
        return sum;
    }

    /**
     * @return the proportion of the course that is of this type
     */
    @Contract(pure = true)
    public double proportionOfLine(){
        double sum = 0.0;
        for(Stitch stitch: this)
            sum+= stitch.proportionOfLine();
        return sum;
    }

    /**
     * @return the proportion of the course that is of this type
     */
    @Contract(pure = true)
    private double proportionOfWale(){
        if(this.owningCourse.owningTextureGraph.waleChains.isEmpty())
            return 0;
        double sum = 0.0;
        for(Stitch stitch: this)
            sum+= stitch.proportionOfWale();
        return sum;
    }

    /**
     * @return the stitch whose child is prior to the first child in this set
     */
    @Nullable
    @Contract(pure = true)
    public Stitch getPriorStitchInLine(){
        StitchSet prior = this.getPriorSetInLine();
        if(prior != null)
            return prior.getLastStitch();
        return null;
    }

    /**
     * @return the stitch set who has the child that is prior to the first child in this set
     */
    @Contract(pure = true)
    @Nullable
    public StitchSet getPriorSetInLine(){
        if(this.owningCourse == null){
            Loop firstChild = this.stitches.getFirst().childLoop;
            assert firstChild != null;
            Loop priorChild = firstChild.getPriorNeighbor();
            assert priorChild != null;
            Stitch lastChildOf = priorChild.getLastChildOf();
            assert lastChildOf != null;
            return lastChildOf.owningStitchSet;
        }
        int index = this.getIndexInLine();
        if(index <=0)
            return null;
        return this.owningCourse.stitchSets.get(index-1);
    }

    /**
     * @return the stitch whose child is after to the last child in this set
     */
    @Nullable
    @Contract(pure = true)
    public Stitch getNextStitchInLine(){
        StitchSet next = this.getNextSetInLine();
        if(next != null)
            return next.getFirstStitch();
        return null;
    }
    /**
     * @return the stitch set whose has the child that is after to the last child in this set
     */
    @Nullable
    @Contract(pure = true)
    public StitchSet getNextSetInLine(){
        if(this.owningCourse == null){
            Loop lastChild = this.stitches.getLast().childLoop;
            assert lastChild != null;
            Loop nextChild = lastChild.getNextNeighbor();
            if(nextChild== null)
                return null;
            return Objects.requireNonNull(nextChild).getChildOf().iterator().next().owningStitchSet;
        }
        int index = this.getIndexInLine();
        if(index + 1 >= this.owningCourse.stitchSets.size())
            return null;
        return this.owningCourse.stitchSets.get(index+1);
    }

    /**
     * @return the index of this set in the course
     */
    @Contract(pure = true)
    private int getIndexInLine() {
        assert this.owningCourse != null;
        if(!this.owningCourse.stitchSets.contains(this))
            assert this.owningCourse.stitchSets.contains(this);
        return this.owningCourse.stitchSets.indexOf(this);
    }

    /**
     * @return the proportion of the neighobring stitch sets that is of this type
     */
    public double proportionOfLocal() {
        Stitch priorStitch = this.getPriorStitchInLine();
        Stitch nextStitch = this.getNextStitchInLine();
        double totalSets = 1.0;
        double matchingSets = 1.0;
        if (nextStitch != null) {
            assert nextStitch.owningStitchSet != this;
            if (nextStitch.owningStitchSet.getClass().equals(this.getClass()))
                matchingSets++;
            totalSets++;
        }
        if (priorStitch != null) {
            assert priorStitch.owningStitchSet != this;
            if (priorStitch.owningStitchSet.getClass().equals(this.getClass()))
                matchingSets++;
            totalSets++;
        }
        return (totalSets-matchingSets) / totalSets;
    }

    /**
     * @return the stitch sets that have stitches made whose parents are children in this set
     */
    @Nonnull
    @Contract(pure = true)
    public LinkedHashSet<StitchSet> descendants(){
        LinkedHashSet<StitchSet> descendants = new LinkedHashSet<>();
        for(Stitch stitch: this){
            for(Stitch descendantStitch: stitch.descendants())
                descendants.add(descendantStitch.owningStitchSet);
        }
        return descendants;
    }

    /**
     * @return the set that is the first descendant of the next stitch in course
     */
    @Nullable
    @Contract(pure = true)
    public StitchSet descendantOfNextStitchInLine(){
        LinkedList<Stitch> descendants = new LinkedList<>(this.getLastStitch().childLoop.getParentOf());
        if(descendants.isEmpty())
            return null;
        Stitch lastDescendant = descendants.getLast();
        Stitch neighbor = lastDescendant.nextNeighbor();
        if(neighbor == null)
            return null;
        return neighbor.owningStitchSet;
//        Loop next = lastDescendant.childLoop.getPriorNeighbor();
//        if(next == null)
//            return null;
//        if(next.getChildOf().isEmpty())
//            assert false;
//        return Objects.requireNonNull(next.getLastChildOf()).owningStitchSet;
    }

    /**
     * @return the set that is the last descendant of the prior stitch in course
     */
    @Nullable
    @Contract(pure = true)
    public StitchSet descendantofPriorStitchInLine(){
        LinkedList<Stitch> descendants = new LinkedList<>(this.getFirstStitch().childLoop.getParentOf());
        if(descendants.isEmpty())
            return null;
        Stitch firstDescendant = descendants.getFirst();
        Stitch neighbor = firstDescendant.priorNeighbor();
        if(neighbor == null)
            return null;
        return neighbor.owningStitchSet;
//        Loop prior = firstDescendant.childLoop.getNextNeighbor();
//        if(prior == null)
//            return null;
//        return Objects.requireNonNull(prior.getFirstChildOf()).owningStitchSet;
    }

    /**
     * @return the number of stitches in this set
     */
    @Contract(pure = true)
    public int stitchCount(){return stitches.size();}

    /**
     * creates a copy of this on a new yarn
     * @param yarn the yarn to copy to
     * @param parents the parent loops to build the new set from
     * @return the new that copies this stitch
     */
    @NotNull
    @Contract(pure = true)
    public StitchSet copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents){
        LinkedList<Stitch> newStitches = new LinkedList<>();
        for(Stitch stitch: this)
            newStitches.add(stitch.copyFromParents( yarn, parents));
        return new StitchSet(newStitches);
    }

    /**
     * creates a copy of this on a new yarn
     * @param priorLoop before the new copy
     * @param parents the parent loops to build the new set from
     * @return the new that copies this stitch
     */
    @NotNull
    @Contract(pure = true)
    public StitchSet copyFromParents(@NotNull Loop priorLoop, @NotNull LinkedList<Loop> parents){
        LinkedList<Stitch> newStitches = new LinkedList<>();
        for(Stitch stitch: this)
            newStitches.add(stitch.copyFromParents( priorLoop, parents));
        return new StitchSet(newStitches);
    }

    /**
     * Sets the stitch list of this set
     * @param stitches the stitches of this set
     */
    public void setStitches(@NotNull LinkedList<Stitch> stitches){
        this.stitches = stitches;
        for(Stitch stitch: this.stitches)
            stitch.owningStitchSet = this;
    }

    /**
     * adds the stitches to this set
     * @param newStitches the stitches to be added in iterator order
     */
    protected void addStitches(@NotNull Collection<Stitch> newStitches){
        for( Stitch s: newStitches)
            this.addStitch(s);
    }

    /**
     * adds the stitch to the end of the set
     * @param stitch the stitch to be added
     */
    protected void addStitch(Stitch stitch){
        stitch.owningStitchSet = this;
        this.stitches.addLast(stitch);
    }
    @Contract("null->false")
    public boolean equals(Object o){
        if(!(o instanceof StitchSet))
            return false;
        StitchSet ss = ((StitchSet) o);
        if(this.stitches.size() != ss.stitches.size())
            return false;
        for(int i= 0; i<this.stitches.size(); i++){
            Stitch s = this.stitches.get(i);
            Stitch s1 = ss.stitches.get(i);
            if(!s.equals(s1))
                return false;
        }
        return true;
    }
    @Nonnull
    @Contract(pure = true)
    public Stitch getLastStitch(){return this.stitches.getLast();}
    @Contract(pure = true)
    @Nonnull
    public Stitch getFirstStitch() {
        assert !this.stitches.isEmpty();
        return this.stitches.getFirst();}

    @NotNull
    @Override
    public Iterator<Stitch> iterator(){ return this.stitches.listIterator();}

    /**
     * @return the ordered set of loops that are children of stitches in this set
     */
    @NotNull
    @Contract(pure = true)
    public LinkedList<Loop> getChildLoops() {
        LinkedHashSet<Loop> childLoops = new LinkedHashSet<>();
        for(Stitch s: this)
            if(s.childLoop != null)
                childLoops.add(s.childLoop);
        return new LinkedList<>(childLoops);
    }
    @NotNull
    @Contract(pure = true)
    public LinkedList<Loop> getParentLoops(){
        LinkedHashSet<Loop> parentLoops = new LinkedHashSet<>();
        for(Stitch s: this)
            parentLoops.addAll(s.parentLoops);
        return new LinkedList<>(parentLoops);
    }

    @Override
    public String toString(){
        if(this.stitches.size() ==1) {
            String str =this.stitches.get(0).toString();
            if(this.pathCalculated)
                if(this.verticalPathCost ==Double.MAX_VALUE)
                    str+="(Infinity)";
                else
                    str+=String.format("%1$,.2f",this.verticalPathCost)+")";
            return str;
        }
        StringBuilder str = new StringBuilder("[");
        for(Stitch st: this.stitches)
            str.append(st.toString()).append(",");
        str.append("]");
        if(this.pathCalculated){
            if(this.verticalPathCost ==Double.MAX_VALUE)
                str.append("(Infinity)");
            else
                str.append("(").append(String.format("%1$,.2f",this.verticalPathCost)).append(")");
        }
        return str.toString();
    }
}
