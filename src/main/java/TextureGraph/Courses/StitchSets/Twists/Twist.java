package TextureGraph.Courses.StitchSets.Twists;

import TextureGraph.Courses.StitchSets.Cables.Cable;
import TextureGraph.Courses.StitchSets.Cables.CrossSide;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 4/18/2017.
 * a set of 2 stitches that get crossed over each other
 */
public abstract class Twist extends Cable {
    Twist(CrossSide crossSide, @NotNull Yarn yarn, @NotNull LinkedList<Loop> parents, boolean purl){
        super(crossSide, parents,1,2,purl, yarn);
    }
    Twist(CrossSide crossSide, @NotNull LinkedList<Loop> parents, boolean purl, @NotNull LinkedList<Loop> children){
        super(crossSide, parents, 1, 2, purl, children);
    }

    public static Twist twistFactory(CrossSide crossSide, @NotNull Yarn yarn, @NotNull LinkedList<Loop> parents, boolean purl){
        if(crossSide == CrossSide.FRONT){
            if(purl)
                return new T2LP(parents, yarn);
            else
                return new T2L(parents, yarn);
        }else{
            if(purl)
                return new T2RP(parents, yarn);
            else
                return new T2R(parents, yarn);
        }
    }

    public static Twist twistFactory(CrossSide crossSide, @NotNull LinkedList<Loop> parents, boolean purl, @NotNull LinkedList<Loop> children){
        if(crossSide == CrossSide.FRONT){
            if(purl)
                return new T2LP(parents, children);
            else
                return new T2L(parents, children);
        }else{
            if(purl)
                return new T2RP(parents, children);
            else
                return new T2R(parents, children);
        }
    }

}
