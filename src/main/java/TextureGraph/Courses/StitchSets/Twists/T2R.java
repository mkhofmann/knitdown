package TextureGraph.Courses.StitchSets.Twists;

import TextureGraph.Courses.StitchSets.Cables.CrossSide;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 7/22/2016.
 * a twist set where the second stitch passes over to the right
 */
public class T2R extends Twist {
    public T2R(@NotNull LinkedList<Loop> parents, @NotNull Yarn yarn){
        super(CrossSide.BACK, yarn, parents, false);
    }
    public T2R(@NotNull LinkedList<Loop> parents, @NotNull LinkedList<Loop> children){
        super(CrossSide.BACK, parents, false, children);
    }
}
