package TextureGraph.Courses.StitchSets.Twists;

import TextureGraph.Courses.StitchSets.Cables.CrossSide;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 7/22/2016.
 * a twist set where the second stitch passes over to the right, with back as purl
 */
public class T2RP extends Twist {
    public T2RP(@NotNull LinkedList<Loop> parents, @NotNull Yarn yarn){
        super(CrossSide.BACK, yarn, parents, true);
    }
    public T2RP(@NotNull LinkedList<Loop> parents, @NotNull LinkedList<Loop> children){
        super(CrossSide.BACK, parents, true, children);
    }
}
