package TextureGraph.Courses.StitchSets.Twists;

import TextureGraph.Courses.StitchSets.Cables.CrossSide;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 7/22/2016.
 * a twist set where the first stitch passes over to the left
 */
public class T2L extends Twist {
    public T2L(@NotNull LinkedList<Loop> parents,@NotNull Yarn yarn){
        super(CrossSide.FRONT, yarn, parents, false);
    }
    public T2L(@NotNull LinkedList<Loop> parents, @NotNull LinkedList<Loop> children){
        super(CrossSide.FRONT, parents, false, children);
    }
}
