package TextureGraph.Courses.StitchSets.Increases;

import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Knit.K;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.Purl.P;
import TextureGraph.Courses.Stitches.Stitch;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 1/5/2017.
 * increase from one parent to 6
 */
public class Inc6 extends IncreaseStitchMaps {
    public Inc6(@NotNull Loop parent, @NotNull Yarn yarn) {
        super(parent, yarn);
    }
    public Inc6(@NotNull Loop parent, @NotNull LinkedList<Loop> children){
        super(parent, children);
    }
    @Override
    @NotNull
    public StitchSet copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents) {return new Inc6(parents.removeFirst(), yarn);}
    @NotNull
    @Override
    protected LinkedList<Stitch> makeStitches(@NotNull Yarn yarn) {
        LinkedList<Stitch> newStitches = new LinkedList<>();
        newStitches.add(new K(this.parent, yarn));
        newStitches.add(new P(this.parent, yarn));
        newStitches.add(new K(this.parent, yarn));
        newStitches.add(new P(this.parent, yarn));
        newStitches.add(new K(this.parent, yarn));
        newStitches.add(new P(this.parent, yarn));
        return newStitches;
    }
    @NotNull
    @Override
    protected LinkedList<Stitch> makeStitches(@NotNull LinkedList<Loop> children) {
        assert children.size()==6;
        LinkedList<Stitch> newStitches = new LinkedList<>();
        newStitches.add(new K(this.parent, children.get(0)));
        newStitches.add(new P(this.parent, children.get(1)));
        newStitches.add(new K(this.parent, children.get(2)));
        newStitches.add(new P(this.parent, children.get(3)));
        newStitches.add(new K(this.parent, children.get(4)));
        newStitches.add(new P(this.parent, children.get(5)));
        return newStitches;
    }

}