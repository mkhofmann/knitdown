package TextureGraph.Courses.StitchSets.Increases;

import TextureGraph.Courses.StitchSets.CanonicalStitchSet;
import TextureGraph.Courses.StitchSets.FlattenableSet;
import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Stitch;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * An Increase is a set of stitches that share the same parent loop
 */
public abstract class Increase extends CanonicalStitchSet implements FlattenableSet{
    Increase(@NotNull Stitch front, @NotNull Stitch back) {
        super(front, back);
    }

    Increase() {
        super();
    }

    private boolean isFlattened = false;
    @NotNull
    @Override
    public LinkedList<Single> flattenSet(boolean retainSetStructure){
        LinkedList<Single> newSets = new LinkedList<>();
        for(Stitch stitch: stitches){
            stitch.owningStitchSet = null;
            newSets.add(new Single(stitch));
        }
        this.stitches.clear();
        if(retainSetStructure)
            for(Single single: newSets)
                this.addStitch(single.stitches.getFirst());
        this.isFlattened =true;
        return newSets;
    }

    @Override
    public StitchSet unFlattenSet(){
        assert !this.stitches.isEmpty();
        this.isFlattened = false;
        return this;
    }

    @Override
    public boolean isFlattened(){
        return isFlattened;
    }

}
