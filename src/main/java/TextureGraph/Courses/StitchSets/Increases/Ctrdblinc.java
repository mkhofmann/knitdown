package TextureGraph.Courses.StitchSets.Increases;

import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Knit.K;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 1/5/2017.
 * center dobule increase, K2 in one loop, m1L between knits
 */
public class Ctrdblinc extends Increase {
    /**
     * @param parent the parent loop of the stitches
     * @param yarn the yarn to create the  children from
     */
    public Ctrdblinc(@NotNull Loop parent, @NotNull Yarn yarn){
        super();
        for(int i= 0; i<3; i++)
            this.addStitch(new K(parent, yarn));
    }

    /**
     * @param parent the parent loop of the stitches
     * @param children the child loops of this increase
     */
    public Ctrdblinc(@NotNull Loop parent, @NotNull LinkedList<Loop> children){
        super();
        assert children.size()==3;
        for(Loop child: children)
            this.addStitch(new K(parent, child));
    }
    @Override
    @NotNull
    public StitchSet copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents) {
        return new Ctrdblinc(parents.removeFirst(), yarn);
    }

}
