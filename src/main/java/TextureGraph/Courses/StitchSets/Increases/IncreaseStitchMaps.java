package TextureGraph.Courses.StitchSets.Increases;

import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.Stitch;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.util.LinkedList;

/**
 * Created by Megan on 1/5/2017.
 * a set that has one parent leading to multiple parents based on stitch maps structures
 */
public abstract class IncreaseStitchMaps extends Increase {
    /**
     * the loop that is the parent of all the stitches
     */
    Loop parent;

    /**
     * @param parent the parent loop of the stitches
     * @param yarn the yarn to create the children from
     */
    IncreaseStitchMaps(@NotNull Loop parent, @NotNull Yarn yarn) {
        super();
        this.parent = parent;
        this.addStitches(this.makeStitches(yarn));
    }

    /**
     * @param parent the parent loop of the stitches
     * @param children the child loops of this increase
     */
    IncreaseStitchMaps(@NotNull Loop parent, @NotNull LinkedList<Loop> children){
        super();
        this.parent = parent;
        this.addStitches(this.makeStitches(children));
    }

    /**
     * @param yarn the yarn to create the children from
     * @return the stitches that make up the increase
     */
    @Nonnull
    protected abstract LinkedList<Stitch> makeStitches(@NotNull Yarn yarn);

    /**
     * @param children the child loops of this increase
     * @return the stitches that make up the increase
     */
    @Nonnull
    protected abstract LinkedList<Stitch> makeStitches(@NotNull LinkedList<Loop> children);

}
