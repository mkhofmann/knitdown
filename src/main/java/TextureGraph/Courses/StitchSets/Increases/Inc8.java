package TextureGraph.Courses.StitchSets.Increases;

import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Knit.K;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.Purl.P;
import TextureGraph.Courses.Stitches.Stitch;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 1/5/2017.
 * increase from one parent to 8
 */
public class Inc8  extends IncreaseStitchMaps {
    public Inc8(Loop parent, Yarn yarn) {
        super(parent, yarn);
    }
    public Inc8(Loop parent, LinkedList<Loop> children){
        super(parent, children);
    }
    @Override
    @NotNull
    public StitchSet copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents) {
        return new Inc8(parents.removeFirst(), yarn);
    }
    @NotNull
    @Override
    protected LinkedList<Stitch> makeStitches(@NotNull LinkedList<Loop> children) {
        assert children.size()==8;
        LinkedList<Stitch> newStitches = new LinkedList<>();
        newStitches.add(new K(this.parent, children.get(0)));
        newStitches.add(new P(this.parent, children.get(1)));
        newStitches.add(new K(this.parent, children.get(2)));
        newStitches.add(new P(this.parent, children.get(3)));
        newStitches.add(new K(this.parent, children.get(4)));
        newStitches.add(new P(this.parent, children.get(5)));
        newStitches.add(new K(this.parent, children.get(6)));
        newStitches.add(new P(this.parent, children.get(7)));
        return newStitches;
    }

    @NotNull
    @Override
    protected LinkedList<Stitch> makeStitches(@NotNull Yarn yarn) {
        LinkedList<Stitch> newStitches = new LinkedList<>();
        newStitches.add(new K(this.parent, yarn));
        newStitches.add(new P(this.parent, yarn));
        newStitches.add(new K(this.parent, yarn));
        newStitches.add(new P(this.parent, yarn));
        newStitches.add(new K(this.parent, yarn));
        newStitches.add(new P(this.parent, yarn));
        newStitches.add(new K(this.parent, yarn));
        newStitches.add(new P(this.parent, yarn));
        return newStitches;
    }

}
