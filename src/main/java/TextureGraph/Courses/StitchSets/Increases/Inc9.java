package TextureGraph.Courses.StitchSets.Increases;

import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Knit.K;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.Stitch;
import TextureGraph.Courses.Stitches.YO;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 1/5/2017.
 * increase from one parent to 9
 */
public class Inc9 extends IncreaseStitchMaps {
    public Inc9(@NotNull Loop parent, @NotNull Yarn yarn) {
        super(parent, yarn);
    }

    public Inc9(Loop parent, LinkedList<Loop> children) {
        super(parent, children);
    }

    @Override
    @NotNull
    public StitchSet copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents) {
        return new Inc9(parents.removeFirst(), yarn);
    }

    @NotNull
    @Override
    protected LinkedList<Stitch> makeStitches(@NotNull LinkedList<Loop> children) {
        assert children.size()==9;
        LinkedList<Stitch> newStitches = new LinkedList<>();
        newStitches.add(new K(this.parent, children.get(0)));
        newStitches.add(new YO(            children.get(1)));
        newStitches.add(new K(this.parent, children.get(2)));
        newStitches.add(new YO(            children.get(3)));
        newStitches.add(new K(this.parent, children.get(4)));
        newStitches.add(new YO(            children.get(5)));
        newStitches.add(new K(this.parent, children.get(6)));
        newStitches.add(new YO(            children.get(7)));
        newStitches.add(new K(this.parent, children.get(8)));
        return newStitches;
    }

    @NotNull
    @Override
    protected LinkedList<Stitch> makeStitches(@NotNull Yarn yarn) {
        LinkedList<Stitch> newStitches = new LinkedList<>();
        newStitches.add(new K(this.parent, yarn));
        newStitches.add(new YO(            yarn));
        newStitches.add(new K(this.parent, yarn));
        newStitches.add(new YO(            yarn));
        newStitches.add(new K(this.parent, yarn));
        newStitches.add(new YO(            yarn));
        newStitches.add(new K(this.parent, yarn));
        newStitches.add(new YO(            yarn));
        newStitches.add(new K(this.parent, yarn));
        return newStitches;
    }

}
