package TextureGraph.Courses.StitchSets.Increases;

import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.Purl.P;
import TextureGraph.Courses.Stitches.YO;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 7/18/2016.
 * purl into front and back of parent loop for an increase
 */
public class PFB extends Increase {
    /**
     * @param parent the parent loop of the stitches
     * @param yarn the yarn to create the two children from
     */
    public PFB(@NotNull Loop parent, @NotNull Yarn yarn){
        super();
        P purlFront = new P(parent, yarn);
        YO yo = new YO(yarn);
        this.addStitch(purlFront);
        this.addStitch(yo);
    }
    /**
     * @param parent the parent loop of the stitches
     * @param child1 the first child loop
     * @param child2 the second child loop
     */
    public PFB(@NotNull Loop parent, @NotNull Loop child1, @NotNull Loop child2){
        super();
        P purlFront = new P(parent, child1);
        YO yo = new YO(child2);
        this.addStitch(purlFront);
        this.addStitch(yo);
    }

    @Override
    @NotNull
    public StitchSet copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents){return new PFB(parents.removeFirst(), yarn);}

}
