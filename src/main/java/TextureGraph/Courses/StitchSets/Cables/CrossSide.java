package TextureGraph.Courses.StitchSets.Cables;

/**
 * Created by Megan on 11/7/2016.
 * declares the side stitches are being passed in a cable
 */
public enum CrossSide {
    FRONT,//crosses to the left
    BACK  //crosses to the right
}
