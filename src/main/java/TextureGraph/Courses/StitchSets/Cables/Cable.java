package TextureGraph.Courses.StitchSets.Cables;

import TextureGraph.Courses.Lean;
import TextureGraph.Courses.StitchSets.CanonicalStitchSet;
import TextureGraph.Courses.StitchSets.FlattenableSet;
import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.StitchSets.Twists.*;
import TextureGraph.Courses.Stitches.Knit.K;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.OneToOneStitch;
import TextureGraph.Courses.Stitches.Purl.P;
import TextureGraph.Courses.Stitches.Stitch;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * Created by Megan on 11/7/2016.
 * A stitch set of more than 2 stitches that are passed in crossing order
 */
public class Cable extends CanonicalStitchSet implements FlattenableSet{
    /**
     * Notes the side that the stitches are being crossed over creating the leaning effect
     */
    private CrossSide crossSide;
    /**
     * the number of stitches in the cable
     */
    private int stitchesCount;
    /**
     * the number of stitches being heald before knitting
     */
    private int hold;
    /**
     * flags if the unheld stitches are to be purled
     */
    private boolean purl;
    /**
     * the list of stitches that are being heald to be knitted last
     */
    public ArrayList<OneToOneStitch> heald;
    /**
     * the list of stitches that were not heald and are knitted first
     */
    public ArrayList<OneToOneStitch> unHeald;

    /**
     * @param crossSide the side that the unheld stitches are crossed over
     * @param hold the number of stitches to hold
     * @param stitchesCount the total number of stitches in the cable
     * @param purl flags if the unheld sitches are to be purled
     */
    private Cable(@NotNull CrossSide crossSide, int hold, int stitchesCount, boolean purl){
        super();
        this.crossSide  = crossSide;
        this.lean = Cable.compareCrossSideToLean(this.crossSide);
        this.stitchesCount = stitchesCount;
        this.hold = hold;
        if(this.stitchesCount %2==1 && this.crossSide == CrossSide.FRONT)//if its odd, cross the majority in front
            this.hold++;
        this.purl = purl;
        this.setDiagonalCost = Double.MAX_VALUE;
    }
    /**
     * stitch set order will be based on position of children, for easy building
     * @param crossSide the side that heald stitchs will fall on
     * @param parents the parentYarn loops to build the cable from
     * @param hold the number of stitches to be heald
     * @param stitchesCount number of stitches in the cable
     * @param purl true if hte back stitches will be purls
     * @param yarn the yarn to create new loops from
     */
    public Cable(@NotNull CrossSide crossSide, @NotNull LinkedList<Loop> parents, int hold, int stitchesCount, boolean purl, @NotNull Yarn yarn){
        this(crossSide,hold, stitchesCount, purl);
        int i;
        this.heald = new ArrayList<>(this.hold);
        this.unHeald = new ArrayList<>(this.stitchesCount - this.hold);
        for(i=0; i<hold; i++) {
            if(this.crossSide == CrossSide.FRONT) {
                this.heald.add(new K(parents.get(i), yarn));
                this.heald.get(i).stitchOrder = 1;
            } else {
                if(this.purl)
                    this.heald.add(new P(parents.get(i), yarn));
                else
                    this.heald.add(new K(parents.get(i), yarn));
                this.heald.get(i).stitchOrder = -1;
            }
        }
        for(i=hold; i<parents.size(); i++) {
            if(this.crossSide == CrossSide.FRONT) {
                if(this.purl)
                    this.unHeald.add(new P(parents.get(i), yarn));
                else
                    this.unHeald.add(new K(parents.get(i), yarn));
                this.unHeald.get(i-hold).stitchOrder = -1;
            } else {
                this.unHeald.add(new K(parents.get(i), yarn));
                this.unHeald.get(i-hold).stitchOrder = 1;
            }
        }
        this.setStitchesByHolds();

    }

    /**
     * @param crossSide the side that heald stitchs will fall on
     * @param parents the parentYarn loops to build the cable from
     * @param stitchesCount number of stitches in the cable
     * @param purl true if hte back stitches will be purls
     * @param yarn the yarn to create new loops from
     */
    public Cable(@NotNull CrossSide crossSide, @NotNull LinkedList<Loop> parents, int stitchesCount, boolean purl, @NotNull Yarn yarn){
        this(crossSide, parents, stitchesCount /2, stitchesCount, purl, yarn);
    }

    /**
     * @param crossSide the side that heald stitchs will fall on
     * @param parents the parentYarn loops to build the cable from
     * @param hold the number of stitches to be heald
     * @param stitchesCount number of stitches in the cable
     * @param purl true if hte back stitches will be purls
     * @param children the child loops that the cable is constructed from
     */
    public Cable(@NotNull CrossSide crossSide, @NotNull LinkedList<Loop> parents, int hold, int stitchesCount, boolean purl, @NotNull LinkedList<Loop> children){
        this(crossSide,hold, stitchesCount, purl);
        int i;
        this.heald = new ArrayList<>(this.hold);
        this.unHeald = new ArrayList<>(this.stitchesCount - this.hold);
        for(i=0; i<hold; i++) {
            if(this.crossSide == CrossSide.FRONT) {
                this.heald.add(new K(parents.get(i), children.get(i)));
                this.heald.get(i).stitchOrder = 1;
            } else {
                if(this.purl)
                    this.heald.add(new P(parents.get(i), children.get(i)));
                else
                    this.heald.add(new K(parents.get(i), children.get(i)));
                this.heald.get(i).stitchOrder = -1;
            }
        }
        for(i=hold; i<parents.size(); i++) {
            if(this.crossSide == CrossSide.FRONT) {
                if(this.purl)
                    this.unHeald.add(new P(parents.get(i), children.get(i)));
                else
                    this.unHeald.add(new K(parents.get(i), children.get(i)));
                this.unHeald.get(i-hold).stitchOrder = -1;
            } else {
                this.unHeald.add(new K(parents.get(i), children.get(i)));
                this.unHeald.get(i-hold).stitchOrder = 1;
            }
        }
        this.setStitchesByHolds();
    }

    /**
     * @param crossSide the side that heald stitchs will fall on
     * @param parents the parentYarn loops to build the cable from
     * @param stitchesCount number of stitches in the cable
     * @param purl true if hte back stitches will be purls
     * @param children the child loops that the cable is constructed from
     */
    public Cable(@NotNull CrossSide crossSide, @NotNull LinkedList<Loop> parents, int stitchesCount, boolean purl, @NotNull LinkedList<Loop> children){
        this(crossSide, parents, stitchesCount /2, stitchesCount, purl, children);this.stitchesCount = stitchesCount;
        this.hold = stitchesCount /2;
        if(this.stitchesCount %2==1 && crossSide == CrossSide.FRONT)//if its odd, cross the majority in front
            this.hold++;
        this.crossSide  = crossSide;
        this.purl = purl;
        int i;
        this.heald = new ArrayList<>(this.hold);
        this.unHeald = new ArrayList<>(this.stitchesCount - this.hold);
        for(i=0; i<hold; i++) {
            if(this.crossSide == CrossSide.FRONT) {
                this.heald.add(new K(parents.get(i), children.get(i)));
                this.heald.get(i).stitchOrder = 1;
            } else {
                if(this.purl)
                    this.heald.add(new P(parents.get(i), children.get(i)));
                else
                    this.heald.add(new K(parents.get(i), children.get(i)));
                this.heald.get(i).stitchOrder = -1;
            }
        }
        for(i=hold; i<parents.size(); i++) {
            if(this.crossSide == CrossSide.FRONT) {
                if(this.purl)
                    this.unHeald.add(new P(parents.get(i), children.get(i)));
                else
                    this.unHeald.add(new K(parents.get(i), children.get(i)));
                this.unHeald.get(i-hold).stitchOrder = -1;
            } else {
                this.unHeald.add(new K(parents.get(i), children.get(i)));
                this.unHeald.get(i-hold).stitchOrder = 1;
            }
        }
        this.setStitchesByHolds();
    }

    /**
     * @param stitch the stitch being checked
     * @return true if the stitch is in the heald set
     */
    @Contract(pure = true)
    public boolean isHeld(OneToOneStitch stitch){
        assert this.stitches.contains(stitch);
        return this.heald.contains(stitch);
    }

    @Override
    public void reduceWidthOfSet(){
        double minCost = Double.MAX_VALUE;
        Stitch minStitch=null;
        if(this.crossSide == CrossSide.FRONT) {
            for (Stitch stitch : this.heald) {
                if (stitch.costToReduceStitch() < minCost) {
                    minCost = stitch.costToReduceStitch();
                    minStitch = stitch;
                }
            }
        } else{
            for (Stitch stitch : this.unHeald) {
                if (stitch.costToReduceStitch() < minCost) {
                    minCost = stitch.costToReduceStitch();
                    minStitch = stitch;
                }
            }
        }
        assert minStitch!=null;
        minStitch.removeChildFromYarn();
    }
    @Override
    @NotNull
    public StitchSet copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents) {
        LinkedList<Loop> neededParents= new LinkedList<>();
        for(int i=0; i<this.stitches.size(); i++)
            neededParents.add(parents.removeFirst());
        return new Cable(this.crossSide, neededParents, this.stitches.size(), this.purl,yarn);
    }

    /**
     * reset the stitches to meet the standard Stitch Essential strucure
     */
    private void setStitchesByHolds(){
        this.stitches.clear();
        for(OneToOneStitch go: this.unHeald) {
            go.lean = Lean.left;
            this.addStitch(go);
        }
        for(OneToOneStitch hold: this.heald) {
            hold.lean = Lean.right;
            this.addStitch(hold);
        }
    }

    //fixes any twists that result from realigning loops in a decrease/increase pass
    public void unTwistCable(){
        this.unTwistHolds();
        this.unTwistNoHolds();
    }

    /**
     * untwists heald as a result of a realignment
     */
    private void unTwistHolds(){
        ArrayList<OneToOneStitch> holdsSortedByChildIndex = new ArrayList<>(this.heald);
        holdsSortedByChildIndex.sort(OneToOneStitch.ChildLoop);
        ArrayList<OneToOneStitch> holdsSortedByParentIndex = new ArrayList<>(this.heald);
        holdsSortedByParentIndex.sort(OneToOneStitch.ParentLoop);
        ArrayList<OneToOneStitch> invertsByParent = new ArrayList<>(this.heald.size());
        int forward = 0;
        for(int back = this.heald.size()-1; forward<this.heald.size(); back--, forward++)
            if (!holdsSortedByChildIndex.get(forward).equals(holdsSortedByParentIndex.get(back)))
                invertsByParent.add(holdsSortedByParentIndex.get(back));
        if(invertsByParent.size()==0)
            return;
        assert invertsByParent.size()==2;//it shouldn't get more messed up than this right?
        invertsByParent.get(0).swapChildren(invertsByParent.get(1));
    }
    /**
     * untwists heald as a result of a realignment
     */
    private void unTwistNoHolds(){
        ArrayList<OneToOneStitch> noHoldsSortedByChildIndex = new ArrayList<>(this.unHeald);
        noHoldsSortedByChildIndex.sort(OneToOneStitch.ChildLoop);
        ArrayList<OneToOneStitch> noHoldsSortedByParentIndex = new ArrayList<>(this.unHeald);
        noHoldsSortedByParentIndex.sort(OneToOneStitch.ParentLoop);
        ArrayList<OneToOneStitch> invertsByParent = new ArrayList<>(this.unHeald.size());
        int forward = 0;
        for(int back = this.unHeald.size()-1; forward<this.unHeald.size(); back--, forward++)
            if (!noHoldsSortedByChildIndex.get(forward).equals(noHoldsSortedByParentIndex.get(back)))
                invertsByParent.add(noHoldsSortedByParentIndex.get(back));
        if(invertsByParent.size()==0)
            return;
        assert invertsByParent.size()==2;//it shouldn't get more messed up than this right?
        invertsByParent.get(0).swapChildren(invertsByParent.get(1));
        OneToOneStitch first = invertsByParent.get(0);
        OneToOneStitch second = invertsByParent.get(1);
        int firstStitchIndex = this.stitches.indexOf(first);
        int secondStitchIndex = this.stitches.indexOf(second);
        this.stitches.set(firstStitchIndex, second);
        this.stitches.set(secondStitchIndex, first);

        firstStitchIndex = this.unHeald.indexOf(first);
        secondStitchIndex = this.unHeald.indexOf(second);
        this.unHeald.set(firstStitchIndex, second);
        this.unHeald.set(secondStitchIndex, first);
    }

    private boolean isFlattened = false;
    private LinkedList<Loop> originalParentOrder = new LinkedList<>();
    private LinkedList<Loop> originalChildOrder = new LinkedList<>();

    @NotNull
    @Override
    public LinkedList<Single> flattenSet(boolean retainSetStructure) {
        this.originalChildOrder = new LinkedList<>();
        for (Iterator<Loop> it = this.getChildLoops().descendingIterator(); it.hasNext(); )
            this.originalChildOrder.add(it.next());
        this.originalParentOrder = new LinkedList<>();
        for (Iterator<Loop> it = this.getParentLoops().descendingIterator(); it.hasNext(); )
            this.originalParentOrder.add(it.next());
//        this.originalChildOrder = this.getChildLoops();
//        this.originalParentOrder = this.getParentLoops();
        TreeSet<Loop> parents = new TreeSet<>(this.getParentLoops());
        TreeSet<Loop> children = new TreeSet<>(this.getChildLoops());
        HashMap<Loop, Class<? extends Stitch>> parentsToTypeMap = new HashMap<>();
        assert parents.size() == children.size();
        LinkedList<Single> newSets = new LinkedList<>();
        for(Stitch stitch: this) {
            assert stitch instanceof OneToOneStitch;
            parentsToTypeMap.put(((OneToOneStitch)stitch).parentLoops.getFirst(), stitch.getClass());
            stitch.childLoop.removeChildOf(stitch);
            for(Loop parent: stitch.parentLoops)
                parent.removeParentOf(stitch);
            stitch.owningStitchSet = null;
        }
        this.stitches.clear();
        Iterator<Loop> parentItr = parents.descendingIterator();
        for (Loop child : children) {
            assert parentItr.hasNext();
            Loop parent = parentItr.next();
            Class<? extends Stitch> type = parentsToTypeMap.get(parent);
            if (type == K.class)
                newSets.add(new Single(new K(parent, child)));
            else if (type == P.class)
                newSets.add(new Single(new P(parent, child)));
            else
                assert false;
        }
        if(retainSetStructure)
            for(Single single: newSets)
                this.addStitch(single.stitches.getFirst());
        this.isFlattened = true;
        return newSets;
    }

    @Override
    public StitchSet unFlattenSet(){
        HashSet<Loop> currentParents = new HashSet<>(this.getParentLoops());
        HashSet<Loop> currentChildren = new HashSet<>(this.getChildLoops());
        HashSet<Loop> parentsToRemove = new HashSet<>();
        HashSet<Loop> childrenToRemove = new HashSet<>();
        for(Loop originalParent: this.originalParentOrder)
            if(!currentParents.contains(originalParent))
                parentsToRemove.add(originalParent);
        for(Loop originalChild: this.originalChildOrder)
            if(!currentChildren.contains(originalChild))
                childrenToRemove.add(originalChild);
        for(Stitch stitch: this.stitches) {
            stitch.childLoop.removeChildOf(stitch);
            stitch.parentLoops.getFirst().removeParentOf(stitch);
        }
        assert parentsToRemove.size() == childrenToRemove.size();
        this.originalChildOrder.removeAll(childrenToRemove);
        this.originalParentOrder.removeAll(parentsToRemove);
        return cableFactory(this.crossSide, this.originalParentOrder, this.originalChildOrder.size(), this.purl, this.originalChildOrder );
    }
    @Override
    public boolean isFlattened(){
        return isFlattened;
    }

    @Contract(pure = true)
    private static Lean compareCrossSideToLean(@NotNull CrossSide crossSide){
        switch (crossSide){
            case FRONT:
                return Lean.left;
            case BACK:
                return Lean.right;
            default:
                assert false;
                return Lean.centered;
        }
    }


    public static Cable cableFactory(CrossSide crossSide, @NotNull Yarn yarn, @NotNull LinkedList<Loop> parents, int hold, int stitchesCount,boolean purl){
        if(stitchesCount == 2)
            return Twist.twistFactory(crossSide, yarn, parents, purl);
        else
            return new Cable(crossSide, parents,hold, stitchesCount, purl, yarn);
    }

    public static Cable cableFactory(CrossSide crossSide, @NotNull LinkedList<Loop> parents, int stitchesCount, boolean purl, @NotNull LinkedList<Loop> children){

        if(stitchesCount == 2)
            return Twist.twistFactory(crossSide,parents, purl, children);
        else
            return new Cable(crossSide, parents, stitchesCount, purl, children);
    }
}
