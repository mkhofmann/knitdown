package TextureGraph.Courses.StitchSets.Cables;

import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 1/5/2017.
 * a cable declared by a stitch maps set
 */
public class CableStitchMaps extends Cable{
    public CableStitchMaps(@NotNull CrossSide crossSide, @NotNull LinkedList<Loop> parents, int holdCount, int startCount, boolean purl, @NotNull Yarn yarn, int repSection){
        super(crossSide, parents, holdCount+startCount, purl, yarn);
    }

}
