package TextureGraph.Courses.StitchSets;

import TextureGraph.Courses.Stitches.Stitch;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

/**
 * Created by Megan on 2/23/2017.
 * a stitch set that is based on an established stitch set representation, like cables, twists and standard increases
 */
public abstract class CanonicalStitchSet extends StitchSet{
    protected CanonicalStitchSet(){super();}

    public CanonicalStitchSet(@NotNull Stitch... stitches){super(stitches);}
    @Override
    @Contract(pure = true)
    public double proportionOfTexture(){
        double typeFreq = this.owningCourse.owningTextureGraph.stitchSetTypeToCountMap.get(this.getClass());
        double setCount = this.owningCourse.owningTextureGraph.stitchSetCount;
        double setTypesCount = this.owningCourse.owningTextureGraph.stitchSetTypeCount;
        return (typeFreq/setCount)+(1.0/setTypesCount);
    }
    @Override
    @Contract(pure = true)
    public double proportionOfLine(){
        double typeFreq = this.owningCourse.stitchSetTypeToCountMap.get(this.getClass());
        double setCount = this.owningCourse.getStitchSetCount();
        double setTypesCount = this.owningCourse.stitchSetTypeCount;
        return (typeFreq/setCount)+(1.0/setTypesCount);
    }
}
