package TextureGraph.Courses.StitchSets;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Interface for Stitch sets that can be flattened to a set of Single
 */
public interface FlattenableSet {
    @NotNull
    LinkedList<Single> flattenSet(boolean retainSetStructure);

    StitchSet unFlattenSet();

    boolean isFlattened();
}
