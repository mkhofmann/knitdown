package TextureGraph.Courses;

import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.StitchInRow.CastOn;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 11/8/2016.
 * A course of only cast ons that starts the textureGraph
 */
public class CastOnCourse extends Course {
    /**
     * @param numberOfCastOns the number of cast ons in the new row
     * @param yarn the yarn to create the cast ons from
     */
    public CastOnCourse(int numberOfCastOns, @NotNull Yarn yarn){
        super(new LinkedList<>(), 0);
        for(int i=0; i<numberOfCastOns; i++)
            this.stitchSets.addLast(new Single(new CastOn(yarn)));
        this.turnAtEndOfLine();
    }

}
