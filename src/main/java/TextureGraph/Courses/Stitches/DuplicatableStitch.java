package TextureGraph.Courses.Stitches;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

/**
 * Created by Megan on 4/21/2017.
 */
public interface DuplicatableStitch {
    /**
     * @return the stitch that duplicates this and is inserted next
     */
    @NotNull
    @Contract(pure = true)
    OneToOneStitch duplicateStitchNext();

}
