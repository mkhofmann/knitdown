package TextureGraph.Courses.Stitches.Purl;

import TextureGraph.Courses.Lean;
import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Decrease;
import TextureGraph.Courses.Stitches.IncreasableDecrease;
import TextureGraph.Courses.Stitches.Knit.K;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.LoopDirection;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.Stitch;
import Tracking.TypeTracking.KPStyle;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 7/18/2016.
 * A decrease many to one stitch that gathers X parents to purl one child
 */
public class Pxtog extends Decrease implements IncreasableDecrease {
    /**
     * @param yarn the yarn to  make the child loop at the end off
     * @param parents the parent loops to be decreased together
     */
    public Pxtog(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents){
        super(parents, new Loop(yarn, LoopDirection.FtB));
        this.lean = Lean.right;
        for(Loop parent: this.parentLoops)
            parent.makeParentOf(this);
        this.expectedParentCount = parents.size();
        this.kpStyle = KPStyle.PStyle;
    }
    /**
     * @param parents the parent loops to be decreased together
     * @param child the child loop
     */
    public Pxtog(@NotNull LinkedList<Loop> parents,@NotNull Loop child){
        super(parents, child);
        this.lean = Lean.right;
        this.expectedParentCount = parents.size();
        for(Loop parent: this.parentLoops)
            parent.makeParentOf(this);
        this.expectedParentCount = parents.size();
        this.kpStyle = KPStyle.PStyle;
    }

    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents) {
        LinkedList<Loop> neededParents= new LinkedList<>();
        for(int i=0; i<this.parentLoops.size(); i++)
            neededParents.add(parents.removeFirst());
        return new Pxtog(yarn, neededParents);
    }

    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Loop priorLoop, @NotNull LinkedList<Loop> parents){
        LinkedList<Loop> neededParents= new LinkedList<>();
        for(int i=0; i<this.parentLoops.size(); i++)
            neededParents.add(parents.removeFirst());
        return new Pxtog(neededParents, priorLoop.insertNextLoop(LoopDirection.FtB));
    }

    @NotNull
    @Override
    public StitchSet increaseStitch() {//kxtog-> kx-1tog, k
        childLoop.removeChildOf(this);
        for(Loop parent: this.parentLoops)
            parent.removeParentOf(this);
        LinkedList<Loop> decreaseParents = new LinkedList<>();
        for(int i= 0; i<this.parentLoops.size()-1; i++)
            decreaseParents.add(this.parentLoops.get(i));
        Pxtog pxtog = new Pxtog(decreaseParents, this.childLoop);
        LinkedList<Stitch> newStitches = new LinkedList<>();
        newStitches.add(pxtog);
        this.owningStitchSet.setStitches(newStitches);
        this.owningStitchSet = null;
        Loop newChild = this.childLoop.insertNextLoop(LoopDirection.BtF);
        K secondK = new K(this.parentLoops.getLast(), newChild);
        return new Single(secondK);
    }

}
