package TextureGraph.Courses.Stitches.Purl;

import TextureGraph.Courses.StitchSets.Increases.*;
import TextureGraph.Courses.Stitches.*;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.LoopDirection;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import Tracking.TypeTracking.KPStyle;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 7/12/2016.
 * the reverse of knit stitch
 */
public class P extends OneToOneStitch implements DuplicatableStitch, IncreasableStitch{
    /**
     * @param parent the loop that is the parent of the stitch
     * @param yarn the yarn to create the child loop at the end of
     */
    public P(@NotNull Loop parent, @NotNull Yarn yarn){
        super(parent, new Loop(yarn, LoopDirection.FtB));
        this.kpStyle = KPStyle.PStyle;
    }

    public P(@NotNull Loop parent, @NotNull Loop child){
        super(parent, child);
        this.kpStyle = KPStyle.PStyle;
    }

    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents) {
        return new P(parents.removeFirst(), yarn);
    }

    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Loop priorLoop, @NotNull LinkedList<Loop> parents) {
        return new P(parents.removeFirst(), priorLoop.insertNextLoop(LoopDirection.FtB));
    }

    @NotNull
    @Override
    public OneToOneStitch duplicateStitchNext() {
        Loop parentLoop = this.parentLoops.getFirst();
        assert childLoop != null;
        return new P(parentLoop.insertPriorLoop(LoopDirection.FtB), this.childLoop.insertNextLoop(LoopDirection.FtB) );
    }

    @Override
    public Increase increaseStitch(int newChildCount, boolean increaseAfter) {
        LinkedList<Loop> children = new LinkedList<>();
        children.add(this.childLoop);
        Loop lastLoop = this.childLoop;
        for(int i=1; i<newChildCount; i++){
            lastLoop = lastLoop.insertNextLoop(LoopDirection.FtB);
            children.add(lastLoop);
        }
        switch(newChildCount){
            case 2:
                return new PFB(this.parentLoops.getFirst(), children.getFirst(), children.getLast());
            case 3:
                return new Ctrdblinc(this.parentLoops.getFirst(),children);
            case 4:
                return new Inc4(this.parentLoops.getFirst(), children);
            case 5:
                return new Inc5(this.parentLoops.getFirst(), children);
            case 6:
                return new Inc6(this.parentLoops.getFirst(), children);
            case 7:
                return new Inc7(this.parentLoops.getFirst(), children);
            case 8:
                return new Inc8(this.parentLoops.getFirst(), children);
            case 9:
                return new Inc9(this.parentLoops.getFirst(), children);
            default:
                assert false;
        }
        return null;
    }
}
