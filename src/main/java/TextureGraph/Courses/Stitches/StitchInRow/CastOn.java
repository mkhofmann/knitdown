package TextureGraph.Courses.Stitches.StitchInRow;

import TextureGraph.Courses.Stitches.DuplicatableStitch;
import TextureGraph.Courses.Stitches.Loops.*;
import TextureGraph.Courses.Stitches.OneToOneStitch;
import TextureGraph.Courses.Stitches.Stitch;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 7/11/2016.
 * the base stitch for a textureGraph
 */
public class CastOn extends OneToOneStitch implements DuplicatableStitch {
    /**
     * @param yarn the yarn to create the parent and child from
     */
    public CastOn(@NotNull Yarn yarn){
        super(new Loop(yarn, LoopDirection.BtF), yarn, LoopDirection.BtF);
    }

    public CastOn(@NotNull Loop parent, @NotNull Loop child){
        super(parent, child);
    }

    /**
     * @param child the child loop to create the cast on from. THe parent loop is inserted prior to the child
     */
    public CastOn(@NotNull Loop child){
        super(child.insertPriorLoop(LoopDirection.BtF), child);
    }
    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents){
        return new CastOn(yarn);
    }

    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Loop priorLoop, @NotNull LinkedList<Loop> parents){
        Loop parent = priorLoop.insertNextLoop(LoopDirection.BtF);
        Loop child = parent.insertNextLoop(LoopDirection.BtF);
        return new CastOn(parent, child);
    }

    @NotNull
    @Override
    public OneToOneStitch duplicateStitchNext() {
        Loop parent = this.childLoop.insertNextLoop(LoopDirection.BtF);
        Loop child =  parent.insertNextLoop(LoopDirection.BtF);
        return new CastOn(parent, child);
    }

}
