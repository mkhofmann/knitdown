package TextureGraph.Courses.Stitches.StitchInRow;

import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.OneToOneStitch;
import TextureGraph.Courses.Stitches.Stitch;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 12/20/2016.
 * a stitch that pass a loop up a row
 */
public class SlipStitch extends OneToOneStitch {
    /**
     * @param parent the parent and child loop in this stitch
     */
    public SlipStitch(@NotNull Loop parent) {
        super(parent, parent);
    }

    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents){
        return new SlipStitch(parents.getFirst());}

    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Loop priorLoop, @NotNull LinkedList<Loop> parents){
        return new SlipStitch(parents.getFirst());}
}
