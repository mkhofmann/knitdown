package TextureGraph.Courses.Stitches.Loops;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by Megan on 7/11/2016.
 * A Yarn section heald space for 2 loops, a standard loop and a M1 loop
 */
public class YarnSection implements Comparable<YarnSection> {
    /**
     * degugging flag that makes for easier degugging but slower processing due to hash function
     */
    private static final boolean VISUALIZE = true;
    /**
     * the owning yarn for this section
     */
    public Yarn parentYarn;
    /**
     * The loop, or null, heald on this section
     */
    public Loop loop = null;

    /**
     * @param parentYarn the yarn that owns this yarn section
     */
    YarnSection(@NotNull Yarn parentYarn){
        this.parentYarn = parentYarn;
    }

    /**
     * @return this sections index in the parent Yarn, the method will assert false if this section is not in its parent
     */
    @Contract(pure = true)
    public int indexInYarn(){
        int index = this.parentYarn.sections.indexOf(this);
        assert  index >= 0;
        return index;
    }

    /**
     * @return the section following this in the parent yarn, or null if nothing follows this
     */
    @Nullable
    @Contract(pure = true)
    public YarnSection getNextSection(){
        int index = this.indexInYarn();
        if(index+1>=this.parentYarn.sections.size())
            return null;
        return this.parentYarn.sections.get(index+1);
    }

    /**
     * removes the first loop, if present. It also moves the second pos loop up to first, if it exists
     */
    void removeFirstPos(){
        this.loop = null;
        this.parentYarn.sections.remove(this);//remove empty section from yarn
    }

    /**
     * @return the neighbor following this, or null if no neighbor exists, or if the next section is a TurnSection
     */
    @Nullable
    @Contract(pure = true)
    YarnSection getNextNeighbor() {
        int index = this.indexInYarn();
        YarnSection next = this.parentYarn.sections.get(index+1);
        if(next instanceof TurnSection)//previously end of course
            return null;
        return next;
    }
    /**
     * @return the neighbor before this, or null if no neighbor exists, or if the prior section is a TurnSection
     */
    @Nullable
    @Contract(pure = true)
    YarnSection getPriorNeighbor(){//add return turns parameter
        int index = this.indexInYarn();
        if(index<=0)
            return  null;
        YarnSection prior = this.parentYarn.sections.get(index-1);
        if(prior instanceof TurnSection)//previously end of course
            return null;
        return prior;
    }

    @Override
    public String toString(){
        if(!YarnSection.VISUALIZE)
            return String.valueOf(this.hashCode());
        else
            return this.parentYarn.toString()+":"+this.parentYarn.sections.indexOf(this);
    }

    @Override
    public int hashCode(){
        return super.hashCode();
    }

    @Override
    public int compareTo(@NotNull YarnSection o) {
        return Integer.compare(this.indexInYarn(), o.indexInYarn());
    }
}
