package TextureGraph.Courses.Stitches.Loops;

import TextureGraph.Courses.Stitches.Stitch;
import TextureGraph.Courses.Stitches.YO;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedHashSet;
import java.util.LinkedList;

/**
 * Created by Megan on 7/11/2016.
 * Loop data for loops that build up the knit structure
 */
public class Loop implements Comparable<Loop>{
    /**
     * th yarn section the owns this loop
     */
    public YarnSection yarnSection;
    /**
     * direction the loop is pulled through its parent
     */
    public LoopDirection direction;
    /**
     * The set of stitches that have this loop as their child
     */
    private LinkedHashSet<Stitch> childOf= new LinkedHashSet<>();
    /**
     * The set of stitches that have this loop as a parent
     */
    private LinkedHashSet<Stitch> parentOf= new LinkedHashSet<>();

    /**
     * @return a set of stitches that have this loop as a child
     */
    @NotNull
    public LinkedHashSet<Stitch> getChildOf() {return childOf;}

    /**
     * @return a set of stitches that have this loop as a parent
     */
    public LinkedHashSet<Stitch> getParentOf() {return parentOf;}

    /**
     * @param yarnSection the yarn section that the loop will be created on
     * @param direction the direction the loop is pulled through its parents
     */
    public Loop(@NotNull YarnSection yarnSection, @NotNull LoopDirection direction) {
        this.yarnSection = yarnSection;
        this.direction = direction;
        assert this.yarnSection.loop == null;
        this.yarnSection.loop = this;
    }

    /**
     * @param yarn the yarn where this loop will be created at the end
     * @param direction the direction the loop is pulled through its parents
     */
    public Loop(@NotNull Yarn yarn,@NotNull LoopDirection direction) {
        this(yarn.newSection(), direction);
    }

    /**
     * @param yarn the yarn that heald this loop
     * @param direction the direction the loop is pulled through its parents
     * @param yarnIndex the index of the yarn where a new section for the loop will be inserted
     */
    public Loop(@NotNull Yarn yarn,@NotNull LoopDirection direction, int yarnIndex) {
        this(yarn.newSection(yarnIndex), direction);
    }

    public void swapYarnSections(Loop other){
        int otherSectionIndex = other.yarnSection.indexInYarn();
        int thisSectionIndex = this.yarnSection.indexInYarn();
        this.yarnSection = this.yarnSection.parentYarn.sections.get(otherSectionIndex);
        this.yarnSection.loop = this;
        other.yarnSection = this.yarnSection.parentYarn.sections.get(thisSectionIndex);
        other.yarnSection.loop = other;
    }

    /**
     * insert a loop into a new section after this loop
     * @param direction the direction to pull the loop through
     * @return the inserted loop
     */
    @NotNull
    public Loop insertNextLoop(@NotNull LoopDirection direction) {
        return new Loop(this.yarnSection.parentYarn, direction, this.yarnSection.indexInYarn());
    }
    /**
     * insert a loop into a new section before this loop
     * @param direction the direction to pull the loop through
     * @return the inserted loop
     */
    @NotNull
    public Loop insertPriorLoop(@NotNull LoopDirection direction) {
        return new Loop(this.yarnSection.parentYarn, direction, this.yarnSection.indexInYarn()-1);
    }

    /**
     * Sets s as a stitch that parents this loop
     * @param s the stitch that is a parent of this
     */
    public void makeChildOf(@NotNull Stitch s){this.childOf.add(s);}
    /**
     * Sets s as a stitch whose child is this
     * @param s the stitch whose child is this
     */
    public void makeParentOf(@NotNull Stitch s){
        assert !( s instanceof YO);
        this.parentOf.add(s);
    }
    /**
     * removes s from the set of stitch that parent this, breaking the edge
     * @param s the stitch that is no longer the parent of this
     */
    public void removeChildOf(@NotNull Stitch s){this.childOf.remove(s);}
    /**
     * removes s from the set of stitch that have this as a child, breaking the edge
     * @param s the stitch that is no longer  has this as a child
     */
    public void removeParentOf(@NotNull Stitch s){this.parentOf.remove(s);}

    /**
     * clear the set of stitches have this as a parent
     */
    public void clearParentsOf(){this.parentOf.clear();}

    /**
     * clear the stitches that have this as a child
     */
    public void clearChildOf(){this.childOf.clear();}

    /**
     * @return the first stitch that has this as a child
     */
    @Nullable
    @Contract(pure = true)
    public Stitch getFirstChildOf(){
        //noinspection LoopStatementThatDoesntLoop
        for(Stitch stitch: this.childOf)
            return stitch;
        return null;
    }

    /**
     * @return the last stitch that has this as a child
     */
    @Nullable
    @Contract(pure = true)
    public Stitch getLastChildOf(){
        if(this.childOf.isEmpty())
            return null;
        return new LinkedList<>(this.childOf).getLast();
    }

    /**
     * @return loop prior to this in course
     */
    @Nullable
    @Contract(pure = true)
    public Loop getPriorNeighbor(){
        YarnSection priorSection = this.yarnSection.getPriorNeighbor();
        if(priorSection == null)
            return null;
        assert !(priorSection instanceof TurnSection);
        return priorSection.loop;
    }

    /**
     * @return loop after this in course
     */
    @Nullable
    @Contract(pure =  true)
    public Loop getNextNeighbor(){
        YarnSection nextSection = this.yarnSection.getNextNeighbor();
        if(nextSection ==null)
            return null;
        else
            return nextSection.loop;
    }

    /**
     * remove the loop from the yarn and yarn section
     */
    public void removeLoopFromYarn(){
        for(Stitch parent: this.childOf)
            parent.childLoop = null;
        this.childOf.clear();
        for(Stitch childStitch: this.parentOf) {
            if(childStitch.parentLoops.size()>0) {
                int indexToRemove = -1;
                int curIndex = 0;
                for (Loop p : childStitch.parentLoops) {
                    if (p == this) {
                        indexToRemove = curIndex;
                        break;
                    }
                    curIndex++;
                }
                assert indexToRemove >= 0;
                childStitch.parentLoops.remove(indexToRemove);
            }
        }
        this.yarnSection.removeFirstPos();
    }
    @Override
    public String toString(){
        return this.yarnSection.toString();}

    @Override
    public int hashCode(){
        return this.yarnSection.hashCode();
    }
    @Contract("null->false")
    @Override
    public boolean equals(Object o){
        if(!(o instanceof Loop))
            return false;
        Loop l = ((Loop)o);
        return  l.yarnSection == this.yarnSection;
    }

    @Override
    public int compareTo(@NotNull Loop o) {
        return this.yarnSection.compareTo(o.yarnSection);
    }
}
