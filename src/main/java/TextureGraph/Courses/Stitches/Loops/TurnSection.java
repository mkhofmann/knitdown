package TextureGraph.Courses.Stitches.Loops;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Megan on 4/19/2017.
 * A yarn section that marks when a row has turned
 */
public class TurnSection extends YarnSection {
    /**
     * @param parent the yarn where this turn is being created
     */
    TurnSection(@NotNull Yarn parent) {
        super(parent);
    }

    @Override
    public String toString(){
        return "Turn:"+this.parentYarn.toString()+":"+this.parentYarn.sections.indexOf(this);
    }
}
