package TextureGraph.Courses.Stitches.Loops;


import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Created by Megan on 7/11/2016.
 * The Yarn class maintains an ordered list of YarnSection which orgnize the order of loops in a textureGraph graph
 */
public class  Yarn implements Iterable<Loop>{
    /**
     * variable that counts the number of yarns that have been created for easy debug namming
     */
    private static int yarnCounts = 0;
    /**
     * The count of when this yarn was created
     */
    private int myCount;
    /**
     * the ordered list of sections maintained by the Yarn class
     */
    public LinkedList<YarnSection> sections;
    /**
     * The indexable name of the yarn
     */
    private String yarnName;

    /**
     * Creates an empty yarn named Y## where ## is its creation count
     */
    public Yarn(){
        this.myCount = Yarn.yarnCounts;
        this.yarnName = "y"+this.myCount;
        Yarn.yarnCounts++;
        this.sections = new LinkedList<>();
    }

    /**
     * creates and empty yarn with a custom name
     * @param name the custom yarn name
     */
    public Yarn(@NotNull String name){
        this();
        this.yarnName = name;
    }

    /**
     * adds a section to the end of the yarn
     * @return returns the created section
     */
    @NotNull
    YarnSection newSection(){
        this.sections.addLast(new YarnSection(this));
        return this.sections.getLast();
    }

    /**
     * removes teh first yarn section from the yarn
     */
    public void removeFirst(){
        this.sections.removeFirst();
    }

    /**
     * adds a section at the index in the yarn
     * @param index index to add the yarn at, section will be added after the section at that current index
     * @return the created section
     */
    @NotNull
    YarnSection newSection(int index){
        if(index<0) {
            this.sections.addFirst(new YarnSection(this));
            return this.sections.getFirst();
        }
        this.sections.add(index+1, new YarnSection(this));
        return sections.get(index+1);
    }

    /**
     * add a Turn section at the specified index
     * @param index to add the Turn at, the Turn will follow the section at that current location
     */
    public void newTurn(int index){
        TurnSection value = new TurnSection(this);
        if(index<0)
            this.sections.addFirst(value);
        this.sections.add(index+1, value);
    }

    /**
     * @param o to compare to
     * @return true if the yarn has the same name
     */
    @Override
    @Contract(value = "null->false")
    public boolean equals(Object o){
        if(!(o instanceof Yarn))
            return false;
        Yarn y = ((Yarn)o);
        return this.yarnName.equals(y.yarnName);
    }

    /**
     * @param yarnSection seeking the neighbor following this section
     * @return the next section or null if the yarnSection doesn't exist or is the last section
     */
    @SuppressWarnings("unused")
    @Nullable
    public YarnSection nextNeighbor(@NotNull YarnSection yarnSection){
        int index = this.sections.indexOf(yarnSection);
        if(index<0 || index==this.sections.size()-1)
            return null;
        return this.sections.get(index+1);
    }

    @Override
    public String toString(){return this.yarnName;}

    /**
     * @return a number corrisponding to this yarn's creating since runtime.
     */
    @Override
    public int hashCode(){return this.myCount;}


    @NotNull
    @Override
    public Iterator<Loop> iterator() {
        return new YarnLoopIterator();
    }
    @NotNull
    public Iterator<Loop> iterator(int index){
        return new YarnLoopIterator(index);
    }

    private class YarnLoopIterator implements Iterator<Loop>{
        ListIterator<YarnSection> sectionIterator;
        YarnSection next = null;
        YarnLoopIterator(int index){
            this.sectionIterator = sections.listIterator(index);
        }
        YarnLoopIterator(){
            this.sectionIterator = sections.listIterator();
        }
        @Override
        public boolean hasNext() {
            boolean hasNext = sectionIterator.hasNext();
            if(hasNext) {
                next = sectionIterator.next();
                if(next instanceof TurnSection)
                    hasNext = this.hasNext();
            }
            return hasNext;
        }

        @Override
        @Nullable
        public Loop next() {
            return next.loop;
        }

    }
}
