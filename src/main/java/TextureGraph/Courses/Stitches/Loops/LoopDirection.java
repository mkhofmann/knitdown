package TextureGraph.Courses.Stitches.Loops;

/**
 * Created by Megan on 7/11/2016.
 * Declares the direction that a loop can be pulled through its parents
 */
public enum LoopDirection {
    BtF,//Stitches.K.K from front of piece
    FtB//P from front of piece
}
