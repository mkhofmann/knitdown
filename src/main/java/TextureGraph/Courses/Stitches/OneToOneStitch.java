package TextureGraph.Courses.Stitches;

import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.LoopDirection;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.WaleNavigation.HorizontalStitchCarve;
import TextureGraph.WaleNavigation.Wale;
import TextureGraph.WaleNavigation.WaleChain;
import javafx.util.Pair;
import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * Created by Megan on 7/12/2016.
 * A stitch that has a one to one relationship of parent to child
 */
public abstract class OneToOneStitch extends Stitch {
    public OneToOneStitch(@NotNull Loop parent, @NotNull Loop child){super(parent, child);}
    public OneToOneStitch(@NotNull Loop parent, @NotNull Yarn yarn, @NotNull LoopDirection direction){super(parent, yarn, direction);}

    /**
     * trade children with another one to one stitch, used for twists and cables
     * @param other the stitch to swap with
     */
    public void swapChildren(@NotNull OneToOneStitch other){
        Loop myChild = this.childLoop;
        this.childLoop = other.childLoop;
        other.childLoop = myChild;
        this.childLoop.removeParentOf(other);
        this.childLoop.makeParentOf(this);
        other.childLoop.removeParentOf(this);
        other.childLoop.makeParentOf(other);
    }

    @Override
    public void setParents(@NotNull LinkedList<Loop> parents){
        this.parentLoops = parents;
        assert this.parentLoops.size()==1;
        this.parentLoops.getFirst().makeParentOf(this);
        LinkedHashSet<Stitch> toRemove= new LinkedHashSet<>();
        for(Stitch parentOf: this.parentLoops.getFirst().getParentOf())
            if(parentOf.childLoop == null)
                toRemove.add(parentOf);
        for(Stitch remove: toRemove)
            this.parentLoops.getFirst().removeParentOf(remove);
    }

    /**
     * compares witch child loop comes before in yarn
     */
    public static Comparator<OneToOneStitch> ChildLoop = Comparator.comparingInt(o -> o.childLoop.hashCode());
    /**
     * compares which parent loop comes before in yarn
     */
    public static Comparator<OneToOneStitch> ParentLoop = Comparator.comparingInt(o -> o.parentLoops.getFirst().hashCode());

    @Override
    public void disconnectFromWale(boolean allInLine) {
        //above stitch keeps its child and replaces this's child (parent of above) with this's parent
        //below stitch's child becomes the child of above
        ArrayList<Stitch> cacheChain = new ArrayList<>(this.owningStitchSet.owningCourse.owningTextureGraph.courses.size());
        int indexOfStitch = -1;
        Wale wale = this.getOwningWale();
        assert wale != null;
        WaleChain chain = wale.getOwningChain();
        Stitch below = this.parentLoops.getFirst().getFirstChildOf();
        assert below != null;
        assert below.childLoop == this.parentLoops.getFirst();
        HashMap<Stitch, Pair<Loop, Loop>> swapMap = new HashMap<>();
        for(Stitch childStitch: this.childLoop.getParentOf()){
            if(childStitch instanceof YO)//todo: why is this happening?
                continue;
            if(!childStitch.parentLoops.contains(this.childLoop))
                assert childStitch.parentLoops.contains(this.childLoop);
            Pair<Loop,Loop> value = new Pair<>(this.childLoop, below.childLoop);
            swapMap.put(childStitch, value);
        }
        for(Stitch childStitch: swapMap.keySet()){
            Pair<Loop, Loop> loops = swapMap.get(childStitch);
            boolean b = childStitch.replaceParent(loops.getKey(), loops.getValue());
            assert b;
        }
        this.childLoop.removeLoopFromYarn();
        this.parentLoops = new LinkedList<>();
        if(!allInLine) {
            boolean foundStitch = false;
            StitchSet lastStitchSet = this.owningStitchSet;
            Stitch lastStitch = this;
            Iterator<Stitch> stitchIterator;
            if (chain != null)
                stitchIterator = chain.stitchIterator();
            else
                stitchIterator = wale.iterator();
            int indexInCache = 0;
            for (Iterator<Stitch> it = stitchIterator; it.hasNext(); ) {
                Stitch stitchInChain = it.next();
                cacheChain.add(stitchInChain);
                if (foundStitch) {
                    StitchSet cacheSet = stitchInChain.owningStitchSet;
                    boolean b = lastStitchSet.replaceStitch(lastStitch, stitchInChain);
                    assert b;
                    lastStitch = stitchInChain;
                    lastStitchSet = cacheSet;
                } else if (stitchInChain == this) {
                    foundStitch = true;
                    indexOfStitch = indexInCache;
                }
                indexInCache++;
            }
        }

        for(int i= Math.max(indexOfStitch- HorizontalStitchCarve.NEIGHBOR_DROP_OFF, 0); i<=Math.min(cacheChain.size()-1, indexOfStitch+HorizontalStitchCarve.NEIGHBOR_DROP_OFF); i++){
            int distanceToStitch = Math.abs(indexOfStitch-i);
            cacheChain.get(i).neighborReduceHeightCost+= distanceToStitch/((double)HorizontalStitchCarve.NEIGHBOR_DROP_OFF);
        }
    }
}
