package TextureGraph.Courses.Stitches;


import CompilerExceptions.StitchManipulationExceptions.NoTextureCarvesAvailableException;
import TextureGraph.Courses.Stitches.Loops.Loop;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

public abstract class Decrease extends Stitch{
    /**
     * The number of parents expected to be in this decrease
     */
    public int expectedParentCount = 2;

    public Decrease(@NotNull LinkedList<Loop> parents, @NotNull Loop loop) {
        super(parents, loop);
    }

    public Decrease() {
        super();
    }

    @Override
    public void removeChildFromYarn() {
        assert false;
        super.removeChildFromYarn();
    }

    @Override
    public void disconnectFromWale(boolean allInLine) throws NoTextureCarvesAvailableException {
        throw new NoTextureCarvesAvailableException();
    }

}
