package TextureGraph.Courses.Stitches;

import TextureGraph.Courses.StitchSets.Increases.Increase;

/**
 * Created by Megan on 4/25/2017.
 */
public interface IncreasableStitch {
    /**
     * adds another stitch that share these parentYarn loops and increase the number of children those parents have
     * @return the stitch set that contains this stitch, and the increase, set should replace the current owning stitch set
     * @param newChildCount the number of children needed from this set
     * @param increaseAfter true if the new stitches are added after or before the stitch
     */
    Increase increaseStitch(int newChildCount, boolean increaseAfter);
}
