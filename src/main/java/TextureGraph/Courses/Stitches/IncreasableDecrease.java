package TextureGraph.Courses.Stitches;

import TextureGraph.Courses.StitchSets.StitchSet;
import org.jetbrains.annotations.NotNull;

public interface IncreasableDecrease {
    /**
     * This will replace this stitch in its owning stitch set with less decreased stitch
     * @return a new stitchset which takes remaining parent loop and connects to a new child
     */
    @NotNull
    StitchSet increaseStitch();
}
