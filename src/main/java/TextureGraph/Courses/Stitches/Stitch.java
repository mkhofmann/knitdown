package TextureGraph.Courses.Stitches;

import CompilerExceptions.StitchManipulationExceptions.NoTextureCarvesAvailableException;
import TextureGraph.Courses.Lean;
import TextureGraph.Courses.StitchSets.Cables.Cable;
import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.LoopDirection;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.StitchInRow.CastOn;
import TextureGraph.WaleNavigation.HorizontalStitchCarve;
import TextureGraph.WaleNavigation.Wale;
import TextureGraph.WaleNavigation.WaleChain;
import Tracking.TypeTracking.KPStyle;
import javafx.util.Pair;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.Nonnull;
import java.util.*;


/**
 * Created by Megan on 7/11/2016.
 * a many to one relationship of parent loops to one child loop
 */
public abstract class Stitch implements Comparable<Stitch>{

    private double costToRemoveFromWale = 0;
    public double pathToRemoveFromWaleCost = 0;
    public double neighborReduceHeightCost = 0;
    public Stitch pathMinimumNeighbor = null;
    public boolean horizontalPathCalculated = false;
    /**
     * Differentiate between stitches that look like knits or purls
     */
    protected KPStyle kpStyle = KPStyle.KStyle;
    /**
     * The planar order of this stitch. A positive value means it is in front of other stitches, a negative value is behind other stitches
     */
    public int stitchOrder = 0;

    public double costToReduceStitch() {
        double localCost;
        double parentLoops = this.parentLoops.size();
        if(parentLoops ==0)
            localCost=Double.MAX_VALUE;
        else
            localCost = (parentLoops-1.0) / parentLoops;
        double cost = Math.min(localCost, 0);
        if(cost>0)
            cost= Double.MAX_VALUE;
        return cost;
    }

    /**
     * The stitch set that owns this stitch
     */
    public StitchSet owningStitchSet = null;
    /**
     * The ordered set of loops that are parents to the child loop.
     * There can be 0 to many parents
     */
    public LinkedList<Loop> parentLoops;
    /**
     * The loop that is pulled through the parents in this stitch
     */
    public Loop childLoop;
    /**
     * The direction this stitch type tends to lean due to physical properties
     */
    public Lean lean= Lean.centered;

    /**
     * @param parents the loops ordered as the parents of this stitch
     * @param child the child loop being pulled through the parents
     */
    public Stitch(@NotNull LinkedList<Loop> parents, @NotNull Loop child) {
        this.setParents(parents);
        this.setChild(child);
    }

    /**
     * @param parent the only parent loop in this stitch
     * @param child the child loop being pulled through the parents
     */
    public Stitch(@NotNull Loop parent, @NotNull Loop child){
        this.setParents(parent);
        this.setChild(child);
    }

    /**
     * @param parents the loops ordered as the parents of this stitch
     * @param yarn the yarn to create a new child loop on
     * @param direction the direction to pull the child loop through its parents
     */
    public Stitch(@NotNull LinkedList<Loop> parents, @NotNull Yarn yarn, @NotNull LoopDirection direction){
        this(parents, new Loop(yarn, direction));
    }

    /**
     * @param parent the only parent loop in this stitch
     * @param yarn the yarn to create a new child loop on
     * @param direction the direction to pull the child loop through its parents
     */
    public Stitch(@NotNull Loop parent,@NotNull Yarn yarn, @NotNull LoopDirection direction){
        this(parent, new Loop(yarn, direction));
    }

    /**
     * Create an invalid stitch with no child and no parents
     */
    protected Stitch(){this.parentLoops = new LinkedList<>();}

    /**
     * removes the child loop from the yarn, nullifying it
     */
    public void removeChildFromYarn() {
        if(this.childLoop != null)
            this.childLoop.removeLoopFromYarn();
    }

    /**
     * @return proportion of texture that is of a similar type to this
     */
    @Contract(pure = true)
    public double proportionOfTexture(){
        double typeFreq = this.owningStitchSet.owningCourse.owningTextureGraph.stitchTypeToCountMap.get(this.getClass());
        double stitchCount = this.owningStitchSet.owningCourse.owningTextureGraph.stitchCount;
        return (stitchCount - typeFreq) / stitchCount;
    }

    /**
     * @return proportion of the course that is a of a similar type to this
     */
    @Contract(pure = true)
    public double proportionOfLine(){
        double typeFreq = this.owningStitchSet.owningCourse.stitchTypeToCountMap.get(this.getClass());
        double stitchCount = this.owningStitchSet.owningCourse.stitchCount;
//        double stitchTypeCount = this.owningStitchSet.owningCourse.stitchTypeCount;
        return ((stitchCount-typeFreq) / stitchCount);
    }

    /**
     * @return proportion of the course that is a of a similar type to this
     */
    @Contract(pure = true)
    public double proportionOfWale(){
        assert this.owningStitchSet.owningCourse.owningTextureGraph.stitchesToWale.containsKey(this);
        Wale wale = this.owningStitchSet.owningCourse.owningTextureGraph.stitchesToWale.get(this);
        assert wale != null;
        assert wale.stitchTypeToCountMap.containsKey(this.getClass());
        double typeFreq = wale.stitchTypeToCountMap.get(this.getClass());
        double stitchCount = wale.getStitchCount();
        return ((stitchCount-typeFreq) / stitchCount);
    }

    public void setDynamicWaleRemovalCost(){
        this.pathToRemoveFromWaleCost = this.costToRemoveFromWale;
        if(this.pathToRemoveFromWaleCost == Double.MAX_VALUE)
            return;
        Stitch directRightNeighbor = this.getDirectRightNeighbor();
        if(directRightNeighbor != null){
            double neighborCost =  directRightNeighbor.pathToRemoveFromWaleCost;
            if(!directRightNeighbor.getClass().equals(this.getClass()) || !directRightNeighbor.owningStitchSet.getClass().equals(this.owningStitchSet.getClass()))
                neighborCost += HorizontalStitchCarve.HORIZONTAL_DISSIMILARITY_COST;//dissimilarity Cost
            Pair<Stitch, Stitch> neighbors = directRightNeighbor.aboveAndBelowInWale();
            Stitch aboveNeighbor = neighbors.getKey();
            Stitch belowNeighbor = neighbors.getValue();
            double abovePathCost;
            if(aboveNeighbor == null)
                abovePathCost = Double.MAX_VALUE;
            else {
                if(aboveNeighbor.owningStitchSet instanceof Cable && ((Cable)aboveNeighbor.owningStitchSet).unHeald.get(((Cable)aboveNeighbor.owningStitchSet).unHeald.size()-1) != aboveNeighbor)
                    abovePathCost = Double.MAX_VALUE;//do not enter a cable mid way
                else {
                    abovePathCost = aboveNeighbor.pathToRemoveFromWaleCost + HorizontalStitchCarve.HORIZONTAL_DIAGONAL_COST;
                    if (!aboveNeighbor.getClass().equals(this.getClass()) || !aboveNeighbor.owningStitchSet.getClass().equals(this.owningStitchSet.getClass()))
                        abovePathCost += HorizontalStitchCarve.HORIZONTAL_DISSIMILARITY_COST;//dissimilarity Cost
                }
            }
            double belowPathCost;
            if(belowNeighbor == null)
                belowPathCost = Double.MAX_VALUE;
            else {
                if(belowNeighbor.owningStitchSet instanceof Cable && ((Cable)belowNeighbor.owningStitchSet).heald.get(((Cable)belowNeighbor.owningStitchSet).heald.size()-1) != belowNeighbor)
                    belowPathCost = Double.MAX_VALUE;//do not enter a cable mid way
                else {
                    belowPathCost= belowNeighbor.pathToRemoveFromWaleCost+ HorizontalStitchCarve.HORIZONTAL_DIAGONAL_COST;
                    if(!belowNeighbor.getClass().equals(this.getClass()) || !belowNeighbor.owningStitchSet.getClass().equals(this.owningStitchSet.getClass()))
                        belowPathCost += HorizontalStitchCarve.HORIZONTAL_DISSIMILARITY_COST;//dissimilarity Cost
                }
            }
            if(abovePathCost == belowPathCost && abovePathCost == neighborCost){
                if(abovePathCost == Double.MAX_VALUE) {
                    this.pathToRemoveFromWaleCost = Double.MAX_VALUE;
                    this.pathMinimumNeighbor = null;
                }else{
                    this.pathToRemoveFromWaleCost += neighborCost;
                    this.pathMinimumNeighbor = directRightNeighbor;
                }
            }
            else if(neighborCost<abovePathCost){
                if(neighborCost<belowPathCost){
                    this.pathToRemoveFromWaleCost += neighborCost;
                    this.pathMinimumNeighbor = directRightNeighbor;
                }else{
                    this.pathToRemoveFromWaleCost += belowPathCost;
                    this.pathMinimumNeighbor = belowNeighbor;
                }
            }else if(abovePathCost<belowPathCost){
                this.pathToRemoveFromWaleCost += abovePathCost;
                this.pathMinimumNeighbor = aboveNeighbor;
            }else{
                this.pathToRemoveFromWaleCost += belowPathCost;
                this.pathMinimumNeighbor = belowNeighbor;
            }
        }
        this.horizontalPathCalculated = true;
    }
    @SuppressWarnings("SuspiciousMethodCalls")
    @Nullable
    private Stitch getDirectRightNeighbor() {
        Stitch directRightNeighbor;
        Cable cable= null;
        int indexHeald=-1;
        int indexUnheald=-1;
        if(this.owningStitchSet instanceof Cable) {
            cable = (Cable) this.owningStitchSet;
            indexHeald = cable.heald.indexOf(this);
            indexUnheald = cable.unHeald.indexOf(this);
        }
        if(this.owningStitchSet.owningCourse.isStandardRightSide()) {
            if(this.owningStitchSet instanceof Cable){
                if(indexHeald>=0){
                    if(indexHeald>0)
                        directRightNeighbor = cable.heald.get(indexHeald - 1);
                    else
                        directRightNeighbor = cable.unHeald.get(cable.unHeald.size()-1);
                }
                else {
                    if (indexUnheald > 0)
                        directRightNeighbor = cable.unHeald.get(indexUnheald - 1);
                    else
                        directRightNeighbor = this.owningStitchSet.getPriorStitchInLine();
                }
            }else
                directRightNeighbor = this.priorNeighbor();
        } else {
            if(this.owningStitchSet instanceof Cable){
                if(indexHeald>=0){
                    if(indexHeald<cable.heald.size()-1)
                        directRightNeighbor = cable.heald.get(indexHeald + 1);
                    else
                        directRightNeighbor = this.owningStitchSet.getNextStitchInLine();
                }
                else {
                    if (indexUnheald <cable.unHeald.size()-1)
                        directRightNeighbor = cable.unHeald.get(indexUnheald + 1);
                    else
                        directRightNeighbor = cable.heald.get(0);
                }
            }
            else
                directRightNeighbor = this.nextNeighbor();
        }
        assert directRightNeighbor == null || directRightNeighbor.childLoop != null;
        return directRightNeighbor;
    }

    @NotNull
    private Pair<Stitch,Stitch> aboveAndBelowInWale() {
        Stitch above = null;
        Stitch below = null;
        Wale wale = this.getOwningWale();
        WaleChain chain = this.owningStitchSet.owningCourse.owningTextureGraph.waleToWaleChains.get(wale);
        assert wale != null;
        ListIterator<Stitch> stitchListIterator = wale.iterator(this);
        ListIterator<Wale> waleChainListIterator = null;
        if (chain != null)
            waleChainListIterator = chain.listIterator(wale);
        Wale next;
        if (waleChainListIterator != null){
            next = waleChainListIterator.next();
            assert next == wale;
        }
        assert stitchListIterator != null;
        assert stitchListIterator.hasNext();
        assert stitchListIterator.next() == this;
        if(stitchListIterator.hasNext()) {
            above= stitchListIterator.next();
            stitchListIterator.previous();
            assert stitchListIterator.previous() == this;
        } else if(waleChainListIterator != null && waleChainListIterator.hasNext()){
            Wale nextWale = waleChainListIterator.next();
            above = nextWale.stitches.getFirst();
            assert waleChainListIterator.previous() == nextWale;
            assert waleChainListIterator.previous() == wale;
        }
        while(stitchListIterator.hasPrevious()){
            below = stitchListIterator.previous();
            if(below != this) break;
        }
        if(below == null) {
            if(waleChainListIterator != null && waleChainListIterator.hasPrevious()){
                Wale previousWale = waleChainListIterator.previous();
                below = previousWale.stitches.getLast();
            }
        }
        return new Pair<>(above, below);
    }

    @Nullable
    Wale getOwningWale() {
        if(this.owningStitchSet.owningCourse.owningTextureGraph.waleChains.isEmpty())
            return null;
        return this.owningStitchSet.owningCourse.owningTextureGraph.stitchesToWale.get(this);
    }


    /**
     * sets the proportional cost for this based on how common it is in the texture
     */
    public void setWaleRemovalCost(){
        if(this instanceof CastOn || this instanceof Decrease || this instanceof YO) {
            this.costToRemoveFromWale = Double.MAX_VALUE;
            return;
        }
        this.costToRemoveFromWale = this.getProportionalCost() + this.owningStitchSet.neighborReduceWidthCost + this.neighborReduceHeightCost;
    }

    private double getProportionalCost() {
        return this.proportionOfTexture() + this.proportionOfLine() + this.proportionOfLocal() + this.proportionOfWale()+this.getPortionToEnd();
    }

    private int getPortionToEnd() {
        int yarnSize = this.childLoop.yarnSection.parentYarn.sections.size();
        int distanceToEndOfYarn = yarnSize - this.childLoop.yarnSection.indexInYarn();
        return distanceToEndOfYarn/ yarnSize;
    }

    /**
     * @return the proportion of the neighboring stitch sets that is of this type
     */
    private double proportionOfLocal() {
        return this.owningStitchSet.proportionOfLocal();
    }


    /**
     * @return the stitchSet that created this stitch, and has its parents as children, null if there are no owning stitches
     */
    @Nullable
    @Contract(pure = true)
    public StitchSet getParentStitchSet(){
        if(parentLoops.size()==0)
            return null;
        else if(parentLoops.size()==1) {
            if(this.parentLoops.get(0).getChildOf().isEmpty())
                return null;
            if(this.parentLoops.get(0).getChildOf().size()==1) {
                Iterator<Stitch> itr = this.parentLoops.get(0).getChildOf().iterator();
                return new Single(itr.next());
            } else
                return new StitchSet(new LinkedList<>(this.parentLoops.get(0).getChildOf()));
        }
        LinkedList<Stitch> parentStitches = new LinkedList<>();
        for(Loop parentLoop: this.parentLoops)
            parentStitches.addAll(parentLoop.getChildOf());
        if(parentStitches.size()==0)
            return null;
        StitchSet primary = parentStitches.get(0).owningStitchSet;
        if(primary==null)
            return new StitchSet(parentStitches);
        boolean allMatch = true;
        for(Stitch parentStitch: parentStitches)
            if(parentStitch != null && !primary.equals(parentStitch.owningStitchSet)) {
                allMatch = false;
                break;
            }
        if(allMatch)
            return primary;
        return new StitchSet(parentStitches);
    }

    boolean replaceParent(@NotNull Loop oldParent, @NotNull Loop newParent){
        assert this.childLoop.compareTo(newParent)>=0;
        if(!this.parentLoops.contains(oldParent) || this.parentLoops.contains(newParent))
            return false;
        int oldIndex = this.parentLoops.indexOf(oldParent);
        this.parentLoops.set(oldIndex, newParent);
        oldParent.removeParentOf(this);
        newParent.makeParentOf(this);
        newParent.removeParentOf(this);
        return true;
    }

    /**
     * set the parent stitches of this stitch, overriding past parent
     * @param parents the new parents for this stitch
     */
    public void setParents(@NotNull LinkedList<Loop> parents){
        if(this.childLoop != null)
            for(Loop parent: this.parentLoops)
                assert this.childLoop.compareTo(parent)>=0;
        this.parentLoops = parents;
        for( Loop p: this.parentLoops){
            p.makeParentOf(this);
        }
    }

    /**
     * @param parent set as the only parent of this loop
     */
    public void setParents(@NotNull Loop parent){
        LinkedList<Loop> parents = new LinkedList<>();
        parents.add(parent);
        this.setParents(parents);
        parent.makeParentOf(this);
    }

    /**
     * @param loop the loop to become the child of this stitch
     */
    protected void setChild(@NotNull Loop loop){
        for(Loop parent: this.parentLoops)
            assert loop.compareTo(parent)>=0;
        this.childLoop = loop;
        this.childLoop.makeChildOf(this);

    }

    /**
     * @return the yarn that makes up this stitches loops
     */
    @NotNull
    @Contract(pure = true)
    public Yarn getYarn(){
        assert this.childLoop != null;
        assert this.childLoop.yarnSection != null;
        return this.childLoop.yarnSection.parentYarn;
    }

    /**
     * neighbor Stitch whose child is yarn wise prior to this child loop
     * @return a stitch that owns this loop
     */
    @Nullable
    @Contract(pure = true)
    public Stitch priorNeighbor(){
        int indexInSet = this.owningStitchSet.stitches.indexOf(this);
        assert indexInSet>=0;
        if(indexInSet==0)
//            return this.owningStitchSet.getNextStitchInLine();//todo run other tests to ensure this bug wasn't on purpose
            return this.owningStitchSet.getPriorStitchInLine();
        else
            return this.owningStitchSet.stitches.get(indexInSet-1);
    }
    /**
     * neighbor Stitch whose child is yarn wise next to this child loop
     * @return a stitch that owns this loop
     */
    @Nullable
    @Contract(pure = true)
    public Stitch nextNeighbor() {//grabbing from next course
        int indexInSet = this.owningStitchSet.stitches.indexOf(this);
        if(indexInSet+1==this.owningStitchSet.stitches.size())
            return this.owningStitchSet.getNextStitchInLine();
        else
            return this.owningStitchSet.stitches.get(indexInSet+1);
    }

    @NotNull
    @Contract(pure = true)
    private String typeName(){
        return this.getClass().getSimpleName();
    }
    @Override
    @Contract(pure = true)
    public String toString(){
        StringBuilder str = new StringBuilder(this.typeName() + "[ p(");
        if(this.parentLoops != null){
            if(this.parentLoops.size()==1)
                str.append(this.parentLoops.get(0).toString());
            else
                for (Loop p : this.parentLoops)
                    str.append(p.toString()).append(",");
        }
        if(this.childLoop == null)
            str.append("), c() ]");
        else
            str.append("), c(").append(this.childLoop.toString()).append(") ]");
        return str.toString();
    }

    /**
     * @param yarn the yarn to construct the copy from
     * @param parents the parent loops of the copy
     * @return the copy of this stitch
     */
    @NotNull
    @Contract(pure = true)
    public abstract Stitch copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents);

    /**
     * @param priorLoop the loop that occurs before the copy
     * @param parents the parent loops of the copy
     * @return the copy of this stitch
     */
    @NotNull
    @Contract(pure = true)
    public abstract Stitch copyFromParents(@NotNull Loop priorLoop,@NotNull LinkedList<Loop> parents);

    /**
     * @return the stitches that are made by the child loop
     */
    @Contract(pure = true)
    @Nonnull
    public LinkedHashSet<Stitch> descendants(){
        return this.childLoop.getParentOf();
    }

    @Contract(value = "null->false",pure = true)
    public boolean isSymmetrical(@Nullable Stitch other){
        if(other==null)
            return false;
        boolean leanSymmetry = (this.lean==Lean.centered && other.lean==Lean.centered ) || this.lean != other.lean;
        return other.parentLoops.size()== this.parentLoops.size() && leanSymmetry && this.kpStyle == other.kpStyle;
    }

    @Override
    public int compareTo(@NotNull Stitch o) {
        return this.childLoop.compareTo(o.childLoop);
    }

    public static Comparator<Stitch> LeftMost = (o1, o2) -> {
        boolean isRS = o1.owningStitchSet.owningCourse.isStandardRightSide();
        int comparePos = o1.compareTo(o2);
        if(isRS)//child loops are increasing right to Left;
            return comparePos*-1;
        else
            return comparePos;
    };
    public static Comparator<Stitch> RightMost = (o1, o2) -> -1*LeftMost.compare(o1,o2);

    public abstract void disconnectFromWale(boolean allInLine) throws NoTextureCarvesAvailableException;
}
