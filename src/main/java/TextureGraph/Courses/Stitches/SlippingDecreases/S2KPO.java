package TextureGraph.Courses.Stitches.SlippingDecreases;

import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Knit.K;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.LoopDirection;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.Stitch;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 7/21/2016.
 * slip 2, k next, pass slipped stitches over
 */
public class S2KPO extends SlippingDecrease {
    /**
     * @param yarn the yarn to  make the child loop at the end off
     * @param parents the parent loops to be decreased together
     */
    public S2KPO(@NotNull LinkedList<Loop> parents, @NotNull Yarn yarn) {
        super();
        this.expectedParentCount = 3;
        Loop firstLoop = parents.remove();
        Loop secondLoop = parents.remove();
        this.childLoop = new Loop(yarn, LoopDirection.BtF);
        this.setChild(this.childLoop);
        this.parentLoops.add(parents.remove());
        this.parentLoops.add(firstLoop);
        this.parentLoops.add(secondLoop);
        for(Loop l: this.parentLoops)
            l.makeParentOf(this);
    }
    /**
     * @param parents the parent loops to be decreased together
     * @param child the child loop
     */
    public S2KPO(@NotNull LinkedList<Loop> parents, @NotNull Loop child){
        super();
        Loop firstLoop = parents.remove();
        Loop secondLoop = parents.remove();
        this.childLoop = child;
        this.setChild(this.childLoop);
        this.parentLoops.add(parents.remove());
        this.parentLoops.add(firstLoop);
        this.parentLoops.add(secondLoop);
    }

    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents)  {
        return new S2KPO(parents, yarn);
    }

    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Loop priorLoop, @NotNull LinkedList<Loop> parents){
        return new S2KPO(parents,priorLoop.insertNextLoop(LoopDirection.BtF));
    }

    @NotNull
    @Override
    public StitchSet increaseStitch() {//skpo-> k, k
        childLoop.removeChildOf(this);
        for(Loop parent: this.parentLoops)
            parent.removeParentOf(this);
        LinkedList<Loop> decreaseParents = new LinkedList<>();
        decreaseParents.add(this.parentLoops.get(1));
        decreaseParents.add(this.parentLoops.getLast());
        Skpo skpo = new Skpo(decreaseParents, this.childLoop);
        LinkedList<Stitch> newStitches = new LinkedList<>();
        newStitches.add(skpo);
        this.owningStitchSet.setStitches(newStitches);
        this.owningStitchSet = null;
        Loop newChild = this.childLoop.insertNextLoop(LoopDirection.BtF);
        K secondK = new K(this.parentLoops.getFirst(), newChild);
        return new Single(secondK);
    }
}
