package TextureGraph.Courses.Stitches.SlippingDecreases;

import TextureGraph.Courses.Lean;
import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Knit.K;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.LoopDirection;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.Stitch;
import Tracking.TypeTracking.KPStyle;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 7/21/2016.
 * slip one, knit next 2 together, pass slipp stitch over
 */
public class SK2PO extends SlippingDecrease {
    /**
     * @param yarn the yarn to  make the child loop at the end off
     * @param parents the parent loops to be decreased together
     */
    public SK2PO(@NotNull LinkedList<Loop> parents,@NotNull Yarn yarn) {
        super();
        this.lean = Lean.left;
        this.expectedParentCount = 3;
        Loop firstLoop = parents.remove();
        this.childLoop = new Loop(yarn, LoopDirection.BtF);
        this.setChild(this.childLoop);
        this.parentLoops.addAll(parents);
        this.parentLoops.add(firstLoop);
        for(Loop l: this.parentLoops)
            l.makeParentOf(this);
        this.kpStyle = KPStyle.KStyle;
    }
    /**
     * @param parents the parent loops to be decreased together
     * @param child the child loop
     */
    public SK2PO(@NotNull LinkedList<Loop> parents, @NotNull Loop child) {
        super();
        Loop firstLoop = parents.remove();
        this.childLoop = child;
        this.setChild(this.childLoop);
        this.parentLoops.addAll(parents);
        this.parentLoops.add(firstLoop);
        for(Loop l: this.parentLoops)
            l.makeParentOf(this);
        this.kpStyle = KPStyle.KStyle;
    }

    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents){
        LinkedList<Loop> neededParents= new LinkedList<>();
        for(int i=0; i<this.parentLoops.size(); i++)
            neededParents.add(parents.removeFirst());
        return new SK2PO( neededParents, yarn);
    }

    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Loop priorLoop, @NotNull LinkedList<Loop> parents) {
        LinkedList<Loop> neededParents= new LinkedList<>();
        for(int i=0; i<this.parentLoops.size(); i++)
            neededParents.add(parents.removeFirst());
        return new SK2PO( neededParents, priorLoop.insertNextLoop(LoopDirection.BtF));
    }

    @NotNull
    @Override
    public StitchSet increaseStitch() {//skpo-> k, k
        childLoop.removeChildOf(this);
        for(Loop parent: this.parentLoops)
            parent.removeParentOf(this);
        LinkedList<Loop> decreaseParents = new LinkedList<>();
        decreaseParents.add(this.parentLoops.getLast());
        decreaseParents.add(this.parentLoops.getFirst());
        Skpo skpo = new Skpo(decreaseParents, this.childLoop);
        LinkedList<Stitch> newStitches = new LinkedList<>();
        newStitches.add(skpo);
        this.owningStitchSet.setStitches(newStitches);
        this.owningStitchSet = null;
        Loop newChild = this.childLoop.insertNextLoop(LoopDirection.BtF);
        K secondK = new K(this.parentLoops.get(1), newChild);
        return new Single(secondK);
    }

}
