package TextureGraph.Courses.Stitches.SlippingDecreases;

import TextureGraph.Courses.Stitches.Decrease;
import TextureGraph.Courses.Stitches.IncreasableDecrease;
import Tracking.TypeTracking.KPStyle;

/**
 * Created by Megan on 2/23/2017.
 * decreases that involve passing slipped stitches over
 */
public abstract class SlippingDecrease extends Decrease implements IncreasableDecrease{
    SlippingDecrease(){
        super();
        this.kpStyle = KPStyle.PStyle;
    }

}
