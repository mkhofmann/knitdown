package TextureGraph.Courses.Stitches.SlippingDecreases;

import TextureGraph.Courses.Lean;
import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Knit.K;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.LoopDirection;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.Stitch;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 7/18/2016.
 * slip 1, knit next, pass slip over
 */


public class Skpo extends SlippingDecrease {
    /**
     * @param yarn the yarn to  make the child loop at the end off
     * @param parents the parent loops to be decreased together
     */
    public Skpo(@NotNull LinkedList<Loop> parents,@NotNull Yarn yarn){
        super();
        this.lean = Lean.left;
        Loop firstLoop = parents.remove();//save first loop
        this.childLoop = new Loop(yarn, LoopDirection.BtF);
        this.setChild(this.childLoop);
        this.parentLoops.add(parents.remove());
        this.parentLoops.add(firstLoop);//add the stored loop
        for(Loop l: this.parentLoops)
            l.makeParentOf(this);
    }
    /**
     * @param parents the parent loops to be decreased together
     * @param child the child loop
     */
    public Skpo(@NotNull LinkedList<Loop> parents, @NotNull Loop child){
        super();
        this.lean = Lean.left;
        assert parents.size()>=2;
        Loop firstLoop = parents.remove();
        this.childLoop = child;
        this.setChild(this.childLoop);
        this.parentLoops.add(parents.remove());
        this.parentLoops.add(firstLoop);
        for(Loop l: this.parentLoops)
            l.makeParentOf(this);
    }

    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents) {
        LinkedList<Loop> neededParents= new LinkedList<>();
        for(int i=0; i<this.parentLoops.size(); i++)
            neededParents.add(parents.removeFirst());
        return new Skpo( neededParents, yarn);
    }

    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Loop priorLoop, @NotNull LinkedList<Loop> parents) {
        LinkedList<Loop> neededParents= new LinkedList<>();
        for(int i=0; i<this.parentLoops.size(); i++)
            neededParents.add(parents.removeFirst());
        return new Skpo( neededParents, priorLoop.insertNextLoop(LoopDirection.BtF));
    }


    @NotNull
    @Override
    public StitchSet increaseStitch() {//skpo-> k, k
        childLoop.removeChildOf(this);
        for(Loop parent: this.parentLoops)
            parent.removeParentOf(this);
        K firstK = new K(this.parentLoops.getLast(), this.childLoop);
        LinkedList<Stitch> newStitches = new LinkedList<>();
        newStitches.add(firstK);
        this.owningStitchSet.setStitches(newStitches);
        this.owningStitchSet = null;
        Loop newChild = this.childLoop.insertNextLoop(LoopDirection.BtF);
        K secondK = new K(this.parentLoops.getFirst(), newChild);
        return new Single(secondK);
    }


}