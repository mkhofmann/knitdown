package TextureGraph.Courses.Stitches;

import CompilerExceptions.StitchManipulationExceptions.NoTextureCarvesAvailableException;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.LoopDirection;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import Tracking.TypeTracking.KPStyle;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 11/17/2016.
 * A none to one stitch that is just a yarn over loop created as a lacy increase
 */
public class YO extends Stitch {
    /**
     * @param yarn the yarn to create a yo at the end off
     */
    public YO(@NotNull Yarn yarn) {
        super(new LinkedList<>(), new Loop(yarn, LoopDirection.BtF));
        this.kpStyle = KPStyle.niether;
    }

    /**
     * @param loop the child loop of the yo
     */
    public YO(@NotNull Loop loop){
        super(new LinkedList<>(), loop);
        this.kpStyle = KPStyle.niether;
    }

    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents) {
        return new YO(yarn);
    }

    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Loop priorLoop, @NotNull LinkedList<Loop> parents) {
        return new YO(priorLoop.insertNextLoop(LoopDirection.BtF));
    }

    @Override
    public void disconnectFromWale(boolean allInLine) throws NoTextureCarvesAvailableException {
        throw new NoTextureCarvesAvailableException();
    }

}
