package TextureGraph.Courses.Stitches.Knit;

import TextureGraph.Courses.StitchSets.Increases.*;
import TextureGraph.Courses.Stitches.DuplicatableStitch;
import TextureGraph.Courses.Stitches.IncreasableStitch;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.LoopDirection;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.OneToOneStitch;
import TextureGraph.Courses.Stitches.Stitch;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;

/**
 * Created by Megan on 7/12/2016.
 * The most basic stitch, one to one stitch with child pulled btf
 */
public class K extends OneToOneStitch implements DuplicatableStitch, IncreasableStitch {
    /**
     * @param parent the loop that is the parent of the stitch
     * @param yarn the yarn to create the child loop at the end of
     */
    public K(@NotNull Loop parent, @NotNull Yarn yarn){
        super(parent, new Loop(yarn, LoopDirection.BtF));
    }

    public K(@NotNull Loop parent, @NotNull Loop child){
        super(parent, child);
    }

    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Yarn yarn, @NotNull LinkedList<Loop> parents) {
        assert !parents.isEmpty();
        return new K(parents.removeFirst(), yarn);
    }

    @NotNull
    @Override
    public Stitch copyFromParents(@NotNull Loop priorLoop, @NotNull LinkedList<Loop> parents){
        return new K(parents.removeFirst(),priorLoop.insertNextLoop(LoopDirection.BtF));
    }

    @NotNull
    @Override
    public OneToOneStitch duplicateStitchNext() { return new K(this.parentLoops.getFirst().insertPriorLoop(LoopDirection.BtF), this.childLoop.insertNextLoop(LoopDirection.BtF) );}


    @Override
    public Increase increaseStitch(int newChildCount, boolean increaseAfter) {
        LinkedList<Loop> children = new LinkedList<>();
        children.add(this.childLoop);
        Loop lastLoop = this.childLoop;
        if(increaseAfter){
            for(int i=1; i<newChildCount; i++){
                lastLoop = lastLoop.insertNextLoop(LoopDirection.BtF);
                children.add(lastLoop);
            }
        }else{//insert before
            for(int i=1; i<newChildCount; i++){
                lastLoop = lastLoop.insertPriorLoop(LoopDirection.BtF);
                children.addFirst(lastLoop);
            }
        }
        switch(newChildCount){
            case 2:
                return new KFB(this.parentLoops.getFirst(), children.getFirst(),children.getLast());
            case 3:
                return new Ctrdblinc(this.parentLoops.getFirst(),children);
            case 4:
                return new Inc4(this.parentLoops.getFirst(), children);
            case 5:
                return new Inc5(this.parentLoops.getFirst(), children);
            case 6:
                return new Inc6(this.parentLoops.getFirst(), children);
            case 7:
                return new Inc7(this.parentLoops.getFirst(), children);
            case 8:
                return new Inc8(this.parentLoops.getFirst(), children);
            case 9:
                return new Inc9(this.parentLoops.getFirst(), children);
            default:
                assert false;
        }
        return null;
    }
}
