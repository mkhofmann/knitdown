package TextureGraph.Courses;

import TextureGraph.Courses.StitchSets.Cables.Cable;
import TextureGraph.Courses.StitchSets.FlattenableSet;
import TextureGraph.Courses.StitchSets.Increases.Increase;
import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.*;
import TextureGraph.Courses.Stitches.Loops.*;
import TextureGraph.TextureGraph;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * Created by Megan on 7/20/2016.
 * A course heald a set of stitch sets in the order they occur on the yarn
 */
public class Course implements Iterable<StitchSet>, Comparable<Course>{
    /**
     * the sets of stitches that occur in the course, in order that they occur on the yarn
     */
    public LinkedList<StitchSet> stitchSets;

    /**
     * @return the index of the course in the graph, should corrispond to its declared index in knitspeak
     */
    public int getCourseID() {return lineID;}

    /**
     * the course's index in the graph
     */
    public int lineID;
    /**
     * Maps the type of stitch sets to their number of occurrances in the course
     */
    public LinkedHashMap<Class<? extends StitchSet>, Integer> stitchSetTypeToCountMap = new LinkedHashMap<>();
    /**
     * Maps the type of stitches to their number of occurrances in the course
     */
    public LinkedHashMap<Class<? extends Stitch>, Integer> stitchTypeToCountMap = new LinkedHashMap<>();
    /**
     * the number of stitches in this course, for efficient reference
     */
    public int stitchCount = 0;
    /**
     * the number of different types of stitch sets in the course
     */
    public int stitchSetTypeCount = 0;
    /**
     * the number of different stitch types in the course
     */
    public int stitchTypeCount =0;
    /**
     * the texture graph that owns this course
     */
    public TextureGraph owningTextureGraph = null;

    /**
     * creates an empty course at index id
     * @param id the index of the course in the graph
     */
    public Course(int id){
        this.stitchSets = new LinkedList<>();
        this.lineID = id;
    }

    /**
     * creates a course with the declared stitchsets
     * @param stitchSets the stitch sets that make up the course
     * @param id the index of the course in the graph
     */
    public Course(@NotNull LinkedList<StitchSet> stitchSets, int id){
        this.stitchSets = stitchSets;
        this.lineID = id;
    }

    /**
     * creates a course with the declared stitchsets
     * @param stitchSets the stitch sets that make up the course
     * @param id the index of the course in the graph
     */
    public Course(int id, @NotNull StitchSet... stitchSets){
        this.stitchSets = new LinkedList<>();
        for( StitchSet ss: stitchSets){
            this.addStitchSet(ss);
        }
        this.lineID = id;
    }

    /**
     * Add a regular turn at the end of this course
     */
    public void turnAtEndOfLine(){
        int lastSectionIndex;
        StitchSet lastStitchSet = this.stitchSets.getLast();
        if(lastStitchSet instanceof Cable)
            lastSectionIndex = ((Cable)lastStitchSet).unHeald.get(((Cable)lastStitchSet).unHeald.size()-1).childLoop.yarnSection.indexInYarn();
        else
            lastSectionIndex = lastStitchSet.stitches.getLast().childLoop.yarnSection.indexInYarn();
        if(lastSectionIndex+1<this.getYarn().sections.size() && (this.getYarn().sections.get(lastSectionIndex+1) instanceof TurnSection))
            return;
        this.getYarn().newTurn(lastSectionIndex);
    }

    /**
     * Flatten the specified set in the course
     * @param setToFlatten the set that needs to be flattened
     * @return the resulting sets from the flattening
     */
    @SuppressWarnings("SuspiciousMethodCalls")
    @NotNull
    public LinkedList<Single> flattenSet(@NotNull FlattenableSet setToFlatten) {
        int indexOfSet = this.stitchSets.indexOf(setToFlatten);
        assert indexOfSet>=0;
        assert this.stitchSets.remove(setToFlatten);
        LinkedList<Single> flattenedSingles = setToFlatten.flattenSet(false);
        for (Iterator<Single> it = flattenedSingles.descendingIterator(); it.hasNext(); ) {
            Single set = it.next();
            this.stitchSets.add(indexOfSet, set);
            set.owningCourse = this;
        }
        return flattenedSingles;
    }

    @SuppressWarnings("SuspiciousMethodCalls")
    public StitchSet unFlattenSet(@NotNull FlattenableSet setToUnFlatten){
        if(!setToUnFlatten.isFlattened())
            return (StitchSet) setToUnFlatten;
        int indexOfSet = this.stitchSets.indexOf(setToUnFlatten);
        assert indexOfSet>=0;
        assert this.stitchSets.remove(setToUnFlatten);
        StitchSet newSet = setToUnFlatten.unFlattenSet();
        this.stitchSets.add(indexOfSet, newSet);
        newSet.owningCourse = this;
        return newSet;
    }

    /**
     * Increases the specified set and replaces the defunct sets in the course
     * @param increasableStitch the stitch that can be converted into an increase
     * @param newChildCount the number of children in the new increase
     * @param increaseAfter flags if the increase should put new loops after the original child
     * @return the increase that has been placed in the course
     */
    @NotNull
    public Increase increaseSet(@NotNull IncreasableStitch increasableStitch, int newChildCount, boolean increaseAfter) {
        StitchSet setToIncrease = ((Stitch) increasableStitch).owningStitchSet;
        int indexOfStitch = this.stitchSets.indexOf(setToIncrease);
        assert indexOfStitch>=0 && setToIncrease instanceof Single;
        assert this.stitchSets.remove(setToIncrease);
        Increase increase = increasableStitch.increaseStitch(newChildCount, increaseAfter);
        this.stitchSets.add(indexOfStitch, increase);
        return increase;
    }

    /**
     * @return the yarn that makes up the graph
     */
    @Contract(pure = true)
    public Yarn getYarn(){
        return this.stitchSets.getFirst().stitches.getFirst().childLoop.yarnSection.parentYarn;
    }

    /**
     * @param ss the set added at the end of the course
     */
    void addStitchSet(@NotNull StitchSet ss) {
        this.stitchSets.addLast(ss);
        if(this.stitchSetTypeToCountMap.containsKey(ss.getClass()))
            this.stitchSetTypeToCountMap.put(ss.getClass(), this.stitchSetTypeToCountMap.get(ss.getClass())+1);
        else{
            this.stitchSetTypeCount++;
            this.stitchSetTypeToCountMap.put(ss.getClass(), 1);
        }
        for(Stitch stitch: ss){
            if(this.stitchTypeToCountMap.containsKey(stitch.getClass()))
                this.stitchTypeToCountMap.put(stitch.getClass(), this.stitchTypeToCountMap.get(stitch.getClass())+1);
            else{
                this.stitchTypeCount++;
                this.stitchTypeToCountMap.put(stitch.getClass(),1);
            }
        }
        ss.owningCourse = this;
    }

    /**
     * @return the number of stitch sets in the course
     */
    @Contract(pure = true)
    public int getStitchSetCount(){return this.stitchSets.size();}

    /**
     * @return get an ordered list of child loops in this course
     */
    @NotNull
    @Contract(pure = true)
    public LinkedList<Loop> getChildLoops(){
        LinkedHashSet<Loop> children = new LinkedHashSet<>();
        for (StitchSet ss: this )
            for (Stitch s : ss)
                children.add(s.childLoop);
        return new LinkedList<>(children);
    }
    /**
     * @return get an ordered list of parent loops in this course
     */
    @NotNull
    @Contract(pure = true)
    public LinkedList<Loop> getParentLoops(){
        LinkedHashSet<Loop> parents = new LinkedHashSet<>();
        for(StitchSet ss: this)
            for(Stitch s: ss)
                parents.addAll(s.parentLoops);
        return new LinkedList<>(parents);
    }

    /**
     * @return true if the index is an odd number
     */
    @Contract(pure = true)
    public boolean isStandardRightSide(){return this.lineID%2 == 1;}

    @NotNull
    @Override
    public Iterator<StitchSet> iterator(){return this.stitchSets.listIterator();}
    @NotNull
    public Iterator<StitchSet> iterator(int index){ return this.stitchSets.listIterator(index);}

    /**
     * @return an iterator that reads the stitches as if it were reading from wrong side
     */
    @NotNull
    private Iterator<StitchSet> iteratorWS(){ return this.stitchSets.descendingIterator();}

    @NotNull
    private Iterator<Stitch> stitchIteratorForward(){return new StitchIteratorForward();}

    @Override
    public int compareTo(@NotNull Course o) {
        return Integer.compare(this.lineID, o.lineID);
    }

    /**
     * @return a list of the stitches in this course, ignoring their sets
     */
    @Nonnull
    @Contract(pure = true)
    public LinkedList<Stitch> getStitches() {
        LinkedList<Stitch> stitches = new LinkedList<>();
        for(StitchSet set: this)
            for(Stitch stitch: set)
                stitches.add(stitch);
        return stitches;
    }

    private class StitchIteratorForward implements Iterator<Stitch>{
        Iterator<StitchSet> stitchSetIteratorForward;
        StitchSet curStitchSet;
        Iterator<Stitch> curStitchIterator;
        StitchIteratorForward(){
            stitchSetIteratorForward = Course.this.iterator();
            if(this.hasNextSS()) {
                this.curStitchSet = this.nextSS();
                this.curStitchIterator = this.nextSSIterator();
            }
        }
        @Override
        public boolean hasNext(){
            if(this.curStitchIterator != null && this.curStitchIterator.hasNext())
                return true;
            if(this.hasNextSS()){
                this.curStitchSet = this.nextSS();
                this.curStitchIterator = this.nextSSIterator();
                return this.hasNext();
            }else{
                return false;
            }
        }
        @Override
        public Stitch next(){
            if(!this.hasNext())//properly sets the curStitchIterator.
                throw new IndexOutOfBoundsException("No more stitches");
            return this.curStitchIterator.next();
        }
        private boolean hasNextSS(){return stitchSetIteratorForward.hasNext();}
        private StitchSet nextSS(){return this.stitchSetIteratorForward.next();}
        @NotNull
        private Iterator<Stitch> nextSSIterator(){return this.curStitchSet.iterator();}
    }

    @Override
    public String toString(){
        StringBuilder str = new StringBuilder(this.lineID + ":("+this.stitchCount+"): ");
        for(StitchSet ss: this)
            str.append(ss.toString()).append(", ");
        return str.toString();
    }

    /**
     * @param rs flags the order of the stitches in the to string
     * @return the to string, ordering the stitches according to the yarn direction
     */
    public String toString(boolean rs){
        StringBuilder str = new StringBuilder(this.lineID + ":("+this.stitchCount+"): ");
        if(rs)
            for(StitchSet ss: this){
                str.append(ss.toString()).append(", ");
            }
        else{
            Iterator<StitchSet> iter = this.iteratorWS();
            while(iter.hasNext()){
                str.append(iter.next().toString()).append(", ");
            }
        }
        return str.toString();
    }


    @Override
    @Contract("null->false")
    public boolean equals(Object o){
        if(o instanceof Course){
            Course l = (Course) o;
            if(l.getStitchSetCount() != this.getStitchSetCount())
                return false;
            Iterator<Stitch> thisIter= this.stitchIteratorForward();
            Iterator<Stitch> lIter = l.stitchIteratorForward();
            while(thisIter.hasNext() && lIter.hasNext()){
                if(!thisIter.next().getClass().equals(lIter.next().getClass()))//if the types don't match
                    return false;
            }
            return true;
        }
        return false;
    }
}
