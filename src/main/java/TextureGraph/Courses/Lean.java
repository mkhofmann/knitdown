package TextureGraph.Courses;

/**
 * Enumerator for tracking the physical lean direction of types of stitches and stitch sets
 */
public enum Lean {
    left,
    right,
    centered
}
