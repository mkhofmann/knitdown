package TextureGraph;

import CompilerExceptions.StitchManipulationExceptions.NoTextureCarvesAvailableException;
import TextureGraph.Courses.CastOnCourse;
import TextureGraph.Courses.Course;
import TextureGraph.Courses.StitchSets.Cables.Cable;
import TextureGraph.Courses.StitchSets.FlattenableSet;
import TextureGraph.Courses.StitchSets.Increases.Increase;
import TextureGraph.Courses.StitchSets.Single;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.StitchSets.Twists.Twist;
import TextureGraph.Courses.Stitches.*;
import TextureGraph.Courses.Stitches.Knit.K;
import TextureGraph.Courses.Stitches.Loops.*;
import TextureGraph.Courses.Stitches.Purl.P;
import TextureGraph.Courses.Stitches.StitchInRow.CastOn;
import TextureGraph.Courses.VerticalSetCarve;
import TextureGraph.WaleNavigation.HorizontalStitchCarve;
import TextureGraph.WaleNavigation.Wale;
import TextureGraph.WaleNavigation.WaleChain;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * Created by Megan on 7/18/2016.
 * a loop to loop graph structure with a topological yarn ordering that describes a single section of textured knit fabric
 */
public abstract class TextureGraph implements  Iterable<Course>, Comparable<TextureGraph>{
    /**
     * The yarn that heald the loops in this texture
     */
    private Yarn yarn;
    @NotNull
    @Contract(pure = true)
    public Yarn getYarn() {return this.yarn;}

    public LinkedHashMap<Stitch, Wale> stitchesToWale = new LinkedHashMap<>();

    public LinkedList<WaleChain> waleChains = new LinkedList<>();

    public LinkedHashMap<Wale, WaleChain> waleToWaleChains = new LinkedHashMap<>();
    /**
     * the courses that make up the textureGraph in order
     */
    public LinkedList<Course> courses = new LinkedList<>();
    /**
     * An index map that maps courses by their numerical index
     * Used for efficiently jumping through the texture
     */
    private LinkedHashMap<Integer, Course> lineIndex = new LinkedHashMap<>();
    /**
     * Maps the stitch sets in the texture to the course they are on for efficient search through the texture
     */
    private LinkedHashMap<StitchSet, Course> stitchSetToLineMap = new LinkedHashMap<>();
    /**
     * maps the stitches in the texture to the course they are on for efficient search through the texture
     */
    private LinkedHashMap<Stitch, Course> stitchToLineMap = new LinkedHashMap<>();
    /**
     * maps the loops in the texture to the course they are a parent of
     */
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    private LinkedHashMap<Loop, Course> parentLoopToLineMap = new LinkedHashMap<>();
    /**
     * maps the loops in the texture to the course they are a child of
     */
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    private LinkedHashMap<Loop, Course> childLoopToLineMap = new LinkedHashMap<>();
    /**
     * Maps the types of stitchsets to the number of occurrences of that type
     */
    public LinkedHashMap<Class<? extends StitchSet>, Integer> stitchSetTypeToCountMap = new LinkedHashMap<>();
    /**
     * Maps the types of stitches to the number of occurrences of that type
     */
    public LinkedHashMap<Class<? extends Stitch>, Integer> stitchTypeToCountMap = new LinkedHashMap<>();
    /**
     * Counts the number of stitchsets in the texture
     */
    public int stitchSetCount = 0;
    /**
     * Counts the number of stitches in  the texture
     */
    public int stitchCount = 0;
    /**
     * Counts the number of stitch set types in the texture
     */
    public int stitchSetTypeCount = 0;

    /**
     * the creates an empty texture graph with no loops or yarn
     */
    public TextureGraph(){
    }

    /**
     * Copy constructor for a texture graph
     * @param textureGraph the texture graph to copy from
     * @param copyYarn the yarn to create the new graph from
     */
    public TextureGraph(@NotNull TextureGraph textureGraph, @NotNull Yarn copyYarn){
        this();
        this.yarn = copyYarn;
        LinkedList<Course> copyCourses = new LinkedList<>();
        LinkedList<StitchSet> castOns = new LinkedList<>();
        HashMap<StitchSet,LinkedHashSet<StitchSet>> childSets = new HashMap<>();
        Course firstCourse = textureGraph.getFirstLine();
        for(StitchSet stitchSet: firstCourse){
            StitchSet copy = stitchSet.copyFromParents(this.yarn, new LinkedList<>());
            castOns.add(copy);
            for(StitchSet descendant: stitchSet.descendants()) {
                if(!childSets.containsKey(descendant))
                    childSets.put(descendant, new LinkedHashSet<>());
                childSets.get(descendant).add(copy);
            }
        }
        copyCourses.add(new Course(castOns, 0));
        for (Iterator<Course> it = textureGraph.iterator(1); it.hasNext(); ) {
            Course course = it.next();
            LinkedList<StitchSet> newSets = new LinkedList<>();
            HashMap<StitchSet, LinkedHashSet<StitchSet>> childSets_Next = new HashMap<>();
            for(StitchSet set: course){
                LinkedHashSet<StitchSet> descendants = set.descendants();
                if(childSets.containsKey(set)){
                    LinkedHashSet<StitchSet> parentSets = childSets.get(set);
                    LinkedHashSet<Loop> parentSet = new LinkedHashSet<>();
                    for(StitchSet parentStitchSet: parentSets)
                        parentSet.addAll(parentStitchSet.getChildLoops());
                    LinkedList<Loop> parents = new LinkedList<>(parentSet);
                    StitchSet copy = set.copyFromParents(copyYarn, parents);
                    newSets.add(copy);
                    for(StitchSet descendant: descendants) {
                        if(!childSets_Next.containsKey(descendant))
                            childSets_Next.put(descendant, new LinkedHashSet<>());
                        childSets_Next.get(descendant).add(copy);
                    }
                }
            }
            copyCourses.add(new Course(newSets, course.getCourseID()));
            childSets = childSets_Next;
        }
        for(Course copyCourse : copyCourses)
            this.addLine(copyCourse);
        this.buildWales();
    }

    private void buildWales(){
        this.stitchesToWale.clear();
        for(StitchSet set: this.getFirstLine())
            for (Stitch stitch : set)
                if (!this.stitchesToWale.containsKey(stitch)) {
                    LinkedHashSet<Wale> walesFromStitch = Wale.buildWales(stitch);
                    for (Wale wale : walesFromStitch)
                        for (Stitch waleStitch : wale)
                            this.stitchesToWale.put(waleStitch, wale);
                }

    }

    public void buildWaleChains(){
        this.buildWales();
        this.waleChains.clear();
        for(StitchSet set: this.getFirstLine())
            for(Stitch stitch: set){
                WaleChain chain = new WaleChain(this.stitchesToWale.get(stitch), this.waleChains);
                this.waleChains.add(chain);
            }
        for(WaleChain chain: this.waleChains)
            for(Wale wale: chain) {
                this.waleToWaleChains.put(wale, chain);
                for(Stitch stitch: wale)
                    this.stitchesToWale.put(stitch, wale);
            }

    }

    /**
     * Build the graph from a list of courses by simply adding them to the texture
     * @param courses the courses that make up the texture
     */
    public TextureGraph(@NotNull LinkedList<Course> courses){
        for(Course course : courses)
            this.addLine(course);
        this.yarn = this.getFirstLine().stitchSets.getFirst().stitches.getFirst().getYarn();
    }
    /**
     * switches a row or round construction to the alternative
     * @param yarn the new yarn to build the switched textureGraph graph
     * @return a textureGraph graph in the alternative construction order
     */
    @NotNull
    @Contract(pure = true)
    public abstract TextureGraph switchRowRounds(@NotNull Yarn yarn);
    /**
     * Clear the costs of creating a carve near past carves
     */
    private void clearNeighborCosts(){
        for(Course course : this){
            for(StitchSet set: course) {
                set.neighborReduceWidthCost = 0.0;
                for(Stitch stitch: set)
                    stitch.neighborReduceHeightCost = 0.0;
            }
        }
    }
    /**
     * Narrow the texture graph with texture carving in stepped intervals
     * @param startLine the first course to start tapering from
     * @param endLine the last course to taper
     * @param narrowPerStep the number of loops removed per row in each step
     * @param stepHeight the distance between taper calls
     * @throws NoTextureCarvesAvailableException if there are no possible narrowed paths from this course
     */
    public void taperNarrow(int startLine, int endLine, int narrowPerStep, int stepHeight) throws NoTextureCarvesAvailableException {
        if(startLine<0)
            startLine=0;
        if(endLine>this.courses.size())
            endLine=this.courses.size();
        int stepCount = (endLine-startLine)/stepHeight;
        for(int i=0; i<stepCount; i++) {
            int narrowLine = startLine + (i * stepHeight);
            if(this.getLine(narrowLine).stitchCount<=narrowPerStep)
                break;
            this.narrowTextureGraph(narrowPerStep, narrowLine);
        }
    }
    /**
     * Decrease the texture from a course onward
     * @param narrowPaths the number of narrows to be done
     * @param narrowRow the row to start narrowing from
     * @throws NoTextureCarvesAvailableException if there are no possible narrowed paths from this course
     */
    public void narrowTextureGraph(int narrowPaths, int narrowRow) throws NoTextureCarvesAvailableException {
        this.narrowTextureGraph(narrowPaths, narrowRow, false);
    }

    @SuppressWarnings("SuspiciousMethodCalls")
    public void shortenTextureGraph(int carveCount) throws NoTextureCarvesAvailableException {
        this.clearNeighborCosts();
        for(int carveIndex=0; carveIndex<carveCount; carveIndex++) {
            if (this.stitchesToWale.isEmpty())
                this.buildWaleChains();
            assert !this.stitchesToWale.isEmpty();
            int remaining = this.disconnectStitchesFromHorizontalCarves();
            if (remaining == carveCount)
                throw new NoTextureCarvesAvailableException();
            //clear brokenStitchSets:
            HashSet<Course> coursesToRemove = new HashSet<>();
            for (Course course : this) {
                HashSet<StitchSet> setsToRemove = new HashSet<>();
                for (StitchSet set : course) {
                    for (Stitch stitch : set)
                        if (stitch.owningStitchSet != set || stitch.childLoop == null)
                            setsToRemove.add(set);
                }
                course.stitchSets.removeAll(setsToRemove);//todo: possible inefficiency
                if (course.stitchSets.isEmpty())
                    coursesToRemove.add(course);
            }
            this.courses.removeAll(coursesToRemove);
            int i = 0;
            for (Course course : this) {
                course.lineID = i;
                i++;
            }

            //repair yarn order
            for (Course course : this) {
                LinkedList<Stitch> stitches = course.getStitches();
                Iterator<Stitch> stitchIterator = stitches.iterator();
                Stitch priorStitch = stitchIterator.next();
                for (; stitchIterator.hasNext(); ) {
                    Stitch curStitch = stitchIterator.next();
                    Loop priorLoop = priorStitch.childLoop;
                    Loop curLoop = curStitch.childLoop;
                    int compare = priorLoop.compareTo(curLoop);
                    assert compare != 0;
                    if (compare > 0) {//the prior loop is greater than the next loop
                        if (curStitch.owningStitchSet instanceof Cable) {
                            boolean isHealdCur = ((Cable) curStitch.owningStitchSet).heald.contains(curStitch);
                            boolean isHealdPrior = ((Cable) curStitch.owningStitchSet).heald.contains(priorStitch);
                            if (isHealdCur == isHealdPrior)
                                priorLoop.swapYarnSections(curLoop);
                        } else
                            priorLoop.swapYarnSections(curLoop);
                    }
                    priorStitch = curStitch;
                }
            }

            Iterator<YarnSection> sectionIterator = this.yarn.sections.iterator();
            YarnSection priorSection = sectionIterator.next();
            YarnSection curSection = sectionIterator.next();
            HashSet<YarnSection> turnsToRemove = new HashSet<>();
            while (sectionIterator.hasNext()) {
                if (priorSection instanceof TurnSection && curSection instanceof TurnSection)
                    turnsToRemove.add(priorSection);
                priorSection = curSection;
                curSection = sectionIterator.next();
            }
            this.yarn.sections.removeAll(turnsToRemove);
            turnsToRemove.clear();
            sectionIterator = this.yarn.sections.iterator();
            priorSection = sectionIterator.next();
            curSection = sectionIterator.next();
            for (; sectionIterator.hasNext(); ) {
                YarnSection nextSection = sectionIterator.next();
                if (curSection instanceof TurnSection) {
                    if (nextSection instanceof TurnSection)
                        turnsToRemove.add(curSection);
                    else {
                        assert priorSection != null;
                        assert !(priorSection instanceof TurnSection);
                        assert priorSection.loop.yarnSection == priorSection;
                        assert nextSection != null;
                        if( nextSection.loop.yarnSection != nextSection)
                            assert nextSection.loop.yarnSection == nextSection;
                        assert priorSection.loop.getLastChildOf() != null;
                        Course priorOwningCourse = priorSection.loop.getLastChildOf().owningStitchSet.owningCourse;
                        assert nextSection.loop.getLastChildOf() != null;
                        Course nextOwningCourse = nextSection.loop.getLastChildOf().owningStitchSet.owningCourse;
                        if (priorOwningCourse == nextOwningCourse)
                            turnsToRemove.add(curSection);
                    }
                }
                priorSection = curSection;
                curSection = nextSection;
            }
            this.yarn.sections.removeAll(turnsToRemove);
            this.resetMaps();
        }
    }

    /**
     * Decrease the texture from a course onward
     * @param carveCount the number of narrows to be done
     * @param narrowRow the row to start narrowing from
     * @param recursed flags if this is being called as a recursive function
     * @throws NoTextureCarvesAvailableException if there are no possible narrowed paths from this course
     */
    private void narrowTextureGraph(int carveCount, int narrowRow, boolean recursed) throws NoTextureCarvesAvailableException {
        if(!recursed)
            this.clearNeighborCosts();
        int narrowsRemaining = this.removeChildrenFromVerticalCarves(carveCount, narrowRow);
        //if we could narrow at all, do next best thing (widen edges
        if(narrowsRemaining == carveCount)
            throw new NoTextureCarvesAvailableException();
        Iterator<Course> lineIterator;
        if(narrowRow==0) {
            lineIterator = this.iterator();
        } else {
            lineIterator = this.iterator(narrowRow+1);
            Course decreaseCourse = this.getLine(narrowRow);
            LinkedList<Single> createDecreaseSets = new LinkedList<>();
            LinkedHashMap<Single, Integer> setIndices = new LinkedHashMap<>();
            for(int i = 0; i< decreaseCourse.stitchSets.size(); i++){
                StitchSet set = decreaseCourse.stitchSets.get(i);
                if(set instanceof Single) {
                    Stitch firstStitch = set.getFirstStitch();
                    if(((Single) set).mergableWithNextNeighbor()) {
                        Stitch nextNeighbor = firstStitch.nextNeighbor();
                        if (firstStitch.childLoop == null || (nextNeighbor != null && nextNeighbor.childLoop == null)){
                            createDecreaseSets.add((Single) set);
                            setIndices.put((Single) set, i);
                            i++;//the next set will be merged if this is selected
                        }
                    }
                }
            }
            int decsNeeded = carveCount - narrowsRemaining;
            if(createDecreaseSets.size()> decsNeeded)//there are more increases than needed decreases so remove a few evenly spaced ones
                createDecreaseSets = this.getSpacedSingleSets(decsNeeded, createDecreaseSets, setIndices);
            assert createDecreaseSets.size() <= decsNeeded;
            for(Single decreaseStarterSet: createDecreaseSets){
                Stitch firstStitch = decreaseStarterSet.getFirstStitch();
                Stitch nextStitch = Objects.requireNonNull(decreaseStarterSet.getNextStitchInLine());
                if(firstStitch.childLoop == null)
                    firstStitch.childLoop = nextStitch.childLoop;
                else
                    nextStitch.childLoop = firstStitch.childLoop;
                int index = setIndices.get(decreaseStarterSet);
                Single merged = decreaseStarterSet.mergeToDecreaseWithNextNeighbor();
                assert merged != null;
                decreaseCourse.stitchSets.set(index, merged);
                if(nextStitch.childLoop  == merged.getFirstStitch().childLoop)
                    decreaseCourse.stitchSets.set(index+1, null);
            }
            decreaseCourse.stitchSets.removeIf(Objects::isNull);
        }
        for (Iterator<Course> it = lineIterator; it.hasNext(); ) {
            Course course = it.next();
            HashSet<Integer> easyRemoves = new HashSet<>();
            HashMap<Integer, Single> replacements = new HashMap<>();
            int i=0;
            for(StitchSet set: course){
                if(set instanceof Single &&
                        set.getFirstStitch().childLoop==null &&
                        (set.getFirstStitch() instanceof CastOn ||
                            set.getFirstStitch() instanceof YO  ||
                            set.getFirstStitch().parentLoops.isEmpty()) )
                    easyRemoves.add(i);
                else if(set instanceof Twist || set instanceof Cable){
                    OneToOneStitch missingParent = null;
                    OneToOneStitch missingChild = null;
                    LinkedList<Loop> survivingChildren = new LinkedList<>();
                    LinkedList<Loop> survivingParents = new LinkedList<>();
                    for(Stitch stitch: set){
                        if(stitch.childLoop == null)
                            missingChild = (OneToOneStitch) stitch;
                        else
                            survivingChildren.add(stitch.childLoop);
                        if(stitch.parentLoops.isEmpty()) {
                            assert !(stitch instanceof YO);
                            missingParent = (OneToOneStitch) stitch;
                        } else
                            survivingParents.add(stitch.parentLoops.getFirst());
                    }
                    if(missingChild != null && missingParent != null) {
                        if (set instanceof Twist) {
                            assert survivingChildren.size() == 1;
                            assert survivingParents.size()==1;
                            assert !survivingChildren.getFirst().getChildOf().isEmpty();
                            if(survivingChildren.getFirst().getFirstChildOf() instanceof K) //if the child is a K, then make a K
                                replacements.put(i, new Single(new K(survivingParents.getFirst(), survivingChildren.getFirst())));
                            else
                                replacements.put(i, new Single(new P(survivingParents.getFirst(), survivingChildren.getFirst())));
                        }
                        else {
                            assert set.stitches.size()>2;
                            if(missingChild == missingParent) {
                                assert set.stitches.remove(missingChild);
                                assert ((Cable) set).heald.remove(missingChild) || ((Cable) set).unHeald.remove(missingChild);
                            }
                        }

                    }
                } else if(set instanceof Increase){
                    Stitch removeFromSet = null;
                    for(Stitch stitch: set){
                        if(stitch.childLoop == null) {
                            removeFromSet =stitch;
                            break;
                        }
                    }
                    set.stitches.remove(removeFromSet);
                    if(set.stitches.size()==1)
                        replacements.put(i, new Single(set.stitches.getFirst()));
                }
                i++;
            }
            for(Integer index: replacements.keySet())
                course.stitchSets.set(index, replacements.get(index));
            for(Integer index: easyRemoves) {
                Single set = (Single) course.stitchSets.get(index);
                for(Loop parent: set.getFirstStitch().parentLoops)
                    parent.removeLoopFromYarn();
                course.stitchSets.set(index, null);
            }
            course.stitchSets.removeIf(Objects::isNull);
            LinkedList<Stitch> missingChildren = new LinkedList<>();
            LinkedList<Stitch> missingParents = new LinkedList<>();
            for(StitchSet set: course){
                for(Stitch stitch: set){
                    if(stitch.childLoop==null)
                        missingChildren.add(stitch);
                    if(stitch instanceof OneToOneStitch && stitch.parentLoops.isEmpty()
                            || (stitch instanceof Decrease && ((Decrease)stitch).expectedParentCount != stitch.parentLoops.size()))
                        missingParents.add(stitch);
                }
            }
            assert missingChildren.size() == missingParents.size() ;
            if(missingChildren.size()>0) {
                Iterator<Stitch> missingChildIter = missingChildren.iterator();
                Iterator<Stitch> missingParentIter = missingParents.iterator();
                for (; missingChildIter.hasNext(); ) {
                    Stitch missingChild = missingChildIter.next();
                    assert missingParentIter.hasNext();
                    Stitch missingParent = missingParentIter.next();
                    assert missingChild instanceof OneToOneStitch;
                    missingChild.parentLoops.getFirst().removeParentOf(missingChild);
                    missingChild.parentLoops.getFirst().makeParentOf(missingParent);
                    missingParent.parentLoops.add(missingChild.parentLoops.getFirst());
                    missingChild.parentLoops.removeFirst();
                }
                easyRemoves.clear();
                i=0;
                for(StitchSet set: course){
                    for(Stitch stitch: set)
                        if(stitch.childLoop == null)
                            easyRemoves.add(i);
                    i++;
                }
                for(Integer index: easyRemoves)
                    course.stitchSets.set(index, null);
                course.stitchSets.removeIf(Objects::isNull);
            }
            for(StitchSet set: course)
                if(set instanceof Cable)
                    ((Cable)set).unTwistCable();
        }
        this.resetMaps();
        if(narrowsRemaining>0)
            this.narrowTextureGraph(narrowsRemaining, narrowRow, true);
    }

    private int disconnectStitchesFromHorizontalCarves() throws NoTextureCarvesAvailableException {
        TreeSet<HorizontalStitchCarve> carveSet = this.buildPossibleHorizontalCarves();
        int remaining = 1 - carveSet.size();
        assert remaining>=0;
        for(HorizontalStitchCarve carve: carveSet)
            carve.disconnectStitches();
        return remaining;
    }

    /**
     * Removes the loops that reduce each set along a given number of paths
     * @param carves the number of paths to be reduced
     * @param narrowRow the first row to be narrowed
     * @return the number of paths that were successfully reduced
     */
    private int removeChildrenFromVerticalCarves(int carves, int narrowRow) {
        TreeSet<VerticalSetCarve> carveSet =this.getVerticalCarves(carves, narrowRow);
        int remaining =carves-carveSet.size();
        assert remaining>=0;
        for(VerticalSetCarve carve: carveSet)
            carve.removeChildrenFromSets();
        return remaining;
    }

    private TreeSet<HorizontalStitchCarve> buildPossibleHorizontalCarves(){
        TreeSet<HorizontalStitchCarve> carveSet = new TreeSet<>();
        TreeSet<HorizontalStitchCarve> carves = this.getHorizontalCarves();
        if(carves.isEmpty())
            return  carveSet;
        HorizontalStitchCarve first = carves.pollFirst();
        carveSet.add(first);
        assert first != null;
        HashSet<Stitch> crossedSets = new HashSet<>(first.carve);
        while(carveSet.size()< 1){
            first = carves.pollFirst();
            while(first==null && !carves.isEmpty())
                first = carves.pollFirst();
            if(carves.isEmpty())
                break;
            if(first.pathIsExclusive(crossedSets)) {
                carveSet.add(first);
                crossedSets.addAll(first.carve);
            }
        }
        return carveSet;
    }

    /**
     * @param carveCount the number of paths desired to reduce
     * @param narrowRow the first row to be narrowed
     * @return a set of paths that can be narrowed sorted by their cost to reduce
     */
    @Nonnull
    private TreeSet<VerticalSetCarve> getVerticalCarves(int carveCount, int narrowRow) {
        TreeSet<VerticalSetCarve> verticalCarveSet = new TreeSet<>();
        if(carveCount<1)
            return verticalCarveSet;
        TreeSet<VerticalSetCarve> carves = this.getVerticalCarves(narrowRow);
        if(carves.isEmpty())
            return  verticalCarveSet;
        VerticalSetCarve first = carves.pollFirst();
        verticalCarveSet.add(first);
        assert first != null;
        HashSet<StitchSet> crossedSets = new HashSet<>(first.carve);
        while(verticalCarveSet.size()<carveCount){
            first = carves.pollFirst();
            while(first==null && !carves.isEmpty())
                first = carves.pollFirst();
            if(carves.isEmpty())
                break;
            if(first.pathIsExclusive(crossedSets)) {
                verticalCarveSet.add(first);
                crossedSets.addAll(first.carve);
            }
        }
        return verticalCarveSet;
    }

    private TreeSet<HorizontalStitchCarve> getHorizontalCarves(){
        this.getDynamicHorizontalTextureCarveCosts();
        TreeSet<HorizontalStitchCarve> carves = new TreeSet<>();
        for(Course course: this){
            Stitch leftMost;
            if(course.isStandardRightSide())
                leftMost = course.getStitches().getLast();
            else
                leftMost = course.getStitches().getFirst();
            if(leftMost.horizontalPathCalculated && leftMost.pathToRemoveFromWaleCost <Double.MAX_VALUE) {
                HorizontalStitchCarve carve = new HorizontalStitchCarve(leftMost);
                carves.add(carve);
            }
        }
//        for (Iterator<Stitch> it = this.waleChains.getFirst().stitchIterator(); it.hasNext(); ) {
//            Stitch stitch = it.next();
//            if(stitch.horizontalPathCalculated && stitch.pathToRemoveFromWaleCost <Double.MAX_VALUE)
//                carves.add(new HorizontalStitchCarve(stitch));
//        }
        TreeSet<HorizontalStitchCarve> fullCarves = new TreeSet<>();
        for(HorizontalStitchCarve carve: carves)
            if(carve.carve.size()==this.waleChains.size())
                fullCarves.add(carve);
        return fullCarves;
    }

    /**
     * @param narrowRow the first row to be narrowed
     * @return all possible paths that can be narrowed sorted by their cost to reduce
     */
    @Nonnull
    private TreeSet<VerticalSetCarve> getVerticalCarves(int narrowRow) {
        this.setVerticalTextureCarveCosts();
        TreeSet<VerticalSetCarve> carves = new TreeSet<>();
        Course startNarrows;
        if(narrowRow == 0)
            startNarrows = this.getFirstLine();
        else
            startNarrows= this.getLine(narrowRow);
        for(StitchSet set: startNarrows) {
            if((set.pathCalculated && set.verticalPathCost <Double.MAX_VALUE) &&
                    (set instanceof Single && (set.getFirstStitch() instanceof CastOn || ((Single) set).mergableWithNextNeighbor())))//mergability constraint for decreasing
                carves.add(new VerticalSetCarve(set));
        }
        TreeSet<VerticalSetCarve> fullCarves = new TreeSet<>();
        for(VerticalSetCarve carve: carves)
            if(carve.carve.size()==(this.courses.size()-narrowRow))
                fullCarves.add(carve);
        return fullCarves;
    }

    /**
     * Dynamically assigns the path cost to each stitch set based on the sum of the sets decendents
     */
    private void setVerticalTextureCarveCosts(){
        this.setReduceWidthCosts();
        Iterator<Course> lineIterator = this.courses.descendingIterator();
        assert lineIterator.hasNext();
        for(StitchSet set: lineIterator.next()){ //single set paths from last course
            set.verticalPathCost = set.proportionalCostToReduceWidth;
            set.pathCalculated = true;
        }
        for (; lineIterator.hasNext(); ) {
            Course course = lineIterator.next();
            for(StitchSet set: course) {
                if(set.proportionalCostToReduceWidth == Double.MAX_VALUE) {
                    set.pathCalculated = true;
                    set.verticalPathCost = set.costToReduceWidthOfSet;
                    continue;
                }
                double minDescendantPathCost = Double.MAX_VALUE;
                LinkedHashSet<StitchSet> descendants = set.descendants();
                assert !descendants.isEmpty();
                for(StitchSet descendant: descendants){
                    assert descendant.pathCalculated;
                    if(descendant.verticalPathCost >=minDescendantPathCost)
                        continue;
                    double descendantWeight = 1.0+((double)descendant.stitchCount()) / ((double)set.stitchCount());
                    boolean disSimilarTypes = descendant.getClass() != descendant.getClass();
                    if(disSimilarTypes && descendant instanceof Single)
                        disSimilarTypes = set.getFirstStitch().getClass() != descendant.getFirstStitch().getClass();
                    double dissimilarityCost = 0;
                    if(disSimilarTypes)
                        dissimilarityCost = VerticalSetCarve.DIAGONAL_VERTICAL_PENALTY;
                    minDescendantPathCost = Double.min(minDescendantPathCost, (descendant.verticalPathCost *descendantWeight)+dissimilarityCost);
                }
                StitchSet descendantNextNeighbor = set.descendantOfNextStitchInLine();
                StitchSet descendantPriorNeighbor = set.descendantofPriorStitchInLine();
                if(descendantNextNeighbor != null && descendantNextNeighbor.verticalPathCost < Double.MAX_VALUE) {
                    double nextPathWeight= 1.0+((double)descendantNextNeighbor.stitchCount()) / ((double)set.stitchCount());//size of set over the size of cur set
                    boolean disSimilarTypes = descendantNextNeighbor.getClass() != descendantNextNeighbor.getClass();
                    if(disSimilarTypes && descendantNextNeighbor instanceof Single)
                        disSimilarTypes = set.getFirstStitch().getClass() != descendantNextNeighbor.getFirstStitch().getClass();
                    double dissimilarityCost = 0;
                    if(disSimilarTypes)
                        dissimilarityCost = VerticalSetCarve.DIAGONAL_VERTICAL_PENALTY;
                    set.verticalPathCost = (nextPathWeight*descendantNextNeighbor.verticalPathCost) + (VerticalSetCarve.DIAGONAL_VERTICAL_PENALTY)+dissimilarityCost;
                }
                if(descendantPriorNeighbor != null && descendantPriorNeighbor.verticalPathCost < Double.MAX_VALUE) {
                    double priorPathWeight = 1.0+((double)descendantPriorNeighbor.stitchCount()) / ((double)set.stitchCount());//size of set over the size of cur set
                    boolean disSimilarTypes = descendantPriorNeighbor.getClass() != descendantPriorNeighbor.getClass();
                    if(disSimilarTypes && descendantPriorNeighbor instanceof Single)
                        disSimilarTypes = set.getFirstStitch().getClass() != descendantPriorNeighbor.getFirstStitch().getClass();
                    double dissimilarityCost = 0;
                    if(disSimilarTypes)
                        dissimilarityCost = VerticalSetCarve.DIAGONAL_VERTICAL_PENALTY;
                    set.verticalPathCost = Double.min(set.verticalPathCost, (priorPathWeight*descendantPriorNeighbor.verticalPathCost)+ (VerticalSetCarve.DIAGONAL_VERTICAL_PENALTY)+dissimilarityCost);
                }
                set.verticalPathCost = Double.min(set.verticalPathCost, minDescendantPathCost);
                set.verticalPathCost += set.proportionalCostToReduceWidth;
                set.pathCalculated = true;
            }
        }
    }

    private void getDynamicHorizontalTextureCarveCosts(){
        this.setHorizontalTextureCarveCosts();
        for (Iterator<WaleChain> it = this.waleChains.descendingIterator(); it.hasNext(); ) {
            WaleChain chain = it.next();
            for (Iterator<Stitch> it1 = chain.stitchIterator(); it1.hasNext(); ) {
                Stitch stitch = it1.next();
                stitch.setDynamicWaleRemovalCost();
            }

        }
    }


    /**
     * Assign the local cost of reducing each stitch set
     */
    private void setReduceWidthCosts(){
        this.resetMaps();
        for(Course course : this)
            for(StitchSet set: course)
                set.setProportionalCost();
    }

    /**
     * Assign the local cost of reducing each stitch set
     */
    private void setHorizontalTextureCarveCosts(){
        for(Course course : this)
            for(StitchSet set: course)
                for(Stitch stitch: set)
                    stitch.setWaleRemovalCost();
    }

    /**
     * adds a course to the end of the textureGraph, then inserts a turn section
     * @param l the course to add in
     */
    public void addLine(@NotNull Course l){
        if(l.stitchSets.size()<=0)
            return;
        this.courses.addLast(l);
        this.lineIndex.put(l.getCourseID(), l);
        l.turnAtEndOfLine();
        this.mapCourse(l);

    }
    /**
     * maps the contents of a course into the graph's indices
     * @param course the course to map
     */
    private void mapCourse(Course course){
        course.owningTextureGraph = this;
        course.stitchTypeToCountMap.clear();
        course.stitchSetTypeToCountMap.clear();
        course.stitchCount=0;
        course.stitchSetTypeCount=0;
        course.stitchTypeCount=0;
        for(StitchSet set: course){
            for(Stitch stitch: set){
                stitch.owningStitchSet = set;
                if(this.stitchToLineMap.put(stitch, course)==null)
                    this.stitchCount++;
                if(this.stitchTypeToCountMap.containsKey(stitch.getClass()))
                    this.stitchTypeToCountMap.put(stitch.getClass(), this.stitchTypeToCountMap.get(stitch.getClass())+1);
                else{
                    this.stitchTypeToCountMap.put(stitch.getClass(), 1);
                }
                course.stitchCount++;
                if(course.stitchTypeToCountMap.containsKey(stitch.getClass())) {
                    course.stitchTypeToCountMap.put(stitch.getClass(), course.stitchTypeToCountMap.get(stitch.getClass())+1);
                    assert course.stitchTypeToCountMap.get(stitch.getClass()) <= course.stitchCount || course.stitchTypeToCountMap.get(stitch.getClass()) <= course.stitchCount;
                } else{
                    course.stitchTypeCount++;
                    course.stitchTypeToCountMap.put(stitch.getClass(), 1);
                }
                this.childLoopToLineMap.put(stitch.childLoop, course);
                assert stitch.childLoop != null;
                stitch.childLoop.makeChildOf(stitch);
                for(Loop parent: stitch.parentLoops) {
                    this.parentLoopToLineMap.put(parent, course);
                    parent.makeParentOf(stitch);
                }
            }

            set.owningCourse = course;
            set.pathCalculated = false;
            if(this.stitchSetToLineMap.put(set, course)==null)
                this.stitchSetCount++;
            if(this.stitchSetTypeToCountMap.containsKey(set.getClass()))
                this.stitchSetTypeToCountMap.put(set.getClass(), this.stitchSetTypeToCountMap.get(set.getClass())+1);
            else{
                this.stitchSetTypeCount++;
                this.stitchSetTypeToCountMap.put(set.getClass(), 1);
            }
            if(course.stitchSetTypeToCountMap.containsKey(set.getClass()))
                course.stitchSetTypeToCountMap.put(set.getClass(), course.stitchSetTypeToCountMap.get(set.getClass())+1);
            else{
                course.stitchSetTypeCount++;
                course.stitchSetTypeToCountMap.put(set.getClass(), 1);
            }
        }
    }
    /**
     * reset the maps for each course because of large scale changes such as texture carving
     */
    private void resetMaps(){
        this.stitchesToWale.clear();
        this.waleChains.clear();
        this.waleToWaleChains.clear();
        this.stitchSetToLineMap = new LinkedHashMap<>();
        this.stitchToLineMap = new LinkedHashMap<>();
        this.parentLoopToLineMap = new LinkedHashMap<>();
        this.childLoopToLineMap = new LinkedHashMap<>();
        this.stitchTypeToCountMap = new LinkedHashMap<>();
        this.stitchSetTypeToCountMap = new LinkedHashMap<>();
        for (Loop loop : this.yarn) {
            loop.clearParentsOf();
            loop.clearChildOf();
        }
        HashSet<Course> courseToRemove = new HashSet<>();
        for(Course course: this){
            HashSet<StitchSet> setsToRemove = new HashSet<>();
            for(StitchSet set: course){
                HashSet<Stitch> stitchToRemove = new HashSet<>();
                for(Stitch stitch: set)
                    if(stitch.childLoop == null)
                        stitchToRemove.add(stitch);
                set.stitches.removeAll(stitchToRemove);
                if(set.stitches.isEmpty())
                    setsToRemove.add(set);
            }
            course.stitchSets.removeAll(setsToRemove);
            if(course.stitchSets.isEmpty())
                courseToRemove.add(course);
        }
        this.courses.removeAll(courseToRemove);
        int i=0;
        for(Course course : this) {
            course.lineID=i;
            i++;
            this.mapCourse(course);
        }

        for(Loop loop: yarn){
            if(loop.getChildOf().isEmpty() && loop.getParentOf().isEmpty()) {
                loop.removeLoopFromYarn();
                break;
            }
            assert this.childLoopToLineMap.containsKey(loop) || this.parentLoopToLineMap.containsKey(loop);
            for(Stitch stitch: loop.getChildOf()){
                assert this.stitchToLineMap.containsKey(stitch);
                assert this.stitchSetToLineMap.containsKey(stitch.owningStitchSet);
            }
        }
    }
    @NotNull
    @Contract(pure = true)
    public Course getLastLine(){return this.courses.getLast();}
    @NotNull
    @Contract(pure = true)
    public Course getFirstLine() {return this.courses.getFirst();}
    /**
     * @param index of the course requested
     * @return the course at the index
     */
    @NotNull
    @Contract(pure = true)
    public Course getLine(int index){
        assert this.lineIndex.containsKey(index);
        return this.lineIndex.get(index);
    }
    /**
     * @return the stitch set count of the first course, often the cast on
     */
    @Contract(pure = true)
    public int getFirstStitchSetCount(){return this.courses.getFirst().getStitchSetCount();}
    /**
     * @return the number of courses (not including cast on) in this textureGraph
     */
    @Contract(pure = true)
    public int getHeight(){return this.courses.size()-1;}
    /**
     * creates decreases on top course of textureGraph graph so that the outgoing loops is decreased
     * @param decreaseBy number of loops to decrease outgoing course to
     */
    public void decreaseTopEdge(int decreaseBy) {
        Course lastCourse = this.getLastLine();
        int stitchCount = lastCourse.stitchCount;
        HashSet<Single> decreasableSingleSet = new HashSet<>();
        Iterator<StitchSet> setIterator = lastCourse.iterator();
        StitchSet set = setIterator.next();
        //get all of the sets that can be decreased
        for (; setIterator.hasNext(); ) {
            StitchSet nextSet = setIterator.next();
            if(set instanceof Single && ((Single) set).mergableWithNextNeighbor()) {
                decreasableSingleSet.add((Single) set);
                if (setIterator.hasNext())
                    set = setIterator.next();
                else
                    set = null;
            }
            else
                set=nextSet;
        }
        //Build up an indexable list of the decreasables and null in positions where ther are none
        ArrayList<Single> decreasableSingles = new ArrayList<>(stitchCount);
        for(Stitch stitch: lastCourse.getStitches())
            //noinspection RedundantCast
            if (decreasableSingleSet.contains((Single)stitch.owningStitchSet))
                decreasableSingles.add((Single) stitch.owningStitchSet);
            else
                decreasableSingles.add(null);
        LinkedList<Single> bestSingles = new LinkedList<>();
        HashMap<Single, Integer> setToIndexMap = new HashMap<>();
        double exactSpacer = ((double)stitchCount)/((double)decreaseBy);
        int totalSpacer = (int) Math.ceil(exactSpacer);
        int remainingSpace = 0;
        while(bestSingles.size()<decreaseBy) {
            for (int i = 0; i < decreasableSingles.size(); i++) {
                if (decreasableSingles.get(i) != null && remainingSpace <= 0) {
                    bestSingles.add(decreasableSingles.get(i));
                    decreasableSingleSet.remove(decreasableSingles.get(i));
                    decreasableSingles.set(i, null);//use up this single
                    remainingSpace += totalSpacer;
                    if(bestSingles.size()>=decreaseBy)
                        break;
                }
                remainingSpace--;
            }
            if(decreasableSingleSet.isEmpty())
                break;
        }
        HashSet<Single> bestSinglesSet = new HashSet<>(bestSingles);
        setIterator= lastCourse.iterator();
        for(int i = 0; i< lastCourse.stitchSets.size(); i++){
            set = setIterator.next();
            //noinspection SuspiciousMethodCalls
            if(bestSinglesSet.contains(set))
                setToIndexMap.put((Single) set,i);
        }
        //remove bestSingles from Sets
        HashSet<Integer> easyRemoves = new HashSet<>();
        HashMap<Integer, Single> replacements = new HashMap<>();
        for(Single single: bestSingles){
            int singleIndex = setToIndexMap.get(single);
            easyRemoves.add(singleIndex);
            Single replacement = single.mergeToDecreaseWithNextNeighbor();
            replacements.put(singleIndex+1, replacement);
        }
        for(Integer index: replacements.keySet())
            lastCourse.stitchSets.set(index, replacements.get(index));
        for(Integer index: easyRemoves)
            lastCourse.stitchSets.set(index, null);
        lastCourse.stitchSets.removeIf(Objects::isNull);
        this.resetMaps();
        bestSingles.size();
    }
    /**
     * creates decreases on bottom course (not caston) of textureGraph graph so that textureGraph can accept more ingoing loops
     * @param increaseBy number of loops to increase incoming edge by
     */
    public void decreaseBottomEdge(int increaseBy) {
        CastOnCourse castOnLine = (CastOnCourse) this.getFirstLine();
        Course firstCourse = this.getLine(1);
        this.flattenLine(firstCourse);
        assert !(firstCourse instanceof CastOnCourse);
        LinkedList<Single> createDecreaseSets = new LinkedList<>();
        LinkedHashMap<Single, Integer> setIndices = new LinkedHashMap<>();
        LinkedHashMap<Single, CastOn> parentCastOns = new LinkedHashMap<>();
        LinkedHashMap<CastOn, Integer> castOnIndices = new LinkedHashMap<>();
        for(int i = 0; i< firstCourse.stitchSets.size(); i++){
            StitchSet set = firstCourse.stitchSets.get(i);
            if(set instanceof Single && set.getFirstStitch() instanceof DuplicatableStitch &&  ((Single) set).mergableWithNextNeighbor()){
                createDecreaseSets.add((Single) set);
                setIndices.put((Single) set, i);
                StitchSet parentStitchSet = set.getFirstStitch().getParentStitchSet();
                assert parentStitchSet instanceof Single;
                Single parentSet = (Single) parentStitchSet;
                parentCastOns.put((Single) set, (CastOn) parentSet.getFirstStitch());
                i++;//the next set will be merged with if this is selected
            }
        }
        Collection<CastOn> castOns = parentCastOns.values();
        for(int i=0; i<castOnLine.stitchSets.size(); i++){
            CastOn inLine = (CastOn) castOnLine.stitchSets.get(i).getFirstStitch();
            if(castOns.contains(inLine))
                castOnIndices.put(inLine, i);
        }
        //there are more increases than needed decreases so remove a few evenly spaced ones
        if(createDecreaseSets.size()>increaseBy)
            createDecreaseSets = this.getSpacedSingleSets(increaseBy, createDecreaseSets, setIndices);
        assert createDecreaseSets.size() <= increaseBy;
        for(Single decreaseStarterSet: createDecreaseSets){
            assert decreaseStarterSet.getFirstStitch().childLoop != null;
            int index = setIndices.get(decreaseStarterSet);
            int castonIndex = castOnIndices.get(parentCastOns.get(decreaseStarterSet));
            DuplicatableStitch decreaseStarter = (DuplicatableStitch) decreaseStarterSet.getFirstStitch();
            CastOn parentCaston = parentCastOns.get(decreaseStarterSet);
            Loop addCOChild = parentCaston.parentLoops.getFirst().insertPriorLoop(LoopDirection.BtF);
            CastOn addedCastOn = new CastOn(addCOChild.insertPriorLoop(LoopDirection.BtF), addCOChild);
            OneToOneStitch addedNeighbor = decreaseStarter.duplicateStitchNext();
            addedNeighbor.parentLoops.getFirst().removeLoopFromYarn();
            addedNeighbor.setParents(addedCastOn.childLoop);
            Single merged = decreaseStarterSet.mergeToDecreaseWithNextNeighbor();
            assert merged != null;
            firstCourse.stitchSets.set(index, merged);
            StitchSet nextSet = firstCourse.stitchSets.get(index + 1);
            LinkedList<Loop> nextChildren = nextSet.getChildLoops();
            if(nextChildren.isEmpty())
                firstCourse.stitchSets.remove(index+1);
            castOnLine.stitchSets.add(castonIndex, new Single(addedCastOn));
        }
        this.resetMaps();
    }
    /**
     * creates increases on top course of textureGraph graph so that the outgoing loops is increased
     * @param increaseBy number of loops to increase outgoing course to
     */
    public void increaseTopEdge(int increaseBy){
        assert this.getHeight()>=1;
        Course topCourse = this.getLastLine();
        LinkedList<Single> increasableSets = new LinkedList<>();
        LinkedHashMap<Single, Integer> setIndices = new LinkedHashMap<>();
        for(int i = 0; i< topCourse.stitchSets.size(); i++){
            StitchSet set = topCourse.stitchSets.get(i);
            if( set instanceof Single &&
                    (set.getFirstStitch() instanceof IncreasableStitch ||
                    (set.getFirstStitch() instanceof IncreasableDecrease))){
                increasableSets.add((Single) set);
                setIndices.put((Single) set, i );
            }
        }
        //there are more increases than needed decreases so remove a few evenly spaced ones
        if(increasableSets.size()>increaseBy)
            increasableSets = this.getSpacedSingleSets(increaseBy, increasableSets, setIndices);
        assert increasableSets.size() <= increaseBy;
        int indexAddition=0;
        for(Single increasableSet: increasableSets){
            int index = setIndices.get(increasableSet)+indexAddition;
            Stitch increasable = increasableSet.getFirstStitch();
            if(increasable instanceof IncreasableStitch){ //p->pfb, k->kfb
                StitchSet increased = ((IncreasableStitch) increasable).increaseStitch(2, true);
                topCourse.stitchSets.set(index, increased);
            } else{//must be increasable decrease
                StitchSet addition = ((IncreasableDecrease) increasable).increaseStitch();
                if(index+1> topCourse.stitchSets.size())
                    topCourse.stitchSets.addLast(addition);
                else
                    topCourse.stitchSets.add(index + 1, addition);
                indexAddition++;//to push forward all indices
            }
        }
    }

    /**
     * @param neededSets the number of sets that need to be returned
     * @param potentialSets all of the potential sets that can be used
     * @param setIndices a mapping of potential sets to their index in the course
     * @return an ordered set of Single stitch sets that are spaced out across the pline
     */
    @NotNull
    private LinkedList<Single> getSpacedSingleSets(int neededSets, @Nonnull LinkedList<Single> potentialSets, @Nonnull LinkedHashMap<Single, Integer> setIndices) {
        if(neededSets==1){
            LinkedList<Single> set = new LinkedList<>();
            set.add(potentialSets.pollFirst());
            return set;
        }
        TreeSet<Integer> acceptedIncreaseIndices = new TreeSet<>();
        TreeMap<Integer, Single> acceptedIncreaseByIndex = new TreeMap<>();
        Single firstIncrease = potentialSets.pollFirst();
        acceptedIncreaseIndices.add(setIndices.get(firstIncrease));
        acceptedIncreaseByIndex.put(setIndices.get(firstIncrease), firstIncrease);
        Single lastIncrease = potentialSets.pollLast();
        acceptedIncreaseIndices.add(setIndices.get(lastIncrease));
        acceptedIncreaseByIndex.put(setIndices.get(lastIncrease), lastIncrease);
        while(acceptedIncreaseIndices.size()< neededSets && !potentialSets.isEmpty()){
            TreeMap<Integer, Single> costsToSet = new TreeMap<>();
            for(Single potentialSet: potentialSets){
                int minDisLeft = Integer.MAX_VALUE;
                int minDisRight = Integer.MAX_VALUE;
                Integer pi = setIndices.get(potentialSet);
                for(int i: acceptedIncreaseIndices) {
                    if(i< pi) {
                        int disLeft = pi - i;
                        if (disLeft < minDisLeft)
                            minDisLeft = disLeft;
                    }else if(i>pi){
                        int disRight = i-pi;
                        if (disRight < minDisRight)
                            minDisRight = disRight;
                    }else
                        assert false;
                }
                int distance = Math.abs(minDisLeft-minDisRight);
                costsToSet.put(distance, potentialSet);
            }
            Single nextIncrease = costsToSet.get(costsToSet.firstKey());
            assert potentialSets.remove(nextIncrease);
            acceptedIncreaseIndices.add(setIndices.get(nextIncrease));
            acceptedIncreaseByIndex.put(setIndices.get(nextIncrease), nextIncrease);
        }
        potentialSets.clear();
        potentialSets.addAll(acceptedIncreaseByIndex.values());
        return potentialSets;
    }
    /**
     * creates increases on bottom course (Not cast on) of textureGraph graph so that textureGraph accepts fewer incoming loops
     * @param decreaseBy number of loops to decrease incoming edge to
     */
    public void increaseBottomEdge(int decreaseBy) {
        CastOnCourse castOnLine = (CastOnCourse) this.getFirstLine();
        Course firstCourse = this.getLine(1);
        this.flattenLine(firstCourse);
        assert !(firstCourse instanceof CastOnCourse);
        LinkedList<Single> mergeableSets = new LinkedList<>();
        LinkedHashMap<Single, Integer> setIndices = new LinkedHashMap<>();
        LinkedHashMap<Single, CastOn> parentCastOns = new LinkedHashMap<>();
        for(int i = 0; i< firstCourse.stitchSets.size(); i++){
            StitchSet set = firstCourse.stitchSets.get(i);
            if(set instanceof Single && ((Single) set).mergableWithNextNeighbor()){
                mergeableSets.add((Single) set);
                setIndices.put((Single) set, i);
                Single parentSet = (Single) set.getFirstStitch().getParentStitchSet();
                assert parentSet != null;
                parentCastOns.put((Single) set, (CastOn) parentSet.getFirstStitch());
                i++;//because next one will be merged with this
            }
        }
        if(mergeableSets.size()>decreaseBy)//there are more increases than needed decreases so remove a few evenly spaced ones
            mergeableSets = this.getSpacedSingleSets(decreaseBy, mergeableSets, setIndices);
        assert mergeableSets.size() <= decreaseBy;
        int indexModifier = 0;
        for(Single mergeStarter: mergeableSets){
            int index = setIndices.get(mergeStarter)+indexModifier;
            CastOn parentCaston = parentCastOns.get(mergeStarter);
            StitchSet nextSet = firstCourse.stitchSets.get(index + 1);
            if(nextSet instanceof FlattenableSet){
                LinkedList<Single> newSets;
                newSets = firstCourse.flattenSet((FlattenableSet) nextSet);
                indexModifier+= newSets.size()-1;
            }
            StitchSet mergedSet = mergeStarter.mergeToIncreaseWithNextNeighbor();
            firstCourse.stitchSets.set(index, mergedSet);
            assert  parentCaston.childLoop == null;
            parentCaston.parentLoops.getFirst().removeLoopFromYarn();
        }
        firstCourse.stitchSets.removeIf((StitchSet set)->set.getFirstStitch().owningStitchSet == null);
        castOnLine.stitchSets.removeIf((StitchSet set)->set.getFirstStitch().childLoop == null);
    }

    /**
     * flattens all of the cables/twists in the course so that they are made of single stitch sets and don't cross
     * @param course the course to be flattened
     */
    private void flattenLine(@NotNull Course course) {
        int stitchIndex=0;
        LinkedList<Integer> indicesToFlatten = new LinkedList<>();
        for(StitchSet set: course) {
            if(set instanceof FlattenableSet) {
                indicesToFlatten.addLast(stitchIndex);
            }
            stitchIndex++;
        }
        for (Iterator<Integer> it = indicesToFlatten.descendingIterator(); it.hasNext(); ) {
            int index = it.next();
            course.flattenSet((FlattenableSet) course.stitchSets.get(index));
        }
        this.mapCourse(course);
    }

    @SuppressWarnings("unused")
    private void unFlattenLine(@NotNull Course course) {
        int stitchIndex=0;
        LinkedList<Integer> indicesToUnFlatten = new LinkedList<>();
        for(StitchSet set: course) {
            if(set instanceof FlattenableSet && ((FlattenableSet)set).isFlattened())
                indicesToUnFlatten.addLast(stitchIndex);
            stitchIndex++;
        }
        for (Iterator<Integer> it = indicesToUnFlatten.descendingIterator(); it.hasNext(); ) {
            int index = it.next();
            course.unFlattenSet((FlattenableSet) course.stitchSets.get(index));
        }
        this.mapCourse(course);
    }

    /**
     * Widens the graph by adding collumns of knits to either side
     * @param widenBy the number of stitches to add per row
     * @param widenLine the course to start widening from.
     */
    public void widenWithSTST(int widenBy, int widenLine){
        if(widenBy<0)
            return;
        if(widenBy==1) {
            this.widenWithSingleSTSTPath(widenLine);
            return;
        }
        int widenRight = widenBy/2;
        ArrayList<Loop> newParents = new ArrayList<>(widenBy);
        if((widenLine==0 ||widenLine==1) && this.getFirstLine() instanceof CastOnCourse) {
            CastOnCourse castOnLine = (CastOnCourse) this.getFirstLine();
            int castOnRight = widenBy-widenRight;
            for(int l=castOnRight; l<widenBy; l++){
                Loop parentLoop = castOnLine.stitchSets.getLast().getFirstStitch().childLoop.insertNextLoop(LoopDirection.BtF);
                Loop childLoop = parentLoop.insertNextLoop(LoopDirection.BtF);
                castOnLine.stitchSets.addLast(new Single(new CastOn(parentLoop, childLoop)));
                newParents.add(childLoop);
            }
            for(int r=0; r<castOnRight; r++){
                Loop childLoop = castOnLine.stitchSets.getFirst().getFirstStitch().parentLoops.getFirst().insertPriorLoop(LoopDirection.BtF);
                Loop parentLoop = childLoop.insertPriorLoop(LoopDirection.BtF);
                castOnLine.stitchSets.addFirst(new Single(new CastOn(parentLoop, childLoop)));
                newParents.add(childLoop);
            }
        }
        else if(widenLine>0){//adds an increase on both edges. Note this should be used sparingly because it makes big increases
            Course increaseCourse = this.courses.get(widenLine-1);
            if(increaseCourse.stitchSets.getFirst() instanceof FlattenableSet)//flatten edge sets to get a single stitch set on edge for increasing
                increaseCourse.flattenSet((FlattenableSet) increaseCourse.stitchSets.getFirst());
            if(increaseCourse.stitchSets.getLast() instanceof FlattenableSet)
                increaseCourse.flattenSet((FlattenableSet) increaseCourse.stitchSets.getLast());
            assert increaseCourse.stitchSets.getFirst().getFirstStitch() instanceof IncreasableStitch;
            assert increaseCourse.stitchSets.getLast().getLastStitch() instanceof IncreasableStitch;
            Stitch firstStitch = increaseCourse.stitchSets.getFirst().getFirstStitch();
            Stitch lastStitch = increaseCourse.stitchSets.getLast().getLastStitch();
            if(increaseCourse.isStandardRightSide()) {
                Increase increaseRight = increaseCourse.increaseSet(((IncreasableStitch) firstStitch), widenRight+1, false);
                Increase increaseLeft = increaseCourse.increaseSet(((IncreasableStitch) lastStitch), (widenBy-widenRight)+1, true);
                for (Stitch newStitch : increaseRight.stitches)
                    if (newStitch.childLoop != firstStitch.childLoop)
                        newParents.add(0,newStitch.childLoop);
                for (Stitch newStitch : increaseLeft.stitches)
                    if (newStitch.childLoop != lastStitch.childLoop)
                        newParents.add(widenRight,newStitch.childLoop);
            } else{//wrong side
                Increase increaseLeft = increaseCourse.increaseSet(((IncreasableStitch) firstStitch), (widenBy-widenRight)+1, false);
                Increase increaseRight = increaseCourse.increaseSet(((IncreasableStitch) lastStitch), widenRight+1, true);
                for (Stitch newStitch : increaseRight.stitches)
                    if (newStitch.childLoop != lastStitch.childLoop)
                        newParents.add(newStitch.childLoop);
                for (Stitch newStitch : increaseLeft.stitches)
                    if (newStitch.childLoop != firstStitch.childLoop)
                        newParents.add(widenRight, newStitch.childLoop);
            }
        }
        for (Iterator<Course> it = this.iterator(widenLine); it.hasNext(); ) {
            Course course = it.next();
            if(course instanceof CastOnCourse)
                continue;
            if(course.isStandardRightSide()){
                for(int r=0; r<widenRight; r++){
                    Loop childLoop = course.stitchSets.getFirst().getFirstStitch().childLoop.insertPriorLoop(LoopDirection.BtF);
                    Loop parent = newParents.get(r);
                    course.stitchSets.addFirst(new Single(new K(parent, childLoop)));
                    newParents.set(r, childLoop);
                }
                for(int l=widenRight; l<widenBy; l++){
                    Loop childLoop = course.stitchSets.getLast().stitches.getLast().childLoop.insertNextLoop(LoopDirection.BtF);
                    Loop parent = newParents.get(l);
                    course.stitchSets.addLast(new Single(new K(parent, childLoop)));
                    newParents.set(l, childLoop);
                }
            }
            else{
                for(int l= widenRight; l<widenBy; l++){
                    Loop childLoop = course.stitchSets.getFirst().getFirstStitch().childLoop.insertPriorLoop(LoopDirection.BtF);
                    course.stitchSets.addFirst(new Single(new K(newParents.get(l), childLoop)));
                    newParents.set(l, childLoop);
                }
                for(int r=0; r<widenRight; r++){
                    Loop childLoop = course.stitchSets.getLast().stitches.getLast().childLoop.insertNextLoop(LoopDirection.BtF);
                    course.stitchSets.addLast(new Single(new K(newParents.get(r), childLoop)));
                    newParents.set(r, childLoop);
                }
            }

        }
        this.resetMaps();

    }

    /**
     * Widens the graph by adding one collumn of knits
     * @param widenLine the course to start widening from
     */
    private void widenWithSingleSTSTPath(int widenLine){
        Course firstWideCourse = this.getLine(widenLine);
        Loop newParent=null;
        if(firstWideCourse instanceof CastOnCourse){
            CastOnCourse castOnLine = (CastOnCourse) firstWideCourse;
            Loop parentLoop = castOnLine.stitchSets.getLast().getFirstStitch().childLoop.insertNextLoop(LoopDirection.BtF);
            Loop childLoop = parentLoop.insertNextLoop(LoopDirection.BtF);
            castOnLine.stitchSets.addLast(new Single(new CastOn(parentLoop, childLoop)));
            newParent=childLoop;
        }else{
            Course increaseCourse = this.courses.get(widenLine-1);
            if(firstWideCourse.isStandardRightSide()) {
                if (increaseCourse.stitchSets.getLast() instanceof FlattenableSet)
                    increaseCourse.flattenSet((FlattenableSet) increaseCourse.stitchSets.getLast());
                assert increaseCourse.stitchSets.getLast().getLastStitch() instanceof IncreasableStitch;
                Stitch lastStitch = increaseCourse.stitchSets.getLast().getLastStitch();
                Increase increase = increaseCourse.increaseSet((IncreasableStitch) lastStitch,1,true);
                for(Stitch newStitch: increase.stitches)
                    if(newStitch.childLoop != lastStitch.childLoop)
                        newParent = newStitch.childLoop;
                assert newParent != null;
            }else{
                if (increaseCourse.stitchSets.getFirst() instanceof FlattenableSet)
                    increaseCourse.flattenSet((FlattenableSet) increaseCourse.stitchSets.getFirst());
                assert increaseCourse.stitchSets.getFirst().getFirstStitch() instanceof IncreasableStitch;
                Stitch firstStitch = increaseCourse.stitchSets.getLast().getLastStitch();
                Increase increase = increaseCourse.increaseSet((IncreasableStitch) firstStitch,1,false);
                for(Stitch newStitch: increase.stitches)
                    if(newStitch.childLoop != firstStitch.childLoop)
                        newParent = newStitch.childLoop;
                assert newParent != null;
            }
        }
        for (Iterator<Course> it = this.iterator(widenLine); it.hasNext(); ) {
            Course course = it.next();
            if(course instanceof CastOnCourse)
                continue;
            Loop childLoop;
            if(course.isStandardRightSide()){
                childLoop = course.stitchSets.getFirst().getFirstStitch().childLoop.insertPriorLoop(LoopDirection.BtF);
                course.stitchSets.addFirst(new Single(new K(newParent, childLoop)));
            }
            else{
                childLoop = course.stitchSets.getLast().getLastStitch().childLoop.insertNextLoop(LoopDirection.BtF);
                course.stitchSets.addLast(new Single(new K(newParent, childLoop)));
            }
            newParent = childLoop;
        }
        this.resetMaps();
    }

    /**
     * removes the last course from the graph
     * @param clear flags if the loops on this course should be deleted
     */
    private void removeLastLine(boolean clear){
        this.courses.removeLast();
        if(clear)
            for (StitchSet set : this.getLastLine())
                for (Stitch stitch : set)
                    stitch.childLoop.clearParentsOf();
    }

    /**
     * removes the last set of courses from the textureGraph to shrink it
     * @param courses the number of courses to remove
     * @param clear true if those courses should be cleared from the memory
     */
    public void removeXCourses(int courses, boolean clear) {
        if(clear) {
            try {
                this.shortenTextureGraph(courses);
            }catch (NoTextureCarvesAvailableException e){
                for(int i=courses; i>1; i--)
                    this.removeLastLine(false);
                this.removeLastLine(clear);
            }
        }else {
            for (int i = courses; i > 1; i--)
                this.removeLastLine(false);
            this.removeLastLine(clear);
        }
    }

    /**
     * @return an iterator that works from the first course (Caston) to last course
     */
    @Override
    @NotNull
    public Iterator<Course> iterator(){ return new LineIterator();}
    public Iterator<Course> iterator(int index){ return new LineIterator(index);}
    private class LineIterator implements  Iterator<Course>{
        ListIterator<Course> listIterator;
        LineIterator(){listIterator = courses.listIterator(0);}
        LineIterator(int index){listIterator = courses.listIterator(index);}
        @Override
        public boolean hasNext(){return listIterator.hasNext();}
        @Override
        public Course next(){return listIterator.next();}
        public void remove(){listIterator.remove();}
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof TextureGraph){
            TextureGraph g = (TextureGraph)o;
            if(g.getHeight() != this.getHeight())
                return false;
            Iterator<Course> thisIter= this.iterator();
            Iterator<Course> gIter = g.iterator();
            while(thisIter.hasNext() && gIter.hasNext()){
                if(!thisIter.next().equals(gIter.next()))
                    return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(@NotNull TextureGraph o) {
        int compare = TextureGraph.heightComparator.compare(this,o);
        if(compare == 0)
            return TextureGraph.widthComparator.compare(this,o);
        return compare;
    }
    @Override
    public String toString(){
        return this.getHeight()+": "+this.getFirstLine().stitchCount+"->"+this.getLastLine().stitchCount;
    }

    private static Comparator<TextureGraph> heightComparator = Comparator.comparingInt(TextureGraph::getHeight);
    private static Comparator<TextureGraph> widthComparator = Comparator.comparingInt(o -> o.stitchCount);
}