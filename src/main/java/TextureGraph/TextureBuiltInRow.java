package TextureGraph;

import TextureGraph.Courses.CastOnCourse;
import TextureGraph.Courses.Course;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Loops.Yarn;
import TextureGraph.Courses.Stitches.Stitch;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * Created by Megan on 12/6/2016.
 *
 * for textures when the courses meet in rows
 */
public class TextureBuiltInRow extends TextureGraph {

    public TextureBuiltInRow(@NotNull LinkedList<Course> courses){super(courses);}

    public TextureBuiltInRow(@Nonnull TextureBuiltInRow copy, @Nonnull Yarn yarn){
        super(copy, yarn);
    }

    @NotNull
    @Override
    public TextureGraph switchRowRounds(@NotNull Yarn yarn){
        LinkedList<Course> newCourses = new LinkedList<>();
        newCourses.add(new CastOnCourse(this.getFirstStitchSetCount(), yarn));
        LinkedList<Loop> parents = new LinkedList<>();
        for(int l = 1; l<this.courses.size(); l++){
            Course lastCourse = newCourses.getLast();
            for(StitchSet set: lastCourse)
                for(Stitch stitch: set)
                    parents.add(stitch.childLoop);
            Course thisCourse = this.getLine(l);
            Iterator<StitchSet> setIterator;
            if(thisCourse.isStandardRightSide())
                setIterator = thisCourse.stitchSets.iterator();
            else
                setIterator = thisCourse.stitchSets.descendingIterator();
            LinkedList<StitchSet> newSets = new LinkedList<>();
            for (Iterator<StitchSet> it = setIterator; it.hasNext(); ) {
                StitchSet set = it.next();
                newSets.add(set.copyFromParents(yarn,parents));
            }
            Course newCourse = new Course(newSets, l);
            newCourses.add(newCourse);
            newCourse.turnAtEndOfLine();
        }
        return new TextureBuiltInRound(newCourses);
    }
}
