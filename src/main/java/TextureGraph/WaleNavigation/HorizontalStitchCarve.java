package TextureGraph.WaleNavigation;

import CompilerExceptions.StitchManipulationExceptions.NoTextureCarvesAvailableException;
import TextureGraph.Courses.Course;
import TextureGraph.Courses.StitchSets.Cables.Cable;
import TextureGraph.Courses.StitchSets.StitchSet;
import TextureGraph.Courses.Stitches.Loops.TurnSection;
import TextureGraph.Courses.Stitches.Loops.YarnSection;
import TextureGraph.Courses.Stitches.Stitch;
import TextureGraph.TextureGraph;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class HorizontalStitchCarve implements Comparable<HorizontalStitchCarve>, Iterable<Stitch>{
    /**
     * The cost of moving diagonally from this stitch set
     */
    public static double HORIZONTAL_DIAGONAL_COST = 1.0;//todo set
    public static double HORIZONTAL_DISSIMILARITY_COST = 5.0;//todo set
    public static int NEIGHBOR_DROP_OFF = 4;//todo set
    public LinkedList<Stitch> carve = new LinkedList<>();
    public HashSet<Stitch> stitches = new HashSet<>();

    private boolean allInCourse = true;
    public Course getCourse(){
        if(this.allInCourse)
            return this.carve.getFirst().owningStitchSet.owningCourse;
        else
            return null;
    }

    public TextureGraph getTextureGraph(){
        return this.carve.getFirst().owningStitchSet.owningCourse.owningTextureGraph;
    }

    public HorizontalStitchCarve( Stitch startingStitch){
        this.extendByStitch(startingStitch);
    }

    private void extendByStitch(@NotNull Stitch stitch) {
        if(! stitch.horizontalPathCalculated)
            stitch.setDynamicWaleRemovalCost();
        if(!stitch.horizontalPathCalculated || stitch.pathToRemoveFromWaleCost == Double.MAX_VALUE ||this.stitches.contains(stitch))
            return;
        this.addStitch(stitch);
        if(stitch.pathMinimumNeighbor != null) {
            if(stitch.pathMinimumNeighbor.owningStitchSet instanceof Cable){
                LinkedList<Stitch> cable = stitch.pathMinimumNeighbor.owningStitchSet.stitches;
                assert !cable.isEmpty();
                for(Stitch cableStitch: cable) {
                    assert cableStitch != null;
                    if (!this.stitches.contains(cableStitch)) {
                        this.stitches.add(cableStitch);
                        this.carve.add(cableStitch);
                    }
                }
                if(stitch.pathMinimumNeighbor.owningStitchSet.owningCourse.isStandardRightSide() && cable.getFirst().pathMinimumNeighbor != null)
                    this.extendByStitch(cable.getFirst().pathMinimumNeighbor);
                else if(cable.getLast().pathMinimumNeighbor != null)
                        this.extendByStitch(cable.getLast().pathMinimumNeighbor);
            }else
                this.extendByStitch(stitch.pathMinimumNeighbor);
        }
    }

    private void addStitch(@NotNull Stitch stitch) {
        Stitch prior = null;
        if(!this.carve.isEmpty())
            prior = this.carve.getLast();
        this.carve.add(stitch);
        this.stitches.add(stitch);
        if(this.allInCourse && prior != null && prior.owningStitchSet.owningCourse.getCourseID() != stitch.owningStitchSet.owningCourse.getCourseID())
            this.allInCourse = false;
    }

    /**
     * @return the cost of reducing this whole path
     */
    @Contract(pure = true)
    private double getPathCost(){
        return carve.getFirst().pathToRemoveFromWaleCost;
    }

    /**
     * @param others the other stitches that could be carved out
     * @return true if this path does not intersect any of the other stitch sets
     */
    @Contract(pure = true)
    public boolean pathIsExclusive(@NotNull HashSet<Stitch> others){
        for(Stitch thisStitch: this)
            if(others.contains(thisStitch))
                return false;
        return true;
    }

    @Override
    public int compareTo(@NotNull HorizontalStitchCarve o) {
        int cmp = Double.compare(this.getPathCost(), o.getPathCost());
        if(cmp ==0)
            return Integer.compare(o.carve.getFirst().childLoop.hashCode(), this.carve.getFirst().childLoop.hashCode());
        return cmp;
    }

    @NotNull
    @Override
    public Iterator<Stitch> iterator() {
        return this.carve.iterator();
    }

    @Override
    public String toString(){
        StringBuilder str = new StringBuilder(String.format("%1$,.2f", this.getPathCost()) + ": ");
        for(Stitch set: this)
            str.append(set.toString()).append(", ");
        return str.toString();
    }

    public void disconnectStitches() throws NoTextureCarvesAvailableException {
        if(this.allInCourse){
            Course emptyCourse =  this.carve.getFirst().owningStitchSet.owningCourse;
            int emptyID = emptyCourse.getCourseID();
            assert emptyCourse.getCourseID()>0;
            Course belowCourse = emptyCourse.owningTextureGraph.courses.get(emptyID-1);
            if(emptyID<emptyCourse.owningTextureGraph.courses.size()-1) {
                Course aboveCourse = emptyCourse.owningTextureGraph.courses.get(emptyID + 1);
                Iterator<Stitch> aboveIter = aboveCourse.getStitches().iterator();
                Iterator<Stitch> belowIter = belowCourse.getStitches().descendingIterator();
                for (; aboveIter.hasNext(); ) {
                    Stitch aboveStitch = aboveIter.next();
                    int parentLoops = aboveStitch.parentLoops.size();
                    aboveStitch.parentLoops.clear();
                    for (int i = 0; i < parentLoops; i++) {
                        assert belowIter.hasNext();
                        Stitch belowStitch = belowIter.next();
                        belowStitch.childLoop.clearParentsOf();
                        belowStitch.childLoop.makeParentOf(aboveStitch);
                        aboveStitch.parentLoops.add(belowStitch.childLoop);
                    }
                }
            }
            HashSet<YarnSection> turnToRemove = new HashSet<>();
            for(StitchSet set: emptyCourse){
                for(Stitch stitch: set){
                    YarnSection nextSection = stitch.childLoop.yarnSection.getNextSection();
                    if(nextSection instanceof TurnSection)
                        turnToRemove.add(nextSection);
                    stitch.childLoop.removeChildOf(stitch);
                    stitch.childLoop.clearParentsOf();
                    stitch.childLoop.removeLoopFromYarn();
                    stitch.childLoop = null;
                    stitch.parentLoops.clear();
                }
            }
            this.getTextureGraph().getYarn().sections.removeAll(turnToRemove);
            emptyCourse.owningTextureGraph.courses.remove(emptyID);
            int i=emptyID;
            for (ListIterator<Course> it = emptyCourse.owningTextureGraph.courses.listIterator(Math.max(0,emptyID - NEIGHBOR_DROP_OFF)); it.hasNext(); ) {
                Course course = it.next();
                double courseDistance = Math.abs(course.getCourseID()-emptyID);
                if(courseDistance>NEIGHBOR_DROP_OFF)
                    break;
                for(StitchSet set: course)
                    for(Stitch stitch: set)
                        stitch.neighborReduceHeightCost += courseDistance/((double)NEIGHBOR_DROP_OFF);

            }
            for (ListIterator<Course> it1 = emptyCourse.owningTextureGraph.courses.listIterator(emptyID); it1.hasNext(); ) {
                Course course = it1.next();
                course.lineID = i;
                i++;
            }
        }
        else {
            for (Stitch stitch : this)
                stitch.disconnectFromWale(this.allInCourse);
        }
    }
}
