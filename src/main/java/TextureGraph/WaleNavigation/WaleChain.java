package TextureGraph.WaleNavigation;

import TextureGraph.Courses.Stitches.Stitch;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class WaleChain implements Iterable<Wale>{
    private LinkedList<Wale> chain = new LinkedList<>();
    private HashSet<Wale> wales= new HashSet<>();

    public WaleChain(Wale start, HashSet<Wale> exclude){
        assert !exclude.contains(start);
        chain.add(start);
        wales.add(start);
        TreeSet<Wale> nextConsumers = start.consumingWales;
        while(!nextConsumers.isEmpty()){
            for(Wale candidateConsumer: nextConsumers){
                if(!exclude.contains(candidateConsumer)){
                    chain.add(candidateConsumer);
                    wales.add(candidateConsumer);
                    nextConsumers = candidateConsumer.consumingWales;
                    break;
                }
            }
        }
    }
    public WaleChain(Wale start){
        this(start, new HashSet<Wale>());
    }
    public WaleChain(Wale start, Collection<WaleChain> excludes){
        this(start, buildExclusions(excludes));
    }


    private static HashSet<Wale> buildExclusions(Collection<WaleChain> excludes){
        HashSet<Wale> exclude = new HashSet<>();
        for(WaleChain chain: excludes)
            exclude.addAll(chain.wales);
        return exclude;
    }

    public String printChain(){
        Iterator<Wale> waleIterator = this.iterator();
        StringBuilder str = new StringBuilder(String.format("(%s)", waleIterator.next()));
        for (; waleIterator.hasNext(); ) {
            str.append(String.format("-->(%s", waleIterator.next()));
        }
        return str.toString();
    }

    public String toString(){
        return String.format("(%s)-...->(%s)",this.chain.getFirst().toString(), this.chain.getLast().toString());
    }

    @NotNull
    @Override
    public Iterator<Wale> iterator() {
        return this.chain.iterator();
    }

    public ListIterator<Wale> listIterator(Wale wale){
        int indexOfWale  = this.chain.indexOf(wale);
        return this.chain.listIterator(indexOfWale);
    }

    public Iterator<Stitch> stitchIterator(){return new StitchIterator();}

    private class StitchIterator implements Iterator<Stitch>{
        Iterator<Wale> waleIterator = iterator();
        Iterator<Stitch> stitchIterator = null;
        @Override
        public boolean hasNext() {//todo: assumes wales have at least one stitch
            if(stitchIterator == null || !stitchIterator.hasNext()){
                if(!waleIterator.hasNext())
                    return false;
                stitchIterator = waleIterator.next().iterator();
            }
            return stitchIterator.hasNext();
        }

        @Override
        public Stitch next() {
            assert stitchIterator != null;
            return stitchIterator.next();
        }
    }
}
