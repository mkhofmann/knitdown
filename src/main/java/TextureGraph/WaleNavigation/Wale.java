package TextureGraph.WaleNavigation;

import TextureGraph.Courses.Course;
import TextureGraph.Courses.Stitches.Loops.Loop;
import TextureGraph.Courses.Stitches.Stitch;
import TextureGraph.Courses.Stitches.YO;
import TextureGraph.TextureGraph;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class Wale implements Iterable<Stitch>, Comparable<Wale>{
    TextureGraph owningTextureGraph;
    public LinkedList<Stitch> stitches= new LinkedList<>();

    private HashSet<Stitch> stitchesAsSet = new HashSet<>();
    public boolean hasStitch(Stitch stitch){
        return this.stitchesAsSet.contains(stitch);
    }
    public int getStitchCount(){return this.stitches.size();}
    private LinkedHashMap<Course, Stitch> courseToStitchMap = new LinkedHashMap<>();
    private TreeSet<Wale> feedingWales= new TreeSet<>(LeftMost);
    TreeSet<Wale> consumingWales = new TreeSet<>(LeftMost);
    /**
     * Maps the type of stitches to their number of occurrances in the course
     */
    public LinkedHashMap<Class<? extends Stitch>, Integer> stitchTypeToCountMap = new LinkedHashMap<>();

    private Wale(Stitch start, Wale feedingWale){
        this(start);
        this.feedingWales.add(feedingWale);
    }

    private Wale(Stitch start){
        owningTextureGraph = start.owningStitchSet.owningCourse.owningTextureGraph;
        if(!this.owningTextureGraph.stitchesToWale.containsKey(start))
            this.extendByStitch(start);
    }

    @Nullable
    public WaleChain getOwningChain(){
        return this.owningTextureGraph.waleToWaleChains.get(this);
    }

    public static LinkedHashSet<Wale> buildWales(Stitch startStitch){
        LinkedHashSet<Wale> wales = new LinkedHashSet<>();
        Wale firstWale = new Wale(startStitch);
        if(firstWale.stitches.isEmpty())
            return wales;
        for(Wale wale: firstWale.breadthFirstFeederList())
            if(!wale.stitches.isEmpty())
                wales.add(wale);
        for(Wale wale: firstWale.breadthFirstConsumerList())
            if(!wale.stitches.isEmpty())
                wales.add(wale);
        return wales;
    }

    private void extendByStitch(Stitch start) {
        this.addStitch(start);
        LinkedHashSet<Stitch> nextStitches = start.childLoop.getParentOf();
        LinkedHashSet<YO> yos = new LinkedHashSet<>();
        HashSet<Stitch> nextToRemove = new HashSet<>();
        for(Stitch next: nextStitches){
            assert next != null;
            if(next.childLoop == null) {
                nextToRemove.add(next);
                continue;
            }
            Loop priorLoop = next.childLoop.getPriorNeighbor();
            if (priorLoop != null) {
                Stitch priorNeighbor = priorLoop.getLastChildOf();
                if (priorNeighbor instanceof YO)
                    yos.add((YO) priorNeighbor);
            }
            Loop nextLoop = next.childLoop.getNextNeighbor();
            if (nextLoop != null) {
                Stitch nextNeighbor = nextLoop.getFirstChildOf();
                if (nextNeighbor instanceof YO)
                    yos.add((YO) nextNeighbor);
            }
        }
        for(Stitch toRemove: nextToRemove)
            start.childLoop.removeParentOf(toRemove);
        nextStitches.removeAll(nextToRemove);
        nextStitches.addAll(yos);
        if(nextStitches.size() == 1)
            this.extendByStitch(nextStitches.iterator().next());
        else if(nextStitches.size() > 1)
            for (Stitch next : nextStitches) {
                Wale newWale = new Wale(next, this);
                if(!newWale.stitches.isEmpty())
                    this.consumingWales.add(newWale);
            }

    }

    private void addStitch(Stitch stitch) {
        assert stitch.childLoop != null;
        this.owningTextureGraph.stitchesToWale.put(stitch, this);
        this.stitches.add(stitch);
        this.stitchesAsSet.add(stitch);
        Course owningCourse = stitch.owningStitchSet.owningCourse;
        if(this.courseToStitchMap.containsKey(owningCourse))
            assert !this.courseToStitchMap.containsKey(owningCourse);
        this.courseToStitchMap.put(owningCourse, stitch);
        if(!this.stitchTypeToCountMap.containsKey(stitch.getClass()))
            this.stitchTypeToCountMap.put(stitch.getClass(), 1);
        this.stitchTypeToCountMap.put(stitch.getClass(), this.stitchTypeToCountMap.get(stitch.getClass())+1);
    }


    @NotNull
    @Override
    public Iterator<Stitch> iterator() {
        return this.stitches.iterator();
    }
    @Nullable
    public ListIterator<Stitch> iterator(Stitch stitch){
        if(!this.hasStitch(stitch))
            return null;
        int stitchIndex = this.stitches.indexOf(stitch);
        return this.stitches.listIterator(stitchIndex);
    }

    @SuppressWarnings("unused")
    private Iterator<Stitch> descendingIterator(){
        return this.stitches.descendingIterator();
    }
    @Override
    public int hashCode(){
        return this.stitches.getFirst().hashCode();
    }
    @Override
    public boolean equals(Object o){
        if( !(o instanceof Wale))
            return false;
        Wale other = (Wale)o;
        if(this.stitches.size() != other.stitches.size())
            return false;
        Iterator<Stitch> otherStitchIter = other.iterator();
        for (Stitch thisStitch : this) {
            Stitch otherStitch = otherStitchIter.next();
            if (!thisStitch.equals(otherStitch))
                return false;
        }
        return true;
    }



    private Iterator<Wale> breadthFirstConsumerWaleIterator(){return new BreadthFirstConsumerWaleIterator(this);}
    private static Iterator<Wale> breadthFirstConsumerWaleIterator(Collection<Wale> startWales){return new BreadthFirstConsumerWaleIterator(startWales);}
    private LinkedList<Wale> breadthFirstConsumerList(){
        LinkedList<Wale> wales = new LinkedList<>();
        for (Iterator<Wale> it = this.breadthFirstConsumerWaleIterator(); it.hasNext(); )
            wales.add(it.next());
        return wales;
    }
    @SuppressWarnings("unused")
    public static LinkedList<Wale> breadthFirstConsumerList(Collection<Wale> startWales){
        LinkedList<Wale> wales = new LinkedList<>();
        for (Iterator<Wale> it = breadthFirstConsumerWaleIterator(startWales); it.hasNext(); )
            wales.add(it.next());
        return wales;
    }
    private Iterator<Wale> depthFirstConsumerWaleIterator(){return new DepthFirstConsumerWaleIterator(this);}
    private static Iterator<Wale> depthFirstConsumerWaleIterator(Collection<Wale> startWales){return new DepthFirstConsumerWaleIterator(startWales);}
    @SuppressWarnings("unused")
    public LinkedList<Wale> depthFirstConsumerList(){
        LinkedList<Wale> wales = new LinkedList<>();
        for (Iterator<Wale> it = this.depthFirstConsumerWaleIterator(); it.hasNext(); )
            wales.add(it.next());
        return wales;
    }
    @SuppressWarnings("unused")
    public static LinkedList<Wale> depthFirstConsumerList(Collection<Wale> startWales){
        LinkedList<Wale> wales = new LinkedList<>();
        for (Iterator<Wale> it = depthFirstConsumerWaleIterator(startWales); it.hasNext(); )
            wales.add(it.next());
        return wales;
    }

    @Override
    public int compareTo(@NotNull Wale o) {
        return this.stitches.getFirst().compareTo(o.stitches.getFirst());
    }

    @SuppressWarnings("WeakerAccess")
    public static Comparator<Wale> LeftMost = (o1, o2) -> Stitch.LeftMost.compare(o1.stitches.getFirst(), o2.stitches.getFirst());
    @SuppressWarnings("unused")
    public static Comparator<Wale> RightMost = (o1, o2) -> Stitch.RightMost.compare(o1.stitches.getFirst(), o2.stitches.getFirst());

    private static class BreadthFirstConsumerWaleIterator implements Iterator<Wale>{
        LinkedList<Wale> queue = new LinkedList<>();
        HashSet<Wale> visited = new HashSet<>();
        BreadthFirstConsumerWaleIterator(Wale startWale){
            queue.add(startWale);
        }
        BreadthFirstConsumerWaleIterator(Collection<Wale> startWales){
            queue.addAll(startWales);
        }
        @Override
        public boolean hasNext() {
            return !queue.isEmpty();
        }

        @Override
        public Wale next() {
            Wale first = queue.removeFirst();
            visited.add(first);
            for(Wale consumer: first.consumingWales)
                if(!visited.contains(consumer))
                    queue.add(consumer);
            return first;
        }
    }

    public static class DepthFirstConsumerWaleIterator implements  Iterator<Wale>{
        LinkedList<Wale> stack = new LinkedList<>();
        HashSet<Wale> visited = new HashSet<>();
        DepthFirstConsumerWaleIterator(Wale start){
            stack.add(start);
        }
        DepthFirstConsumerWaleIterator(Collection<Wale> startWales){
            stack.addAll(startWales);
        }
        @Override
        public boolean hasNext() {
            return !stack.isEmpty();
        }

        @Override
        public Wale next() {
            Wale last = stack.pop();
            visited.add(last);
            for(Wale consumer: last.consumingWales.descendingSet())
                if(!visited.contains(consumer))
                    stack.addFirst(consumer);
            return last;
        }
    }

    private static class BreadthFirstFeederWaleIterator implements Iterator<Wale>{
        LinkedList<Wale> queue = new LinkedList<>();
        HashSet<Wale> visited = new HashSet<>();
        BreadthFirstFeederWaleIterator(Wale startWale){
            queue.add(startWale);
        }
        BreadthFirstFeederWaleIterator(Collection<Wale> startWales){
            queue.addAll(startWales);
        }
        @Override
        public boolean hasNext() {
            return !queue.isEmpty();
        }

        @Override
        public Wale next() {
            Wale first = queue.removeFirst();
            visited.add(first);
            for(Wale feeder: first.feedingWales)
                if(!visited.contains(feeder))
                    queue.add(feeder);
            return first;
        }
    }
    private Iterator<Wale> breadthFirstFeederWaleIterator(){return new BreadthFirstFeederWaleIterator(this);}
    private static Iterator<Wale> breadthFirstFeederWaleIterator(Collection<Wale> startWales){return new BreadthFirstFeederWaleIterator(startWales);}
    private LinkedList<Wale> breadthFirstFeederList(){
        LinkedList<Wale> wales = new LinkedList<>();
        for (Iterator<Wale> it = this.breadthFirstFeederWaleIterator(); it.hasNext(); )
            wales.add(it.next());
        return wales;
    }
    @SuppressWarnings("unused")
    public static LinkedList<Wale> breadthFirstFeederList(Collection<Wale> startWales){
        LinkedList<Wale> wales = new LinkedList<>();
        for (Iterator<Wale> it = breadthFirstFeederWaleIterator(startWales); it.hasNext(); )
            wales.add(it.next());
        return wales;
    }
    private Iterator<Wale> depthFirstFeederWaleIterator(){return new DepthFirstFeederWaleIterator(this);}
    private static Iterator<Wale> depthFirstFeederWaleIterator(Collection<Wale> startWales){return new DepthFirstFeederWaleIterator(startWales);}
    @SuppressWarnings("unused")
    public LinkedList<Wale> depthFirstFeederList(){
        LinkedList<Wale> wales = new LinkedList<>();
        for (Iterator<Wale> it = this.depthFirstFeederWaleIterator(); it.hasNext(); )
            wales.add(it.next());
        return wales;
    }
    @SuppressWarnings("unused")
    public static LinkedList<Wale> depthFirstFeederList(Collection<Wale> startWales){
        LinkedList<Wale> wales = new LinkedList<>();
        for (Iterator<Wale> it = depthFirstFeederWaleIterator(startWales); it.hasNext(); )
            wales.add(it.next());
        return wales;
    }
    public static class DepthFirstFeederWaleIterator implements  Iterator<Wale>{
        LinkedList<Wale> stack = new LinkedList<>();
        HashSet<Wale> visited = new HashSet<>();
        DepthFirstFeederWaleIterator(Wale start){
            stack.add(start);
        }
        DepthFirstFeederWaleIterator(Collection<Wale> startWales){
            stack.addAll(startWales);
        }
        @Override
        public boolean hasNext() {
            return !stack.isEmpty();
        }

        @Override
        public Wale next() {
            Wale last = stack.pop();
            visited.add(last);
            for(Wale feeder: last.feedingWales)
                if(!visited.contains(feeder))
                    stack.add(feeder);
            return last;
        }
    }

    @Override
    public String toString(){
        return this.stitches.getFirst().toString()+"->"+this.stitches.getLast().toString();
    }
}
