package KnitSpeakParser;

import com.intellij.openapi.fileTypes.*;
import org.jetbrains.annotations.NotNull;

public class KnitSpeakFileTypeFactory extends FileTypeFactory {
    @Override
    public void createFileTypes(@NotNull FileTypeConsumer fileTypeConsumer) {
        fileTypeConsumer.consume(KnitSpeakFileType.INSTANCE, "knitspeak");
    }
}
