package KnitSpeakParser;

import com.intellij.lexer.FlexAdapter;

public class KnitSpeakLexerAdapter extends FlexAdapter {
    public KnitSpeakLexerAdapter() {
        super(new _KnitSpeakLexer(null));
    }
}
