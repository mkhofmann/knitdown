package KnitSpeakParser;

import com.intellij.openapi.fileTypes.LanguageFileType;
import com.intellij.openapi.util.IconLoader;
import org.jetbrains.annotations.*;

import javax.swing.*;

public class KnitSpeakFileType extends LanguageFileType {
    public static final KnitSpeakFileType INSTANCE = new KnitSpeakFileType();

    private KnitSpeakFileType() {
        super(KnitSpeakLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return "Knit Speak file";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Knit Speak language file";
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return "knitspeak";
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return IconLoader.getIcon("/icons/knitIcon.png");
    }
}
