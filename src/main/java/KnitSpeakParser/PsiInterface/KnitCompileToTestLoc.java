package KnitSpeakParser.PsiInterface;

import CompilerExceptions.KnitDownException;
import com.intellij.psi.PsiFile;

/**
 * Created by Megan on 1/4/2017.
 * A KnitCompiler that overrides the standard behavior to specify a specific text directory
 */
public class KnitCompileToTestLoc extends KnitCompiler {
    public KnitCompileToTestLoc(PsiFile psiFile, String testDataDirectory) throws KnitDownException {
        super(psiFile);
        this.directoryPath = testDataDirectory;
    }

}
