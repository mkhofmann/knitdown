package KnitSpeakParser;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;

import static com.intellij.psi.TokenType.BAD_CHARACTER;
import static com.intellij.psi.TokenType.WHITE_SPACE;
import static KnitSpeakParser.psi.KnitSpeakTypes.*;

%%

%{
  public _KnitSpeakLexer() {
    this((java.io.Reader)null);
  }
%}

%public
%class _KnitSpeakLexer
%implements FlexLexer
%function advance
%type IElementType
%unicode
%ignorecase

WRAPWORD=(w)(rap)?
REP=(rep)(eat)?
S2KPO=(s2kpo)|(sl2-k1-p2sso)
SK2PO=(sk2po)|( (sl1-k2)(tog)?(-psso))
SSK=(ssk)|(cdd[ \t\n\x0B\f\r]twisted)
SSP=(ssp)|(cddp)|(cddp[ \t\n\x0B\f\r]twisted)
T2LP=(t2lp)|(c2fp)|(1"/"1[ \t\n\x0B\f\r]lpc)
T2L=(t2l)|(c2f)|(1"/"1[ \t\n\x0B\f\r]lc)
T2RP=(t2rp)|(c2bp)|(1"/"1[ \t\n\x0B\f\r]rpc)
T2R=(t2r)|(1"/"1[ \t\n\x0B\f\r]rc)
WYAB=(wy)([ai])(b)
WYIF=(wy)([ai])(f)
M1L=(m1l)|(m1)|(incl)
M1P=(m1p)|(inclp)
M1R=(m1r)|(incr)
M1RP=(m1rp)|(incrp)
K=(k)(nit)?
P=(p)(url)?
LETTER=[a-z]
WHS=[\s]+
DIGITS=[2-9]
SUFFIX=(nd)|(rd)|(th)
CTRDBLINC=(ctr)([ \t\n\x0B\f\r])(dbl)([ \t\n\x0B\f\r])(inc)
WRITTENINSTRUCTIONS=(written)([ \t\n\x0B\f\r])(instructions)

%%
<YYINITIAL> {

  "cont"                     { return CONT; }
  "end"                      { return END; }
  "knitwise"                 { return KNITWISE; }
  "l"                        { return L; }
  "last"                     { return LAST; }
  "purlwise"                 { return PURLWISE; }
  "r"                        { return R; }
  "rem"                      { return REM; }
  "row"                      { return ROW; }
  "rows"                     { return ROWS; }
  "rs"                       { return RS; }
  "skpo"                     { return SKPO; }
  "sssp"                     { return SSSP; }
  "sl"                       { return SL; }
  "st"                       { return ST; }
  "sts"                      { return STS; }
  "tbl"                      { return TBL; }
  "tfl"                      { return TFL; }
  "ws"                       { return WS; }
  "yb"                       { return YB; }
  "yf"                       { return YF; }
  "kfb"                      { return KFB; }
  "pfb"                      { return PFB; }
  "psso"                     { return PSSO; }
  "yo"                       { return YO; }
  "bo"                       { return BO; }
  "c3f"                      { return C3F; }
  "c3fp"                     { return C3FP; }
  "c3b"                      { return C3B; }
  "c3bp"                     { return C3BP; }
  "c4f"                      { return C4F; }
  "c4fp"                     { return C4FP; }
  "c4b"                      { return C4B; }
  "c4bp"                     { return C4BP; }
  "c5f"                      { return C5F; }
  "c5fp"                     { return C5FP; }
  "c5b"                      { return C5B; }
  "c5bp"                     { return C5BP; }
  "c6f"                      { return C6F; }
  "c6fp"                     { return C6FP; }
  "c6b"                      { return C6B; }
  "c6bp"                     { return C6BP; }
  "c"                        { return C; }
  "pc"                       { return PC; }
  "pt"                       { return PT; }
  "b"                        { return B; }
  "f"                        { return F; }
  "t"                        { return T; }
  "s"                        { return S; }
  "tog"                      { return TOG; }
  "&"                        { return ANDSYM; }
  "."                        { return EOL; }
  "*"                        { return STAR; }
  "("                        { return LP; }
  ")"                        { return RP; }
  "["                        { return LB; }
  "]"                        { return RB; }
  ","                        { return COM; }
  ";"                        { return SEMI; }
  ":"                        { return COLLON; }
  "_"                        { return UNDER_SCORE; }
  "\""                       { return QUOTE; }
  "-"                        { return DASH; }
  "/"                        { return SLASH; }
  "twice"                    { return TWICE; }
  "times"                    { return TIMES; }
  "to"                       { return TO; }
  "as"                       { return AS; }
  "all"                      { return ALL; }
  "and"                      { return AND; }
  "any"                      { return ANY; }
  "every"                    { return EVERY; }
  "from"                     { return FROM; }
  "in"                       { return IN; }
  "on"                       { return ON; }
  "of"                       { return OF; }
  "multiple"                 { return MULTIPLE; }
  "plus"                     { return PLUS; }
  "number"                   { return NUMBER; }
  "these"                    { return THESE; }
  "form"                     { return FORM; }
  "the"                      { return THE; }
  "pattern"                  { return PATTERNWORD; }
  "inc"                      { return INC; }
  "one"                      { return ONEWORD; }
  "1"                        { return ONE; }
  "0"                        { return ZERO; }
  "co"                       { return CO; }
  "twisted"                  { return TWISTED; }
  "turn"                     { return TURN; }
  "next"                     { return NEXT; }
  "more"                     { return MORE; }

  {WRAPWORD}                 { return WRAPWORD; }
  {REP}                      { return REP; }
  {S2KPO}                    { return S2KPO; }
  {SK2PO}                    { return SK2PO; }
  {SSK}                      { return SSK; }
  {SSP}                      { return SSP; }
  {T2LP}                     { return T2LP; }
  {T2L}                      { return T2L; }
  {T2RP}                     { return T2RP; }
  {T2R}                      { return T2R; }
  {WYAB}                     { return WYAB; }
  {WYIF}                     { return WYIF; }
  {M1L}                      { return M1L; }
  {M1P}                      { return M1P; }
  {M1R}                      { return M1R; }
  {M1RP}                     { return M1RP; }
  {K}                        { return K; }
  {P}                        { return P; }
  {LETTER}                   { return LETTER; }
  {WHS}                      { return WHS; }
  {DIGITS}                   { return DIGITS; }
  {SUFFIX}                   { return SUFFIX; }
  {CTRDBLINC}                { return CTRDBLINC; }
  {WRITTENINSTRUCTIONS}      { return WRITTENINSTRUCTIONS; }

}

[^] { return BAD_CHARACTER; }
