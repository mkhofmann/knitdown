package KnitSpeakParser;

import com.intellij.lang.Language;

public class KnitSpeakLanguage extends Language {
    public static final KnitSpeakLanguage INSTANCE = new KnitSpeakLanguage();

    private KnitSpeakLanguage() {
        super("KnitSpeak");
    }
}
