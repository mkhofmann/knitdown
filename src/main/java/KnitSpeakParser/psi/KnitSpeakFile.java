package KnitSpeakParser.psi;

/**
 * Created by Megan on 11/14/2016.
 */
import KnitSpeakParser.KnitSpeakFileType;
import KnitSpeakParser.KnitSpeakLanguage;
import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class KnitSpeakFile extends PsiFileBase implements Comparable{
    public KnitSpeakFile(@NotNull FileViewProvider viewProvider) {
        super(viewProvider, KnitSpeakLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return KnitSpeakFileType.INSTANCE;
    }

    @Override
    public String toString() {
        return "Knit Speak File";
    }

    @Override
    public Icon getIcon(int flags) {
        return super.getIcon(flags);
    }

    @Override
    public int compareTo(@NotNull Object o) {
        if( o instanceof PsiFile)
            return this.getName().compareTo(((PsiFile) o).getName());
        else
            return 0;
    }
}