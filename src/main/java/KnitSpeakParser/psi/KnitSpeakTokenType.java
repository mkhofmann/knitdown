package KnitSpeakParser.psi;

import KnitSpeakParser.KnitSpeakLanguage;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class KnitSpeakTokenType extends IElementType {
    public KnitSpeakTokenType(@NotNull @NonNls String debugName) {
        super(debugName, KnitSpeakLanguage.INSTANCE);
    }

    @Override
    public String toString() {
        return "OptimizedPanelTokenType." + super.toString();
    }
}