package KnitSpeakParser.psi;

import KnitSpeakParser.KnitSpeakLanguage;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class KnitSpeakElementType extends IElementType {
    public KnitSpeakElementType(@NotNull @NonNls String debugName) {
        super(debugName, KnitSpeakLanguage.INSTANCE);
    }
}
