package KnitSpeakParser;


import KnitSpeakParser.parser.KnitSpeakParser;
import KnitSpeakParser.psi.KnitSpeakFile;
import KnitSpeakParser.psi.KnitSpeakTypes;
import com.intellij.lang.ASTNode;
import com.intellij.lang.Language;
import com.intellij.lang.ParserDefinition;
import com.intellij.lang.PsiParser;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.project.Project;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.tree.IFileElementType;
import com.intellij.psi.tree.TokenSet;
import org.jetbrains.annotations.NotNull;

public class KnitSpeakParserDefintion implements ParserDefinition {

    public static final IFileElementType FILE =
            new IFileElementType(Language.<KnitSpeakLanguage>findInstance(KnitSpeakLanguage.class));

    @NotNull
    @Override
    public Lexer createLexer(Project project) {
        return new KnitSpeakLexerAdapter();
    }


    @NotNull
    public TokenSet getCommentTokens() {
        return TokenSet.EMPTY;
    }

    @NotNull
    public TokenSet getStringLiteralElements() {
        return TokenSet.EMPTY;
    }

    @NotNull
    public PsiParser createParser(final Project project) {
        return new KnitSpeakParser();
    }

    @Override
    public IFileElementType getFileNodeType() {
        return FILE;
    }

    public PsiFile createFile(FileViewProvider viewProvider) {
        return new KnitSpeakFile(viewProvider);
    }

    public SpaceRequirements spaceExistanceTypeBetweenTokens(ASTNode left, ASTNode right) {
        return SpaceRequirements.MAY;
    }

    @NotNull
    public PsiElement createElement(ASTNode node) {
        return KnitSpeakTypes.Factory.createElement(node);
    }
}