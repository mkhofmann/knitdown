package CompilerExceptions;

public class ChartGeneratorBugException extends KnitDownException {
    public ChartGeneratorBugException() {
        super("WHY DOES THIS HAPPEN");
    }
}
