package CompilerExceptions.PanelExceptions;

import CompilerExceptions.KnitDownException;

/**
 * Created by Megan on 4/27/2017.
 * Thrown when a texture is declared on a cell that already has a set texture.
 */
public class PreviouslyCoveredCellException extends KnitDownException {
    public PreviouslyCoveredCellException(String p0) {
        super(p0);
    }
}
