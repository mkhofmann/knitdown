package CompilerExceptions.StitchManipulationExceptions;

import CompilerExceptions.KnitDownException;

/**
 * Created by Megan on 7/13/2017.
 * Exception occurs when a course is being decreased by cannot physically be done.
 */
public class NoTextureCarvesAvailableException extends KnitDownException {
    public NoTextureCarvesAvailableException(){
        super("Courses cannot be decreased without failed decrease stitches");
    }
}
