package CompilerExceptions.ParsingExceptions;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Megan on 12/1/2016.
 * Thown when the requested number of loops is not a multiple of the available repetions
 *
 */
public class InvalidRepetitionLoopsException extends InsufficientLoopsException {
    public InvalidRepetitionLoopsException(@NotNull String rowId){
        super(rowId+": Remaining Loops are not multiple of repetitions");
    }
}
