package CompilerExceptions.ParsingExceptions;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Megan on 12/2/2016.
 * Thrown when a specific row is being overwritten in the knitspeak
 */
public class LineTypePreviouslyDeclaredException extends KnitDownParsingException {
    public LineTypePreviouslyDeclaredException(@NotNull String rowId){
        super(rowId+": A row type was already declared in this pattern");
    }
}
