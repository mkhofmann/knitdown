package CompilerExceptions.ParsingExceptions;

/**
 * Created by Megan on 1/3/2017.
 * Thrown when the Grammar-Kit parser cannot process a knitspeak file
 */
public class IncompatibleFileException extends KnitDownParsingException {
    public IncompatibleFileException(String psiName) {
        super("This is not a knitspeak file, or will not parse as one: "+psiName);
    }
}
