package CompilerExceptions.ParsingExceptions;

import CompilerExceptions.KnitDownException;

public class KnitDownParsingException extends KnitDownException {
    public KnitDownParsingException(String s) {
        super(s);
    }
}
