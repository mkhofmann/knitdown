package CompilerExceptions.ParsingExceptions;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Megan on 12/20/2016.
 * When a all in 1 statement tries to use invalid stitches
 */
public class OnlyCommonStitchesInOneStitchException extends KnitDownParsingException {
    public OnlyCommonStitchesInOneStitchException(@NotNull String text) {
        super(text+": All in 1 st statements can only uses common stitches with one parentYarn.");
    }
}
