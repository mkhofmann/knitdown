package CompilerExceptions.ParsingExceptions.SizeExceptions;

import KnitSpeakParser.psi.KnitSpeakStitch;
import org.jetbrains.annotations.NotNull;

public class TooSmallIncreaseException extends TooSmallStitchStructureException {
    public TooSmallIncreaseException(@NotNull KnitSpeakStitch stitchElement) {
        super(stitchElement, "Increase");
    }
}
