package CompilerExceptions.ParsingExceptions.SizeExceptions;

import KnitSpeakParser.psi.KnitSpeakStitch;
import org.jetbrains.annotations.NotNull;

/**
 * thrown when a declared cable is to large to be created.
 */
public class TooWideCableException extends TooLargeStitchStructureExceptions {
    /**
     * the maximum width of a cable on the shimisheki based on Lea Albaughs implementation of knit down.
     */
    public static final int MAX_CABLE_WIDTH = 6;
    public TooWideCableException(@NotNull KnitSpeakStitch stitchElement) {
        super(stitchElement, "Cable");
    }
}
