package CompilerExceptions.ParsingExceptions.SizeExceptions;

import KnitSpeakParser.psi.KnitSpeakStitch;
import org.jetbrains.annotations.NotNull;

public class TooSmallCableException extends TooSmallStitchStructureException {
    public TooSmallCableException(@NotNull KnitSpeakStitch stitchElement) {
        super(stitchElement, "Cable");
    }
}
