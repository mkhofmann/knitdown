package CompilerExceptions.ParsingExceptions.SizeExceptions;

import CompilerExceptions.ParsingExceptions.KnitDownParsingException;
import KnitSpeakParser.psi.KnitSpeakStitch;
import org.jetbrains.annotations.NotNull;

public class TooSmallStitchStructureException extends KnitDownParsingException {
    TooSmallStitchStructureException(@NotNull KnitSpeakStitch stitchElement, String type) {
        super(type+ " is too small: "+stitchElement.getText());
    }
}
