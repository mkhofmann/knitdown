package CompilerExceptions.ParsingExceptions.SizeExceptions;

import KnitSpeakParser.psi.KnitSpeakStitch;
import org.jetbrains.annotations.NotNull;

public class TooWideIncreaseException extends TooLargeStitchStructureExceptions {
    /**
     * the maximum width of a cable on the shimisheki based on Lea Albaughs implementation of knit down.
     */
    public static final int MAX_INCREASE_WIDTH = 9;
    public TooWideIncreaseException(@NotNull KnitSpeakStitch stitchElement) {
        super(stitchElement, "Increase");
    }
}
