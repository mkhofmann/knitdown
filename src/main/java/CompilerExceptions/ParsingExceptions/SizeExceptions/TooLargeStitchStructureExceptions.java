package CompilerExceptions.ParsingExceptions.SizeExceptions;

import CompilerExceptions.ParsingExceptions.KnitDownParsingException;
import KnitSpeakParser.psi.KnitSpeakStitch;
import org.jetbrains.annotations.NotNull;

class TooLargeStitchStructureExceptions extends KnitDownParsingException {
    TooLargeStitchStructureExceptions(@NotNull KnitSpeakStitch stitchElement, String type) {
        super(type+ " is too wide: "+stitchElement.getText());
    }
}
