package CompilerExceptions.ParsingExceptions;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Megan on 12/2/2016.
 * When a needed course is not described in the knitspeak
 */
public class UnDefinedLineException extends KnitDownParsingException {
    public UnDefinedLineException(@NotNull String rowId){
        super(rowId+": A row was not declared");
    }
}
