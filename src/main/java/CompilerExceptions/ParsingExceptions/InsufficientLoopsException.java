package CompilerExceptions.ParsingExceptions;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Megan on 12/1/2016.
 * Thrown if a row is being constructed but no enough loops are available to consume
 */
public class InsufficientLoopsException extends KnitDownParsingException {
    public InsufficientLoopsException(@NotNull String loc){//todo: report defficiency
        super(loc+": InsufficientLoops");
    }
}
