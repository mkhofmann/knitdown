package CompilerExceptions;

/**
 * Created by Megan on 1/3/2017.
 * This is the catch all exception for exceptions produced in the KnitDown Framework
 *
 */
public class KnitDownException extends Exception {
    public KnitDownException(String s){
        super(s);
    }
}
