package TestCases;

import CompilerExceptions.KnitDownException;
import KnitSpeakParser.PsiInterface.KnitCompileToTestLoc;
import KnitSpeakParser.PsiInterface.KnitCompiler;
import TestCases.Functions.TestSetUp;
import TextureGraph.TextureBuiltInRow;
import com.intellij.psi.PsiFile;
import org.junit.Before;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.TreeSet;

public class TextureCarveTillDeadTest extends TestSetUp {
    @Before
    public void setUp() throws Exception {
        super.setUp();
        this.setUpDirectories(false, true, false, true, false);
    }

    public void testCategorizePatterns() {
        TreeSet<PsiFile> sortedMaps = new TreeSet<>(Arrays.asList(this.allStitchMaps));
//        TreeSet<PsiFile> sortedMaps = new TreeSet<>(Arrays.asList(this.testFiles));
        HashMap<String,Integer> patternTCMax = new HashMap<>();
        HashMap<String, Integer> patternTCDifFromRep = new HashMap<>();
        for(PsiFile psiFile:sortedMaps)
            try {
                KnitCompiler knitCompiler = new KnitCompileToTestLoc(psiFile, this.getTestDataPath() + File.separator+"NewStitchMaps"+File.separator+"Everything");
//                KnitCompiler knitCompiler = new KnitCompileToTestLoc(psiFile, this.getTestDataPath()+File.separator+"allTests");
                TextureBuiltInRow texture = knitCompiler.buildTextureByWidthRepsAndHeightReps(1,1);
                int loopCount = texture.getFirstLine().getChildLoops().size();
                for(int i = 1; i<= loopCount; i++)
                    texture.narrowTextureGraph(1, 0);
                patternTCMax.put(psiFile.getName(), loopCount);
                patternTCDifFromRep.put(psiFile.getName(), 0);
            } catch( KnitDownException | AssertionError e){
                System.err.println("Failed to read "+psiFile.getName()+": "+e.getMessage());
            }
        try {
            System.out.println("file: "+ Paths.get("").toAbsolutePath().toString());
            PrintWriter csvWriter = new PrintWriter(new File(Paths.get("").toAbsolutePath().toString(),"SM_TextureTCData.csv"));
//            PrintWriter csvWriter = new PrintWriter(new File(Paths.get("").toAbsolutePath().toString(), "SE_TextureTCData.csv"));
            StringBuilder csvData = new StringBuilder();
            csvData.append("texture,Max Texture Carves,Dif From Rep Size\n");
            for (String fileName : patternTCMax.keySet())
                csvData.append(fileName).append(",")
                        .append(patternTCMax.get(fileName)).append(",")
                        .append(patternTCDifFromRep.get(fileName))
                        .append("\n");
            csvWriter.write(csvData.toString());
            csvWriter.close();
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}