package TestCases;

import TextureGraph.TextureGraphOutput.ChartGenerator;
import KnitSpeakParser.PsiInterface.KnitCompileToTestLoc;
import KnitSpeakParser.PsiInterface.KnitCompiler;
import TestCases.Functions.TestSetUp;
import TextureDefinition.TextureDefinition;
import TextureGraph.TextureBuiltInRow;
import com.intellij.psi.PsiFile;

import java.io.File;
import java.util.HashMap;

public class NarrowTest extends TestSetUp {

    public void setUp() throws Exception {
        super.setUp();
        this.setUpDirectories(true, false, false, false, false);
    }

    public void testAllFiles() throws Exception {
        HashMap<String, KnitCompiler> knitCompilers = new HashMap<>(this.specialFiles.length);
        for (PsiFile psiFile : this.specialFiles)
            try {
                knitCompilers.put(psiFile.getName(), new KnitCompileToTestLoc(psiFile, this.getTestDataPath() + "\\SpecialTests"));
            } catch (Exception e) {
                System.err.println("Failed on " + psiFile.getName());
                throw e;
            }
        TextureDefinition pattern = knitCompilers.get("124SpotStitch.knitspeak").getTextureDefinition();
        TextureBuiltInRow textureBuiltInRow = pattern.buildGarmentGraphByWidthReps(1,false);
        textureBuiltInRow.narrowTextureGraph(2, 4);
        ChartGenerator chartGenerator = new ChartGenerator(this.getTestDataPath() + File.separator+"SpecialTests"+ File.separator+"charts"+File.separator+"narrow_out.chart", textureBuiltInRow);
        chartGenerator.writeFile();
    }
}
