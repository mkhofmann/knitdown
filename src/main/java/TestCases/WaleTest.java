package TestCases;

import KnitSpeakParser.PsiInterface.KnitCompileToTestLoc;
import KnitSpeakParser.PsiInterface.KnitCompiler;
import TestCases.Functions.TestSetUp;
import TextureDefinition.TextureDefinition;
import TextureGraph.TextureBuiltInRow;
import TextureGraph.WaleNavigation.WaleChain;
import com.intellij.psi.PsiFile;

import java.util.HashMap;

public class WaleTest extends TestSetUp {

    public void setUp() throws Exception {
        super.setUp();
        this.setUpDirectories(true, false, false, false, false);
    }

    public void testAllFiles() throws Exception {
        HashMap<String, KnitCompiler> knitCompilers = new HashMap<>(this.specialFiles.length);
        for (PsiFile psiFile : this.specialFiles)
            try {
                knitCompilers.put(psiFile.getName(), new KnitCompileToTestLoc(psiFile, this.getTestDataPath() + "\\SpecialTests"));
            } catch (Exception e) {
                System.err.println("Failed on " + psiFile.getName());
                throw e;
            }
        TextureDefinition pattern = knitCompilers.get("WaleTest.knitspeak").getTextureDefinition();
        TextureBuiltInRow textureBuiltInRow = pattern.buildGarmentGraphByWidthReps(1, false);
        textureBuiltInRow.buildWaleChains();
        System.out.println("Complete:");
        for(WaleChain chain: textureBuiltInRow.waleChains){
            System.out.println(chain.printChain());
        }
    }

}
