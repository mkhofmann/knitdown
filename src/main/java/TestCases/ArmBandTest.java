package TestCases;
import CompilerExceptions.KnitDownException;
import KnitSpeakParser.PsiInterface.KnitCompileToTestLoc;
import KnitSpeakParser.PsiInterface.KnitCompiler;
import TestCases.Functions.TestSetUp;
import TextureGraph.TextureBuiltInRow;
import TextureGraph.TextureGraphOutput.ChartGenerator;
import com.intellij.psi.PsiFile;
import org.junit.Before;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class ArmBandTest extends TestSetUp {
    private static final String TEST_NAME = "StretchIn";
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        this.setUpDirectories(true, false, false, false, false);
    }

    public void testArmBand() throws KnitDownException, IOException {
        HashMap<String, KnitCompiler> knitCompilers = new HashMap<>(this.specialFiles.length);
        for (PsiFile psiFile : this.specialFiles)
            try {
                knitCompilers.put(psiFile.getName(), new KnitCompileToTestLoc(psiFile, this.getTestDataPath() + "\\SpecialTests"));
            } catch (Exception e) {
                System.err.println("Failed on " + psiFile.getName());
                throw e;
            }
        TextureBuiltInRow texture = knitCompilers.get("012Ribk2p2.knitspeak").buildTextureByWidthRepsAndHeightReps(6*8,8*1);
        ChartGenerator chartGenerator = new ChartGenerator(this.getTestDataPath() + File.separator+"SpecialTests"+ File.separator+"charts"+File.separator+"RibbedArmBand.chart", texture);
        chartGenerator.writeFile();
    }
}
