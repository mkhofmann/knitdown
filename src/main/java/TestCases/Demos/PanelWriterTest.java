package TestCases.Demos;

import CompilerExceptions.KnitDownException;
import Panels.Panel;
import Panels.PanelWriter;
import Panels.Patches.Texture;
import Panels.OptimizedPanelReader;
import TextureGraph.TextureGraphOutput.ChartGenerator;
import KnitSpeakParser.PsiInterface.KnitCompileToTestLoc;
import KnitSpeakParser.PsiInterface.KnitCompiler;
import TestCases.Functions.TestSetUp;
import TextureGraph.TextureBuiltInRow;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class PanelWriterTest extends TestSetUp {
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        this.setUpDirectories(true, false, false, false, true);
    }
    public void testWritePanel() throws Exception {
        Panel panel = getPanel();
        TextureBuiltInRow textureBuiltInRow = panel.buildPanel();
        ChartGenerator chartGenerator = new ChartGenerator(this.getTestDataPath() + File.separator+"SpecialTests"+ File.separator+"charts"+File.separator+"ApproxPanel.chart", textureBuiltInRow);
        chartGenerator.writeFile();
        PanelWriter panelWriter = new PanelWriter("testPanel.panel",panel);
        panelWriter.writeFile(false);
    }

    @NotNull
    private Panel getPanel() throws KnitDownException {
        HashMap<String, KnitCompiler> knitCompilers = new HashMap<>(this.specialFiles.length);
        for (PsiFile psiFile : this.specialFiles)
            try {
                knitCompilers.put(psiFile.getName(), new KnitCompileToTestLoc(psiFile, this.getTestDataPath() + "\\SpecialTests"));
            } catch (Exception e) {
                System.err.println("Failed on " + psiFile.getName());
                throw e;
            }
        Texture a = new Texture(knitCompilers.get("007Moss.knitspeak").getTextureDefinition(),14.8, 14.7);
        Texture b = new Texture(knitCompilers.get("006Seed.knitspeak").getTextureDefinition(),12.8, 17.9);
        Texture c = new Texture(knitCompilers.get("129BranchingFern.knitspeak").getTextureDefinition(),16.2, 18.1);
        Texture d = new Texture(knitCompilers.get("015KPDiagonals.knitspeak").getTextureDefinition(),14.8, 18.8);
        Texture e = new Texture(knitCompilers.get("012Ribk2p2.knitspeak").getTextureDefinition(),25.0, 16.0);
        Texture f = new Texture(knitCompilers.get("078CrackerChecks.knitspeak").getTextureDefinition(),16.7, 19.3);
        Texture g = new Texture(knitCompilers.get("037SubtleTwist.knitspeak").getTextureDefinition(),16.4, 19.1);
        Texture h = new Texture(knitCompilers.get("044InterlacedRib.knitspeak").getTextureDefinition(),17.9, 17.1);
        Texture i = new Texture(knitCompilers.get("009k1p1rib.knitspeak").getTextureDefinition(),21.5, 14.8);
        Panel panel = new Panel(5,5);
        panel.addPatch(a,0,0,3.99,0.99,1,false);
        panel.addPatch(b,0,1,0.99,3.99,1,false);
        panel.addPatch(c,1,1,0.99,1.99,2,false);
        panel.addPatch(d,1,3,1.99,0.99,1,false);
        panel.addPatch(e,1,4,3.99,0.99,1,false);
        panel.addPatch(f,2,1,1.99,0.99,1,false);
        panel.addPatch(g,2,2,0.99,0.99,1,false);
        panel.addPatch(h,3,2,0.99,1.99,1,false);
        panel.addPatch(i,4,0,0.99,3.99,1,false);
        panel.adjustPanel();
        return panel;
    }

    public void testReadPanel() throws Exception{
        Panel panel = getPanel();
        OptimizedPanelReader optimizedPanelReader = new OptimizedPanelReader(new File(this.getTestDataPath()+"\\TestPanels\\test3-opt.oppanel"));
        panel.easePanel(optimizedPanelReader);
    }

    public void testNaivePanel() throws KnitDownException, IOException {
        HashMap<String, KnitCompiler> knitCompilers = new HashMap<>(this.specialFiles.length);
        for (PsiFile psiFile : this.specialFiles)
            try {
                knitCompilers.put(psiFile.getName(), new KnitCompileToTestLoc(psiFile, this.getTestDataPath() + "\\SpecialTests"));
            } catch (Exception e) {
                System.err.println("Failed on " + psiFile.getName());
                throw e;
            }
        TextureBuiltInRow naivePatch = knitCompilers.get("NaivePatch.knitspeak").buildTextureByWidthRepsAndHeightReps(1,1);
        ChartGenerator chartGenerator2 = new ChartGenerator(this.getTestDataPath() + File.separator+"SpecialTests"+ File.separator+"charts"+File.separator+"NaivePanel_out.chart", naivePatch);
        chartGenerator2.writeFile();
    }
}
