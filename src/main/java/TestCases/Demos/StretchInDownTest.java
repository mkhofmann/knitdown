package TestCases.Demos;

import CompilerExceptions.KnitDownException;
import Panels.OptimizedPanelReader;
import Panels.Panel;
import Panels.PanelWriter;
import Panels.Patches.Texture;
import TextureGraph.TextureGraphOutput.ChartGenerator;
import KnitSpeakParser.PsiInterface.KnitCompileToTestLoc;
import KnitSpeakParser.PsiInterface.KnitCompiler;
import TestCases.Functions.TestSetUp;
import TextureGraph.TextureBuiltInRow;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class StretchInDownTest extends TestSetUp {
    private static final String TEST_NAME = "StretchIn";
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        this.setUpDirectories(true, false, false, false, true);
    }
    public void testWritePanel() throws Exception {
        Panel panel = this.getPanel();
        TextureBuiltInRow textureBuiltInRow = panel.buildPanel();
        ChartGenerator chartGenerator = new ChartGenerator(this.getTestDataPath() + File.separator+"SpecialTests"+ File.separator+"charts"+File.separator+"Approx_"+TEST_NAME+".chart", textureBuiltInRow);
        chartGenerator.writeFile();
        PanelWriter panelWriter = new PanelWriter("testPanel.panel",panel);
        panelWriter.writeFile(false);
    }

    @NotNull
    private Panel getPanel() throws KnitDownException {
        HashMap<String, KnitCompiler> knitCompilers = new HashMap<>(this.specialFiles.length);
        for (PsiFile psiFile : this.specialFiles)
            try {
                knitCompilers.put(psiFile.getName(), new KnitCompileToTestLoc(psiFile, this.getTestDataPath() + "\\SpecialTests"));
            } catch (Exception e) {
                System.err.println("Failed on " + psiFile.getName());
                throw e;
            }
        Texture a = new Texture(knitCompilers.get("006Seed.knitspeak").getTextureDefinition(),12.8, 17.9);
        Texture b = new Texture(knitCompilers.get("004Stockinette.knitspeak").getTextureDefinition(),15.4, 18.1);
        Texture c = new Texture(knitCompilers.get("037SubtleTwist.knitspeak").getTextureDefinition(),16.4, 19.0);
        Texture d = new Texture(knitCompilers.get("084NarrowRopeB.knitspeak").getTextureDefinition(),18.1, 21.2);
        Texture e = new Texture(knitCompilers.get("009k1p1rib.knitspeak").getTextureDefinition(),21.46, 14.8);
        Texture f = new Texture(knitCompilers.get("012Ribk2p2.knitspeak").getTextureDefinition(),24.98, 16.0);
        Panel panel = new Panel(2,6);
        panel.addPatch(f,0,0,1.99,0.99,1,false);
        panel.addPatch(e,0,1,1.99,0.99,1,false);
        panel.addPatch(d,0,2,1.99,0.99,1,false);
        panel.addPatch(c,0,3,1.99,0.99,1,false);
        panel.addPatch(b,0,4,1.99,0.99,1,false);
        panel.addPatch(a,0,5,1.99,0.99,1,false);
        panel.adjustPanel();
        return panel;
    }

    public void testReadPanel() throws Exception{
        Panel panel = getPanel();
        OptimizedPanelReader optimizedPanelReader = new OptimizedPanelReader(new File(this.getTestDataPath()+"\\TestPanels\\stretch_outPanel.oppanel"));
        panel.easePanel(optimizedPanelReader);
    }

    public void testNaivePanel() throws KnitDownException, IOException {
        HashMap<String, KnitCompiler> knitCompilers = new HashMap<>(this.specialFiles.length);
        for (PsiFile psiFile : this.specialFiles)
            try {
                knitCompilers.put(psiFile.getName(), new KnitCompileToTestLoc(psiFile, this.getTestDataPath() + "\\SpecialTests"));
            } catch (Exception e) {
                System.err.println("Failed on " + psiFile.getName());
                throw e;
            }
        TextureBuiltInRow texture = knitCompilers.get("012Ribk2p2.knitspeak").buildTextureByWidthRepsAndHeightReps(10,6);
        texture = knitCompilers.get("009k1p1rib.knitspeak").getTextureDefinition().extendGarmentGraph(texture, 6);
        texture = knitCompilers.get("084NarrowRopeB.knitspeak").getTextureDefinition().extendGarmentGraph(texture, 4);
        texture = knitCompilers.get("037SubtleTwist.knitspeak").getTextureDefinition().extendGarmentGraph(texture, 3);
        texture = knitCompilers.get("004Stockinette.knitspeak").getTextureDefinition().extendGarmentGraph(texture,6);
        texture = knitCompilers.get("006Seed.knitspeak").getTextureDefinition().extendGarmentGraph(texture, 6);
        ChartGenerator chartGenerator = new ChartGenerator(this.getTestDataPath() + File.separator+"SpecialTests"+ File.separator+"charts"+File.separator+"Naive_"+TEST_NAME+".chart", texture);
        chartGenerator.writeFile();
    }
}
