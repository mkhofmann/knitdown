package TestCases.Demos;

import KnitSpeakParser.PsiInterface.KnitCompileToTestLoc;
import KnitSpeakParser.PsiInterface.KnitCompiler;
import TestCases.Functions.TestSetUp;
import TextureGraph.TextureBuiltInRow;
import TextureGraph.TextureGraphOutput.ChartGenerator;
import com.intellij.psi.PsiFile;
import org.junit.Before;

import java.io.File;
import java.util.HashMap;

public class TriangleFabricTest extends TestSetUp {
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        this.setUpDirectories(true, false, false, false, true);
    }
    public void testTriangle() throws Exception {
        HashMap<String, KnitCompiler> knitCompilers = new HashMap<>(this.specialFiles.length);
        for (PsiFile psiFile : this.specialFiles)
            try {
                knitCompilers.put(psiFile.getName(), new KnitCompileToTestLoc(psiFile, this.getTestDataPath() + "\\SpecialTests"));
            } catch (Exception e) {
                System.err.println("Failed on " + psiFile.getName());
                throw e;
            }
        TextureBuiltInRow textureBuiltInRow = knitCompilers.get("040GarterDiamond.knitspeak").buildTextureByWidthRepsAndHeightReps(9,25);
        textureBuiltInRow.taperNarrow(18,textureBuiltInRow.getHeight(), 3,18);
        ChartGenerator chartGenerator = new ChartGenerator(this.getTestDataPath() + File.separator+"SpecialTests"+ File.separator+"charts"+File.separator+"Triangle.chart", textureBuiltInRow);
        chartGenerator.writeFile();
    }
}
