package TestCases;

import CompilerExceptions.ChartGeneratorBugException;
import CompilerExceptions.StitchManipulationExceptions.NoTextureCarvesAvailableException;
import KnitSpeakParser.PsiInterface.KnitCompileToTestLoc;
import KnitSpeakParser.PsiInterface.KnitCompiler;
import TestCases.Functions.ConvertDBtoCharts;
import TestCases.Functions.TestSetUp;
import TextureDefinition.TextureDefinition;
import TextureGraph.TextureBuiltInRow;
import TextureGraph.TextureGraphOutput.ChartGenerator;
import com.intellij.psi.PsiFile;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.TreeSet;

public class HorizontalCarveTest extends TestSetUp {

    public void setUp() throws Exception {
        super.setUp();
        this.setUpDirectories(true, false, false, false, false);
    }

    public void testAllFiles() throws Exception {
        HashMap<String, KnitCompiler> knitCompilers = new HashMap<>(this.specialFiles.length);
        for (PsiFile psiFile : this.specialFiles)
            try {
                knitCompilers.put(psiFile.getName(), new KnitCompileToTestLoc(psiFile, this.getTestDataPath() + "\\SpecialTests"));
            } catch (Exception e) {
                System.err.println("Failed on " + psiFile.getName());
                throw e;
            }
        TreeSet<PsiFile> sortedSpecialFiles = new TreeSet<>(new ConvertDBtoCharts.PsiFileComparator());
        sortedSpecialFiles.addAll(Arrays.asList(this.specialFiles));
        for(PsiFile psiFile: sortedSpecialFiles){
            KnitCompiler compiler = knitCompilers.get(psiFile.getName());
            TextureDefinition pattern = compiler.getTextureDefinition();
            int chartTries = 5;
            boolean pass = false;
            while(!pass && chartTries>=0) {
                try {
                    TextureBuiltInRow textureBuiltInRow = pattern.buildGarmentGraphByWidthReps(1, false);
//            textureBuiltInRow = pattern.extendGarmentGraph(textureBuiltInRow, 4);
                    ChartGenerator chartGenerator = new ChartGenerator(String.format("%s%sSpecialTests%scharts%sFull_%s.chart",this.getTestDataPath(),File.separator,File.separator,File.separator, compiler.psiName), textureBuiltInRow);
                    chartGenerator.writeFile();
                    System.out.println(psiFile.getName());
                    textureBuiltInRow.buildWaleChains();
                    textureBuiltInRow.shortenTextureGraph(1);
                    chartGenerator = new ChartGenerator(String.format("%s%sSpecialTests%scharts%sShort_%s.chart", this.getTestDataPath(), File.separator, File.separator, File.separator, compiler.psiName), textureBuiltInRow);
                    chartGenerator.writeFile();
                    pass = true;
                } catch (NoTextureCarvesAvailableException e) {
                    System.out.println("Can't carve:" + psiFile.getName());
                    break;
                } catch (ChartGeneratorBugException e) {
                    chartTries--;
                }
            }
        }

    }
}
