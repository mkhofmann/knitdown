package TestCases;

import KnitSpeakParser.PsiInterface.KnitCompileToTestLoc;
import KnitSpeakParser.PsiInterface.KnitCompiler;
import TextureGraph.TextureGraphOutput.ChartGenerator;
import TestCases.Functions.TestSetUp;
import TextureDefinition.TextureDefinition;
import TextureGraph.TextureBuiltInRow;
import com.intellij.psi.PsiFile;
import org.junit.Before;

import java.io.File;
import java.util.HashMap;

public class DecreaseTopTest extends TestSetUp {
    static final int widthInStitches =100;//TODO Change these variables to give the stitchCount of textureGraph you want
    static final int heightInStitches = 100;
    @Before
    public void setUp() throws Exception {
        super.setUp();
        this.setUpDirectories(true, false, false, false, false);
    }

    public void testAllFiles() throws Exception {
        HashMap<String, KnitCompiler> knitCompilers = new HashMap<String, KnitCompiler>(this.specialFiles.length);
        for (PsiFile psiFile : this.specialFiles)
            try {
                knitCompilers.put(psiFile.getName(), new KnitCompileToTestLoc(psiFile, this.getTestDataPath() + "\\SpecialTests"));
            } catch (Exception e) {
                System.err.println("Failed on " + psiFile.getName());
                throw e;
            }
        TextureDefinition pattern = knitCompilers.get("DecreaseTopEdgeTestStructure.knitspeak").getTextureDefinition();
        TextureBuiltInRow garment = pattern.buildGarmentGraphByWidthReps(1, false);
        garment.decreaseTopEdge(5);
        ChartGenerator chartGenerator = new ChartGenerator(this.getTestDataPath() + File.separator+"SpecialTests"+ File.separator+"charts"+File.separator+"DecreaseTop.chart", garment);
        chartGenerator.writeFile();
    }
}
