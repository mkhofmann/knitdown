package TestCases;

import KnitSpeakParser.PsiInterface.KnitCompileToTestLoc;
import KnitSpeakParser.PsiInterface.KnitCompiler;
import TestCases.Functions.TestSetUp;
import TextureDefinition.TextureDefinition;
import TextureGraph.TextureBuiltInRow;
import com.intellij.psi.PsiFile;
import org.junit.Before;

import java.util.HashMap;


public class IncreaseBotTest extends TestSetUp {
    @Before
    public void setUp() throws Exception {
        super.setUp();
        this.setUpDirectories(true, false, false, false, false);
    }

    public void testAllFiles() throws Exception {
        HashMap<String, KnitCompiler> knitCompilers = new HashMap<>(this.specialFiles.length);
        for (PsiFile psiFile : this.specialFiles)
            try {
                knitCompilers.put(psiFile.getName(), new KnitCompileToTestLoc(psiFile, this.getTestDataPath() + "\\SpecialTests"));
            } catch (Exception e) {
                System.err.println("Failed on " + psiFile.getName());
                throw e;
            }
        TextureDefinition pattern = knitCompilers.get("IncreaseBotEdgeTestStructure.knitspeak").getTextureDefinition();
        TextureBuiltInRow garment = pattern.buildGarmentGraphByWidthReps(1, false);
        garment.increaseBottomEdge(2);
    }
}
