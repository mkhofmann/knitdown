package TestCases.Functions;

import CompilerExceptions.KnitDownException;
import KnitSpeakParser.PsiInterface.KnitCompileToTestLoc;
import KnitSpeakParser.PsiInterface.KnitCompiler;
import com.intellij.psi.PsiFile;
import org.junit.Before;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.TreeSet;

public class RepCountDataCollection extends TestSetUp {
    @Before
    public void setUp() throws Exception {
        super.setUp();
        this.setUpDirectories(false, false, false, true, false);
    }

    public void testCategorizePatterns() {
        TreeSet<PsiFile> sortedStitchMaps = new TreeSet<>(Arrays.asList(this.allStitchMaps));
        HashMap<String,Integer> patternWidthRep = new HashMap<>();
        HashMap<String,Integer> patternBorder = new HashMap<>();
        HashMap<String,Integer> patternHeightRep = new HashMap<>();
        for(PsiFile psiFile:sortedStitchMaps)
            try {
                KnitCompiler knitCompiler = new KnitCompileToTestLoc(psiFile, this.getTestDataPath() + File.separator+"NewStitchMaps"+File.separator+"Everything");
//                KnitCompiler knitCompiler = new KnitCompileToTestLoc(psiFile, this.getTestDataPath()+File.separator+"allTests");
                patternWidthRep.put(psiFile.getName(), knitCompiler.getTextureDefinition().getRepSize());
                patternBorder.put(psiFile.getName(), knitCompiler.getTextureDefinition().getBorderSize());
                patternHeightRep.put(psiFile.getName(), knitCompiler.getTextureDefinition().getHeight());
            } catch( KnitDownException | AssertionError e){
                System.err.println("Failed to read "+psiFile.getName()+": "+e.getMessage());
                patternWidthRep.put(psiFile.getName(), -1);
                patternBorder.put(psiFile.getName(), -1);
                patternHeightRep.put(psiFile.getName(), -1);
            }
        try {
            System.out.println("file: "+ Paths.get("").toAbsolutePath().toString());
            PrintWriter csvWriter = new PrintWriter(new File(Paths.get("").toAbsolutePath().toString(),"StitchMapRepData.csv"));
//            PrintWriter csvWriter = new PrintWriter(new File(Paths.get("").toAbsolutePath().toString(), "EssentialRepData.csv"));
            StringBuilder csvData = new StringBuilder();
            for (String fileName : patternWidthRep.keySet())
                csvData.append(fileName).append(",")
                        .append(patternWidthRep.get(fileName)).append(",")
                        .append(patternBorder.get(fileName)).append(",")
                        .append(patternHeightRep.get(fileName)).append("\n");
            csvWriter.write(csvData.toString());
            csvWriter.close();
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}
