package TestCases.Functions;

import CompilerExceptions.KnitDownException;
import KnitSpeakParser.PsiInterface.KnitCompileToTestLoc;
import KnitSpeakParser.PsiInterface.KnitCompiler;
import TextureGraph.TextureBuiltInRow;
import TextureGraph.TextureGraph;
import com.intellij.psi.PsiFile;
import org.junit.Before;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Created by Megan on 6/27/2017.
 * Use this JUnit test to run the database and convert it to chart files of an approximate stitch and length stitchCount
 */
public class ConvertDBtoCharts extends TestSetUp {
    private static final int widthInStitches =60;//TODO Change these variables to give the stitchCount of textureGraph you want
    private static final int heightInStitches = 60;
    @Before
    public void setUp() throws Exception {
        super.setUp();
        this.setUpDirectories(true, false, false, false, false);
    }


    public void testAllFilesWithSTSTPadding()throws Exception{
        ArrayList<KnitCompiler> knitCompilers = new ArrayList<>();
        for(PsiFile psiFile:this.specialFiles)
            try {
                knitCompilers.add(new KnitCompileToTestLoc(psiFile, this.getTestDataPath() + File.separator+"SpecialTests"));
            } catch( KnitDownException e){
                System.err.println("Failed to read "+psiFile.getName());
            }
//        ArrayList<KnitCompiler> knitCompilers = new ArrayList<>();
//        for(PsiFile psiFile:this.testFiles)
//            try {
//                knitCompilers.add(new KnitCompileToTestLoc(psiFile, this.getTestDataPath() + File.separator+"allTests"));
//            } catch( KnitDownException e){
//                System.err.println("Failed to read "+psiFile.getName());
//            }
//        TreeSet<PsiFile> sortedStitchMaps = new TreeSet<>(new PsiFileComparator());
//        sortedStitchMaps.addAll(Arrays.asList(this.stitchMapsFiles));
////        ArrayList<PsiFile> sortedStitchMaps = new ArrayList<>(Arrays.asList(this.stitchMapsFailedFiles));
//        for(PsiFile psiFile:sortedStitchMaps)
//            try {
//                KnitCompileToTestLoc knitCompiler = new KnitCompileToTestLoc(psiFile, this.getTestDataPath() + File.separator + "NewStitchMaps" + File.separator + "failed" + File.separator + "LoopVariance");
//                knitCompiler.dataSetType = DataSetType.StitchMaps;
//                knitCompilers.add(knitCompiler);
//            } catch( KnitDownException e){
//                System.err.println("Failed to read "+psiFile.getName()+": "+e.getMessage());
//            }
////        Collections.shuffle(sortedStitchMaps);
        int charted = 0;
        int failedCharts = 0;
        int remaining = knitCompilers.size();
        HashMap<KnitCompiler, TextureGraph> knitCompilerTextureGraphHashMap = new HashMap<>();
        for(KnitCompiler knitCompiler : knitCompilers) {
            try {
                System.out.println(charted + " charted; " + failedCharts + " failed; " + remaining + " remaining; next up:" + knitCompiler.psiFile.getName());
                if(knitCompiler.getTextureDefinition().getRepSize()+knitCompiler.getTextureDefinition().getBorderSize()>ConvertDBtoCharts.widthInStitches)
                    throw new KnitDownException("Too wide for this chart stitchCount");
//                TextureBuiltInRow textureGraphReps = knitCompiler.buildTextureByWidthRepsAndHeightReps(1,1);
//                textureGraphReps.dataSetType = knitCompiler.dataSetType;
                TextureBuiltInRow textureGraph = knitCompiler.buildTextureByWidthAndHeightWithSTSTPadding(ConvertDBtoCharts.widthInStitches, ConvertDBtoCharts.heightInStitches);
//                knitCompilerTextureGraphHashMap.put(knitCompiler, textureGraphReps);
//                knitCompiler.outputChart(textureGraphReps, String.format("%03d", charted)+knitCompiler.psiName);
                knitCompiler.outputChart(textureGraph, knitCompiler.psiName);
                charted++;
            } catch (KnitDownException e) {
                failedCharts++;
                System.err.println("Failed to chart " + knitCompiler.psiFile.getName() + ":" + e.getMessage());
            } catch (AssertionError ae) {
                failedCharts++;
                System.err.println("Assertion error on chart " + knitCompiler.psiFile.getName() + ":" + ae.getMessage());
            }
            remaining--;
        }

//        HashMap<KnitCompiler, TextureGraph> relevantTextures = trackerWriter.containsStitchMapsCable();
//        for(KnitCompiler knitCompiler: relevantTextures.keySet())
//            knitCompiler.outputChart(relevantTextures.get(knitCompiler), String.format("%03d", charted)+knitCompiler.psiName);
    }

    @SuppressWarnings("unused")
    public static
    class PsiFileComparator implements Comparator<PsiFile>
    {
        public int compare(PsiFile a, PsiFile b)
        {
            return a.getName().compareTo(b.getName());
        }
    }

}
