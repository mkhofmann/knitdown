package TestCases.Functions;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiFile;
import com.intellij.testFramework.fixtures.LightPlatformCodeInsightFixtureTestCase;
import org.apache.log4j.BasicConfigurator;
import org.junit.Before;

import java.io.File;

/**
 * Created by Megan on 1/18/2017.
 */
public class TestSetUp extends LightPlatformCodeInsightFixtureTestCase {
    public PsiFile[] testFiles;
    PsiFile[] failedFiles;
    public PsiFile[] stitchMapsFiles;
    private PsiFile[] stitchMapsFailedFiles;
    public PsiFile[] allStitchMaps;
    private PsiDirectory specialTestsDirectory;
    protected PsiFile[] specialFiles;
    protected PsiFile[] testPanels;



    @Override
    protected String getTestDataPath(){
        return System.getProperty("user.dir")+File.separator+"testData";
    }

    @Before
    public  void setUpDirectories(boolean specialTests, boolean allTest, boolean failedTest, boolean stitchMapsTest, boolean testPanels) {
        if (specialTests && this.specialTestsDirectory == null) {
            VirtualFile specialTestsDirectory = this.myFixture.copyDirectoryToProject(File.separator+"SpecialTests", "SpecialTests");
            this.specialTestsDirectory = this.getPsiManager().findDirectory(specialTestsDirectory);
            assert this.specialTestsDirectory != null;
            this.specialFiles = this.specialTestsDirectory.getFiles();
            assert this.specialFiles.length > 0;
        }
        if (allTest && (this.testFiles == null || this.testFiles.length <= 0)){
            VirtualFile allTestsDirectoryVirt = this.myFixture.copyDirectoryToProject(File.separator+"allTests", "allTests");
            PsiDirectory allTestsDirectory = this.getPsiManager().findDirectory(allTestsDirectoryVirt);
            assert allTestsDirectory != null;
            this.testFiles = allTestsDirectory.getFiles();
            assert this.testFiles.length > 0;
        }
        if(failedTest && (this.failedFiles == null || this.failedFiles.length<=0)) {
            VirtualFile failedTestsDirectoryVirt = this.myFixture.copyDirectoryToProject(File.separator+"failedFiles", "failedFiles");
            PsiDirectory failedTestsDirectory = this.getPsiManager().findDirectory(failedTestsDirectoryVirt);
            assert failedTestsDirectory != null;
            this.failedFiles = failedTestsDirectory.getFiles();
        }
        if(stitchMapsTest && (this.stitchMapsFiles == null || this.stitchMapsFiles.length<=0)) {
            VirtualFile stitchMapsDirectoryVirt = this.myFixture.copyDirectoryToProject(File.separator+"NewStitchMaps", "NewStitchMaps");
            PsiDirectory stitchMapsTopDirectory = this.getPsiManager().findDirectory(stitchMapsDirectoryVirt);
            assert stitchMapsTopDirectory != null;
            PsiDirectory stitchMapsCleanDirectory = stitchMapsTopDirectory.findSubdirectory("clean");
            assert stitchMapsCleanDirectory != null;
            this.stitchMapsFiles = stitchMapsCleanDirectory.getFiles();
            PsiDirectory stitchMapsFailDirectory = stitchMapsTopDirectory.findSubdirectory("failed");
            assert stitchMapsFailDirectory != null;
            stitchMapsFailDirectory = stitchMapsFailDirectory.findSubdirectory("LoopVariance");
            assert stitchMapsFailDirectory != null;
            this.stitchMapsFailedFiles = stitchMapsFailDirectory.getFiles();
            PsiDirectory stitchMapsAllDirectory = stitchMapsTopDirectory.findSubdirectory("Everything");
            assert stitchMapsAllDirectory != null;
            this.allStitchMaps = stitchMapsAllDirectory.getFiles();
        }
        if(testPanels && (this.testPanels == null || this.testPanels.length<=0)){
            VirtualFile testPanelDirectoryVirt = this.myFixture.copyDirectoryToProject(File.separator+"TestPanels", "TestPanels");
            PsiDirectory testPanelsDirectory = this.getPsiManager().findDirectory(testPanelDirectoryVirt);
            assert testPanelsDirectory != null;
            this.testPanels = testPanelsDirectory.getFiles();
        }
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Override
    public void setUp() throws Exception {
        BasicConfigurator.configure();
        super.setUp();
    }
}
