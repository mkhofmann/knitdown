package TestCases.Functions;

import CompilerExceptions.KnitDownException;
import KnitSpeakParser.PsiInterface.KnitCompileToTestLoc;
import KnitSpeakParser.PsiInterface.KnitCompiler;
import Tracking.RegionTracking.StitchAreaByNamesTracker;
import Tracking.TextureDataTracker;
import Tracking.TextureTracking.DataSetType;
import Tracking.TextureTracking.TextureType;
import com.intellij.psi.PsiFile;
import org.junit.Before;

import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;

public class StratifyStitchData extends TestSetUp {
    private static final boolean printStackTrace = false;
    @Before
    public void setUp() throws Exception {
        super.setUp();
        this.setUpDirectories(false, true, false, true, false);
    }

    public void testCollectData() throws Exception {
        String dataPath = Paths.get("").toAbsolutePath().toString() + File.separator + "Data";
        LinkedList<PrintWriter> printWriters = new LinkedList<>();
        PrintWriter writeAll= new PrintWriter(new File(dataPath,"StitchData_all.csv"));
        printWriters.add(writeAll);
        PrintWriter writeAll_dev= new PrintWriter(new File(dataPath, "StitchData_all_dev.csv"));
        printWriters.add(writeAll_dev);
        PrintWriter writeAll_test= new PrintWriter(new File(dataPath, "StitchData_all_test.csv"));
        printWriters.add(writeAll_test);
        PrintWriter writeAll_final= new PrintWriter(new File(dataPath, "StitchData_all_final.csv"));
        printWriters.add(writeAll_final);
        PrintWriter writeSE = new PrintWriter(new File(dataPath, "StitchData_SE.csv"));
        printWriters.add(writeSE);
        PrintWriter writeSE_dev = new PrintWriter(new File(dataPath, "StitchData_SE_dev.csv"));
        printWriters.add(writeSE_dev);
        PrintWriter writeSE_test = new PrintWriter(new File(dataPath, "StitchData_SE_test.csv"));
        printWriters.add(writeSE_test);
        PrintWriter writeSE_final = new PrintWriter(new File(dataPath, "StitchData_SE_final.csv"));
        printWriters.add(writeSE_final);
        PrintWriter writeSM = new PrintWriter(new File(dataPath, "StitchData_SM.csv"));
        printWriters.add(writeSM);
        PrintWriter writeSM_dev =   new PrintWriter(new File(dataPath, "StitchData_SM_dev.csv"));
        printWriters.add(writeSM_dev);
        PrintWriter writeSM_test =  new PrintWriter(new File(dataPath, "StitchData_SM_test.csv"));
        printWriters.add(writeSM_test);
        PrintWriter writeSM_final = new PrintWriter(new File(dataPath, "StitchData_SM_final.csv"));
        printWriters.add(writeSM_final);
        HashSet<PsiFile> se_all = new HashSet<>(Arrays.asList(this.testFiles));
        HashSet<PsiFile> twistFiles = new HashSet<>();
        PrintWriter writeSE_twist = new PrintWriter(new File(dataPath, "StitchData_SE_twist.csv"));
        printWriters.add(writeSE_twist);
        for(PsiFile psiFile: se_all)
            if (TextureDataTracker.textureTypeBySEName(psiFile.getName()) == TextureType.Twist)
                twistFiles.add(psiFile);
        se_all.removeAll(twistFiles);
        HashSet<PsiFile> seFiles_split = new HashSet<>(se_all.size()/4);
        PrintWriter writeSE_split_dev = new PrintWriter(new File(dataPath, "StitchData_SE_split_dev.csv"));
        printWriters.add(writeSE_split_dev);
        PrintWriter writeSE_split_test = new PrintWriter(new File(dataPath, "StitchData_SE_split_test.csv"));
        printWriters.add(writeSE_split_test);
        PrintWriter writeSE_split_final = new PrintWriter(new File(dataPath, "StitchData_SE_split_final.csv"));
        printWriters.add(writeSE_split_final);
        HashSet<PsiFile> seFiles_dev =   new HashSet<>(se_all.size()/4);
        HashSet<PsiFile> seFiles_test =  new HashSet<>(se_all.size()/4);
        HashSet<PsiFile> seFiles_final = new HashSet<>(se_all.size()/4);
        int i=0;
        for(PsiFile psiFile: se_all){
            switch (i){
                case 0:
                    seFiles_split.add(psiFile);
                    break;
                case 1:
                    seFiles_dev.add(psiFile);
                    break;
                case 2:
                    seFiles_test.add(psiFile);
                    break;
                case 3:
                    seFiles_final.add(psiFile);
                    break;
            }
            i++;
            i= i%4;
        }
        HashSet<PsiFile> sm_all = new HashSet<>(Arrays.asList(this.stitchMapsFiles));
        HashSet<PsiFile> smFiles_split = new HashSet<>(se_all.size()/4);
        PrintWriter writeSM_split_dev =   new PrintWriter(new File(dataPath, "StitchData_SM_split_dev.csv"));
        printWriters.add(writeSM_split_dev);
        PrintWriter writeSM_split_test =  new PrintWriter(new File(dataPath, "StitchData_SM_split_test.csv"));
        printWriters.add(writeSM_split_test);
        PrintWriter writeSM_split_final = new PrintWriter(new File(dataPath, "StitchData_SM_split_final.csv"));
        printWriters.add(writeSM_split_final);
        HashSet<PsiFile> smFiles_dev =   new HashSet<>(se_all.size()/4);
        HashSet<PsiFile> smFiles_test =  new HashSet<>(se_all.size()/4);
        HashSet<PsiFile> smFiles_final = new HashSet<>(se_all.size()/4);
        i=0;
        for(PsiFile psiFile: sm_all){
            switch (i){
                case 0:
                    smFiles_split.add(psiFile);
                    break;
                case 2:
                    smFiles_dev.add(psiFile);
                    break;
                case 7:
                    smFiles_final.add(psiFile);
                    break;
                default:
                    smFiles_test.add(psiFile);
                    break;
            }
            i++;
            i= i%8;
        }
        for(PrintWriter writer: printWriters)
            writer.write("TextureName, TextureType, " + StitchAreaByNamesTracker.STITCH_AREA_NAME_HEADER+"\n");
        for(PsiFile psiFile:twistFiles){
            try {
                KnitCompiler knitCompiler= new KnitCompileToTestLoc(psiFile, this.getTestDataPath()+File.separator+"allTests");
                TextureDataTracker textureDataTracker = new TextureDataTracker(knitCompiler, DataSetType.StitchEssentials);
                for(StitchAreaByNamesTracker byNamesTracker: textureDataTracker.allStitchAreaByNameTrackers) {
                    String line = psiFile.getName() + ", " +textureDataTracker.textureType+", "+ byNamesTracker.toString() + "\n";
                    writeAll.write(line);
                    writeAll_final.write(line);
                    writeSE.write(line);
                    writeSE_twist.write(line);
                    writeSE_final.write(line);
                }
            } catch( KnitDownException | AssertionError e){
                System.err.println("Failed to read "+psiFile.getName()+": "+e.getMessage());
                if(printStackTrace)
                    e.printStackTrace();
            }
        }
        for(PsiFile psiFile:seFiles_dev){
            try {
                KnitCompiler knitCompiler= new KnitCompileToTestLoc(psiFile, this.getTestDataPath()+File.separator+"allTests");
                TextureDataTracker textureDataTracker = new TextureDataTracker(knitCompiler, DataSetType.StitchEssentials);
                for(StitchAreaByNamesTracker byNamesTracker: textureDataTracker.allStitchAreaByNameTrackers) {
                    String line = textureDataTracker.textureName + ", " +textureDataTracker.textureType+", "+ byNamesTracker.toString() + "\n";
                    writeAll.write(line);
                    writeSE.write(line);
                    writeAll_dev.write(line);
                    writeSE_dev.write(line);
                }
            } catch( KnitDownException | AssertionError e){
                System.err.println("Failed to read "+psiFile.getName()+": "+e.getMessage());
                if(printStackTrace)
                    e.printStackTrace();
            }
        }
        for(PsiFile psiFile:seFiles_test){
            try {
                KnitCompiler knitCompiler= new KnitCompileToTestLoc(psiFile, this.getTestDataPath()+File.separator+"allTests");
                TextureDataTracker textureDataTracker = new TextureDataTracker(knitCompiler, DataSetType.StitchEssentials);
                for(StitchAreaByNamesTracker byNamesTracker: textureDataTracker.allStitchAreaByNameTrackers) {
                    String line = textureDataTracker.textureName + ", " +textureDataTracker.textureType+", "+ byNamesTracker.toString() + "\n";
                    writeAll.write(line);
                    writeSE.write(line);
                    writeAll_test.write(line);
                    writeSE_test.write(line);

                }
            } catch( KnitDownException | AssertionError e){
                System.err.println("Failed to read "+psiFile.getName()+": "+e.getMessage());
                if(printStackTrace)
                    e.printStackTrace();
            }
        }
        for(PsiFile psiFile:seFiles_final){
            try {
                KnitCompiler knitCompiler= new KnitCompileToTestLoc(psiFile, this.getTestDataPath()+File.separator+"allTests");
                TextureDataTracker textureDataTracker = new TextureDataTracker(knitCompiler, DataSetType.StitchEssentials);
                for(StitchAreaByNamesTracker byNamesTracker: textureDataTracker.allStitchAreaByNameTrackers) {
                    String line = textureDataTracker.textureName + ", " +textureDataTracker.textureType+", "+ byNamesTracker.toString() + "\n";
                    writeAll.write(line);
                    writeSE.write(line);
                    writeAll_final.write(line);
                    writeSE_final.write(line);
                }
            } catch( KnitDownException | AssertionError e){
                System.err.println("Failed to read "+psiFile.getName()+": "+e.getMessage());
                if(printStackTrace)
                    e.printStackTrace();
            }
        }
        for(PsiFile psiFile:seFiles_split){
            try {
                KnitCompiler knitCompiler= new KnitCompileToTestLoc(psiFile, this.getTestDataPath()+File.separator+"allTests");
                TextureDataTracker textureDataTracker = new TextureDataTracker(knitCompiler, DataSetType.StitchEssentials);
                i=0;
                for(StitchAreaByNamesTracker byNamesTracker: textureDataTracker.allStitchAreaByNameTrackers) {
                    String line = textureDataTracker.textureName + ", " +textureDataTracker.textureType+", "+ byNamesTracker.toString() + "\n";
                    writeAll.write(line);
                    writeSE.write(line);
                    switch(i){
                        case 2:
                            writeAll_dev.write(line);
                            writeSE_dev.write(line);
                            writeSE_split_dev.write(line);
                            break;
                        case 3:
                            writeAll_final.write(line);
                            writeSE_final.write(line);
                            writeSE_split_final.write(line);
                            break;
                        default:
                            writeAll_test.write(line);
                            writeSE_test.write(line);
                            writeSE_split_test.write(line);
                            break;
                    }
                    i++;
                    i= i%4;
                }
            } catch( KnitDownException | AssertionError e){
                System.err.println("Failed to read "+psiFile.getName()+": "+e.getMessage());
                if(printStackTrace)
                    e.printStackTrace();
            }
        }
        for(PsiFile psiFile:smFiles_dev){
            try {
                KnitCompiler knitCompiler= new KnitCompileToTestLoc(psiFile, this.getTestDataPath()+File.separator+"NewStitchMaps"+File.separator+"Everything");
                TextureDataTracker textureDataTracker = new TextureDataTracker(knitCompiler, DataSetType.StitchMaps);
                for(StitchAreaByNamesTracker byNamesTracker: textureDataTracker.allStitchAreaByNameTrackers) {
                    String line = textureDataTracker.textureName + ", " +textureDataTracker.textureType+", "+ byNamesTracker.toString() + "\n";
                    writeAll.write(line);
                    writeSM.write(line);
                    writeAll_dev.write(line);
                    writeSM_dev.write(line);
                }
            } catch( KnitDownException | AssertionError e){
                System.err.println("Failed to read "+psiFile.getName()+": "+e.getMessage());
                if(printStackTrace)
                    e.printStackTrace();
            }
        }
        for(PsiFile psiFile:smFiles_test){
            try {
                KnitCompiler knitCompiler= new KnitCompileToTestLoc(psiFile, this.getTestDataPath()+File.separator+"NewStitchMaps"+File.separator+"Everything");
                TextureDataTracker textureDataTracker = new TextureDataTracker(knitCompiler, DataSetType.StitchMaps);
                for(StitchAreaByNamesTracker byNamesTracker: textureDataTracker.allStitchAreaByNameTrackers) {
                    String line = textureDataTracker.textureName + ", " +textureDataTracker.textureType+", "+ byNamesTracker.toString() + "\n";
                    writeAll.write(line);
                    writeSM.write(line);
                    writeAll_test.write(line);
                    writeSM_test.write(line);
                }
            } catch( KnitDownException | AssertionError e){
                System.err.println("Failed to read "+psiFile.getName()+": "+e.getMessage());
                if(printStackTrace)
                    e.printStackTrace();
            }
        }
        for(PsiFile psiFile:smFiles_final){
            try {
                KnitCompiler knitCompiler= new KnitCompileToTestLoc(psiFile, this.getTestDataPath()+File.separator+"NewStitchMaps"+File.separator+"Everything");
                TextureDataTracker textureDataTracker = new TextureDataTracker(knitCompiler, DataSetType.StitchMaps);
                for(StitchAreaByNamesTracker byNamesTracker: textureDataTracker.allStitchAreaByNameTrackers) {
                    String line = textureDataTracker.textureName + ", " +textureDataTracker.textureType+", "+ byNamesTracker.toString() + "\n";
                    writeAll.write(line);
                    writeSM.write(line);
                    writeAll_final.write(line);
                    writeSM_final.write(line);
                }
            } catch( KnitDownException | AssertionError e){
                System.err.println("Failed to read "+psiFile.getName()+": "+e.getMessage());
                if(printStackTrace)
                    e.printStackTrace();
            }
        }
        for(PsiFile psiFile:smFiles_split){
            try {
                KnitCompiler knitCompiler= new KnitCompileToTestLoc(psiFile, this.getTestDataPath()+File.separator+"NewStitchMaps"+File.separator+"Everything");
                TextureDataTracker textureDataTracker = new TextureDataTracker(knitCompiler, DataSetType.StitchMaps);
                i=0;
                for(StitchAreaByNamesTracker byNamesTracker: textureDataTracker.allStitchAreaByNameTrackers) {
                    String line = textureDataTracker.textureName + ", " +textureDataTracker.textureType+", "+ byNamesTracker.toString() + "\n";
                    writeAll.write(line);
                    writeSM.write(line);
                    switch(i){
                        case 0:
                            writeAll_dev.write(line);
                            writeSM_dev.write(line);
                            writeSM_split_dev.write(line);
                            break;
                        case 7:
                            writeAll_final.write(line);
                            writeSM_final.write(line);
                            writeSM_split_final.write(line);
                            break;
                        default:
                            writeAll_test.write(line);
                            writeSM_test.write(line);
                            writeSM_split_test.write(line);
                            break;
                    }
                    i++;
                    i= i%8;
                }
            } catch( KnitDownException | AssertionError e){
                System.err.println("Failed to read "+psiFile.getName()+": "+e.getMessage());
                if(printStackTrace)
                    e.printStackTrace();
            }
        }
        for(PrintWriter writer: printWriters)
            writer.close();
    }


}
